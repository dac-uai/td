USE [master]
GO

/****** Object:  Database [DAC]    Script Date: 11/27/2018 6:40:54 AM ******/
CREATE DATABASE [DAC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DAC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\DAC.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DAC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\DAC_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [DAC] SET COMPATIBILITY_LEVEL = 130
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DAC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [DAC] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [DAC] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [DAC] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [DAC] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [DAC] SET ARITHABORT OFF 
GO

ALTER DATABASE [DAC] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [DAC] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [DAC] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [DAC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [DAC] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [DAC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [DAC] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [DAC] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [DAC] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [DAC] SET  DISABLE_BROKER 
GO

ALTER DATABASE [DAC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [DAC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [DAC] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [DAC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [DAC] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [DAC] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [DAC] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [DAC] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [DAC] SET  MULTI_USER 
GO

ALTER DATABASE [DAC] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [DAC] SET DB_CHAINING OFF 
GO

ALTER DATABASE [DAC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [DAC] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [DAC] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [DAC] SET QUERY_STORE = OFF
GO

USE [DAC]
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO

ALTER DATABASE [DAC] SET  READ_WRITE 
GO

USE [DAC]
GO
/****** Object:  Table [dbo].[AptitudUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AptitudUsuario](
	[idAptitudUsuario] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idTecnologia] [int] NOT NULL,
	[experiencia] [int] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_AptitudUsuario] PRIMARY KEY CLUSTERED 
(
	[idAptitudUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AsignacionUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AsignacionUsuario](
	[idAsignacionUsuario] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idProyecto] [int] NOT NULL,
	[fechaDesde] [datetime] NOT NULL,
	[fechaHasta] [datetime] NOT NULL,
	[costoHora] [int] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[dvh] [varchar](50) NOT NULL,
	[esBorrado] [bit] NOT NULL,
 CONSTRAINT [PK_AsignacionUsuario] PRIMARY KEY CLUSTERED 
(
	[idAsignacionUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Auditoria]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auditoria](
	[idAuditoria] [int] IDENTITY(1,1) NOT NULL,
	[valorPrevio] [varchar](50) NOT NULL,
	[valorPosterior] [varchar](50) NOT NULL,
	[Columna] [varchar](50) NOT NULL,
	[Tabla] [varchar](50) NOT NULL,
	[fechaEvento] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Backups]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Backups](
	[path] [varchar](max) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[usuario] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bitacora]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bitacora](
	[idBitacora] [int] IDENTITY(1,1) NOT NULL,
	[mensaje] [varchar](200) NOT NULL,
	[fechaEvento] [datetime] NOT NULL,
	[idTipoEvento] [int] NOT NULL,
	[idUsuario] [int] NULL,
	[ip] [varchar](50) NULL,
 CONSTRAINT [PK_Bitacora] PRIMARY KEY CLUSTERED 
(
	[idBitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[idCliente] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[cuit] [varchar](50) NOT NULL,
	[mailContacto] [varchar](50) NOT NULL,
	[nombreContacto] [varchar](50) NOT NULL,
	[telefonoContacto] [varchar](50) NOT NULL,
	[razonSocial] [varchar](50) NOT NULL,
	[activo] [bit] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[dvh] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ControlCambios]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ControlCambios](
	[accion] [nchar](50) NOT NULL,
	[nombreTabla] [varchar](128) NOT NULL,
	[primaryKey] [varchar](1000) NOT NULL,
	[campo] [varchar](1000) NULL,
	[valorOriginal] [varchar](1000) NULL,
	[valorNuevo] [varchar](1000) NULL,
	[fechaModificacion] [datetime] NULL,
	[usuarioModificacion] [varchar](128) NULL,
	[session] [varchar](128) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DVV]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DVV](
	[idDVV] [int] IDENTITY(1,1) NOT NULL,
	[nombreTabla] [varchar](50) NOT NULL,
	[dvv] [varchar](50) NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_DVV2] PRIMARY KEY CLUSTERED 
(
	[idDVV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Idioma]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Idioma](
	[idIdioma] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[codigo] [varchar](50) NULL,
	[fechaCreacion] [datetime] NULL,
 CONSTRAINT [PK_Idioma] PRIMARY KEY CLUSTERED 
(
	[idIdioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Licencia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Licencia](
	[idLicencia] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[esBorrado] [bit] NOT NULL,
 CONSTRAINT [PK_Licencia] PRIMARY KEY CLUSTERED 
(
	[idLicencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LicenciaSolicitada]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LicenciaSolicitada](
	[idLicenciaSolicitada] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idLicencia] [int] NOT NULL,
	[idAprobador] [int] NULL,
	[fechaDesde] [datetime] NOT NULL,
	[fechaHasta] [datetime] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[licenciaAprobada] [bit] NOT NULL,
	[dvh] [varchar](50) NOT NULL,
 CONSTRAINT [PK_LicenciaSolicitada2] PRIMARY KEY CLUSTERED 
(
	[idLicenciaSolicitada] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mensaje]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mensaje](
	[idMensaje] [int] IDENTITY(1,1) NOT NULL,
	[etiqueta] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Mensaje] PRIMARY KEY CLUSTERED 
(
	[idMensaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PatenteFamilia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatenteFamilia](
	[idPatenteFamilia] [int] IDENTITY(1,1) NOT NULL,
	[idPermisoPadre] [int] NOT NULL,
	[idPermisoHijo] [int] NOT NULL,
	[fechaCreacion] [varchar](50) NULL,
 CONSTRAINT [PK_PatenteFamilia] PRIMARY KEY CLUSTERED 
(
	[idPatenteFamilia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permiso]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permiso](
	[idPermiso] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [varchar](50) NULL,
	[descripcion] [varchar](200) NULL,
	[esPermisoBase] [bit] NULL,
	[fechaCreacion] [datetime] NULL,
 CONSTRAINT [PK_Permiso] PRIMARY KEY CLUSTERED 
(
	[idPermiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proyecto]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proyecto](
	[idProyecto] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[descripcion] [varchar](max) NULL,
	[presupuestoInicial] [int] NOT NULL,
	[fechaDesde] [datetime] NOT NULL,
	[fechaHasta] [datetime] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[dvh] [varchar](50) NULL,
	[esBorrado] [bit] NOT NULL,
	[idCliente] [int] NOT NULL,
 CONSTRAINT [PK_Proyecto] PRIMARY KEY CLUSTERED 
(
	[idProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegistroHoras]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistroHoras](
	[idRegistroHoras] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idProyecto] [int] NOT NULL,
	[cantidadHoras] [int] NOT NULL,
	[fecha] [datetime] NOT NULL,
	[comentarios] [varchar](max) NULL,
	[fechaCreacion] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tecnologia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tecnologia](
	[idTecnologia] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](50) NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[esBorrado] [bit] NOT NULL,
 CONSTRAINT [PK_Tecnologia] PRIMARY KEY CLUSTERED 
(
	[idTecnologia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoEvento]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoEvento](
	[idTipoEvento] [int] IDENTITY(1,1) NOT NULL,
	[tipoEvento] [varchar](50) NULL,
 CONSTRAINT [PK_TipoEvento] PRIMARY KEY CLUSTERED 
(
	[idTipoEvento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Traduccion]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Traduccion](
	[idTraduccion] [int] IDENTITY(1,1) NOT NULL,
	[idMensaje] [int] NOT NULL,
	[idIdioma] [int] NOT NULL,
	[texto] [varchar](200) NOT NULL,
 CONSTRAINT [PK_Traduccion] PRIMARY KEY CLUSTERED 
(
	[idTraduccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nombreUsuario] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[erroresLogin] [int] NULL,
	[esBloqueado] [bit] NULL,
	[fechaAlta] [datetime] NOT NULL,
	[idIdioma] [int] NULL,
	[idPermiso] [int] NULL,
	[dvh] [varchar](50) NULL,
	[esBorrado] [bit] NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[mail] [varchar](50) NULL,
	[fechaNacimiento] [datetime] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuarioPatente]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioPatente](
	[idUsuarioPatente] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idPatenteFamilia] [int] NOT NULL,
 CONSTRAINT [PK_UsuarioPatente] PRIMARY KEY CLUSTERED 
(
	[idUsuarioPatente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AptitudUsuario] ON 

INSERT [dbo].[AptitudUsuario] ([idAptitudUsuario], [idUsuario], [idTecnologia], [experiencia], [fechaCreacion]) VALUES (12, 7, 2, 0, CAST(N'2018-10-27T22:11:49.273' AS DateTime))
INSERT [dbo].[AptitudUsuario] ([idAptitudUsuario], [idUsuario], [idTecnologia], [experiencia], [fechaCreacion]) VALUES (14, 7, 1, 1, CAST(N'2018-11-20T21:22:24.313' AS DateTime))
INSERT [dbo].[AptitudUsuario] ([idAptitudUsuario], [idUsuario], [idTecnologia], [experiencia], [fechaCreacion]) VALUES (15, 18, 2, 2, CAST(N'2018-11-26T23:58:18.220' AS DateTime))
INSERT [dbo].[AptitudUsuario] ([idAptitudUsuario], [idUsuario], [idTecnologia], [experiencia], [fechaCreacion]) VALUES (16, 18, 1, 1, CAST(N'2018-11-26T23:58:26.723' AS DateTime))
SET IDENTITY_INSERT [dbo].[AptitudUsuario] OFF
SET IDENTITY_INSERT [dbo].[AsignacionUsuario] ON 

INSERT [dbo].[AsignacionUsuario] ([idAsignacionUsuario], [idUsuario], [idProyecto], [fechaDesde], [fechaHasta], [costoHora], [fechaCreacion], [dvh], [esBorrado]) VALUES (1, 7, 3, CAST(N'2018-10-30T08:28:15.000' AS DateTime), CAST(N'2018-10-31T08:28:15.000' AS DateTime), 5, CAST(N'2018-10-30T08:28:50.890' AS DateTime), N'8feddc42cf89ddbab081a8b71dc2ecaa', 0)
INSERT [dbo].[AsignacionUsuario] ([idAsignacionUsuario], [idUsuario], [idProyecto], [fechaDesde], [fechaHasta], [costoHora], [fechaCreacion], [dvh], [esBorrado]) VALUES (2, 7, 3, CAST(N'2018-11-01T08:33:51.000' AS DateTime), CAST(N'2018-11-10T08:33:51.000' AS DateTime), 10, CAST(N'2018-10-30T08:34:40.723' AS DateTime), N'dd3e3e59254ca27400cba9bcd379b833', 0)
INSERT [dbo].[AsignacionUsuario] ([idAsignacionUsuario], [idUsuario], [idProyecto], [fechaDesde], [fechaHasta], [costoHora], [fechaCreacion], [dvh], [esBorrado]) VALUES (3, 7, 3, CAST(N'2018-11-13T13:57:24.000' AS DateTime), CAST(N'2019-03-09T13:57:24.000' AS DateTime), 1000, CAST(N'2018-10-30T13:58:16.317' AS DateTime), N'fc5746ef6fb73cd9c35d42c313bca8c6', 0)
INSERT [dbo].[AsignacionUsuario] ([idAsignacionUsuario], [idUsuario], [idProyecto], [fechaDesde], [fechaHasta], [costoHora], [fechaCreacion], [dvh], [esBorrado]) VALUES (6, 7, 3, CAST(N'2019-05-10T19:23:41.000' AS DateTime), CAST(N'2019-05-15T19:23:41.000' AS DateTime), 100, CAST(N'2018-10-30T19:24:47.080' AS DateTime), N'929265f91f0673953ff9d9fc60d91a8c', 1)
INSERT [dbo].[AsignacionUsuario] ([idAsignacionUsuario], [idUsuario], [idProyecto], [fechaDesde], [fechaHasta], [costoHora], [fechaCreacion], [dvh], [esBorrado]) VALUES (7, 7, 4, CAST(N'2019-05-20T21:23:29.000' AS DateTime), CAST(N'2019-05-31T21:23:29.000' AS DateTime), 100, CAST(N'2018-11-20T21:25:10.557' AS DateTime), N'178f4cf00b0c25c804df14e1a665d334', 0)
INSERT [dbo].[AsignacionUsuario] ([idAsignacionUsuario], [idUsuario], [idProyecto], [fechaDesde], [fechaHasta], [costoHora], [fechaCreacion], [dvh], [esBorrado]) VALUES (8, 18, 3, CAST(N'2018-11-26T23:58:39.000' AS DateTime), CAST(N'2018-11-28T23:58:39.000' AS DateTime), 100, CAST(N'2018-11-26T23:59:02.883' AS DateTime), N'4b2ecfe17cd9490ba5058b3ea71a9d6d', 0)
INSERT [dbo].[AsignacionUsuario] ([idAsignacionUsuario], [idUsuario], [idProyecto], [fechaDesde], [fechaHasta], [costoHora], [fechaCreacion], [dvh], [esBorrado]) VALUES (9, 18, 3, CAST(N'2018-12-01T01:05:11.000' AS DateTime), CAST(N'2018-12-07T01:05:11.000' AS DateTime), 500, CAST(N'2018-11-27T01:05:30.217' AS DateTime), N'cac0231b167f1667611161e161b25433', 0)
SET IDENTITY_INSERT [dbo].[AsignacionUsuario] OFF
INSERT [dbo].[Backups] ([path], [fecha], [usuario]) VALUES (N'C:\tmp\\BKP_HOY-2018-11-20.bak', CAST(N'2018-11-20T16:29:44.850' AS DateTime), N'Diego')
SET IDENTITY_INSERT [dbo].[Bitacora] ON 

INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (37, N'String was not recognized as a valid DateTime.', CAST(N'2018-09-25T05:43:51.040' AS DateTime), 5, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (38, N'String was not recognized as a valid DateTime.', CAST(N'2018-09-25T05:44:07.647' AS DateTime), 5, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (44, N'Login exitoso', CAST(N'2018-09-25T05:59:51.350' AS DateTime), 3, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (48, N'Login exitoso', CAST(N'2018-09-25T06:02:45.020' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (50, N'Error intentando el login para el usuario ADMIN', CAST(N'2018-09-25T06:03:30.550' AS DateTime), 5, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (51, N'Login exitoso', CAST(N'2018-09-25T06:04:38.160' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (52, N'Login exitoso', CAST(N'2018-09-25T06:10:36.507' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (53, N'Login exitoso', CAST(N'2018-09-25T06:13:09.717' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (54, N'Login exitoso', CAST(N'2018-09-25T06:24:01.163' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (55, N'Login exitoso', CAST(N'2018-09-25T06:25:18.007' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (56, N'Login exitoso', CAST(N'2018-09-25T06:29:44.143' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (57, N'Login exitoso', CAST(N'2018-09-25T06:30:06.533' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (58, N'Login exitoso', CAST(N'2018-09-25T06:30:46.133' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (59, N'Login exitoso', CAST(N'2018-09-25T06:31:23.493' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (60, N'Login exitoso', CAST(N'2018-09-25T06:39:24.730' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (61, N'Login exitoso', CAST(N'2018-09-25T06:42:45.893' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (62, N'Login exitoso', CAST(N'2018-09-25T07:01:33.197' AS DateTime), 4, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (63, N'Se ha eliminado exitosamente a Diego', CAST(N'2018-09-25T07:01:50.057' AS DateTime), 3, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (64, N'Login exitoso para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T07:18:18.367' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (65, N'Login exitoso para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T07:18:59.200' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (66, N'Logout exitoso para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T07:19:00.940' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (67, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:20:27.747' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (68, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:20:31.210' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (69, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:36:57.630' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (70, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:41:03.807' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (71, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:43:10.460' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (72, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:43:14.550' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (73, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:52:43.573' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (74, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:52:46.177' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (75, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:53:26.343' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (76, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:53:37.330' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (77, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:56:30.530' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (78, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T07:58:33.293' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (79, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:04:26.600' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (80, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:05:10.087' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (81, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:05:18.880' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (82, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:06:00.487' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (83, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:06:02.507' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (84, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:06:30.597' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (85, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:06:36.900' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (86, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:06:59.333' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (87, N'Se ha eliminado exitosamente a diego2', CAST(N'2018-09-25T08:07:45.187' AS DateTime), 3, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (88, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:07:52.463' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (89, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:09:27.537' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (90, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:10:28.880' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (91, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:11:12.800' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (92, N'Se ha eliminado exitosamente a diego2', CAST(N'2018-09-25T08:11:28.853' AS DateTime), 3, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (93, N'Alta exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-09-25T08:13:22.147' AS DateTime), 1, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (94, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:13:31.717' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (95, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T08:17:01.277' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (96, N'Could not find stored procedure ''ActualizarUsuario''.', CAST(N'2018-09-25T08:17:09.697' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (97, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:20:08.413' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (98, N'Procedure or function ''ActualizarUsuario'' expects parameter ''@dvh'', which was not supplied.', CAST(N'2018-09-25T08:20:19.520' AS DateTime), 6, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (99, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:21:34.087' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (100, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T08:21:40.677' AS DateTime), 2, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (101, N'There is no row at position 0.', CAST(N'2018-09-25T08:21:47.633' AS DateTime), 6, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (102, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:23:15.183' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (103, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T08:23:24.003' AS DateTime), 2, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (104, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T08:24:49.640' AS DateTime), 2, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (105, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T08:26:50.813' AS DateTime), 2, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (106, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T08:29:05.277' AS DateTime), 2, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (107, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:31:39.873' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (108, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:32:18.503' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (109, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:33:10.690' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (110, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T08:35:40.750' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (111, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T08:36:42.787' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (112, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T08:36:55.930' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (113, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T08:42:08.060' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (114, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T08:42:56.733' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (115, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T08:43:10.243' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (116, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:43:23.190' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (117, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T08:43:52.967' AS DateTime), 2, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (118, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:43:59.863' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (119, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:44:11.330' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (120, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:44:12.967' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (121, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:52:59.460' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (122, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:53:06.533' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (123, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:57:48.833' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (124, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:57:53.527' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (125, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:58:31.070' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (126, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T08:59:59.130' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (127, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T09:38:41.943' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (128, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T09:45:04.543' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (129, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T09:49:07.777' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (130, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T09:51:43.080' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (131, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T10:09:05.687' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (132, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T10:09:37.643' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (133, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T10:35:15.067' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (134, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T10:35:21.860' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (135, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T10:35:54.663' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (136, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T10:36:59.500' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (137, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T10:40:38.430' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (138, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T10:40:47.550' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (139, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T11:35:02.473' AS DateTime), 4, 6, N'10.104.9.32')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (140, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T11:37:12.083' AS DateTime), 4, 6, N'10.104.9.32')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (141, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T11:37:30.820' AS DateTime), 7, 6, N'10.104.9.32')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (142, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T13:20:28.050' AS DateTime), 4, 6, N'10.104.7.185')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (143, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T13:28:49.723' AS DateTime), 4, 7, N'10.104.7.185')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (144, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T14:56:03.790' AS DateTime), 4, 7, N'10.0.75.1')
GO
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (145, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T14:58:35.643' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (146, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T15:00:09.860' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (147, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T15:01:12.540' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (148, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T15:02:28.300' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (149, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T15:05:10.813' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (150, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T15:12:45.610' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (151, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T15:13:09.387' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (152, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T15:13:41.197' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (153, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T15:14:05.480' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (154, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T15:16:49.070' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (155, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T15:17:08.000' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (156, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T15:30:48.730' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (157, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T15:31:39.910' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (158, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T15:39:15.930' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (159, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T16:14:49.240' AS DateTime), 4, 7, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (160, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T16:15:24.723' AS DateTime), 4, 7, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (161, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T16:16:13.620' AS DateTime), 4, 7, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (162, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T16:17:52.797' AS DateTime), 4, 7, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (163, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T16:27:35.210' AS DateTime), 4, 7, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (164, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T16:30:23.417' AS DateTime), 4, 7, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (165, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:09:06.020' AS DateTime), 4, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (166, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:11:47.570' AS DateTime), 4, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (167, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:13:00.857' AS DateTime), 7, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (168, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:14:45.010' AS DateTime), 4, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (169, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:21:19.933' AS DateTime), 4, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (170, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:28:16.917' AS DateTime), 4, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (171, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:28:50.463' AS DateTime), 7, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (172, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:37:55.953' AS DateTime), 4, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (173, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:41:27.550' AS DateTime), 4, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (174, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:41:30.550' AS DateTime), 7, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (175, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T17:41:43.083' AS DateTime), 4, 7, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (176, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T17:41:54.227' AS DateTime), 7, 7, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (177, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:42:00.407' AS DateTime), 4, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (178, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T17:42:05.360' AS DateTime), 7, 6, N'10.104.8.96')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (179, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T18:23:00.390' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (180, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-09-25T18:23:33.493' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (181, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T18:24:10.667' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (182, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T18:24:21.060' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (183, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T18:24:30.787' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (184, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T18:24:41.067' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (185, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T18:24:50.283' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (186, N'Login exitoso para el usuario ADMIN', CAST(N'2018-09-25T19:02:31.183' AS DateTime), 4, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (187, N'Logout exitoso para el usuario ADMIN', CAST(N'2018-09-25T19:04:28.897' AS DateTime), 7, 6, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (188, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T19:04:49.017' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (189, N'Login exitoso para el usuario Diego', CAST(N'2018-09-25T19:06:11.513' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (190, N'Logout exitoso para el usuario Diego', CAST(N'2018-09-25T19:11:37.153' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (191, N'Login exitoso para el usuario Diego', CAST(N'2018-10-16T20:06:01.330' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (192, N'Login exitoso para el usuario Diego', CAST(N'2018-10-16T20:13:31.177' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (193, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-16T20:13:35.030' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (194, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T09:40:12.760' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (195, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-17T09:40:21.360' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (196, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T09:45:36.000' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (197, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T09:57:21.373' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (198, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-17T09:57:45.987' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (199, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T13:55:31.660' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (200, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T14:02:40.113' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (201, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T14:03:43.513' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (202, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T14:04:30.547' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (203, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T14:05:36.780' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (204, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T14:06:25.830' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (205, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-17T14:06:35.230' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (206, N'Login exitoso para el usuario Diego', CAST(N'2018-10-17T14:06:55.843' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (207, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-17T14:07:40.587' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (208, N'Login exitoso para el usuario Diego', CAST(N'2018-10-18T20:16:49.060' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (209, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T09:36:19.917' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (210, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T09:36:19.940' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (211, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T09:36:33.373' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (212, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T09:37:08.473' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (213, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T09:37:08.490' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (214, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T09:37:16.540' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (215, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T09:37:42.767' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (216, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:10:13.530' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (217, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:10:13.543' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (218, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:10:55.120' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (219, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:11:45.273' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (220, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:11:45.283' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (221, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:11:53.207' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (222, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:13:01.220' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (223, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:13:01.250' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (224, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:13:11.953' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (225, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:13:49.733' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (226, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:13:49.757' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (227, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:13:56.950' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (228, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:15:15.500' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (229, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:15:15.513' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (230, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:15:31.647' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (231, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T20:16:37.040' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (232, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:18:35.597' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (233, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:18:35.600' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (234, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:18:43.083' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (235, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T20:19:39.193' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (236, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:21:01.937' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (237, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:21:01.950' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (238, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:21:08.360' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (239, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:22:54.793' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (240, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:22:54.807' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (241, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:23:01.933' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (242, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T20:24:02.630' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (243, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:24:25.143' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (244, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:24:25.160' AS DateTime), 2, NULL, N'10.0.75.1')
GO
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (245, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:24:32.300' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (246, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:26:04.677' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (247, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:26:04.687' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (248, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:26:10.103' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (249, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T20:26:31.313' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (250, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T20:26:51.873' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (251, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T20:26:51.880' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (252, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T20:35:08.183' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (253, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T21:42:45.997' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (254, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T21:42:50.347' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (255, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T21:44:14.710' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (256, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T21:45:12.370' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (257, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T21:45:27.693' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (258, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T21:45:29.037' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (259, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T21:52:39.213' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (260, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T21:52:40.710' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (261, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T21:55:22.673' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (262, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T21:55:22.680' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (263, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T21:56:22.973' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (264, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T21:56:39.400' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (265, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T22:01:12.347' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (266, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T22:01:12.353' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (267, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T22:02:13.820' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (268, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T22:02:13.830' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (269, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T22:02:19.420' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (270, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T22:02:19.427' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (271, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T22:05:18.567' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (272, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T22:05:29.717' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (273, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T22:10:19.193' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (274, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T22:10:20.510' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (275, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T22:11:22.457' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (276, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T22:11:33.607' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (277, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-25T22:13:37.717' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (278, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-25T22:13:39.257' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (279, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T22:24:17.270' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (280, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T22:24:45.783' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (281, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T22:26:08.893' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (282, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T22:26:29.163' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (283, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T22:29:00.347' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (284, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T22:29:05.073' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (285, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T22:31:39.087' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (286, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T22:31:44.590' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (287, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T22:32:20.863' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (288, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T22:32:23.350' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (289, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T22:33:27.970' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (290, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T22:33:29.357' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (291, N'Login exitoso para el usuario Diego', CAST(N'2018-10-25T22:33:51.920' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (292, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-25T22:34:07.027' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (293, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T11:23:14.403' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (294, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T11:24:39.273' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (295, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T11:24:48.867' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (296, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T11:35:39.943' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (297, N'Column name or number of supplied values does not match table definition.', CAST(N'2018-10-26T11:36:26.063' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (298, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T11:39:37.723' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (299, N'Column name or number of supplied values does not match table definition.', CAST(N'2018-10-26T11:39:55.433' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (300, N'Alta exitosa para la tecnologia JavaScript', CAST(N'2018-10-26T11:40:15.820' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (301, N'Alta exitosa para la tecnologia Visual Basic .net', CAST(N'2018-10-26T11:40:45.930' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (302, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T11:44:23.020' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (303, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-26T11:44:42.157' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (304, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T12:28:18.870' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (305, N'SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.', CAST(N'2018-10-26T12:28:58.577' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (306, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T12:30:43.807' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (307, N'Could not find stored procedure ''ActualizarTecnologia''.', CAST(N'2018-10-26T12:31:16.213' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (308, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T14:46:33.450' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (309, N'Could not find stored procedure ''ActualizarTecnologia''.', CAST(N'2018-10-26T14:46:44.250' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (310, N'Procedure or function ActualizarTecnologia has too many arguments specified.', CAST(N'2018-10-26T14:53:48.027' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (311, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T14:55:03.807' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (312, N'Invalid object name ''Tecnologias''.', CAST(N'2018-10-26T14:55:50.007' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (313, N'Modificacion exitosa para la tecnología vb.net', CAST(N'2018-10-26T14:56:26.337' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (314, N'Modificacion exitosa para la tecnología vb.net', CAST(N'2018-10-26T14:56:37.803' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (315, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T14:57:48.310' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (316, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-26T14:58:14.127' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (317, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T14:58:22.040' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (318, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-26T14:58:40.590' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (319, N'Login exitoso para el usuario Diego', CAST(N'2018-10-26T16:18:47.357' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (320, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-26T16:22:24.420' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (321, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T15:12:21.310' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (322, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T15:13:46.473' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (323, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T15:18:25.477' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (324, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-27T15:18:40.303' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (325, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T15:52:36.997' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (326, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T15:56:36.093' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (327, N'Conversion failed when converting the varchar value ''Media'' to data type int.', CAST(N'2018-10-27T15:58:01.130' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (328, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T16:02:28.417' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (329, N'Error intentando asignar una aptitud a Diego', CAST(N'2018-10-27T16:02:56.923' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (330, N'Column ''fechaCreacion'' does not belong to table .', CAST(N'2018-10-27T16:02:58.790' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (331, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T16:04:58.990' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (332, N'Error intentando asignar una aptitud a Diego', CAST(N'2018-10-27T16:05:27.347' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (333, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T16:17:32.647' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (334, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T16:20:43.800' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (335, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T16:28:17.633' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (336, N'Error intentando asignar una aptitud a Diego', CAST(N'2018-10-27T16:28:49.330' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (337, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T16:31:54.017' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (338, N'Error intentando asignar una aptitud a Diego', CAST(N'2018-10-27T16:32:39.070' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (339, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T16:33:41.500' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (340, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-27T16:35:03.360' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (341, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T16:35:10.273' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (342, N'Error intentando asignar una aptitud a Diego', CAST(N'2018-10-27T16:39:24.837' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (343, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T21:43:42.497' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (344, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T21:44:33.900' AS DateTime), 4, 7, N'10.0.75.1')
GO
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (345, N'Se ha eliminado exitosamente la tecnologia vb.net del usuario Diego', CAST(N'2018-10-27T21:48:24.053' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (346, N'Error intentando eliminar la tecnologia vb.net del usuario Diego', CAST(N'2018-10-27T21:50:32.070' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (347, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T21:52:56.550' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (348, N'Se ha eliminado exitosamente la tecnologia js del usuario Diego', CAST(N'2018-10-27T21:53:10.797' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (349, N'Asignacion exitosa de aptitud para Diego', CAST(N'2018-10-27T21:53:30.507' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (350, N'Se ha actualizado exitosamente la tecnologia js del usuario Diego', CAST(N'2018-10-27T21:53:37.173' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (351, N'Asignacion exitosa de aptitud para Diego', CAST(N'2018-10-27T21:53:47.037' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (352, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T21:56:20.040' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (353, N'Se ha eliminado exitosamente la tecnologia js del usuario Diego', CAST(N'2018-10-27T21:57:00.053' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (354, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T22:02:39.610' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (355, N'Asignacion exitosa de aptitud para Diego', CAST(N'2018-10-27T22:03:05.527' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (356, N'Asignacion exitosa de aptitud para Diego', CAST(N'2018-10-27T22:03:10.890' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (357, N'Index was out of range. Must be non-negative and less than the size of the collection.
Parameter name: index', CAST(N'2018-10-27T22:08:31.910' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (358, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T22:11:03.733' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (359, N'Se ha eliminado exitosamente la tecnologia vb.net del usuario Diego', CAST(N'2018-10-27T22:11:30.570' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (360, N'Asignacion exitosa de aptitud para Diego', CAST(N'2018-10-27T22:11:43.530' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (361, N'Asignacion exitosa de aptitud para Diego', CAST(N'2018-10-27T22:11:49.283' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (362, N'Se ha eliminado exitosamente la tecnologia js del usuario Diego', CAST(N'2018-10-27T22:11:55.897' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (363, N'Se ha actualizado exitosamente la tecnologia vb.net del usuario Diego', CAST(N'2018-10-27T22:12:04.820' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (364, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-27T22:12:13.540' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (365, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T22:13:58.280' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (366, N'Login exitoso para el usuario Diego', CAST(N'2018-10-27T22:14:09.473' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (367, N'Login exitoso para el usuario Diego', CAST(N'2018-10-28T17:12:17.917' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (368, N'Login exitoso para el usuario Diego', CAST(N'2018-10-28T17:14:09.370' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (369, N'Login exitoso para el usuario Diego', CAST(N'2018-10-28T17:18:13.893' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (370, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-28T17:18:33.640' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (371, N'Login exitoso para el usuario Diego', CAST(N'2018-10-28T17:20:50.107' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (372, N'Alta exitosa para la Licencia Enfermedad', CAST(N'2018-10-28T17:21:56.943' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (373, N'Alta exitosa para la Licencia Enfermedad', CAST(N'2018-10-28T17:22:07.700' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (374, N'Alta exitosa para la Licencia Vacaciones', CAST(N'2018-10-28T17:23:35.490' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (375, N'Login exitoso para el usuario Diego', CAST(N'2018-10-28T17:27:07.953' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (376, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-28T17:27:53.173' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (377, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T00:19:50.147' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (378, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T00:42:09.833' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (379, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T00:45:31.937' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (380, N'Asignacion exitosa de lincencia para el usuario Diego', CAST(N'2018-10-29T01:02:32.130' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (381, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T01:05:07.707' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (382, N'Asignacion exitosa de lincencia para el usuario Diego', CAST(N'2018-10-29T01:05:34.100' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (383, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T01:08:39.747' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (384, N'Asignacion exitosa de lincencia para el usuario Diego', CAST(N'2018-10-29T01:09:16.003' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (385, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T01:15:16.830' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (386, N'Column ''dvh'' does not belong to table .', CAST(N'2018-10-29T01:16:07.873' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (387, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T01:21:01.557' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (388, N'Could not find stored procedure ''ObtenerLicenciaPorId''.', CAST(N'2018-10-29T01:21:13.050' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (389, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T01:23:25.337' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (390, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T01:38:49.023' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (391, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T01:41:17.053' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (392, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T01:43:10.740' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (393, N'Modificacion exitosa para la tecnología ENF2', CAST(N'2018-10-29T01:43:34.967' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (394, N'Modificacion exitosa para la tecnología ENF', CAST(N'2018-10-29T01:43:52.767' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (395, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T01:44:25.617' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (396, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T02:37:23.467' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (397, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T02:38:37.853' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (398, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T02:39:49.300' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (399, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T02:40:03.273' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (400, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T03:00:26.950' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (401, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T03:12:01.140' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (402, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T03:17:54.860' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (403, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T03:18:01.467' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (404, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T03:18:04.150' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (405, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T03:19:06.030' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (406, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T03:19:49.877' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (407, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T03:20:04.607' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (408, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T03:20:19.787' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (409, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T15:38:37.200' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (410, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T15:39:49.400' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (411, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T16:57:58.070' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (412, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T17:20:05.627' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (413, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T17:20:08.893' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (414, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T17:45:52.247' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (415, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T17:45:52.257' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (416, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:45:53.523' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (417, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:45:53.533' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (418, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:45:53.537' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (419, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T17:45:59.983' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (420, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T17:46:02.227' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (421, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T17:48:26.913' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (422, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T17:48:26.923' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (423, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:48:28.680' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (424, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:48:28.683' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (425, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:48:28.687' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (426, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T17:48:29.990' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (427, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T17:48:31.500' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (428, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T17:48:43.897' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (429, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T17:48:43.910' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (430, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T17:49:43.430' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (431, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T17:49:43.460' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (432, N'Procedure or function ''ActualizarSolicitudPorUsuarioLicencia'' expects parameter ''@idAprobador'', which was not supplied.', CAST(N'2018-10-29T17:49:44.337' AS DateTime), 6, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (433, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T17:52:34.960' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (434, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T17:52:34.970' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (435, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T17:55:01.053' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (436, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T17:55:01.063' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (437, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:55:01.430' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (438, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:55:01.433' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (439, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T17:55:01.437' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (440, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T19:28:40.910' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (441, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T19:28:41.040' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (442, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:28:42.870' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (443, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:28:42.893' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (444, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:28:42.900' AS DateTime), 2, NULL, N'10.0.75.1')
GO
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (445, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:29:35.080' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (446, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:29:35.100' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (447, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:29:35.110' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (448, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T19:32:59.377' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (449, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T19:32:59.440' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (450, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:32:59.727' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (451, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:32:59.730' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (452, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:32:59.737' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (453, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:33:17.467' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (454, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:33:17.470' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (455, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:33:17.473' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (456, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:33:50.130' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (457, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:33:51.243' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (458, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:33:58.560' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (459, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T19:33:58.713' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (460, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T19:34:48.480' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (461, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T19:34:48.493' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (462, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:34:55.020' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (463, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:34:56.760' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (464, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-29T19:36:08.540' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (465, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-29T19:36:08.593' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (466, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:36:13.570' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (467, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:36:14.963' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (468, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-29T19:36:22.873' AS DateTime), 2, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (469, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T19:36:22.960' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (470, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T19:37:21.413' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (471, N'Login exitoso para el usuario Diego', CAST(N'2018-10-29T19:40:24.750' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (472, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-29T19:40:26.680' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (473, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T00:39:53.177' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (474, N'Could not find stored procedure ''ActualizarProyectoPorId''.', CAST(N'2018-10-30T00:45:40.907' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (475, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T00:46:12.810' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (476, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T00:46:19.097' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (477, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T00:47:53.683' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (478, N'Could not find stored procedure ''ActualizarProyectoPorId''.', CAST(N'2018-10-30T00:48:04.110' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (479, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T00:48:41.410' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (480, N'Se ha eliminado exitosamente a el Proyecto proyecto Dummy', CAST(N'2018-10-30T00:48:58.770' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (481, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T00:50:01.877' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (482, N'Modificacion exitosa para la tecnología proyecto Dummy', CAST(N'2018-10-30T00:50:22.143' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (483, N'Se ha eliminado exitosamente a el Proyecto proyecto Dummy', CAST(N'2018-10-30T00:50:28.483' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (484, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T00:50:33.773' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (485, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T02:44:07.893' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (486, N'Could not find stored procedure ''ObtenerClientes''.', CAST(N'2018-10-30T02:44:14.003' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (487, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T02:46:45.660' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (488, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T02:53:05.907' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (489, N'Procedure or function ''CrearCliente'' expects parameter ''@dvh'', which was not supplied.', CAST(N'2018-10-30T02:55:32.960' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (490, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T02:56:09.280' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (491, N'Alta exitosa para el Cliente Cliente SA', CAST(N'2018-10-30T02:56:29.540' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (492, N'Procedure or function ''ActualizarCliente'' expects parameter ''@idCliente'', which was not supplied.', CAST(N'2018-10-30T02:56:35.923' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (493, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T02:57:16.930' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (494, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T02:57:27.347' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (495, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T02:57:34.110' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (496, N'Object reference not set to an instance of an object.', CAST(N'2018-10-30T02:57:56.973' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (497, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T03:00:18.380' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (498, N'SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.', CAST(N'2018-10-30T03:00:49.087' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (499, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T03:03:27.047' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (500, N'Alta exitosa para el proyecto gbhn', CAST(N'2018-10-30T03:03:49.940' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (501, N'Alta exitosa para el proyecto klj', CAST(N'2018-10-30T03:04:10.260' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (502, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T03:47:46.830' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (503, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T03:48:45.877' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (504, N'Object reference not set to an instance of an object.', CAST(N'2018-10-30T03:48:45.910' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (505, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T03:49:22.913' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (506, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T03:49:42.550' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (507, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-30T03:49:42.580' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (508, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-30T03:49:42.590' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (509, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T03:49:45.433' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (510, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T03:49:45.437' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (511, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T03:49:45.443' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (512, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T03:49:52.733' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (513, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T08:07:16.653' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (514, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T08:07:22.310' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (515, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T08:08:33.397' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (516, N'Alta exitosa para el proyecto Proyecto Prueba', CAST(N'2018-10-30T08:13:59.510' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (517, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T08:25:40.330' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (518, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T08:28:04.977' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (519, N'Alta exitosa para el AsignacionUsuario Diego', CAST(N'2018-10-30T08:28:50.910' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (520, N'Column ''idPresupuesto'' does not belong to table .', CAST(N'2018-10-30T08:28:52.660' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (521, N'Column ''idPresupuesto'' does not belong to table .', CAST(N'2018-10-30T08:30:02.043' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (522, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T08:30:59.097' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (523, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T08:33:24.463' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (524, N'Alta exitosa para el AsignacionUsuario Diego', CAST(N'2018-10-30T08:34:40.740' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (525, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T08:35:04.820' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (526, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T08:41:34.990' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (527, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T09:23:32.267' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (528, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T09:43:48.587' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (529, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T09:43:52.070' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (530, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T09:45:11.757' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (531, N'Alta exitosa para la Licencia Maternidad', CAST(N'2018-10-30T09:45:29.647' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (532, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T10:01:54.593' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (533, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T11:53:08.360' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (534, N'Alta exitosa para el usuario Vg3iO7E9kgsm1g++fPvjMA==', CAST(N'2018-10-30T12:05:45.090' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (535, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:21:49.993' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (536, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T12:21:52.987' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (537, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:22:03.480' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (538, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T12:22:05.090' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (539, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:22:14.903' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (540, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T12:22:16.193' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (541, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:23:10.227' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (542, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T12:23:36.040' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (543, N'Object reference not set to an instance of an object.', CAST(N'2018-10-30T12:23:36.130' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (544, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:25:21.083' AS DateTime), 4, 7, N'10.0.75.1')
GO
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (545, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T12:25:48.770' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (546, N'Could not find stored procedure ''ObtenerClientePorId''.', CAST(N'2018-10-30T12:25:48.887' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (547, N'Could not find stored procedure ''ObtenerClientePorId''.', CAST(N'2018-10-30T12:25:48.910' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (548, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:27:30.410' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (549, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T12:28:00.330' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (550, N'Modificacion exitosa para la tecnología Proyecto Prueba', CAST(N'2018-10-30T12:28:00.910' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (551, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-30T12:28:01.103' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (552, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-30T12:28:01.237' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (553, N'Modificacion exitosa para el usuario Vg3iO7E9kgsm1g++fPvjMA==', CAST(N'2018-10-30T12:28:01.413' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (554, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T12:28:01.473' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (555, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T12:28:01.480' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (556, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T12:28:01.483' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (557, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T12:28:04.023' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (558, N'Modificacion exitosa para la tecnología Proyecto Prueba', CAST(N'2018-10-30T12:28:04.153' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (559, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-30T12:28:04.337' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (560, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-30T12:28:04.470' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (561, N'Modificacion exitosa para el usuario Vg3iO7E9kgsm1g++fPvjMA==', CAST(N'2018-10-30T12:28:04.603' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (562, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T12:28:04.623' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (563, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T12:28:04.630' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (564, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T12:28:04.633' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (565, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:37:59.383' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (566, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:39:39.983' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (567, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T12:43:58.760' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (568, N'Login exitoso para el usuario PM', CAST(N'2018-10-30T12:44:39.100' AS DateTime), 4, 8, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (569, N'Logout exitoso para el usuario PM', CAST(N'2018-10-30T12:44:55.090' AS DateTime), 7, 8, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (570, N'Login exitoso para el usuario PM', CAST(N'2018-10-30T12:46:52.580' AS DateTime), 4, 8, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (571, N'Logout exitoso para el usuario PM', CAST(N'2018-10-30T12:54:21.410' AS DateTime), 7, 8, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (572, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T12:58:20.770' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (573, N'Alta exitosa para el usuario RHPdpbRRWW+iaj+hkYWwug==', CAST(N'2018-10-30T12:58:52.907' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (574, N'Alta exitosa para el usuario dTj7+Yr+6k3TsbxmiWNyNQ==', CAST(N'2018-10-30T12:59:08.173' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (575, N'Alta exitosa para el usuario 8CtBMK13P34AuSPGiiZxvg==', CAST(N'2018-10-30T13:00:26.420' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (576, N'Alta exitosa para el usuario DO/QIo4ZR4JFlHs67m47PQ==', CAST(N'2018-10-30T13:00:43.120' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (577, N'Alta exitosa para el usuario Vt5d7SwShNa8uV/D9jm67w==', CAST(N'2018-10-30T13:00:58.480' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (578, N'Alta exitosa para el usuario YiU1AO6xan8V0o8gFh9zXQ==', CAST(N'2018-10-30T13:01:08.457' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (579, N'Alta exitosa para el usuario AwxHnuocMnjXM7afUXbelA==', CAST(N'2018-10-30T13:01:19.420' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (580, N'Login exitoso para el usuario DEV1', CAST(N'2018-10-30T13:18:17.937' AS DateTime), 4, 11, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (581, N'Logout exitoso para el usuario DEV1', CAST(N'2018-10-30T13:18:19.683' AS DateTime), 7, 11, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (582, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T13:18:30.793' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (583, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-10-30T13:18:44.053' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (584, N'Modificacion exitosa para la tecnología Proyecto Prueba', CAST(N'2018-10-30T13:18:45.850' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (585, N'Modificacion exitosa para el usuario ayLPLQ0Z3rHqbb6X4iNrow==', CAST(N'2018-10-30T13:18:48.223' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (586, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-10-30T13:18:50.920' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (587, N'Modificacion exitosa para el usuario Vg3iO7E9kgsm1g++fPvjMA==', CAST(N'2018-10-30T13:18:54.463' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (588, N'Modificacion exitosa para el usuario RHPdpbRRWW+iaj+hkYWwug==', CAST(N'2018-10-30T13:18:57.117' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (589, N'Modificacion exitosa para el usuario dTj7+Yr+6k3TsbxmiWNyNQ==', CAST(N'2018-10-30T13:18:58.710' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (590, N'Modificacion exitosa para el usuario 8CtBMK13P34AuSPGiiZxvg==', CAST(N'2018-10-30T13:18:59.147' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (591, N'Modificacion exitosa para el usuario DO/QIo4ZR4JFlHs67m47PQ==', CAST(N'2018-10-30T13:19:01.893' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (592, N'Modificacion exitosa para el usuario Vt5d7SwShNa8uV/D9jm67w==', CAST(N'2018-10-30T13:19:05.900' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (593, N'Modificacion exitosa para el usuario YiU1AO6xan8V0o8gFh9zXQ==', CAST(N'2018-10-30T13:19:09.187' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (594, N'Modificacion exitosa para el usuario AwxHnuocMnjXM7afUXbelA==', CAST(N'2018-10-30T13:19:13.520' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (595, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T13:19:14.177' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (596, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T13:19:14.410' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (597, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-10-30T13:19:14.450' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (598, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T13:19:42.403' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (599, N'Login exitoso para el usuario DEV1', CAST(N'2018-10-30T13:20:26.513' AS DateTime), 4, 11, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (600, N'Logout exitoso para el usuario DEV1', CAST(N'2018-10-30T13:23:30.550' AS DateTime), 7, 11, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (601, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T13:54:51.730' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (602, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T13:55:20.413' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (603, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T13:55:43.703' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (604, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T13:55:49.073' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (605, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T13:56:32.060' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (606, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T13:56:57.580' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (607, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T13:57:19.660' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (608, N'Alta exitosa para el AsignacionUsuario Diego', CAST(N'2018-10-30T13:58:16.323' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (609, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T13:58:26.803' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (610, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T13:58:45.347' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (611, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T13:59:10.280' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (612, N'Login exitoso para el usuario PM', CAST(N'2018-10-30T15:30:05.910' AS DateTime), 4, 8, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (613, N'Logout exitoso para el usuario PM', CAST(N'2018-10-30T15:30:11.333' AS DateTime), 7, 8, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (614, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T16:33:17.060' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (615, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T16:33:52.100' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (616, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T16:35:00.213' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (617, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T16:38:23.733' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (618, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T16:42:03.227' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (619, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T16:43:36.433' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (620, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T16:52:56.667' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (621, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T17:20:36.240' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (622, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T17:24:20.440' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (623, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T17:24:41.167' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (624, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T18:46:40.280' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (625, N'Modificacion exitosa para la tecnología js', CAST(N'2018-10-30T18:47:14.010' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (626, N'Modificacion exitosa para la tecnología js', CAST(N'2018-10-30T18:47:21.213' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (627, N'Alta exitosa para el AsignacionUsuario Diego', CAST(N'2018-10-30T18:54:50.373' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (628, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T19:00:33.210' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (629, N'Alta exitosa para el AsignacionUsuario Diego', CAST(N'2018-10-30T19:02:59.140' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (630, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T19:08:11.910' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (631, N'Login exitoso para el usuario Diego', CAST(N'2018-10-30T19:08:38.733' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (632, N'Modificacion exitosa para el usuario m2pkO8o3hc7TixMPi/ZwFQ==', CAST(N'2018-10-30T19:11:33.223' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (633, N'Alta exitosa para el AsignacionUsuario Diego', CAST(N'2018-10-30T19:24:47.100' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (634, N'Alta exitosa para el proyecto SDA', CAST(N'2018-10-30T19:25:23.880' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (635, N'Asignacion exitosa de lincencia para el usuario Diego', CAST(N'2018-10-30T19:32:45.487' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (636, N'Logout exitoso para el usuario Diego', CAST(N'2018-10-30T19:32:52.467' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (637, N'Login exitoso para el usuario Diego', CAST(N'2018-11-01T19:29:46.620' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (638, N'Login exitoso para el usuario Diego', CAST(N'2018-11-01T19:33:10.930' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (639, N'Login exitoso para el usuario Diego', CAST(N'2018-11-01T19:33:46.843' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (640, N'Login exitoso para el usuario Diego', CAST(N'2018-11-01T21:34:07.483' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (641, N'Login exitoso para el usuario Diego', CAST(N'2018-11-01T21:37:47.407' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (642, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-01T21:38:16.970' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (643, N'Login exitoso para el usuario Diego', CAST(N'2018-11-17T20:16:18.383' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (644, N'Login exitoso para el usuario Diego', CAST(N'2018-11-17T20:16:40.263' AS DateTime), 4, 7, N'10.0.75.1')
GO
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (645, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-17T20:16:41.640' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (646, N'Login exitoso para el usuario Diego', CAST(N'2018-11-17T20:16:50.603' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (647, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T22:26:10.323' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (648, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T22:42:17.900' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (649, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T22:43:09.027' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (650, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T22:50:01.903' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (651, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T23:14:08.943' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (652, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-18T23:15:15.373' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (653, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T23:16:05.190' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (654, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-18T23:17:54.870' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (655, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T23:19:03.200' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (656, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T23:25:16.680' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (657, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-18T23:25:29.920' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (658, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T23:43:16.540' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (659, N'Login exitoso para el usuario Diego', CAST(N'2018-11-18T23:45:17.483' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (660, N'Se ha eliminado exitosamente a ADMIN', CAST(N'2018-11-18T23:56:09.920' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (661, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T00:15:20.743' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (662, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T00:17:10.127' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (663, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T00:24:55.693' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (664, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T00:58:41.630' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (665, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T01:15:58.340' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (666, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T01:27:24.850' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (667, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T01:51:12.097' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (668, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T02:03:01.290' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (669, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T12:01:18.590' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (670, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-19T12:01:35.413' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (671, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T12:27:24.607' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (672, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T12:29:17.013' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (673, N'Se ha eliminado exitosamente la asignacion del usuario  Diego', CAST(N'2018-11-19T12:31:54.910' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (674, N'Se ha eliminado exitosamente la asignacion del usuario  Diego', CAST(N'2018-11-19T12:32:13.830' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (675, N'Login exitoso para el usuario Diego', CAST(N'2018-11-19T23:48:12.987' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (676, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T10:36:18.673' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (677, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T10:40:02.093' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (678, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T10:54:02.307' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (679, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-20T10:54:29.127' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (680, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T10:55:38.370' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (681, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T10:58:04.090' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (682, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-20T11:01:53.760' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (683, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T12:44:58.577' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (684, N'Se han registrado horas para el usuario Diego', CAST(N'2018-11-20T12:45:26.570' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (685, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-20T12:45:31.393' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (686, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T13:18:06.903' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (687, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-20T13:20:30.887' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (688, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T14:52:40.247' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (689, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-20T14:53:45.543' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (690, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T16:22:10.520' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (691, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T16:27:12.213' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (692, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T16:29:31.367' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (693, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-20T16:57:45.067' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (694, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T20:58:27.057' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (695, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-20T21:00:06.903' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (696, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T21:00:31.247' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (697, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-20T21:01:01.377' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (698, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T21:02:20.560' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (699, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T21:05:34.427' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (700, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T21:07:37.627' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (701, N'Asignacion exitosa de aptitud para Diego', CAST(N'2018-11-20T21:09:42.717' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (702, N'Se ha eliminado exitosamente la tecnologia js del usuario Diego', CAST(N'2018-11-20T21:09:52.043' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (703, N'Login exitoso para el usuario Diego', CAST(N'2018-11-20T21:12:25.897' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (704, N'Se ha eliminado exitosamente la asignacion del usuario  Diego', CAST(N'2018-11-20T21:13:25.930' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (705, N'Asignacion exitosa de aptitud para Diego', CAST(N'2018-11-20T21:22:24.337' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (706, N'Alta exitosa para el AsignacionUsuario Diego', CAST(N'2018-11-20T21:25:10.570' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (707, N'Se han registrado horas para el usuario Diego', CAST(N'2018-11-20T21:26:28.553' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (708, N'Se han registrado horas para el usuario Diego', CAST(N'2018-11-20T21:27:19.530' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (709, N'Login exitoso para el usuario Diego', CAST(N'2018-11-22T23:32:59.550' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (710, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-22T23:33:55.710' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (711, N'Login exitoso para el usuario Diego', CAST(N'2018-11-22T23:38:34.537' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (712, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-22T23:39:26.610' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (713, N'Login exitoso para el usuario Diego', CAST(N'2018-11-22T23:42:12.437' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (714, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-22T23:42:29.253' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (715, N'Login exitoso para el usuario Diego', CAST(N'2018-11-23T00:20:50.193' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (716, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-23T00:21:43.530' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (717, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T20:42:34.040' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (718, N'Modificacion exitosa para el usuario 8CtBMK13P34AuSPGiiZxvg==', CAST(N'2018-11-24T20:43:09.777' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (719, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-24T20:43:16.320' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (720, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T20:59:08.377' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (721, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-24T21:00:50.433' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (722, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:07:35.203' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (723, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:11:19.177' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (724, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:24:11.577' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (725, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:26:10.350' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (726, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:27:18.040' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (727, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-24T21:27:31.927' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (728, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:27:48.320' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (729, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:30:33.120' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (730, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-24T21:32:15.053' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (731, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:32:41.463' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (732, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:40:01.050' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (733, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-24T21:42:29.180' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (734, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:43:32.117' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (735, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-24T21:45:29.857' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (736, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:46:23.867' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (737, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:48:21.907' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (738, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-24T21:49:33.767' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (739, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:49:50.597' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (740, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-24T21:50:18.670' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (741, N'Login exitoso para el usuario Diego', CAST(N'2018-11-24T21:53:40.137' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (742, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T09:03:29.620' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (743, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-25T09:08:23.827' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (744, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T10:20:47.083' AS DateTime), 4, 7, N'10.0.75.1')
GO
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (745, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-25T10:21:00.147' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (746, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T10:21:48.713' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (747, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T10:23:05.990' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (748, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T10:24:29.370' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (749, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T10:54:32.670' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (750, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T10:57:59.857' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (751, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T11:00:20.530' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (752, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T11:01:19.660' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (753, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T11:04:02.373' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (754, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T11:11:36.777' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (755, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T11:12:49.890' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (756, N'Login exitoso para el usuario Diego', CAST(N'2018-11-25T11:14:14.743' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (757, N'Alta exitosa para el Cliente OtroCli SA', CAST(N'2018-11-25T11:14:44.870' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (758, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-25T11:14:55.110' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (759, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T10:44:37.643' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (760, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T10:45:19.257' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (761, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T10:48:25.617' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (762, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T10:52:28.953' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (763, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T10:53:41.213' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (764, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T11:12:08.923' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (765, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T13:53:50.990' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (766, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T13:55:01.210' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (767, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T14:01:34.013' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (768, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T14:03:51.533' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (769, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T14:09:04.200' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (770, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T14:24:52.373' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (771, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T14:30:01.467' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (772, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T14:30:13.167' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (773, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T14:48:01.090' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (774, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T14:50:09.930' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (775, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T14:50:59.070' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (776, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:03:58.317' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (777, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:24:35.207' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (778, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:30:12.220' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (779, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:32:04.800' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (780, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:34:44.553' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (781, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:36:16.253' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (782, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:39:18.990' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (783, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:52:42.320' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (784, N'Alta exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T15:52:57.543' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (785, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T15:53:08.757' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (786, N'Alta exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T15:53:28.520' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (787, N'Se ha eliminado exitosamente a Diego', CAST(N'2018-11-26T15:53:36.907' AS DateTime), 3, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (788, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T15:53:40.107' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (789, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:54:54.673' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (790, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T15:55:21.863' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (791, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T15:55:32.670' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (792, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T15:57:26.323' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (793, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T15:59:19.903' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (794, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T16:01:00.020' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (795, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T16:15:24.070' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (796, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T16:25:12.480' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (797, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T16:25:46.323' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (798, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T16:26:14.997' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (799, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T16:43:40.127' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (800, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T16:44:00.033' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (801, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T16:52:33.810' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (802, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T17:18:46.947' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (803, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T17:19:03.233' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (804, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T17:24:32.867' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (805, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T17:31:14.340' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (806, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T17:35:23.563' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (807, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T17:36:43.523' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (808, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T17:38:17.660' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (809, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T17:39:25.527' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (810, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-11-26T17:39:29.650' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (811, N'Modificacion exitosa para el cliente OtroCli SA', CAST(N'2018-11-26T17:39:29.650' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (812, N'Object reference not set to an instance of an object.', CAST(N'2018-11-26T17:39:29.720' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (813, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T17:42:02.567' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (814, N'Modificacion exitosa para el cliente Cliente SA', CAST(N'2018-11-26T17:42:10.720' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (815, N'Modificacion exitosa para el cliente OtroCli SA', CAST(N'2018-11-26T17:42:10.730' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (816, N'Modificacion exitosa para la tecnología Proyecto Prueba', CAST(N'2018-11-26T17:42:10.757' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (817, N'Modificacion exitosa para la tecnología SDA', CAST(N'2018-11-26T17:42:10.760' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (818, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T17:42:13.907' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (819, N'Modificacion exitosa para el usuario Vg3iO7E9kgsm1g++fPvjMA==', CAST(N'2018-11-26T17:42:13.997' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (820, N'Modificacion exitosa para el usuario RHPdpbRRWW+iaj+hkYWwug==', CAST(N'2018-11-26T17:42:14.073' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (821, N'Modificacion exitosa para el usuario dTj7+Yr+6k3TsbxmiWNyNQ==', CAST(N'2018-11-26T17:42:14.157' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (822, N'Modificacion exitosa para el usuario 8CtBMK13P34AuSPGiiZxvg==', CAST(N'2018-11-26T17:42:14.250' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (823, N'Modificacion exitosa para el usuario DO/QIo4ZR4JFlHs67m47PQ==', CAST(N'2018-11-26T17:42:14.323' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (824, N'Modificacion exitosa para el usuario Vt5d7SwShNa8uV/D9jm67w==', CAST(N'2018-11-26T17:42:14.463' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (825, N'Modificacion exitosa para el usuario YiU1AO6xan8V0o8gFh9zXQ==', CAST(N'2018-11-26T17:42:14.550' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (826, N'Modificacion exitosa para el usuario m2pkO8o3hc7TixMPi/ZwFQ==', CAST(N'2018-11-26T17:42:14.687' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (827, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T17:42:14.827' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (828, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-11-26T17:42:14.873' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (829, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-11-26T17:42:14.877' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (830, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-11-26T17:42:14.880' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (831, N'Se ha actualizado exitosamente la solicitud de Licencia del usuario Diego', CAST(N'2018-11-26T17:42:14.880' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (832, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T20:15:07.420' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (833, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T21:59:47.353' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (834, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:00:57.120' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (835, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T22:01:54.210' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (836, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:04:31.720' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (837, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:05:24.870' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (838, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T22:05:58.973' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (839, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:07:36.833' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (840, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:14:48.467' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (841, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T22:15:05.050' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (842, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:16:08.573' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (843, N'Modificacion exitosa para el usuario AwxHnuocMnjXM7afUXbelA==', CAST(N'2018-11-26T22:16:30.190' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (844, N'Modificacion exitosa para el usuario AwxHnuocMnjXM7afUXbelA==', CAST(N'2018-11-26T22:16:41.430' AS DateTime), 2, 7, N'10.0.75.1')
GO
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (845, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:24:47.487' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (846, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:26:27.473' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (847, N'Modificacion exitosa para el usuario Diego', CAST(N'2018-11-26T22:26:48.730' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (848, N'Invalid length for a Base-64 char array or string.', CAST(N'2018-11-26T22:32:48.940' AS DateTime), 6, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (849, N'Invalid length for a Base-64 char array or string.', CAST(N'2018-11-26T22:32:56.857' AS DateTime), 6, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (850, N'Invalid length for a Base-64 char array or string.', CAST(N'2018-11-26T22:40:54.550' AS DateTime), 6, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (851, N'Invalid length for a Base-64 char array or string.', CAST(N'2018-11-26T22:43:58.403' AS DateTime), 6, NULL, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (852, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:44:57.110' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (853, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T22:49:30.330' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (854, N'Modificacion exitosa para el usuario joB6DOaFDMWe401t1/N6rw==', CAST(N'2018-11-26T22:49:36.983' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (855, N'Modificacion exitosa para el usuario joB6DOaFDMWe401t1/N6rw==', CAST(N'2018-11-26T22:49:56.737' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (856, N'Invalid length for a Base-64 char array or string.', CAST(N'2018-11-26T22:50:00.303' AS DateTime), 6, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (857, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T22:54:54.240' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (858, N'Modificacion exitosa para el usuario joB6DOaFDMWe401t1/N6rw==', CAST(N'2018-11-26T22:57:06.893' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (859, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T22:57:41.303' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (860, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T22:59:00.777' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (861, N'Modificacion exitosa para el usuario iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T23:02:07.270' AS DateTime), 2, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (862, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T23:02:36.557' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (863, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T23:02:54.977' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (864, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T23:03:53.090' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (865, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T23:20:48.843' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (866, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T23:21:47.397' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (867, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T23:22:31.153' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (868, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T23:22:49.243' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (869, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T23:23:16.947' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (870, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T23:23:28.557' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (871, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T23:31:51.430' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (872, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T23:32:12.910' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (873, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T23:35:16.543' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (874, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T23:35:41.023' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (875, N'Login exitoso para el usuario Diego', CAST(N'2018-11-26T23:56:05.443' AS DateTime), 4, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (876, N'Alta exitosa para el usuario 0fnx/m8Vc7+QURuoZkY7qw==', CAST(N'2018-11-26T23:56:40.817' AS DateTime), 1, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (877, N'Logout exitoso para el usuario Diego', CAST(N'2018-11-26T23:57:20.937' AS DateTime), 7, 7, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (878, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-26T23:57:41.573' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (879, N'Alta exitosa para la tecnologia Angular js', CAST(N'2018-11-26T23:58:09.133' AS DateTime), 1, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (880, N'Asignacion exitosa de aptitud para Diego2', CAST(N'2018-11-26T23:58:18.230' AS DateTime), 1, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (881, N'Asignacion exitosa de aptitud para Diego2', CAST(N'2018-11-26T23:58:26.743' AS DateTime), 1, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (882, N'Alta exitosa para el AsignacionUsuario Diego2', CAST(N'2018-11-26T23:59:02.897' AS DateTime), 1, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (883, N'Se han registrado horas para el usuario Diego2', CAST(N'2018-11-26T23:59:18.627' AS DateTime), 1, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (884, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T00:22:33.820' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (885, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T00:42:25.313' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (886, N'Logout exitoso para el usuario Diego2', CAST(N'2018-11-27T00:51:32.630' AS DateTime), 7, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (887, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T00:53:08.797' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (888, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T00:58:11.840' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (889, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T01:02:40.713' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (890, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T01:05:09.320' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (891, N'Alta exitosa para el AsignacionUsuario Diego2', CAST(N'2018-11-27T01:05:30.227' AS DateTime), 1, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (892, N'Se han registrado horas para el usuario Diego2', CAST(N'2018-11-27T01:06:11.560' AS DateTime), 1, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (893, N'Se han registrado horas para el usuario Diego2', CAST(N'2018-11-27T01:07:27.977' AS DateTime), 1, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (894, N'Logout exitoso para el usuario Diego2', CAST(N'2018-11-27T01:09:27.703' AS DateTime), 7, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (895, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T02:05:04.177' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (896, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T02:07:14.953' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (897, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T02:07:54.077' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (898, N'Logout exitoso para el usuario Diego2', CAST(N'2018-11-27T04:46:22.473' AS DateTime), 7, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (899, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:00:09.540' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (900, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:01:00.930' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (901, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:05:37.007' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (902, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:07:42.447' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (903, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:08:50.087' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (904, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:11:59.347' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (905, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:14:04.990' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (906, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:15:55.383' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (907, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:17:25.500' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (908, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:21:17.707' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (909, N'Logout exitoso para el usuario Diego2', CAST(N'2018-11-27T05:22:37.813' AS DateTime), 7, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (910, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T05:35:41.780' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (911, N'Logout exitoso para el usuario Diego2', CAST(N'2018-11-27T05:41:05.963' AS DateTime), 7, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (912, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:00:29.337' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (913, N'Logout exitoso para el usuario Diego2', CAST(N'2018-11-27T06:00:35.370' AS DateTime), 7, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (914, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:03:07.863' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (915, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:05:11.057' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (916, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:10:40.353' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (917, N'Invalid object name ''FamiliaPatente''.', CAST(N'2018-11-27T06:10:52.330' AS DateTime), 6, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (918, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:11:53.467' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (919, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:15:42.500' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (920, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:16:41.120' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (921, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:17:37.410' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (922, N'Login exitoso para el usuario Diego2', CAST(N'2018-11-27T06:18:29.170' AS DateTime), 4, 18, N'10.0.75.1')
INSERT [dbo].[Bitacora] ([idBitacora], [mensaje], [fechaEvento], [idTipoEvento], [idUsuario], [ip]) VALUES (923, N'Logout exitoso para el usuario Diego2', CAST(N'2018-11-27T06:19:37.577' AS DateTime), 7, 18, N'10.0.75.1')
SET IDENTITY_INSERT [dbo].[Bitacora] OFF
SET IDENTITY_INSERT [dbo].[Cliente] ON 

INSERT [dbo].[Cliente] ([idCliente], [descripcion], [cuit], [mailContacto], [nombreContacto], [telefonoContacto], [razonSocial], [activo], [fechaCreacion], [dvh]) VALUES (1, N'Cliente 2', N'5678a', N'arce@ads.com', N'Di', N'Cliente SA', N'Cliente SA', 1, CAST(N'2018-10-30T02:56:29.523' AS DateTime), N'bc51bce34ac150510878c47bf03fc369')
INSERT [dbo].[Cliente] ([idCliente], [descripcion], [cuit], [mailContacto], [nombreContacto], [telefonoContacto], [razonSocial], [activo], [fechaCreacion], [dvh]) VALUES (2, N'Otro cliente', N'20111333928', N'darce@wwee.com', N'Diego', N'OtroCli SA', N'OtroCli SA', 1, CAST(N'2018-11-25T11:14:44.450' AS DateTime), N'023f2a9e75e054b13c8303bd60fade59')
SET IDENTITY_INSERT [dbo].[Cliente] OFF
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'idUsuario', NULL, N'16', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'nombreUsuario', NULL, N'iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'password', NULL, N'14e1b600b1fd579f47433b88e8d85291', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'erroresLogin', NULL, N'0', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'esBloqueado', NULL, N'0', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'fechaAlta', NULL, N'Nov 26 2018  3:52PM', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'idIdioma', NULL, N'1', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'idPermiso', NULL, N'0', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'dvh', NULL, N'ccdeaf2f7097d52c0c7f883661f0237f', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'esBorrado', NULL, N'0', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'nombre', NULL, N'Z33YPk8cNrwllG5Bs3VJfw==', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'apellido', NULL, N'y+iaIJKwb+uDMhJ/GT9bSw==', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'mail', NULL, N'asdqwe@as.com', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'16', N'fechaNacimiento', NULL, N'Nov 26 2018 12:00AM', CAST(N'2018-11-26T15:52:57.413' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'0EE5FE6B-47CC-45E4-A6EE-56B403690C7F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'dvh', N'c0b571e19e6ced9482b0c6ed56db586f', N'e87b20350d5a39a1bdea42cd19965158', CAST(N'2018-11-26T15:53:08.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'78EB4EC9-3F11-4FB1-A673-6B461E879C85')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'nombre', N'iJEscpXPR+bR+WoYez3BMQ==', N'NE86H1jP2WB+vGYcCcMr6A==', CAST(N'2018-11-26T15:53:08.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'78EB4EC9-3F11-4FB1-A673-6B461E879C85')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'fechaNacimiento', N'Sep 25 2018 12:00AM', N'Nov 26 2018 12:00AM', CAST(N'2018-11-26T15:53:08.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'78EB4EC9-3F11-4FB1-A673-6B461E879C85')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'idUsuario', NULL, N'17', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'nombreUsuario', NULL, N'iJEscpXPR+bR+WoYez3BMQ==', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'password', NULL, N'c56d0e9a7ccec67b4ea131655038d604', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'erroresLogin', NULL, N'0', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'esBloqueado', NULL, N'0', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'fechaAlta', NULL, N'Nov 26 2018  3:53PM', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'idIdioma', NULL, N'1', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'idPermiso', NULL, N'0', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'dvh', NULL, N'f45b0986b327fcbf8fae21d46fb3dd28', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'esBorrado', NULL, N'0', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'nombre', NULL, N'Z33YPk8cNrwllG5Bs3VJfw==', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'apellido', NULL, N'y+iaIJKwb+uDMhJ/GT9bSw==', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'mail', NULL, N'asdqwe@as.com', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'17', N'fechaNacimiento', NULL, N'Oct 20 1987 12:00AM', CAST(N'2018-11-26T15:53:28.390' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'6DCA1A60-4051-467F-9B98-532A0FB2C00F')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'16', N'esBorrado', N'0', N'1', CAST(N'2018-11-26T15:53:36.870' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'E9162E29-3FE2-40C7-907C-7AFD462412EC')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'dvh', N'f45b0986b327fcbf8fae21d46fb3dd28', N'cdff9f3008a170a0b803d9741a8c3992', CAST(N'2018-11-26T15:55:21.780' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'E2E08EC0-F4EC-4D81-9108-C03A5A95FAE5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'dvh', N'cdff9f3008a170a0b803d9741a8c3992', N'ab8c1b561aee9e3b6545e27bb7e2a5b3', CAST(N'2018-11-26T15:55:32.580' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'27E45A2C-8022-4F34-B035-B18AA9252E9B')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'15', N'nombreUsuario', N'm2pkO8o3hc7TixMPi/ZwFQ==', N'AwxHnuocMnjXM7afUXbelA==', CAST(N'2018-11-26T22:16:30.067' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'CBF5F398-25A3-4C9A-B2E7-273FC630BA30')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'15', N'dvh', N'7c4a87256b69ef687212119c9265036a', N'9af1d7777ec333b81c400fdb71ce8671', CAST(N'2018-11-26T22:16:30.067' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'CBF5F398-25A3-4C9A-B2E7-273FC630BA30')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'15', N'nombre', N'09Nk2Dfq3qnc/ZIb72Tf3w==', N'AwxHnuocMnjXM7afUXbelA==', CAST(N'2018-11-26T22:16:30.067' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'CBF5F398-25A3-4C9A-B2E7-273FC630BA30')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'15', N'apellido', N'zSpuB9EWiGg+A+h5mF08Og==', N'AwxHnuocMnjXM7afUXbelA==', CAST(N'2018-11-26T22:16:30.067' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'CBF5F398-25A3-4C9A-B2E7-273FC630BA30')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'15', N'fechaNacimiento', N'Oct 30 2018 12:00AM', N'Jul 16 2018 12:00AM', CAST(N'2018-11-26T22:16:30.067' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'CBF5F398-25A3-4C9A-B2E7-273FC630BA30')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'15', N'dvh', N'9af1d7777ec333b81c400fdb71ce8671', N'944eb63343fddfd05bb79ce743a2f39b', CAST(N'2018-11-26T22:16:41.307' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'E99D93EE-1F90-4EF5-A471-AFE3664AA0F0')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'15', N'fechaNacimiento', N'Jul 16 2018 12:00AM', N'Jul  1 2018 12:00AM', CAST(N'2018-11-26T22:16:41.307' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'E99D93EE-1F90-4EF5-A471-AFE3664AA0F0')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'password', N'c56d0e9a7ccec67b4ea131655038d604', N'f4cc399f0effd13c888e310ea2cf5399', CAST(N'2018-11-26T22:57:06.813' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A98B9CEF-F6C3-4827-BA6B-2FD6A8DD1518')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'dvh', N'920d1bcb2959d11fcc28206a71dc5e6e', N'd713420f4128be6062f0271e6f5a459c', CAST(N'2018-11-26T22:57:06.813' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A98B9CEF-F6C3-4827-BA6B-2FD6A8DD1518')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'fechaNacimiento', N'Oct 20 1987 12:00AM', N'Nov 26 2018 12:00AM', CAST(N'2018-11-26T22:57:06.813' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A98B9CEF-F6C3-4827-BA6B-2FD6A8DD1518')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'fechaNacimiento', N'Nov 26 2018 12:00AM', N'Sep 25 2018 12:00AM', CAST(N'2018-11-26T22:26:48.647' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'85E74F9A-5362-4C38-88F0-2AA0CEDE4DA9')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'dvh', N'f0251ece9af9411aa56992e0fb05f431', N'afda69642998d276485ea75e0203f1c6', CAST(N'2018-11-26T22:57:41.203' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'C6C513E0-3DB4-4FDF-8368-6DF8E4E590D3')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'dvh', N'afda69642998d276485ea75e0203f1c6', N'cf0bea0d83d5fe6ba5347f4a7de42b53', CAST(N'2018-11-26T22:59:00.690' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'CFBF0B56-1B78-483A-A60E-A98B5F97404B')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'fechaNacimiento', N'Nov 26 2018 12:00AM', N'Nov  1 2018 12:00AM', CAST(N'2018-11-26T22:59:00.690' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'CFBF0B56-1B78-483A-A60E-A98B5F97404B')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'password', N'e10adc3949ba59abbe56e057f20f883e', N'81dc9bdb52d04dc20036dbd8313ed055', CAST(N'2018-11-26T23:02:07.160' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'F22280C6-2ED6-4857-8CB7-C6022B6864BB')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'dvh', N'cf0bea0d83d5fe6ba5347f4a7de42b53', N'96bc7e8ec1c68e40160aecf69850ed50', CAST(N'2018-11-26T23:02:07.160' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'F22280C6-2ED6-4857-8CB7-C6022B6864BB')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'fechaNacimiento', N'Nov  1 2018 12:00AM', N'Nov 26 2018 12:00AM', CAST(N'2018-11-26T23:02:07.160' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'F22280C6-2ED6-4857-8CB7-C6022B6864BB')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'dvh', N'9ba7794959f5e4ecd4720e5f154494e8', N'f0251ece9af9411aa56992e0fb05f431', CAST(N'2018-11-26T22:49:30.263' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'54F265E3-6A09-4C13-808F-44216EEE08E6')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'fechaNacimiento', N'Sep 25 2018 12:00AM', N'Nov 26 2018 12:00AM', CAST(N'2018-11-26T22:49:30.263' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'54F265E3-6A09-4C13-808F-44216EEE08E6')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'nombreUsuario', N'iJEscpXPR+bR+WoYez3BMQ==', N'joB6DOaFDMWe401t1/N6rw==', CAST(N'2018-11-26T22:49:36.903' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'52648207-99C7-4EDB-88F9-6E28E6C87DA0')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'dvh', N'7217093a32f01da8f6c49b63791d5d4d', N'd713420f4128be6062f0271e6f5a459c', CAST(N'2018-11-26T22:49:36.903' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'52648207-99C7-4EDB-88F9-6E28E6C87DA0')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'dvh', N'd713420f4128be6062f0271e6f5a459c', N'920d1bcb2959d11fcc28206a71dc5e6e', CAST(N'2018-11-26T22:49:56.670' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A603A0C5-68F5-483E-83AC-B725E7F3D53A')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'fechaNacimiento', N'Nov 26 2018 12:00AM', N'Oct 20 1987 12:00AM', CAST(N'2018-11-26T22:49:56.670' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A603A0C5-68F5-483E-83AC-B725E7F3D53A')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'dvh', N'ab8c1b561aee9e3b6545e27bb7e2a5b3', N'234c6dedfe3088c87b2d2be961df2ab5', CAST(N'2018-11-26T15:59:13.890' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'B97E777B-FC78-482A-8D9E-B6E3FEE4816A')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'mail', N'asdqwe@as.com', N'asdqwe@aasds.com', CAST(N'2018-11-26T15:59:13.890' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'B97E777B-FC78-482A-8D9E-B6E3FEE4816A')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'fechaNacimiento', N'Oct 20 1987 12:00AM', N'Nov 26 2018 12:00AM', CAST(N'2018-11-26T15:59:13.890' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'B97E777B-FC78-482A-8D9E-B6E3FEE4816A')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'apellido', N'iJEscpXPR+bR+WoYez3BMQ==', N'y+iaIJKwb+uDMhJ/GT9bSw==', CAST(N'2018-11-26T22:49:30.263' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'54F265E3-6A09-4C13-808F-44216EEE08E6')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'7', N'dvh', N'e87b20350d5a39a1bdea42cd19965158', N'9ba7794959f5e4ecd4720e5f154494e8', CAST(N'2018-11-26T17:42:13.760' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'81B4FE10-24FC-4F86-BC15-DAD2A607615C')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'8', N'dvh', N'af966782d9b6ea5b7d14ed8ed85e3ecf', N'4eaf35e8cae084655163c9d74414a612', CAST(N'2018-11-26T17:42:13.920' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'7085E269-0B08-417D-9471-E47782C826C4')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'9', N'dvh', N'0444e3a7deddd2d3e574b2ff0fd122f4', N'01c8edc690760873e2aa0388eb35fb6f', CAST(N'2018-11-26T17:42:14.003' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'E5580A69-C51E-46C9-999F-F21059F64323')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'10', N'dvh', N'1466cdf7fbe74ee6574d1381d8a125dc', N'373f1eedb75b4aa78b00ef8e1f77d7aa', CAST(N'2018-11-26T17:42:14.080' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'4BE511A2-EDEF-4514-9B0C-C89B47B1D3B1')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'11', N'dvh', N'19398158466f0607bf35d60c6aa23911', N'a400ca9a84918bb05434f499149a87ac', CAST(N'2018-11-26T17:42:14.163' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'F32EF549-FD18-4E66-BA8C-BE7B976511F0')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'12', N'dvh', N'f566c5cfcc60fb32916ab0a279b7e69b', N'814ffda4e53a2880b49d8218c01e1a44', CAST(N'2018-11-26T17:42:14.257' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'AE88FB47-22A5-459A-8A7D-ADAD698AB7A7')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'13', N'dvh', N'197cab22a068bb85e5da90b89573783a', N'e05123f12101e8079f45bb7ca78e8b03', CAST(N'2018-11-26T17:42:14.330' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'08C2F767-2917-4BBF-A0BF-3450D360DBA1')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'14', N'dvh', N'e78bff484a4d69727d37e00a2f59184b', N'e7035fd3e91a49485c4e7856c9dc6ee6', CAST(N'2018-11-26T17:42:14.473' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'FE46AB3B-EF00-42B9-BE78-3D9024364044')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'15', N'dvh', N'11e3c936c11fe740a3365bff64662797', N'7c4a87256b69ef687212119c9265036a', CAST(N'2018-11-26T17:42:14.560' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'23384539-89EB-40C0-8D68-D0EEB9AA260C')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Update                                            ', N'Usuario', N'17', N'dvh', N'234c6dedfe3088c87b2d2be961df2ab5', N'7217093a32f01da8f6c49b63791d5d4d', CAST(N'2018-11-26T17:42:14.693' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'82297C26-9BC6-4D52-97C3-7C600DFC7FAC')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'idUsuario', NULL, N'18', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'nombreUsuario', NULL, N'0fnx/m8Vc7+QURuoZkY7qw==', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'password', NULL, N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'erroresLogin', NULL, N'0', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'esBloqueado', NULL, N'0', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'fechaAlta', NULL, N'Nov 26 2018 11:56PM', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'idIdioma', NULL, N'1', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'idPermiso', NULL, N'0', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'dvh', NULL, N'526682c9bb43074c1d1990527878cb7a', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'esBorrado', NULL, N'0', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'nombre', NULL, N's3dJWgh00FcadZmx/qfteg==', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'apellido', NULL, N'/avj9+G26i+IkrUM4F1VoQ==', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'mail', NULL, N'darce@admin.com', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
INSERT [dbo].[ControlCambios] ([accion], [nombreTabla], [primaryKey], [campo], [valorOriginal], [valorNuevo], [fechaModificacion], [usuarioModificacion], [session]) VALUES (N'Insert                                            ', N'Usuario', N'18', N'fechaNacimiento', NULL, N'Oct 20 1987 12:00AM', CAST(N'2018-11-26T23:56:40.663' AS DateTime), N'MicrosoftAccount\diego87rc@hotmail.com', N'A5DE7E72-87D1-462A-84CC-1EADA5D079B5')
SET IDENTITY_INSERT [dbo].[DVV] ON 

INSERT [dbo].[DVV] ([idDVV], [nombreTabla], [dvv], [fechaCreacion]) VALUES (2, N'Cliente', N'e2f0dd6a963a671c0f42833443054bfa', CAST(N'2018-10-30T03:49:45.460' AS DateTime))
INSERT [dbo].[DVV] ([idDVV], [nombreTabla], [dvv], [fechaCreacion]) VALUES (3, N'Proyecto', N'785c7c5e3877807a6479b3bbc09977bd', CAST(N'2018-10-30T03:49:45.467' AS DateTime))
INSERT [dbo].[DVV] ([idDVV], [nombreTabla], [dvv], [fechaCreacion]) VALUES (4, N'Usuario', N'2b217a338169ee80b1fc080dea06ac2f', CAST(N'2018-10-30T03:49:48.213' AS DateTime))
INSERT [dbo].[DVV] ([idDVV], [nombreTabla], [dvv], [fechaCreacion]) VALUES (5, N'LicenciaSolicitada', N'44bfab0448543d2ea63033dec2ac12e3', CAST(N'2018-10-30T03:49:48.237' AS DateTime))
SET IDENTITY_INSERT [dbo].[DVV] OFF
SET IDENTITY_INSERT [dbo].[Idioma] ON 

INSERT [dbo].[Idioma] ([idIdioma], [descripcion], [codigo], [fechaCreacion]) VALUES (1, N'Español', N'ES', CAST(N'2018-09-18T20:04:25.740' AS DateTime))
INSERT [dbo].[Idioma] ([idIdioma], [descripcion], [codigo], [fechaCreacion]) VALUES (2, N'Ingles', N'EN', CAST(N'2018-09-18T20:04:25.740' AS DateTime))
SET IDENTITY_INSERT [dbo].[Idioma] OFF
SET IDENTITY_INSERT [dbo].[Licencia] ON 

INSERT [dbo].[Licencia] ([idLicencia], [codigo], [descripcion], [fechaCreacion], [esBorrado]) VALUES (1, N'ENF', N'Enfermedad', CAST(N'2018-10-28T17:21:56.807' AS DateTime), 0)
INSERT [dbo].[Licencia] ([idLicencia], [codigo], [descripcion], [fechaCreacion], [esBorrado]) VALUES (3, N'VAC', N'Vacaciones', CAST(N'2018-10-28T17:23:35.450' AS DateTime), 0)
INSERT [dbo].[Licencia] ([idLicencia], [codigo], [descripcion], [fechaCreacion], [esBorrado]) VALUES (4, N'MAT', N'Maternidad', CAST(N'2018-10-30T09:45:29.053' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Licencia] OFF
SET IDENTITY_INSERT [dbo].[LicenciaSolicitada] ON 

INSERT [dbo].[LicenciaSolicitada] ([idLicenciaSolicitada], [idUsuario], [idLicencia], [idAprobador], [fechaDesde], [fechaHasta], [fechaCreacion], [licenciaAprobada], [dvh]) VALUES (1, 7, 1, NULL, CAST(N'2018-10-29T00:45:41.000' AS DateTime), CAST(N'2018-10-31T00:45:41.000' AS DateTime), CAST(N'2018-10-29T01:02:24.700' AS DateTime), 0, N'cccabff5ef2fc2b39937ccecae90de7d')
INSERT [dbo].[LicenciaSolicitada] ([idLicenciaSolicitada], [idUsuario], [idLicencia], [idAprobador], [fechaDesde], [fechaHasta], [fechaCreacion], [licenciaAprobada], [dvh]) VALUES (2, 7, 3, NULL, CAST(N'2018-10-29T01:05:22.000' AS DateTime), CAST(N'2018-11-08T01:05:22.000' AS DateTime), CAST(N'2018-10-29T01:05:34.080' AS DateTime), 0, N'f49f130e822959869b9d8050f7ead554')
INSERT [dbo].[LicenciaSolicitada] ([idLicenciaSolicitada], [idUsuario], [idLicencia], [idAprobador], [fechaDesde], [fechaHasta], [fechaCreacion], [licenciaAprobada], [dvh]) VALUES (3, 7, 3, 7, CAST(N'2018-10-29T01:08:53.000' AS DateTime), CAST(N'2018-10-31T01:08:53.000' AS DateTime), CAST(N'2018-10-29T01:09:15.997' AS DateTime), 0, N'652af5b3730d7bf5660ce1e3d5bb05a5')
INSERT [dbo].[LicenciaSolicitada] ([idLicenciaSolicitada], [idUsuario], [idLicencia], [idAprobador], [fechaDesde], [fechaHasta], [fechaCreacion], [licenciaAprobada], [dvh]) VALUES (4, 7, 3, NULL, CAST(N'2018-10-31T19:32:30.000' AS DateTime), CAST(N'2018-11-03T19:32:30.000' AS DateTime), CAST(N'2018-10-30T19:32:45.480' AS DateTime), 0, N'2409a884a278216636168e3ac799840b')
SET IDENTITY_INSERT [dbo].[LicenciaSolicitada] OFF
SET IDENTITY_INSERT [dbo].[Mensaje] ON 

INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (1, N'labelSeleccionarIdioma')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (2, N'buttonGuardar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (3, N'groupBoxUsuarios')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (4, N'labelNombreEmpleado')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (5, N'labelNumeroEmpleado')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (6, N'labelApellidoEmpleado')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (7, N'labelFechaNacimiento')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (8, N'labelMail')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (9, N'labelCuit')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (10, N'buttonModificar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (11, N'buttonEliminar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (12, N'buttonSalir')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (13, N'labelNombreUsuario')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (14, N'seleccionarToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (15, N'idiomaToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (16, N'usuarioToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (17, N'gestionToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (18, N'cerrarProgramaToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (19, N'labelPassword')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (20, N'buttonRecargar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (21, N'bitacoraToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (22, N'buttonBackUp')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (23, N'buttonSeleccionarCarpeta')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (24, N'labelNombreBackUp')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (25, N'baseDatosToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (26, N'backupToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (27, N'restoreToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (28, N'buttonSeleccionarArhivo')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (29, N'labelSeleccionarUsuario')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (30, N'buttonAgregarPermiso')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (31, N'buttonRemoverPermiso')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (32, N'labelPermisosDeUsuario')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (33, N'labelPermisosDelSistema')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (34, N'permisosToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (35, N'labelTecnologia')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (36, N'labelExperiencia')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (37, N'labelCodigo')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (38, N'labelDescripcion')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (39, N'labelTecnologia')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (40, N'tecnologiaToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (41, N'abmTecnologiasToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (42, N'gestionarTecnologiaConsultorToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (43, N'checkEliminar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (44, N'labelAptitudesUsuario')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (45, N'licenciaToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (46, N'gestionarLicenciasToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (47, N'solicitarLicenciasToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (48, N'labelLicencia')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (49, N'labelFechaDesde')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (50, N'labelFechaHasta')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (51, N'buttonAprobar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (52, N'buttonRechazar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (53, N'labelNombreProyecto')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (54, N'labelDescripcionProyecto')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (55, N'labelPresupuesto')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (56, N'labelProyectos')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (57, N'analizarLicenciasToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (58, N'proyectosToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (59, N'gestionproyectosToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (60, N'labelClientes')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (61, N'labelRazonSocial')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (62, N'labelCuit')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (63, N'labelNombreContacto')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (64, N'labelTelefonoContacto')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (65, N'labelMailContacto')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (66, N'checkActivo')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (67, N'gestionClientesToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (68, N'gestionProyectosToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (69, N'buttonRegenerar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (70, N'labelUsuariosAsignados')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (72, N'labelCostoHora')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (73, N'labelOcupadoHasta')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (74, N'labelOcupadoDesde')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (75, N'labelUsuariosCapacitados')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (76, N'reporteGananciasToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (77, N'asignacionRecursosToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (78, N'labelCantidadHoras')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (79, N'labelDescripcionDelTrabajo')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (80, N'labelFechaTrabajada')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (81, N'cargarHorasToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (82, N'optionSelect')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (83, N'buttonIngresar')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (84, N'labelTipoEntidad')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (85, N'labelEntidad')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (86, N'labelUsuarioOriginal')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (87, N'labelUsuarioModificado')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (88, N'seguridadToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (89, N'controlCambioToolStripMenuItem')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (90, N'groupBoxTecnologias')
INSERT [dbo].[Mensaje] ([idMensaje], [etiqueta]) VALUES (91, N'groupBoxAptitudes')
SET IDENTITY_INSERT [dbo].[Mensaje] OFF
SET IDENTITY_INSERT [dbo].[PatenteFamilia] ON 

INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (1, 1, 2, N'2018-09-25 00:00:00.000')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (2, 1, 3, N'2018-09-25 00:00:00.000')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (3, 1, 4, N'2018-09-25 00:00:00.000')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (4, 5, 6, N'2018-09-25 00:00:00.000')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (6, 1, 7, N'Oct 30 2018 12:20PM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (7, 8, 9, N'Oct 30 2018 12:43PM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (8, 10, 11, N'Oct 30 2018 12:55PM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (9, 12, 13, N'Oct 30 2018 12:55PM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (10, 12, 14, N'Oct 30 2018 12:55PM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (11, 5, 15, N'Oct 30 2018  1:04PM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (15, 16, 1, N'Nov 27 2018  6:12AM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (16, 16, 10, N'Nov 27 2018  6:12AM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (19, 17, 16, N'Nov 27 2018  6:13AM')
INSERT [dbo].[PatenteFamilia] ([idPatenteFamilia], [idPermisoPadre], [idPermisoHijo], [fechaCreacion]) VALUES (21, 18, 17, N'Nov 27 2018  6:18AM')
SET IDENTITY_INSERT [dbo].[PatenteFamilia] OFF
SET IDENTITY_INSERT [dbo].[Permiso] ON 

INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (1, N'ADM001', N'ADIMN', 0, CAST(N'2018-09-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (2, N'ADM002', N'Ver Bitacora', 1, CAST(N'2018-09-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (3, N'ADM003', N'Realizar Backup', 1, CAST(N'2018-09-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (4, N'ADM004', N'Realizar Restore', 1, CAST(N'2018-09-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (5, N'USR001', N'Gestion Usuario', 0, CAST(N'2018-09-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (6, N'USR002', N'Crear Usuario', 1, CAST(N'2018-09-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (7, N'ADM005', N'Regenerar Digito Verificador', 1, CAST(N'2018-10-30T12:06:20.727' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (8, N'P.M001', N'Project Manager', 0, CAST(N'2018-10-30T12:06:21.857' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (9, N'P.M002', N'Gestionar Proyectos y Clientes', 1, CAST(N'2018-10-30T12:06:22.470' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (10, N'T.L001', N'Technical Lead', 0, CAST(N'2018-10-30T12:54:46.143' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (11, N'T.L002', N'Gestionar Tecnologias', 1, CAST(N'2018-10-30T12:54:46.877' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (12, N'H.R001', N'Recursos Humanos', 0, CAST(N'2018-10-30T12:54:46.967' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (13, N'H.R002', N'Cargar Licencias', 1, CAST(N'2018-10-30T12:54:47.050' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (14, N'H.R003', N'Analizar Licencias', 1, CAST(N'2018-10-30T12:54:47.160' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (15, N'USR003', N'Gestionar Permisos', 1, CAST(N'2018-10-30T13:03:45.743' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (16, N'TEST01', N'Prueba', 0, CAST(N'2018-11-27T05:35:58.263' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (17, N'TEST2', N'Prueba 2', 0, CAST(N'2018-11-27T06:12:34.870' AS DateTime))
INSERT [dbo].[Permiso] ([idPermiso], [codigo], [descripcion], [esPermisoBase], [fechaCreacion]) VALUES (18, N'TEST3', N'asd', 0, CAST(N'2018-11-27T06:18:57.230' AS DateTime))
SET IDENTITY_INSERT [dbo].[Permiso] OFF
SET IDENTITY_INSERT [dbo].[Proyecto] ON 

INSERT [dbo].[Proyecto] ([idProyecto], [nombre], [descripcion], [presupuestoInicial], [fechaDesde], [fechaHasta], [fechaCreacion], [dvh], [esBorrado], [idCliente]) VALUES (3, N'Proyecto Prueba', N'Prueba', 1000000, CAST(N'2018-11-01T08:13:39.000' AS DateTime), CAST(N'2019-05-31T08:13:39.000' AS DateTime), CAST(N'2018-10-30T08:13:59.497' AS DateTime), N'70ce5945eaf651ac9ba595c4b07cf44b', 0, 1)
INSERT [dbo].[Proyecto] ([idProyecto], [nombre], [descripcion], [presupuestoInicial], [fechaDesde], [fechaHasta], [fechaCreacion], [dvh], [esBorrado], [idCliente]) VALUES (4, N'SDA', N'EFDS', 7777, CAST(N'2018-12-14T19:25:06.000' AS DateTime), CAST(N'2019-12-19T19:25:06.000' AS DateTime), CAST(N'2018-10-30T19:25:23.167' AS DateTime), N'd14a199a194a01f946a1940283c1a4bb', 0, 1)
SET IDENTITY_INSERT [dbo].[Proyecto] OFF
SET IDENTITY_INSERT [dbo].[RegistroHoras] ON 

INSERT [dbo].[RegistroHoras] ([idRegistroHoras], [idUsuario], [idProyecto], [cantidadHoras], [fecha], [comentarios], [fechaCreacion]) VALUES (1, 7, 3, 8, CAST(N'2018-11-21T12:45:06.000' AS DateTime), N'Esto es una carga de horas', CAST(N'2018-11-20T12:45:26.450' AS DateTime))
INSERT [dbo].[RegistroHoras] ([idRegistroHoras], [idUsuario], [idProyecto], [cantidadHoras], [fecha], [comentarios], [fechaCreacion]) VALUES (2, 7, 4, 10, CAST(N'2019-05-15T21:26:15.000' AS DateTime), N'jhghjhgj', CAST(N'2018-11-20T21:26:28.540' AS DateTime))
INSERT [dbo].[RegistroHoras] ([idRegistroHoras], [idUsuario], [idProyecto], [cantidadHoras], [fecha], [comentarios], [fechaCreacion]) VALUES (3, 7, 4, 10, CAST(N'2019-05-21T21:26:15.000' AS DateTime), N'jhghjhgj', CAST(N'2018-11-20T21:27:19.523' AS DateTime))
INSERT [dbo].[RegistroHoras] ([idRegistroHoras], [idUsuario], [idProyecto], [cantidadHoras], [fecha], [comentarios], [fechaCreacion]) VALUES (4, 18, 3, 8, CAST(N'2018-11-26T23:59:11.000' AS DateTime), N'', CAST(N'2018-11-26T23:59:18.617' AS DateTime))
INSERT [dbo].[RegistroHoras] ([idRegistroHoras], [idUsuario], [idProyecto], [cantidadHoras], [fecha], [comentarios], [fechaCreacion]) VALUES (5, 18, 3, 8, CAST(N'2018-12-01T01:05:32.000' AS DateTime), N'Prueba', CAST(N'2018-11-27T01:06:11.540' AS DateTime))
INSERT [dbo].[RegistroHoras] ([idRegistroHoras], [idUsuario], [idProyecto], [cantidadHoras], [fecha], [comentarios], [fechaCreacion]) VALUES (6, 18, 3, 8, CAST(N'2018-11-26T01:05:32.000' AS DateTime), N'Prueba', CAST(N'2018-11-27T01:07:27.963' AS DateTime))
SET IDENTITY_INSERT [dbo].[RegistroHoras] OFF
SET IDENTITY_INSERT [dbo].[Tecnologia] ON 

INSERT [dbo].[Tecnologia] ([idTecnologia], [codigo], [descripcion], [fechaCreacion], [esBorrado]) VALUES (1, N'js', N'JavaScript', CAST(N'2018-10-26T11:40:15.730' AS DateTime), 0)
INSERT [dbo].[Tecnologia] ([idTecnologia], [codigo], [descripcion], [fechaCreacion], [esBorrado]) VALUES (2, N'vb.net', N'Visual Basic .net', CAST(N'2018-10-26T11:40:45.923' AS DateTime), 0)
INSERT [dbo].[Tecnologia] ([idTecnologia], [codigo], [descripcion], [fechaCreacion], [esBorrado]) VALUES (3, N'angular', N'Angular js', CAST(N'2018-11-26T23:58:09.117' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Tecnologia] OFF
SET IDENTITY_INSERT [dbo].[TipoEvento] ON 

INSERT [dbo].[TipoEvento] ([idTipoEvento], [tipoEvento]) VALUES (1, N'Alta')
INSERT [dbo].[TipoEvento] ([idTipoEvento], [tipoEvento]) VALUES (2, N'Modificacion')
INSERT [dbo].[TipoEvento] ([idTipoEvento], [tipoEvento]) VALUES (3, N'Baja')
INSERT [dbo].[TipoEvento] ([idTipoEvento], [tipoEvento]) VALUES (4, N'Login')
INSERT [dbo].[TipoEvento] ([idTipoEvento], [tipoEvento]) VALUES (5, N'Login Fallido')
INSERT [dbo].[TipoEvento] ([idTipoEvento], [tipoEvento]) VALUES (6, N'Error')
INSERT [dbo].[TipoEvento] ([idTipoEvento], [tipoEvento]) VALUES (7, N'Logout')
SET IDENTITY_INSERT [dbo].[TipoEvento] OFF
SET IDENTITY_INSERT [dbo].[Traduccion] ON 

INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (1, 1, 1, N'Seleccione un idioma')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (2, 1, 2, N'Select a language')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (4, 2, 1, N'Guardar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (5, 2, 2, N'Save')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (7, 3, 1, N'Gestión de usuarios')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (8, 4, 1, N'Nombre')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (9, 5, 1, N'Número de Empleado')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (10, 6, 1, N'Apellido')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (11, 7, 1, N'Fecha de Nacimiento')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (12, 8, 1, N'Mail')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (13, 9, 1, N'CUIT')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (14, 10, 1, N'Modificar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (15, 11, 1, N'Eliminar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (16, 12, 1, N'Salir')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (17, 13, 1, N'Nombre de Usuario')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (18, 3, 2, N'User Management')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (19, 4, 2, N'First name ')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (20, 5, 2, N'Employee number')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (21, 6, 2, N'Last name')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (22, 7, 2, N'Birthdate')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (23, 8, 2, N'Mail')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (24, 9, 2, N'CUIT')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (25, 10, 2, N'Update')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (26, 11, 2, N'Delete')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (27, 12, 2, N'Exit')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (28, 13, 2, N'Username')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (40, 14, 1, N'Seleccionar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (41, 15, 1, N'Idioma')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (42, 16, 1, N'Usuarios')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (43, 17, 1, N'Gestionar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (44, 14, 2, N'Select')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (45, 15, 2, N'Language')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (46, 16, 2, N'Users')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (47, 17, 2, N'Manage')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (52, 18, 1, N'Cerrar Programa')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (53, 18, 2, N'Close Program')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (55, 19, 1, N'Contraseña')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (56, 19, 2, N'Password')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (59, 20, 1, N'Recargar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (60, 20, 2, N'Reload')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (62, 21, 1, N'Bitacora')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (63, 21, 2, N'Logs')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (65, 22, 1, N'Realizar Backup')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (66, 22, 2, N'Generate Backup')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (68, 23, 1, N'Seleccione una carpeta')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (69, 23, 2, N'Pick a destination folder')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (72, 24, 1, N'Nombre del backup')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (73, 24, 2, N'Backup name')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (75, 25, 1, N'Base de datos')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (76, 25, 2, N'Database')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (79, 26, 2, N'backup')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (80, 26, 1, N'Backup')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (81, 27, 1, N'Restaurar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (82, 27, 2, N'Restore')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (84, 28, 1, N'Seleccione archivo')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (85, 28, 2, N'Select a file')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (87, 29, 1, N'Seleccionar Usuario')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (88, 29, 2, N'Select User')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (90, 30, 1, N'<- Agregar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (91, 30, 2, N'<- Add')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (93, 31, 2, N'Remove ->')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (94, 31, 1, N'Remover ->')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (96, 32, 1, N'Permisos del usuario')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (97, 32, 2, N'User Permissions')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (99, 33, 1, N'Permisos del sistema')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (100, 33, 2, N'System permissions')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (102, 34, 1, N'Gestion de Permisos')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (103, 34, 2, N'Access Management')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (105, 35, 1, N'Tecnologia')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (106, 35, 2, N'Technology')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (108, 36, 1, N'Experiencia')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (109, 36, 2, N'Experience')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (110, 37, 1, N'Codigo')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (111, 37, 2, N'Code')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (112, 38, 1, N'Descripcion')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (113, 38, 2, N'Description')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (114, 39, 1, N'Seleccione una tecnologia')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (115, 39, 2, N'Select a technology')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (116, 40, 1, N'Aptitudes')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (117, 40, 2, N'Skills')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (118, 41, 1, N'Gestion Tecnologias')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (119, 41, 2, N'Manage Techologies')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (120, 42, 1, N'Gestion Aptitudes')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (121, 42, 2, N'Manage Skills')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (122, 43, 1, N'Eliminar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (123, 43, 2, N'Delete')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (124, 44, 1, N'Aptitudes del usuario')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (125, 44, 2, N'User skills')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (126, 45, 1, N'Licencias')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (127, 45, 2, N'Licenses')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (128, 46, 1, N'Gestionar licencias')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (129, 46, 2, N'Manage licenses')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (130, 47, 1, N'Solicitar licencia')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (133, 47, 2, N'Apply for a license')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (134, 48, 1, N'Seleccione una licencia')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (135, 48, 2, N'Select a license')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (136, 49, 1, N'Fecha Desde:')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (137, 49, 2, N'Date From:')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (138, 50, 1, N'Fecha Hasta:')
GO
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (139, 50, 2, N'Date To:')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (140, 51, 1, N'Aprobar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (141, 51, 2, N'Approve')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (142, 52, 1, N'Rechazar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (143, 52, 2, N'Reject')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (144, 53, 1, N'Nombre del proyecto')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (145, 53, 2, N'Project Name')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (146, 54, 1, N'Descripción del Proyecto')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (147, 54, 2, N'Project Description')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (148, 55, 1, N'Presupuesto')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (149, 55, 2, N'Budget')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (150, 56, 1, N'Proyectos')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (151, 56, 2, N'Projects')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (152, 57, 1, N'Analizar Licencias')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (153, 57, 2, N'Manage Licenses')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (154, 58, 1, N'Clientes y Proyectos')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (155, 58, 2, N'Clients and Projects')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (158, 59, 1, N'Gestionar Proyectos')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (159, 59, 2, N'Manage Projects')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (160, 60, 1, N'Clientes')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (161, 60, 2, N'Clients')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (162, 61, 1, N'Razon Social')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (163, 61, 2, N'Business Name')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (164, 62, 1, N'CUIT')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (165, 62, 2, N'CUIT')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (166, 63, 1, N'Nombre de Contacto')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (167, 63, 2, N'Contact Name')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (168, 64, 1, N'Telefono de Contacto')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (169, 64, 2, N'Contact Phone')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (170, 65, 1, N'Mail de Contacto')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (171, 65, 2, N'Contact Mail')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (172, 66, 1, N'Esta activo')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (173, 66, 2, N'Is Active')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (174, 67, 1, N'Gestionar Clientes')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (175, 67, 2, N'Manage Clients')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (176, 68, 1, N'Gestionar Proyectos')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (177, 68, 2, N'Manage Projects')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (178, 69, 1, N'Regenerar Integridad')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (179, 69, 2, N'Restore Integrity')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (180, 70, 1, N'Usuarios Asignados')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (181, 70, 2, N'Assigned Users')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (186, 72, 1, N'Costo por hora')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (187, 72, 2, N'Hour price')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (188, 73, 1, N'Ocupado Hasta')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (189, 73, 2, N'Busy until')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (190, 74, 1, N'Tiempo ya asignado')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (191, 74, 2, N'Busy time')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (192, 75, 1, N'Usuarios Capacitados')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (193, 75, 2, N'Users')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (194, 76, 1, N'Reporte de Ganancias')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (195, 76, 2, N'Profit Report')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (196, 77, 1, N'Asignacion de Recursos')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (197, 77, 2, N'Resource Management')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (198, 78, 1, N'Cantidad de horas')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (199, 78, 2, N'Worked hours')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (200, 79, 1, N'Comentarios')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (201, 79, 2, N'Comments')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (203, 80, 1, N'Fecha de registro')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (204, 80, 2, N'Worked day')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (205, 81, 1, N'Cargar horas')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (206, 81, 2, N'Timesheet')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (207, 82, 1, N'--Seleccionar--')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (208, 82, 2, N'--Select--')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (209, 83, 1, N'Ingresar')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (210, 83, 2, N'Login')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (211, 84, 1, N'Seleccione tipo de entidad')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (212, 84, 2, N'Select entity type')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (213, 85, 1, N'Seleccione entidad')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (214, 85, 2, N'Select entity')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (215, 86, 1, N'Usuario original')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (216, 86, 2, N'Original user')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (217, 87, 1, N'Usuario recuperado')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (218, 87, 2, N'Modified user')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (219, 88, 1, N'Seguridad')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (220, 88, 2, N'Security')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (221, 89, 1, N'Control Cambio')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (222, 89, 2, N'Control Change')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (223, 90, 1, N'Agregar Tecnología')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (224, 90, 2, N'Add Technology')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (225, 91, 1, N'Administrar Aptitudes')
INSERT [dbo].[Traduccion] ([idTraduccion], [idMensaje], [idIdioma], [texto]) VALUES (226, 91, 2, N'Manage Skills')
SET IDENTITY_INSERT [dbo].[Traduccion] OFF
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (2, N'iJEscpXPR+bR+WoYez3BMQ==', N'7815696ecbf1c96e6894b779456d330e', 1, 0, CAST(N'2018-09-23T00:30:12.763' AS DateTime), 1, 0, N'', 1, N'iJEscpXPR+bR+WoYez3BMQ==', N'iJEscpXPR+bR+WoYez3BMQ==', N'ASD@AS.COM', CAST(N'2018-09-25T05:45:55.053' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (3, N'krXCD+qr4SvmiOy9MrEAOQ==', N'92c4cf9df4c01b73f7e757e78ae60841', 0, 0, CAST(N'2018-09-25T05:56:25.677' AS DateTime), 1, 0, N'', 1, N'iJEscpXPR+bR+WoYez3BMQ==', N'y+iaIJKwb+uDMhJ/GT9bSw==', N'asd@aw.com', CAST(N'2018-09-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (6, N'ayLPLQ0Z3rHqbb6X4iNrow==', N'73acd9a5972130b75066c82595a1fae3', 0, 0, CAST(N'2018-09-25T06:02:59.100' AS DateTime), 1, 0, N'9cc3808b788d9577d760186155ab0742', 1, N'ayLPLQ0Z3rHqbb6X4iNrow==', N'slC8u8+/UZs5kH6KfABvnA==', N'ADMIN', CAST(N'2018-09-25T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (7, N'iJEscpXPR+bR+WoYez3BMQ==', N'81dc9bdb52d04dc20036dbd8313ed055', 0, 0, CAST(N'2018-09-25T08:12:56.820' AS DateTime), 1, 0, N'96bc7e8ec1c68e40160aecf69850ed50', 0, N'iJEscpXPR+bR+WoYez3BMQ==', N'y+iaIJKwb+uDMhJ/GT9bSw==', N'asdqwe@as.com', CAST(N'2018-11-26T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (8, N'Vg3iO7E9kgsm1g++fPvjMA==', N'21b7eb30013b04776f5b06bc59209391', 0, 0, CAST(N'2018-10-30T12:05:39.883' AS DateTime), 1, 0, N'4eaf35e8cae084655163c9d74414a612', 0, N'Vg3iO7E9kgsm1g++fPvjMA==', N'Vg3iO7E9kgsm1g++fPvjMA==', N'PM@PM.COM', CAST(N'2018-10-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (9, N'RHPdpbRRWW+iaj+hkYWwug==', N'c4534c00ebdb14023f4f538811df5209', 0, 0, CAST(N'2018-10-30T12:58:51.933' AS DateTime), 1, 0, N'01c8edc690760873e2aa0388eb35fb6f', 0, N'RHPdpbRRWW+iaj+hkYWwug==', N'RHPdpbRRWW+iaj+hkYWwug==', N'TL@TL.COM', CAST(N'2018-10-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (10, N'dTj7+Yr+6k3TsbxmiWNyNQ==', N'fd4c638da5f85d025963f99fe90b1b1a', 0, 0, CAST(N'2018-10-30T12:59:07.343' AS DateTime), 1, 0, N'373f1eedb75b4aa78b00ef8e1f77d7aa', 0, N'dTj7+Yr+6k3TsbxmiWNyNQ==', N'dTj7+Yr+6k3TsbxmiWNyNQ==', N'HR@HR.COM', CAST(N'2018-10-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (11, N'8CtBMK13P34AuSPGiiZxvg==', N'002386b0bb52105a88549477863daeb9', 0, 0, CAST(N'2018-10-30T13:00:23.670' AS DateTime), 1, 0, N'a400ca9a84918bb05434f499149a87ac', 0, N'iQfQCu0ZqDSfklaqvayQOw==', N'6l12j8FuR6jUzLKizb2nSQ==', N'DEV 1', CAST(N'2018-11-24T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (12, N'DO/QIo4ZR4JFlHs67m47PQ==', N'ea49569292296a6d12656341e04a0fb4', 0, 0, CAST(N'2018-10-30T13:00:41.127' AS DateTime), 1, 0, N'814ffda4e53a2880b49d8218c01e1a44', 0, N'DO/QIo4ZR4JFlHs67m47PQ==', N'DO/QIo4ZR4JFlHs67m47PQ==', N'DEV2', CAST(N'2018-10-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (13, N'Vt5d7SwShNa8uV/D9jm67w==', N'639034b49d7c4380208720c8496b0450', 0, 0, CAST(N'2018-10-30T13:00:55.977' AS DateTime), 1, 0, N'e05123f12101e8079f45bb7ca78e8b03', 0, N'Vt5d7SwShNa8uV/D9jm67w==', N'Vt5d7SwShNa8uV/D9jm67w==', N'DEV3', CAST(N'2018-10-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (14, N'YiU1AO6xan8V0o8gFh9zXQ==', N'd53034933c8c073f774f426fb2dfacc2', 0, 0, CAST(N'2018-10-30T13:01:06.503' AS DateTime), 1, 0, N'e7035fd3e91a49485c4e7856c9dc6ee6', 0, N'YiU1AO6xan8V0o8gFh9zXQ==', N'YiU1AO6xan8V0o8gFh9zXQ==', N'DEV4', CAST(N'2018-10-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (15, N'AwxHnuocMnjXM7afUXbelA==', N'b68ca1fda6ffed360edd9edfce7eeaaf', 0, 0, CAST(N'2018-10-30T13:01:16.600' AS DateTime), 1, 0, N'944eb63343fddfd05bb79ce743a2f39b', 0, N'AwxHnuocMnjXM7afUXbelA==', N'AwxHnuocMnjXM7afUXbelA==', N'DEV5', CAST(N'2018-07-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (16, N'iJEscpXPR+bR+WoYez3BMQ==', N'14e1b600b1fd579f47433b88e8d85291', 0, 0, CAST(N'2018-11-26T15:52:57.413' AS DateTime), 1, 0, N'ccdeaf2f7097d52c0c7f883661f0237f', 1, N'Z33YPk8cNrwllG5Bs3VJfw==', N'y+iaIJKwb+uDMhJ/GT9bSw==', N'asdqwe@as.com', CAST(N'2018-11-26T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (17, N'joB6DOaFDMWe401t1/N6rw==', N'f4cc399f0effd13c888e310ea2cf5399', 0, 0, CAST(N'2018-11-26T15:53:28.153' AS DateTime), 1, 0, N'd713420f4128be6062f0271e6f5a459c', 0, N'Z33YPk8cNrwllG5Bs3VJfw==', N'y+iaIJKwb+uDMhJ/GT9bSw==', N'asdqwe@aasds.com', CAST(N'2018-11-26T00:00:00.000' AS DateTime))
INSERT [dbo].[Usuario] ([idUsuario], [nombreUsuario], [password], [erroresLogin], [esBloqueado], [fechaAlta], [idIdioma], [idPermiso], [dvh], [esBorrado], [nombre], [apellido], [mail], [fechaNacimiento]) VALUES (18, N'0fnx/m8Vc7+QURuoZkY7qw==', N'e10adc3949ba59abbe56e057f20f883e', 0, 0, CAST(N'2018-11-26T23:56:40.410' AS DateTime), 1, 0, N'526682c9bb43074c1d1990527878cb7a', 0, N's3dJWgh00FcadZmx/qfteg==', N'/avj9+G26i+IkrUM4F1VoQ==', N'darce@admin.com', CAST(N'1987-10-20T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Usuario] OFF
SET IDENTITY_INSERT [dbo].[UsuarioPatente] ON 

INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (9, 6, 5)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (11, 8, 8)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (12, 7, 1)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (13, 7, 5)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (14, 7, 8)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (15, 7, 10)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (16, 7, 12)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (17, 10, 12)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (18, 10, 5)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (20, 18, 1)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (21, 18, 5)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (22, 18, 8)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (23, 18, 10)
INSERT [dbo].[UsuarioPatente] ([idUsuarioPatente], [idUsuario], [idPatenteFamilia]) VALUES (24, 18, 12)
SET IDENTITY_INSERT [dbo].[UsuarioPatente] OFF
ALTER TABLE [dbo].[AptitudUsuario]  WITH CHECK ADD  CONSTRAINT [FK_AptitudUsuario_Tecnologia] FOREIGN KEY([idTecnologia])
REFERENCES [dbo].[Tecnologia] ([idTecnologia])
GO
ALTER TABLE [dbo].[AptitudUsuario] CHECK CONSTRAINT [FK_AptitudUsuario_Tecnologia]
GO
ALTER TABLE [dbo].[AptitudUsuario]  WITH CHECK ADD  CONSTRAINT [FK_AptitudUsuario_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[AptitudUsuario] CHECK CONSTRAINT [FK_AptitudUsuario_Usuario]
GO
ALTER TABLE [dbo].[AsignacionUsuario]  WITH CHECK ADD  CONSTRAINT [FK_AsignacionUsuario_Proyecto] FOREIGN KEY([idProyecto])
REFERENCES [dbo].[Proyecto] ([idProyecto])
GO
ALTER TABLE [dbo].[AsignacionUsuario] CHECK CONSTRAINT [FK_AsignacionUsuario_Proyecto]
GO
ALTER TABLE [dbo].[AsignacionUsuario]  WITH CHECK ADD  CONSTRAINT [FK_AsignacionUsuario_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[AsignacionUsuario] CHECK CONSTRAINT [FK_AsignacionUsuario_Usuario]
GO
ALTER TABLE [dbo].[Bitacora]  WITH CHECK ADD  CONSTRAINT [FK_Bitacora_TipoEvento] FOREIGN KEY([idTipoEvento])
REFERENCES [dbo].[TipoEvento] ([idTipoEvento])
GO
ALTER TABLE [dbo].[Bitacora] CHECK CONSTRAINT [FK_Bitacora_TipoEvento]
GO
ALTER TABLE [dbo].[Bitacora]  WITH CHECK ADD  CONSTRAINT [FK_Bitacora_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[Bitacora] CHECK CONSTRAINT [FK_Bitacora_Usuario]
GO
ALTER TABLE [dbo].[LicenciaSolicitada]  WITH CHECK ADD  CONSTRAINT [FK_LicenciaSolicitada_Licencia] FOREIGN KEY([idLicencia])
REFERENCES [dbo].[Licencia] ([idLicencia])
GO
ALTER TABLE [dbo].[LicenciaSolicitada] CHECK CONSTRAINT [FK_LicenciaSolicitada_Licencia]
GO
ALTER TABLE [dbo].[LicenciaSolicitada]  WITH CHECK ADD  CONSTRAINT [FK_LicenciaSolicitada_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[LicenciaSolicitada] CHECK CONSTRAINT [FK_LicenciaSolicitada_Usuario]
GO
ALTER TABLE [dbo].[LicenciaSolicitada]  WITH CHECK ADD  CONSTRAINT [FK_LicenciaSolicitada_Usuario1] FOREIGN KEY([idAprobador])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[LicenciaSolicitada] CHECK CONSTRAINT [FK_LicenciaSolicitada_Usuario1]
GO
ALTER TABLE [dbo].[Proyecto]  WITH CHECK ADD  CONSTRAINT [FK_Proyecto_Cliente] FOREIGN KEY([idCliente])
REFERENCES [dbo].[Cliente] ([idCliente])
GO
ALTER TABLE [dbo].[Proyecto] CHECK CONSTRAINT [FK_Proyecto_Cliente]
GO
ALTER TABLE [dbo].[Traduccion]  WITH CHECK ADD  CONSTRAINT [FK_Traduccion_Idioma] FOREIGN KEY([idIdioma])
REFERENCES [dbo].[Idioma] ([idIdioma])
GO
ALTER TABLE [dbo].[Traduccion] CHECK CONSTRAINT [FK_Traduccion_Idioma]
GO
ALTER TABLE [dbo].[Traduccion]  WITH CHECK ADD  CONSTRAINT [FK_Traduccion_Mensaje] FOREIGN KEY([idMensaje])
REFERENCES [dbo].[Mensaje] ([idMensaje])
GO
ALTER TABLE [dbo].[Traduccion] CHECK CONSTRAINT [FK_Traduccion_Mensaje]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Idioma] FOREIGN KEY([idIdioma])
REFERENCES [dbo].[Idioma] ([idIdioma])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Idioma]
GO
ALTER TABLE [dbo].[UsuarioPatente]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioPatente_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[UsuarioPatente] CHECK CONSTRAINT [FK_UsuarioPatente_Usuario]
GO
/****** Object:  StoredProcedure [dbo].[ActualizarAptitudPorUsuarioTecnologia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarAptitudPorUsuarioTecnologia]
	-- Add the parameters for the stored procedure here
	@idTecnologia int,
	@idUsuario int,
	@experiencia int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE 
		AptitudUsuario 
	SET
		experiencia = @experiencia
	WHERE idTecnologia = @idTecnologia AND idUsuario = @idUsuario

	SELECT 1
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarAsignacionUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarAsignacionUsuario]
	-- Add the parameters for the stored procedure here
	@idAsignacionUsuario int,
	@idUsuario int = null,
	@idProyecto int = null, 
	@dvh varchar(50) = null, 
	@fechaDesde Datetime = null,
	@fechaHasta Datetime = null,
	@esBorrado Bit = null,
	@costoHora int = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE
		AsignacionUsuario
	SET
		idUsuario = ISNULL(@idUsuario, idUsuario),
		idProyecto = ISNULL(@idProyecto, idProyecto),
		dvh = ISNULL(@dvh, dvh),
		fechaDesde = ISNULL(@fechaDesde, fechaDesde),
		fechaHasta = ISNULL(@fechaHasta, fechaHasta),
		esBorrado = ISNULL(@esBorrado, esBorrado),
		costoHora = ISNULL(@costoHora, costoHora)
	WHERE
		idAsignacionUsuario = @idAsignacionUsuario

	SELECT @idAsignacionUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarCliente]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarCliente]
	-- Add the parameters for the stored procedure here
	@idCliente int,
	@activo varchar(50)= null, 
	@descripcion varchar(50)= null,
	@dvh varchar(50)= null,
	@cuit varchar(50)= null,
	@mailContacto varchar(50)= null,
	@nombreContacto varchar(50)= null,
	@razonSocial varchar(50)= null,
	@telefonoContacto varchar(50)= null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE
		Cliente
	SET
		activo = ISNULL(@activo, activo),
		descripcion = ISNULL(@descripcion, descripcion),
		dvh = ISNULL(@dvh, dvh),
		cuit = ISNULL(@cuit, cuit),
		mailContacto = ISNULL(@mailContacto, mailContacto),
		nombreContacto = ISNULL(@nombreContacto, nombreContacto),
		razonSocial = ISNULL(@razonSocial, razonSocial),
		telefonoContacto = ISNULL(@telefonoContacto, telefonoContacto)
	WHERE
		idCliente = @idCliente

	SELECT @idCliente
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarDvvPorTabla]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarDvvPorTabla]
	-- Add the parameters for the stored procedure here
	@nombreTabla varchar(50),
	@nombreColumn varchar(50),
	@valor varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT 1 FROM DVV WHERE TABLA = @nombreTabla AND columna = @nombreColumn)
		BEGIN
			UPDATE DVV SET Valor = @valor WHERE  tabla = @nombreTabla and Columna = @nombreColumn
		END
	ELSE
		BEGIN
			INSERT DVV VALUES (@nombreColumn, @valor, @nombreTabla)
		END
	END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarIdiomaPorUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarIdiomaPorUsuario]
	-- Add the parameters for the stored procedure here 
	@idIdioma int,
	@idUsuario int
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE Usuario SET idIdioma = @idIdioma WHERE idUsuario = @idUsuario

END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarLicencia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarLicencia]
	-- Add the parameters for the stored procedure here
	@idLicencia int,
	@codigo varchar(50), 
	@descripcion varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE Licencia SET codigo = @codigo, descripcion = @descripcion WHERE idLicencia = @idLicencia

	SELECT @idLicencia
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarProyecto]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarProyecto]
	-- Add the parameters for the stored procedure here
	@idProyecto int,
	@nombre varchar(50) = null, 
	@descripcion varchar(max) = null,
	@presupuestoInicial int = null,
	@fechaDesde Datetime = null,
	@fechaHasta Datetime = null,
	@dvh varchar(50) = null,
	@esBorrado bit = null,
	@idCliente int = null
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE
		Proyecto
	SET
		nombre = ISNULL(@nombre, nombre),
		descripcion = ISNULL(@descripcion, descripcion),
		dvh = ISNULL(@dvh, dvh),
		fechaDesde = ISNULL(@fechaDesde, fechaDesde),
		fechaHasta = ISNULL(@fechaHasta, fechaHasta),
		presupuestoInicial = ISNULL(@presupuestoInicial, presupuestoInicial),
		esBorrado = ISNULL(@esborrado, esborrado),
		idCliente = ISNULL(@idCliente, idCliente)
	WHERE
		idProyecto = @idProyecto

	SELECT @idProyecto
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarSolicitudPorUsuarioLicencia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarSolicitudPorUsuarioLicencia]
	-- Add the parameters for the stored procedure here
	@idUsuario varchar(50),
	@idLicencia varchar(50),
	@idAprobador varchar(50) = null,
	@fechaDesde Datetime,
	@fechaHasta Datetime,
	@licenciaAprobada varchar(50),
	@dvh varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE 
		LicenciaSolicitada 
	SET
		licenciaAprobada = @licenciaAprobada,
		dvh = @dvh,
		idAprobador = @idAprobador
	WHERE 
		idUsuario = @idUsuario
		AND idLicencia = @idLicencia
		AND fechaDesde = @fechaDesde
		AND fechaHasta = @fechaHasta

	SELECT 1
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarTecnologia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarTecnologia]
	-- Add the parameters for the stored procedure here
	@idTecnologia int,
	@codigo varchar(50), 
	@descripcion varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE Tecnologia SET codigo = @codigo, descripcion = @descripcion WHERE idTecnologia = @idTecnologia

	SELECT @idTecnologia
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ActualizarUsuario]
	-- Add the parameters for the stored procedure here
	@idUsuario int,
	@nombreUsuario varchar(50), 
	@pass varchar(50), 
	@dvh varchar(50), 
	@nombre varchar(50),	
	@apellido varchar(50), 
	@mail varchar(50), 
	@fechaNacimiento DateTime  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE USUARIO SET 
		nombre = @nombre, 
		nombreUsuario = @nombreUsuario, 
		apellido = @apellido, 
		[password] = @pass, 
		dvh = @dvh, 
		fechaNacimiento = @fechaNacimiento,
		mail = @mail
		where idUsuario = @idUsuario
	SELECT IDUSUARIO FROM Usuario WHERE nombreUsuario = @nombreUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[AsignarAptitud]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AsignarAptitud]
	-- Add the parameters for the stored procedure here
	@idUsuario varchar(50), 
	@idTecnologia varchar(50),
	@experiencia varchar(50)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO AptitudUsuario Values (@idUsuario, @idTecnologia, @experiencia, GETDATE())

	SELECT MAX(idAptitudUsuario) FROM AptitudUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[AsignarUsuarioLicencia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AsignarUsuarioLicencia]
	-- Add the parameters for the stored procedure here
	@idUsuario varchar(50), 
	@idLicencia varchar(50),
	@fechaDesde dateTime,
	@fechaHasta dateTime,
	@dvh varchar(50)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO LicenciaSolicitada Values (@idUsuario, @idLicencia, null, @fechaDesde, @fechaHasta, GetDate(), 0, @dvh)

	SELECT MAX(idLicenciaSolicitada) FROM LicenciaSolicitada
END
GO
/****** Object:  StoredProcedure [dbo].[CrearAsignacionUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CrearAsignacionUsuario]
	-- Add the parameters for the stored procedure here
	@idUsuario int, 
	@idProyecto int, 
	@dvh varchar(50), 
	@fechaDesde Datetime,	
	@fechaHasta Datetime,	
	@costoHora int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO AsignacionUsuario Values (@idUsuario, @idProyecto, @fechaDesde, @fechaHasta, @costoHora, GETDATE(), @dvh, 0)

	SELECT max(IdAsignacionUsuario) FROM AsignacionUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[CrearCliente]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CrearCliente]
	-- Add the parameters for the stored procedure here
	@activo varchar(50), 
	@descripcion varchar(50),
	@dvh varchar(50),
	@cuit varchar(50),
	@mailContacto varchar(50),
	@nombreContacto varchar(50),
	@razonSocial varchar(50),
	@telefonoContacto varchar(50)

	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO Cliente Values (@descripcion, @cuit, @mailContacto, @nombreContacto, @telefonoContacto, @razonSocial, @activo, GETDATE(), @dvh)

	SELECT max(idCliente) FROM Cliente
END
GO
/****** Object:  StoredProcedure [dbo].[CrearLicencia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CrearLicencia]
	-- Add the parameters for the stored procedure here
	@codigo varchar(50), 
	@descripcion varchar(50)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO Licencia Values (@codigo, @descripcion, GETDATE(),0)

	SELECT idLicencia FROM Licencia WHERE codigo = @codigo
END
GO
/****** Object:  StoredProcedure [dbo].[CrearPermiso]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CrearPermiso]
	-- Add the parameters for the stored procedure here
	@codigo varchar(50), 
	@descripcion varchar(50)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO Permiso VALUES (@codigo, @descripcion, 0, GetDate())

	SELECT max(idPermiso) from Permiso
END
GO
/****** Object:  StoredProcedure [dbo].[CrearProyecto]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CrearProyecto]
	-- Add the parameters for the stored procedure here
	@nombre varchar(50), 
	@descripcion varchar(max) = null,
	@presupuestoInicial int,
	@fechaDesde Datetime,
	@fechaHasta Datetime,
	@dvh varchar(50) = null,
	@idCliente int 
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO Proyecto Values (@nombre, @descripcion, @presupuestoInicial, @fechaDesde, @fechaHasta, GetDate(), @dvh, 0, @idCliente)

	SELECT max(idProyecto) FROM Proyecto
END
GO
/****** Object:  StoredProcedure [dbo].[CrearRegistroHoras]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CrearRegistroHoras]
	-- Add the parameters for the stored procedure here
	@idUsuario varchar(50), 
	@idProyecto varchar(50),
	@cantidadHoras varchar(50),
	@fecha datetime,
	@comentarios varchar(max)

	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO RegistroHoras Values (@idUsuario, @idProyecto, @cantidadHoras, @fecha, @comentarios, GETDATE())
END
GO
/****** Object:  StoredProcedure [dbo].[CrearTecnologia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CrearTecnologia]
	-- Add the parameters for the stored procedure here
	@codigo varchar(50), 
	@descripcion varchar(50)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO Tecnologia Values (@codigo, @descripcion, GETDATE(),0)

	SELECT idTecnologia FROM Tecnologia WHERE codigo = @codigo
END
GO
/****** Object:  StoredProcedure [dbo].[CrearUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CrearUsuario]
	-- Add the parameters for the stored procedure here
	@nombreUsuario varchar(50), 
	@pass varchar(50), 
	@dvh varchar(50), 
	@nombre varchar(50),	
	@apellido varchar(50), 
	@mail varchar(50), 
	@fechaNacimiento DateTime  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	INSERT INTO Usuario Values (@nombreUsuario, @pass, 0, 0, GETDATE(), 1, 0, @dvh, 0, @nombre, @apellido, @mail, @fechaNacimiento)

	SELECT IDUSUARIO FROM Usuario WHERE nombreUsuario = @nombreUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarAptitudPorUsuarioTecnologia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EliminarAptitudPorUsuarioTecnologia]
	-- Add the parameters for the stored procedure here
	@idTecnologia int,
	@idUsuario int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	DELETE FROM AptitudUsuario WHERE idTecnologia = @idTecnologia AND idUsuario = @idUsuario

	SELECT 1
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarLicenciaPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EliminarLicenciaPorId]
	-- Add the parameters for the stored procedure here
	@idLicencia int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE Licencia SET esBorrado = 1 WHERE idLicencia = @idLicencia

	SELECT 1 FROM Licencia WHERE idLicencia = @idLicencia AND esBorrado = 1
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarProyectoPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EliminarProyectoPorId]
	-- Add the parameters for the stored procedure here
	@idProyecto int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE Proyecto SET esBorrado = 1 WHERE idProyecto = @idProyecto

	SELECT 1 FROM Proyecto WHERE idProyecto = @idProyecto AND esBorrado = 1
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarTecnologiaPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EliminarTecnologiaPorId]
	-- Add the parameters for the stored procedure here
	@idTecnologia int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE Tecnologia SET esBorrado = 1 WHERE idTecnologia = @idTecnologia

	SELECT 1 FROM Tecnologia WHERE idTecnologia = @idTecnologia AND esBorrado = 1
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarUsuarioPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EliminarUsuarioPorId]
	-- Add the parameters for the stored procedure here
	@idUsuario int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	UPDATE Usuario SET esBorrado = 1 WHERE idUsuario = @idUsuario

	SELECT 1 FROM Usuario WHERE idUsuario = @idUsuario AND esBorrado = 1
END
GO
/****** Object:  StoredProcedure [dbo].[GenerarBackup]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GenerarBackup] (
@backupPath VARCHAR(256) -- specify database backup file directory
)
AS BEGIN

DECLARE @dbName VARCHAR(50) -- database name
DECLARE @fileName VARCHAR(256) -- filename for backup
DECLARE @fileDate VARCHAR(20) -- used for file name
DECLARE @fileHour VARCHAR(2) -- used for file name
DECLARE @fileMin VARCHAR(2) -- used for file name
DECLARE @fileSec VARCHAR(2) -- used for file name
-- specify filename format
SELECT @fileDate = CONVERT(VARCHAR(20),GETDATE(),112)
SELECT @fileHour = DATEPART(HOUR,GETDATE())
Select @fileMin = DATEPART(MINUTE,GETDATE())
SELECT @fileSec = DATEPART(SECOND,GETDATE())
DECLARE db_cursor CURSOR FOR
SELECT name
FROM master.dbo.sysdatabases
WHERE name = 'DAC' 

OPEN db_cursor
FETCH NEXT FROM db_cursor INTO @dbName

WHILE @@FETCH_STATUS = 0
BEGIN
SET @fileName = @backupPath + @dbName + '_' + @fileDate + '-' + @fileHour + '.' + @fileMin + '.' + @fileSec + '.BAK'
BACKUP DATABASE @dbName TO DISK = @fileName

FETCH NEXT FROM db_cursor INTO @dbName
END
CLOSE db_cursor
DEALLOCATE db_cursor

END
GO
/****** Object:  StoredProcedure [dbo].[GuardarDVV]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GuardarDVV]
	-- Add the parameters for the stored procedure here
	@nombreTabla varchar(50), 
	@dvv varchar(50)

	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	if exists(SELECT 1 FROM DVV WHERE nombreTabla = @nombreTabla)
		UPDATE DVV SET dvv = @dvv WHERE nombreTabla = @nombreTabla
	ELSE
		INSERT INTO DVV VALUES (@nombreTabla, @dvv, GETDATE())
	
END
GO
/****** Object:  StoredProcedure [dbo].[GuardarEnBitacora]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GuardarEnBitacora]
	-- Add the parameters for the stored procedure here
	@idUsuario int, 
	@criticidad int,
	@mensaje varchar(200),
	@ip varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@idUsuario = -1)
	begin
		set @idUsuario = null
	end

    -- Insert statements for procedure here
	INSERT INTO BITACORA VALUES (@mensaje, GETDATE(), @criticidad, @idUsuario, @ip)
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerAptitudesPorIdUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerAptitudesPorIdUsuario]
	-- Add the parameters for the stored procedure here
	@idUsuario varchar(50)

	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	SELECT 
		AU.idUsuario,
		AU.idTecnologia,
		T.codigo,
		T.descripcion,
		T.fechaCreacion,
		AU.experiencia
	FROM
		AptitudUsuario AU
	INNER JOIN
		Tecnologia T ON T.idTecnologia = AU.idTecnologia
	WHERE
		AU.idUsuario = @idUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerAsignacionUsuarioProyectoFecha]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerAsignacionUsuarioProyectoFecha]
	-- Add the parameters for the stored procedure here
	@idUsuario int,
	@idProyecto int,
	@fechaDesde datetime,
	@fechaHasta datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM AsignacionUsuario
	WHERE 
		idUsuario = @idUsuario
		AND idProyecto = @idProyecto
		AND fechaDesde = @fechaDesde	
		AND fechaHasta = @fechaHasta
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerAsignacionUsuarios]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerAsignacionUsuarios]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AU.* FROM AsignacionUsuario AU
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerAsignacionUsuariosPorCliente]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerAsignacionUsuariosPorCliente]
	-- Add the parameters for the stored procedure here
	@idCliente int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AU.* FROM AsignacionUsuario AU
	INNER JOIN Proyecto P
	ON AU.idProyecto = AU.idProyecto
	WHERE P.idCliente = @idCliente
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerAsignacionUsuariosPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerAsignacionUsuariosPorId]
	-- Add the parameters for the stored procedure here
	@idAsignacionUsuario int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AU.* FROM AsignacionUsuario AU WHERE idAsignacionUsuario = @idAsignacionUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerAsignacionUsuariosPorProyecto]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerAsignacionUsuariosPorProyecto]
	-- Add the parameters for the stored procedure here
	@idProyecto int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AU.* FROM AsignacionUsuario AU WHERE idProyecto = @idProyecto
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerAsignacionUsuariosPorUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerAsignacionUsuariosPorUsuario]
	-- Add the parameters for the stored procedure here
	@idUsuario int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AU.* FROM AsignacionUsuario AU WHERE idUsuario = @idUsuario and esBorrado = 0
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerBackups]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerBackups]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Backups
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerBitacora]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerBitacora]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT B.mensaje, B.fechaEvento, T.tipoEvento, U.nombreUsuario, U.nombre, U.apellido, B.ip FROM Bitacora B 
	LEFT JOIN Usuario U ON U.idUsuario = B.idUsuario
	INNER JOIN TipoEvento T ON T.idTipoEvento = B.idTipoEvento
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerClientePorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerClientePorId]
	-- Add the parameters for the stored procedure here
	@idCliente int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Cliente WHERE idCliente = @idCliente
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerClientes]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerClientes]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Cliente
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerDvvPorTabla]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerDvvPorTabla]
	-- Add the parameters for the stored procedure here
	@nombreTabla varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT columna, VALOR FROM DVV WHERE TABLA = @nombreTabla
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerHorasPorProyecto]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerHorasPorProyecto]
	-- Add the parameters for the stored procedure here
	@idProyecto int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AU.* FROM RegistroHoras AU WHERE idProyecto = @idProyecto
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerIdiomaPorUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerIdiomaPorUsuario]
	@idUsuario varchar(50)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select I.* from Idioma I
	INNER JOIN Usuario U
	ON U.idIdioma = I.idIdioma
	WHERE U.idUsuario = @idUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerLicenciaPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerLicenciaPorId]
	-- Add the parameters for the stored procedure here
	@idLicencia int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Licencia WHERE idLicencia = @idLicencia
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerLicencias]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerLicencias]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Licencia where esBorrado = 0
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerLicenciaSolicitadaPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerLicenciaSolicitadaPorId]
	-- Add the parameters for the stored procedure here
	@idLicenciaSolicitada INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM LicenciaSolicitada WHERE idLicenciaSolicitada = @idLicenciaSolicitada
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerLicenciasPendientes]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerLicenciasPendientes]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		LS.idUsuario,
		U.nombre,
		U.apellido,
		L.descripcion,
		LS.fechaDesde,
		LS.fechaHasta,
		LS.licenciaAprobada,
		LS.idLicenciaSolicitada
	FROM LicenciaSolicitada LS
	INNER JOIN Usuario U
		ON U.idUsuario = LS.idUsuario
	INNER JOIN Licencia L
		ON L.idLicencia = LS.idLicencia
	WHERE LS.fechaHasta >= GETDATE()
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerLicenciasPendientesPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerLicenciasPendientesPorId]
	-- Add the parameters for the stored procedure here
	@idLicenciaPendiente INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM LICENCIAPENDIENTE WHERE IDLICENCIAPENDIENTE = @idLicenciaPendiente
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerLicenciasSolicitadas]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerLicenciasSolicitadas]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM LicenciaSolicitada
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerMensajesPorIdioma]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerMensajesPorIdioma]
	-- Add the parameters for the stored procedure here 
	@idIdioma int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT M.idMensaje, M.etiqueta, T.texto FROM Traduccion T
	INNER JOIN MENSAJE M ON M.idMensaje = T.idMensaje
	WHERE T.idIdioma = @idIdioma

END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerPermisos]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerPermisos]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 
	PPadre.idPermiso AS idPadre,
	PPadre.codigo as codigoPadre,
	PPadre.descripcion as descripcionPadre,
	PPadre.esPermisoBase as esPermisoBasePadre,
	PPadre.fechaCreacion as fechaCreacionPadre,
	PHijo.idPermiso AS idHijo,
	PHijo.codigo as codigoHijo,
	PHijo.descripcion as descripcionHijo,
	PHijo.esPermisoBase as esPermisoBaseHijo,
	PHijo.fechaCreacion as fechaCreacionHijo
FROM PatenteFamilia PF
INNER JOIN Permiso PPadre ON PPadre.idPermiso = PF.idPermisoPadre
INNER JOIN Permiso PHijo ON PHijo.idPermiso = PF.idPermisoHijo
ORDER BY PPadre.idPermiso, PHijo.idPermiso
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerPermisos2]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerPermisos2]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	distinct P.idPermiso, p.codigo, p.descripcion, p.esPermisoBase, p.fechaCreacion
	FROM PatenteFamilia PF 
	INNER JOIN Permiso P ON P.idPermiso = PF.idPermisoPadre


END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerPermisosHijosPorPermisoPadreId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerPermisosHijosPorPermisoPadreId]
	-- Add the parameters for the stored procedure here
	@idPermisoPadre int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT distinct P.idPermiso, p.codigo, p.descripcion, p.esPermisoBase, p.fechaCreacion FROM PatenteFamilia PF
	INNER JOIN Permiso P ON P.idPermiso = PF.idPermisoHijo
	WHERE PF.idPermisoPadre = @idPermisoPadre

END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerPermisosPorIdUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerPermisosPorIdUsuario]
	-- Add the parameters for the stored procedure here
	@idUsuario int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 
	PPadre.idPermiso		AS idPadre,
	PPadre.codigo			AS codigoPadre,
	PPadre.descripcion		AS descripcionPadre,
	PPadre.esPermisoBase	AS esPermisoBasePadre,
	PPadre.fechaCreacion	AS fechaCreacionPadre,
	PHijo.idPermiso			AS idHijo,
	PHijo.codigo			AS codigoHijo,
	PHijo.descripcion		AS descripcionHijo,
	PHijo.esPermisoBase		AS esPermisoBaseHijo,
	PHijo.fechaCreacion		AS fechaCreacionHijo
FROM USUARIO U
INNER JOIN UsuarioPatente UP 
	ON UP.idUsuario = U.idUsuario
INNER JOIN PatenteFamilia PF 
	ON PF.idPermisoPadre = UP.idPatenteFamilia
INNER JOIN Permiso PPadre 
	ON PPadre.idPermiso = PF.idPermisoPadre
INNER JOIN Permiso PHijo 
	ON PHijo.idPermiso = PF.idPermisoHijo
WHERE U.idUsuario = @idUsuario
	UNION (
SELECT 
	P.idPermiso		AS idPadre,
	codigo			AS codigoPadre,
	descripcion		AS descripcionPadre,
	esPermisoBase	AS esPermisoBasePadre,
	fechaCreacion	AS fechaCreacionPadre,
	P.idPermiso			AS idHijo,
	codigo			AS codigoHijo,
	descripcion		AS descripcionHijo,
	esPermisoBase		AS esPermisoBaseHijo,
	fechaCreacion		AS fechaCreacionHijo
 FROM Usuario U
INNER JOIN USUARIOPATENTE UP ON UP.IDUSUARIO = U.idUsuario
INNER JOIN PERMISO P ON P.IDPERMISO = UP.IDPATENTEFAMILIA
WHERE U.idUsuario = @idUsuario
) ORDER BY 
	1,6
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerPermisosPorIdUsuario2]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerPermisosPorIdUsuario2]
	-- Add the parameters for the stored procedure here
	@idUsuario int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT P.* FROM UsuarioPatente UP 
	INNER JOIN PatenteFamilia PF ON PF.idPatenteFamilia = UP.idPatenteFamilia
	INNER JOIN Permiso P ON P.idPermiso = PF.idPermisoPadre
	WHERE UP.idUsuario = @idUsuario

END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerProyectoPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerProyectoPorId]
	-- Add the parameters for the stored procedure here
	@idProyecto int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Proyecto WHERE idProyecto = @idProyecto
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerProyectos]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerProyectos]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Proyecto where esBorrado = 0
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerProyectosPorCliente]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerProyectosPorCliente]
	-- Add the parameters for the stored procedure here
	@idCliente int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Proyecto WHERE idCliente = @idCliente
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerSolicitudLicenciaPorIdUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerSolicitudLicenciaPorIdUsuario]
	-- Add the parameters for the stored procedure here
	@idUsuario varchar(50)

	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

    -- Insert statements for procedure here
	SELECT 
		LS.idUsuario,
		LS.idLicencia,
		LS.idAprobador,
		LS.fechaDesde,
		LS.fechaHasta,
		LS.licenciaAprobada,
		LS.fechaCreacion,
		LS.dvh
	FROM
		LicenciaSolicitada LS
	INNER JOIN
		Licencia L ON L.idLicencia = LS.idLicencia
	WHERE
		LS.idUsuario = @idUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerTecnologias]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerTecnologias]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Tecnologia where esBorrado = 0
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerTodosLosIdiomas]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerTodosLosIdiomas]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Idioma
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerUsuarioPorId]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerUsuarioPorId]
	-- Add the parameters for the stored procedure here
	@idUsuario int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Usuario WHERE idUsuario = @idUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerUsuarioPorNombrePassword]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerUsuarioPorNombrePassword]
	-- Add the parameters for the stored procedure here
	@nombreUsuario varchar(50), 
	@password varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Usuario WHERE nombreUsuario = @nombreUsuario AND [password] = @password
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerUsuarioPorNombreUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerUsuarioPorNombreUsuario]
	-- Add the parameters for the stored procedure here
	@nombreUsuario varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 * FROM Usuario WHERE nombreUsuario = @nombreUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerUsuarios]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerUsuarios]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Usuario where esBorrado = 0
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerUsuariosPorTecnologiaExperiencia]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ObtenerUsuariosPorTecnologiaExperiencia]
	-- Add the parameters for the stored procedure here
	@experiencia varchar(50),
	@idTecnologia varchar(50)

	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

	-- Insert statements for procedure here
	SELECT U.* FROM AptitudUsuario AU
	INNER JOIN USUARIO U
	ON U.idUsuario = AU.idUsuario
	WHERE AU.experiencia = @experiencia AND AU.idTecnologia = @idTecnologia
END
GO
/****** Object:  Trigger [dbo].[ControlCambiosUsuario]    Script Date: 11/27/2018 6:36:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[ControlCambiosUsuario] ON [dbo].[Usuario] FOR INSERT, UPDATE, DELETE
AS

DECLARE @bit INT ,
       @field INT ,
       @maxfield INT ,
       @char INT ,
       @fieldname VARCHAR(128) ,
       @TableName VARCHAR(128) ,
       @PKCols VARCHAR(1000) ,
       @sql VARCHAR(2000), 
       @UpdateDate VARCHAR(21) ,
       @UserName VARCHAR(128) ,
       @Type VARCHAR(50) ,
       @PKSelect VARCHAR(1000),
	   @guid VARCHAR(200)
       
	   SET @guid = NEWID()

SELECT @TableName = 'Usuario'


SELECT         @UserName = SYSTEM_USER ,
       @UpdateDate = CONVERT(VARCHAR(8), GETDATE(), 112) 
               + ' ' + CONVERT(VARCHAR(12), GETDATE(), 114)


IF EXISTS (SELECT * FROM inserted)
       IF EXISTS (SELECT * FROM deleted)
               SELECT @Type = 'Update'
       ELSE
               SELECT @Type = 'Insert'
ELSE
       SELECT @Type = 'Delete'


SELECT * INTO #ins FROM inserted
SELECT * INTO #del FROM deleted


SELECT @PKCols = COALESCE(@PKCols + ' and', ' on') 
               + ' i.' + c.COLUMN_NAME + ' = d.' + c.COLUMN_NAME
       FROM    INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,

              INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
       WHERE   pk.TABLE_NAME = @TableName
       AND     CONSTRAINT_TYPE = 'PRIMARY KEY'
       AND     c.TABLE_NAME = pk.TABLE_NAME
       AND     c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME


SELECT @PKSelect = COALESCE(@PKSelect+'+','') + 'convert(varchar(100),
coalesce(i.' + COLUMN_NAME +',d.' + COLUMN_NAME + '))' 
       FROM    INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
               INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
       WHERE   pk.TABLE_NAME = 'Usuario'
       AND     CONSTRAINT_TYPE = 'PRIMARY KEY'
       AND     c.TABLE_NAME = pk.TABLE_NAME
       AND     c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

IF @PKCols IS NULL
BEGIN
       RAISERROR('no PK on table %s', 16, -1, @TableName)
       RETURN
END

SELECT         @field = 0, 
       @maxfield = MAX(ORDINAL_POSITION) 
       FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName
WHILE @field < @maxfield
BEGIN
       SELECT @field = MIN(ORDINAL_POSITION) 
               FROM INFORMATION_SCHEMA.COLUMNS 
               WHERE TABLE_NAME = @TableName 
               AND ORDINAL_POSITION > @field
       SELECT @bit = (@field - 1 )% 8 + 1
       SELECT @bit = POWER(2,@bit - 1)
       SELECT @char = ((@field - 1) / 8) + 1
       IF SUBSTRING(COLUMNS_UPDATED(),@char, 1) & @bit > 0
OR @Type IN ('I','D')
       BEGIN
               SELECT @fieldname = COLUMN_NAME 
                       FROM INFORMATION_SCHEMA.COLUMNS 
                       WHERE TABLE_NAME = @TableName 
                       AND ORDINAL_POSITION = @field
               SELECT @sql = '
insert ControlCambios (    accion, 
               nombreTabla, 
               primaryKey, 
               campo, 
               valorOriginal, 
               valorNuevo, 
               fechaModificacion, 
               UsuarioModificacion,
			   session)
select ''' + @Type + ''',''' 
       + @TableName + ''',' + @PKSelect
       + ',''' + @fieldname + ''''
       + ',convert(varchar(1000),d.' + @fieldname + ')'
       + ',convert(varchar(1000),i.' + @fieldname + ')'
       + ',''' + @UpdateDate + ''''
       + ',''' + @UserName + ''''
       + ',''' + @guid + '''' 
       + ' from #ins i full outer join #del d'
       + @PKCols
       + ' where i.' + @fieldname + ' <> d.' + @fieldname 
       + ' or (i.' + @fieldname + ' is null and  d.'
+ @fieldname
+ ' is not null)' 
       + ' or (i.' + @fieldname + ' is not null and  d.' 
+ @fieldname
+ ' is null)'
               EXEC (@sql)
       END
END


GO
ALTER TABLE [dbo].[Usuario] ENABLE TRIGGER [ControlCambiosUsuario]
GO
