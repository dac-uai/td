﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.INTERFACES
{
    public interface ICRUD<T>
    {
        T Crear(T entidad);
        T Modificar(T entidad);
        bool Eliminar(T entidad);
        T Seleccionar(int idEntidad);
        IList<T> SeleccionarTodos();
    }
}
