﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.INTERFACES
{
    public interface IUsuario : IEntidad, IDvh
    {
        string nombreUsuario { get; set; }
        bool esBloqueado { get; set; }
        string password { get; set; }
        int erroresLogin { get; set; }
        string nombre { get; set; }
        string apellido { get; set; }
        string mail { get; set; }
        DateTime fechaNacimiento { get; set; }
    }
}
