﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.INTERFACES
{
    public interface IEntidad
    {
        int id { get; set; }
        DateTime fechaCreacion { get; set; }
    }
}
