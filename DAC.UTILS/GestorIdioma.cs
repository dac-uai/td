﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAC.INTERFACES;
using DAC.UTILS.BE;
using DAC.UTILS.DAL;

namespace DAC.UTILS
{
    public class GestorIdioma
    {
        private static GestorIdioma _gestorIdioma = null;
        private static IdiomaDAL _idiomaDal = null;
        private static MensajeDAL _mensajeDal = null;
        private static TraduccionDAL _traduccionDal = null;

        public static GestorIdioma GetInstance()
        {
            if (_gestorIdioma == null)
            {
                _gestorIdioma = new GestorIdioma();
            }

            return _gestorIdioma;
        }

        private GestorIdioma()
        {
            _idiomaDal = new IdiomaDAL();
            _mensajeDal = new MensajeDAL();
            _traduccionDal = new TraduccionDAL();
        }

        /// <summary>
        /// Obtener un idioma según un usuario en particular
        /// </summary>
        /// <param name="usuario">Usuario que se desea obtener el idioma que seleccionó</param>
        /// <returns></returns>
        public IdiomaBE ObtenerIdiomaPorUsuario(INTERFACES.IUsuario usuario)
        {
            IdiomaBE _idioma = null;

            try
            {
                _idioma = _idiomaDal.ObtenerIdiomaPorUsuario(usuario);
            }
            catch (Exception)
            {
                throw;
            }

            return _idioma;
        }

        /// <summary>
        /// Guardar un nuevo idioma para el usuario en sesión
        /// </summary>
        /// <param name="usuario">Usuario en sesión</param>
        /// <param name="idioma">Nuevo idioma seleccionado</param>
        /// <returns></returns>
        public bool GuardarIdioma(IUsuario usuario, IdiomaBE idioma)
        {
            bool _guardadoOk = false;

            try
            {
                _guardadoOk = _idiomaDal.GuardarIdiomaPorUsuario(usuario, idioma);
            }
            catch (Exception)
            {
                throw;
            }

            return _guardadoOk;
        }

        /// <summary>
        /// Obtener todos los idiomas con sus mensajes y traducciones de la aplicación
        /// </summary>
        /// <returns></returns>
        public IList<IdiomaBE> ObtenerTodosLosIdiomas()
        {
            IList<IdiomaBE> _idiomas = null;

            try
            {
                _idiomas = _idiomaDal.SeleccionarTodos();
                IList<IdiomaBE> aux = new List<IdiomaBE>();

                foreach (IdiomaBE idioma in _idiomas)
                {
                    IdiomaBE _idiomaAux = idioma;
                    CargarTodosLosMensajesconTraduccionesPorIdioma(ref _idiomaAux);
                    aux.Add(_idiomaAux);
                }

                _idiomas = aux;
            }
            catch (Exception)
            {
                throw;
            }

            return _idiomas;
        }


        public void CargarTodosLosMensajesPorIdioma(ref IdiomaBE idioma)
        {
            try
            {
                idioma.mensajes = _mensajeDal.SeleccionarPorIdioma(idioma);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CargarTodosLosMensajesconTraduccionesPorIdioma(ref IdiomaBE idioma)
        {
            try
            {
                idioma.mensajes = _mensajeDal.SeleccionarPorIdioma(idioma);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CargarTraduccionPorMensaje(ref MensajeBE mensaje)
        {
            try
            {
                mensaje.traduccion = _traduccionDal.SeleccionarPorMensaje(mensaje);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtener una traducción dependiendo de un idioma y una etiqueta de un control
        /// </summary>
        /// <param name="idioma">Idioma seleccionado por el usuario</param>
        /// <param name="etiqueta">Etiqueta del control</param>
        /// <returns></returns>
        public TraduccionBE ObtenerTraduccion(IdiomaBE idioma, string etiqueta)
        {
            TraduccionBE _traduccion = null;

            try
            {
                _traduccion = idioma.mensajes.FirstOrDefault(x => x.etiqueta.Equals(etiqueta)).traduccion;
            }
            catch (Exception)
            {
                throw;
            }

            return _traduccion;
        }
    }
}