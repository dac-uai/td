﻿using DAC.BE;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.DAL
{
    public class ClienteDAL : BaseDAL
    {
        private Bitacora log = null;

        public ClienteDAL()
        {
            log = new Bitacora();
        }

        public override IEntidad Crear(IEntidad entidad)
        {
            Bitacora log = new Bitacora();

            try
            {
                ClienteBE _entidad = (ClienteBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@activo", _entidad.activo.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", _entidad.descripcion, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvh", _entidad.dvh, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@cuit", _entidad.cuit, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@mailContacto", _entidad.mailContacto, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@nombreContacto", _entidad.nombreContacto, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@razonSocial", _entidad.razonSocial, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@telefonoContacto", _entidad.telefonoContacto, DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("CrearCliente", parameters).Rows[0][0].ToString());

                log.mensaje = (_entidad == null) ? string.Format("Error intentando dar de alta el Cliente {0}", _entidad.razonSocial) : string.Format("Alta exitosa para el Cliente {0}", _entidad.razonSocial);
                TipoEvento evento = (_entidad == null) ? TipoEvento.Error : TipoEvento.Alta;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public void GuardarDVV(string tabla, string dvv)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreTabla", tabla, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvv", dvv, DbType.String)
                };

                SqlHelper.GetInstance().ExecuteQuery("GuardarDVV", parameters);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
        }

        public override bool Eliminar(IEntidad entidad)
        {
            bool eliminadoOk = false;
            try
            {
                ClienteBE _entidad = (ClienteBE)entidad;
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idCliente", _entidad.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@esBorrado", "1", DbType.String)
                };

                eliminadoOk = SqlHelper.GetInstance().ExecuteNonQuery("ActualizarCliente", parameters) > 0;

                log.mensaje = (!eliminadoOk) ? string.Format("Error intentando eliminar el Cliente {0}", _entidad.razonSocial) : string.Format("Se ha eliminado exitosamente a el Cliente {0}", _entidad.razonSocial);
                TipoEvento evento = (_entidad == null) ? TipoEvento.LoginFallido : TipoEvento.Baja;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return eliminadoOk;
        }

        public override IEntidad Modificar(IEntidad entidad)
        {
            try
            {
                ClienteBE _entidad = (ClienteBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idCliente", _entidad.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@activo", _entidad.activo.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", _entidad.descripcion, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvh", _entidad.dvh, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@cuit", _entidad.cuit, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@mailContacto", _entidad.mailContacto, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@nombreContacto", _entidad.nombreContacto, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@razonSocial", _entidad.razonSocial, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@telefonoContacto", _entidad.telefonoContacto, DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("ActualizarCliente", parameters).Rows[0][0].ToString());

                log.mensaje = (_entidad == null) ? string.Format("Error intentando actualizar el cliente {0}", _entidad.razonSocial)
                    : string.Format("Modificacion exitosa para el cliente {0}", _entidad.razonSocial);
                TipoEvento evento = (_entidad == null) ? TipoEvento.Error : TipoEvento.Modificacion;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public ClienteBE Seleccionar2(int idEntidad)
        {
            ClienteBE Cliente = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idCliente", idEntidad.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerClientePorId", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    Cliente = new ClienteBE()
                    {
                        id = int.Parse(row["idCliente"].ToString()),
                        cuit = row["cuit"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        mailContacto = row["mailContacto"].ToString(),
                        nombreContacto = row["nombreContacto"].ToString(),
                        razonSocial = row["razonSocial"].ToString(),
                        dvh = row["dvh"].ToString(),
                        telefonoContacto = row["razonSocial"].ToString(),
                        activo = bool.Parse(row["activo"].ToString())
                    };
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return Cliente;
        }

        public override IEntidad Seleccionar(int idEntidad)
        {
            IEntidad Cliente = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idCliente", idEntidad.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerClientePorId", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    Cliente = new ClienteBE()
                    {
                        id = int.Parse(row["idCliente"].ToString()),
                        cuit = row["cuit"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        mailContacto = row["mailContacto"].ToString(),
                        nombreContacto = row["nombreContacto"].ToString(),
                        razonSocial = row["razonSocial"].ToString(),
                        dvh = row["dvh"].ToString(),
                        telefonoContacto = row["razonSocial"].ToString(),
                        activo = bool.Parse(row["activo"].ToString())
                    };
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return Cliente;
        }

        public override IList<IEntidad> SeleccionarTodos()
        {
            IList<IEntidad> Clientes = new List<IEntidad>();

            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerClientes");

                foreach (DataRow row in dt.Rows)
                {
                    ClienteBE Cliente = new ClienteBE()
                    {
                        id = int.Parse(row["idCliente"].ToString()),
                        cuit = row["cuit"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        mailContacto = row["mailContacto"].ToString(),
                        nombreContacto = row["nombreContacto"].ToString(),
                        razonSocial = row["razonSocial"].ToString(),
                        dvh = row["dvh"].ToString(),
                        telefonoContacto = row["razonSocial"].ToString(),
                        activo = bool.Parse(row["activo"].ToString())
                    };

                    Clientes.Add(Cliente);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return Clientes;
        }
    }
}
