﻿using DAC.BE;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.DAL
{
    public class ProyectoDAL : BaseDAL
    {
        private Bitacora log = null;

        public ProyectoDAL()
        {
            log = new Bitacora();
        }

        public override IEntidad Crear(IEntidad entidad)
        {
            Bitacora log = new Bitacora();

            try
            {
                ProyectoBE _entidad = (ProyectoBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombre", _entidad.nombre, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", _entidad.descripcion, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvh", _entidad.dvh, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@fechaDesde", _entidad.fechaDesde.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@fechaHasta", _entidad.fechaHasta.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@presupuestoInicial", _entidad.presupuestoInicial.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@idCliente", _entidad.cliente.id.ToString(), DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("CrearProyecto", parameters).Rows[0][0].ToString());

                log.mensaje = (_entidad == null) ? string.Format("Error intentando dar de alta el proyecto {0}", _entidad.nombre) : string.Format("Alta exitosa para el proyecto {0}", _entidad.nombre);
                TipoEvento evento = (_entidad == null) ? TipoEvento.Error : TipoEvento.Alta;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public void GuardarDVV(string nombreTabla, string dvv)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreTabla", nombreTabla, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvv", dvv, DbType.String)
                };

                SqlHelper.GetInstance().ExecuteQuery("GuardarDVV", parameters);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
        }

        public IList<ProyectoBE> SeleccionarPorIdCliente(int id)
        {
            IList<ProyectoBE> proyectos = new List<ProyectoBE>();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idCliente", id.ToString(), DbType.String),
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerProyectosPorCliente", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    ProyectoBE proyecto = new ProyectoBE()
                    {
                        id = int.Parse(row["idProyecto"].ToString()),
                        nombre = row["nombre"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        presupuestoInicial = int.Parse(row["presupuestoInicial"].ToString()),
                        cliente = (ClienteBE)new ClienteDAL().Seleccionar(int.Parse(row["idProyecto"].ToString())),
                        dvh = row["dvh"].ToString()
                    };

                    proyectos.Add(proyecto);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return proyectos;
        }

        public override bool Eliminar(IEntidad entidad)
        {
            bool eliminadoOk = false;
            try
            {
                ProyectoBE _entidad = (ProyectoBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idProyecto", _entidad.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@esBorrado", "1", DbType.String)
                };

                eliminadoOk = SqlHelper.GetInstance().ExecuteNonQuery("ActualizarProyecto", parameters) > 0;

                log.mensaje = (!eliminadoOk) ? string.Format("Error intentando eliminar el Proyecto {0}", _entidad.nombre) : 
                    string.Format("Se ha eliminado exitosamente a el Proyecto {0}", _entidad.nombre);
                TipoEvento evento = (entidad == null) ? TipoEvento.LoginFallido : TipoEvento.Baja;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return eliminadoOk;
        }

        public override IEntidad Modificar(IEntidad entidad)
        {
            try
            {
                ProyectoBE _entidad = (ProyectoBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idProyecto", _entidad.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@nombre", _entidad.nombre, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", _entidad.descripcion, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvh", _entidad.dvh, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@fechaDesde", _entidad.fechaDesde.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@fechaHasta", _entidad.fechaHasta.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@presupuestoInicial", _entidad.presupuestoInicial.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@idCliente", _entidad.cliente.id.ToString(), DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("ActualizarProyecto", parameters).Rows[0][0].ToString());

                log.mensaje = (_entidad == null) ? string.Format("Error intentando actualizar la tecnología {0}", _entidad.nombre) 
                    : string.Format("Modificacion exitosa para la tecnología {0}", _entidad.nombre);
                TipoEvento evento = (_entidad == null) ? TipoEvento.Error : TipoEvento.Modificacion;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public override IEntidad Seleccionar(int idEntidad)
        {
            IEntidad proyecto = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idProyecto", idEntidad.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerProyectoPorId", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    proyecto = new ProyectoBE()
                    {
                        id = int.Parse(row["idProyecto"].ToString()),
                        nombre = row["nombre"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        presupuestoInicial = int.Parse(row["presupuestoInicial"].ToString()),
                        cliente = (ClienteBE)new ClienteDAL().Seleccionar(int.Parse(row["idCliente"].ToString())),
                        dvh = row["dvh"].ToString() 
                    };
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return proyecto;
        }

        public override IList<IEntidad> SeleccionarTodos()
        {
            IList<IEntidad> proyectos = new List<IEntidad>();

            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerProyectos");

                foreach (DataRow row in dt.Rows)
                {
                    ProyectoBE proyecto = new ProyectoBE()
                    {
                        id = int.Parse(row["idProyecto"].ToString()),
                        nombre = row["nombre"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        presupuestoInicial = int.Parse(row["presupuestoInicial"].ToString()),
                        cliente = (ClienteBE)new ClienteDAL().Seleccionar(int.Parse(row["idCliente"].ToString())),
                        dvh = row["dvh"].ToString()
                    };

                    proyectos.Add(proyecto);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return proyectos;
        }
    }
}
