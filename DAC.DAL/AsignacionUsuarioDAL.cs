﻿using DAC.BE;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAC.DAL
{
    public class AsignacionUsuarioDAL : BaseDAL
    {
        private Bitacora log = null;

        public AsignacionUsuarioDAL()
        {
            log = new Bitacora();
        }

        public override IEntidad Crear(IEntidad entidad)
        {
            try
            {
                AsignacionUsuarioBE _entidad = (AsignacionUsuarioBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", _entidad.usuario.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@idProyecto", _entidad.proyecto.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvh", _entidad.dvh, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@fechaDesde", _entidad.fechaDesde.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@fechaHasta", _entidad.fechaHasta.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@costoHora", _entidad.costoHora.ToString(), DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("CrearAsignacionUsuario", parameters).Rows[0][0].ToString());

                log.mensaje = (_entidad == null) ? string.Format("Error intentando dar de alta el AsignacionUsuario {0}", _entidad.usuario.nombreUsuario)
                    : string.Format("Alta exitosa para el AsignacionUsuario {0}", _entidad.usuario.nombreUsuario);

                TipoEvento evento = (entidad == null) ? TipoEvento.Error : TipoEvento.Alta;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public void GuardarDVV(string nombreTabla, string dvv)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreTabla", nombreTabla, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvv", dvv, DbType.String)
                };

                SqlHelper.GetInstance().ExecuteQuery("GuardarDVV", parameters);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
        }

        public override bool Eliminar(IEntidad entidad)
        {
            bool eliminadoOk = false;
            try
            {
                AsignacionUsuarioBE _entidad = (AsignacionUsuarioBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idAsignacionUsuario", entidad.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@esBorrado", "1", DbType.String)
                };

                eliminadoOk = SqlHelper.GetInstance().ExecuteNonQuery("ActualizarAsignacionUsuario", parameters) > 0;

                log.mensaje = (!eliminadoOk) ? string.Format("Error intentando eliminar la asignacion del usuario {0}", _entidad.usuario.nombreUsuario)
                    : string.Format("Se ha eliminado exitosamente la asignacion del usuario  {0}", _entidad.usuario.nombreUsuario);
                TipoEvento evento = (entidad == null) ? TipoEvento.LoginFallido : TipoEvento.Baja;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return eliminadoOk;
        }

        public override IEntidad Modificar(IEntidad entidad)
        {
            try
            {
                AsignacionUsuarioBE _entidad = (AsignacionUsuarioBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", _entidad.usuario.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@idProyecto", _entidad.proyecto.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvh", _entidad.dvh, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@fechaDesde", _entidad.fechaDesde.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@fechaHasta", _entidad.fechaHasta.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@costoHora", _entidad.costoHora.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@idAsignacionUsuario", _entidad.id.ToString(), DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("ActualizarAsignacionUsuario", parameters).Rows[0][0].ToString());

                log.mensaje = (entidad == null) ? string.Format("Error intentando actualizar la asignacion del usuario {0}", _entidad.usuario.nombreUsuario)
                    : string.Format("Modificacion exitosa para la asignacion del usuario {0}", _entidad.usuario.nombreUsuario);
                TipoEvento evento = (entidad == null) ? TipoEvento.Error : TipoEvento.Modificacion;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public IList<ReporteHoras> ObtenerRegistroHorasPorProyecto(ProyectoBE proyecto)
        {
            IList<ReporteHoras> reporteHoras = new List<ReporteHoras>();
            try
            {

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idProyecto", proyecto.id.ToString(), DbType.String)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerHorasPorProyecto", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    ReporteHoras reporte = new ReporteHoras()
                    {
                        horas = int.Parse(row["cantidadHoras"].ToString()),
                        usuario = (UsuarioBE)new UsuarioDAL().Seleccionar(int.Parse(row["idUsuario"].ToString())),
                        proyecto = (ProyectoBE)new ProyectoDAL().Seleccionar(int.Parse(row["idProyecto"].ToString())),
                        fecha = DateTime.Parse(row["fecha"].ToString()),
                        comentarios = row["comentarios"].ToString()
                    };

                    reporteHoras.Add(reporte);
                }

            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return reporteHoras;
        }

        public bool RegistrarHorasPorProyecto(ReporteHoras reporte)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", reporte.usuario.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@idProyecto", reporte.proyecto.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@cantidadHoras", reporte.horas.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@fecha", reporte.fecha.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@comentarios", reporte.comentarios, DbType.String)
                };

                SqlHelper.GetInstance().ExecuteQuery("CrearRegistroHoras", parameters);

                log.mensaje = string.Format("Se han registrado horas para el usuario {0}", reporte.usuario.nombreUsuario);

                TipoEvento evento = TipoEvento.Alta;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return true;
        }

        public AsignacionUsuarioBE SeleccionarPorUsuarioProyectoFecha(UsuarioBE usuario, DateTime fechaDesde, DateTime fechaHasta, ProyectoBE proyecto)
        {
            AsignacionUsuarioBE AsignacionUsuario = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", usuario.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@idProyecto", proyecto.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@fechaDesde", fechaDesde.ToString(), DbType.DateTime),
                    SqlHelper.GetInstance().AddParameter("@fechaHasta", fechaHasta.ToString(), DbType.DateTime)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerAsignacionUsuarioProyectoFecha", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    AsignacionUsuario = new AsignacionUsuarioBE()
                    {
                        id = int.Parse(row["idAsignacionUsuario"].ToString()),
                        usuario = (UsuarioBE)new UsuarioDAL().Seleccionar(int.Parse(row["idUsuario"].ToString())),
                        proyecto = (ProyectoBE)new ProyectoDAL().Seleccionar(int.Parse(row["idProyecto"].ToString())),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        costoHora = int.Parse(row["costoHora"].ToString()),
                        dvh = row["dvh"].ToString()
                    };
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return AsignacionUsuario;
        }

        public IList<AsignacionUsuarioMapper> SeleccionarPorProyectoMapper(ProyectoBE proyecto)
        {
            IList<AsignacionUsuarioMapper> AsignacionUsuarios = new List<AsignacionUsuarioMapper>();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idProyecto", proyecto.id.ToString(), DbType.String),
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerAsignacionUsuariosPorProyecto", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    UsuarioBE usuario = (UsuarioBE)new UsuarioDAL().Seleccionar(int.Parse(row["idUsuario"].ToString()));

                    AsignacionUsuarioMapper AsignacionUsuario = new AsignacionUsuarioMapper()
                    {
                        nombreUsuario = usuario.nombreUsuario,
                        nombre = usuario.nombre,
                        apellido = usuario.apellido,
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        costoHora = int.Parse(row["costoHora"].ToString())
                    };

                    AsignacionUsuarios.Add(AsignacionUsuario);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return AsignacionUsuarios;
        }

        public override IEntidad Seleccionar(int idEntidad)
        {
            IEntidad AsignacionUsuario = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idAsignacionUsuario", idEntidad.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerAsignacionUsuarioPorId", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    AsignacionUsuario = new AsignacionUsuarioBE()
                    {
                        id = int.Parse(row["idAsignacionUsuario"].ToString()),
                        usuario = (UsuarioBE)new UsuarioDAL().Seleccionar(int.Parse(row["idUsuario"].ToString())),
                        proyecto = (ProyectoBE)new ProyectoDAL().Seleccionar(int.Parse(row["idProyecto"].ToString())),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        costoHora = int.Parse(row["costoHora"].ToString()),
                        dvh = row["dvh"].ToString()
                    };
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return AsignacionUsuario;
        }

        public override IList<IEntidad> SeleccionarTodos()
        {
            IList<IEntidad> AsignacionUsuarios = new List<IEntidad>();

            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerAsignacionUsuarios");

                foreach (DataRow row in dt.Rows)
                {
                    AsignacionUsuarioBE AsignacionUsuario = new AsignacionUsuarioBE()
                    {
                        id = int.Parse(row["idAsignacionUsuario"].ToString()),
                        usuario = (UsuarioBE)new UsuarioDAL().Seleccionar(int.Parse(row["idUsuario"].ToString())),
                        proyecto = (ProyectoBE)new ProyectoDAL().Seleccionar(int.Parse(row["idProyecto"].ToString())),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        costoHora = int.Parse(row["costoHora"].ToString()),
                        dvh = row["dvh"].ToString()
                    };

                    AsignacionUsuarios.Add(AsignacionUsuario);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return AsignacionUsuarios;
        }


        public IList<AsignacionUsuarioBE> SeleccionarPorIdCliente(int id)
        {
            IList<AsignacionUsuarioBE> AsignacionUsuarios = new List<AsignacionUsuarioBE>();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idCliente", id.ToString(), DbType.String),
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerAsignacionUsuariosPorCliente", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    AsignacionUsuarioBE AsignacionUsuario = new AsignacionUsuarioBE()
                    {
                        id = int.Parse(row["idAsignacionUsuario"].ToString()),
                        usuario = (UsuarioBE)new UsuarioDAL().Seleccionar(int.Parse(row["idUsuario"].ToString())),
                        proyecto = (ProyectoBE)new ProyectoDAL().Seleccionar(int.Parse(row["idProyecto"].ToString())),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        costoHora = int.Parse(row["costoHora"].ToString()),
                        dvh = row["dvh"].ToString()
                    };

                    AsignacionUsuarios.Add(AsignacionUsuario);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return AsignacionUsuarios;
        }

        public IList<AsignacionUsuarioBE> SeleccionarPorIdProyecto(int id)
        {
            IList<AsignacionUsuarioBE> AsignacionUsuarios = new List<AsignacionUsuarioBE>();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idProyecto", id.ToString(), DbType.String),
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerAsignacionUsuariosPorProyecto", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    AsignacionUsuarioBE AsignacionUsuario = new AsignacionUsuarioBE()
                    {
                        id = int.Parse(row["idAsignacionUsuario"].ToString()),
                        usuario = (UsuarioBE)new UsuarioDAL().Seleccionar(int.Parse(row["idUsuario"].ToString())),
                        proyecto = (ProyectoBE)new ProyectoDAL().Seleccionar(int.Parse(row["idProyecto"].ToString())),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        costoHora = int.Parse(row["costoHora"].ToString()),
                        dvh = row["dvh"].ToString()
                    };

                    AsignacionUsuarios.Add(AsignacionUsuario);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return AsignacionUsuarios;
        }

        public IList<AsignacionUsuarioBE> SeleccionarPorIdUsuario(int id)
        {
            IList<AsignacionUsuarioBE> AsignacionUsuarios = new List<AsignacionUsuarioBE>();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", id.ToString(), DbType.String),
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerAsignacionUsuariosPorUsuario", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    AsignacionUsuarioBE AsignacionUsuario = new AsignacionUsuarioBE()
                    {
                        id = int.Parse(row["idAsignacionUsuario"].ToString()),
                        usuario = (UsuarioBE)new UsuarioDAL().Seleccionar(int.Parse(row["idUsuario"].ToString())),
                        proyecto = (ProyectoBE)new ProyectoDAL().Seleccionar(int.Parse(row["idProyecto"].ToString())),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        costoHora = int.Parse(row["costoHora"].ToString()),
                        dvh = row["dvh"].ToString()
                    };

                    AsignacionUsuarios.Add(AsignacionUsuario);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return AsignacionUsuarios;
        }
    }
}
