﻿using DAC.BE;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.DAL
{
    public class TecnologiaDAL : BaseDAL
    {
        private Bitacora log = null;

        public TecnologiaDAL()
        {
            log = new Bitacora();
        }

        public override IEntidad Crear(IEntidad entidad)
        {
            try
            {
                TecnologiaBE _entidad = (TecnologiaBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@codigo", _entidad.codigo, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", _entidad.descripcion, DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("CrearTecnologia", parameters).Rows[0][0].ToString());

                log.mensaje = (entidad == null) ? string.Format("Error intentando dar de alta la tecnologia {0}", _entidad.descripcion) 
                    : string.Format("Alta exitosa para la tecnologia {0}", _entidad.descripcion);
                TipoEvento evento = (entidad == null) ? TipoEvento.Error : TipoEvento.Alta;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public override bool Eliminar(IEntidad entidad)
        {
            bool eliminadoOk = false;
            try
            {
                TecnologiaBE _entidad = (TecnologiaBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idTecnologia", entidad.id.ToString(), DbType.Int32)
                };

                eliminadoOk = SqlHelper.GetInstance().ExecuteNonQuery("EliminarTecnologiaPorId", parameters) > 0;

                log.mensaje = (!eliminadoOk) ? string.Format("Error intentando eliminar la tecnologia {0}", _entidad.codigo) 
                    : string.Format("Se ha eliminado exitosamente a la tecnologia {0}", _entidad.codigo);
                TipoEvento evento = (entidad == null) ? TipoEvento.LoginFallido : TipoEvento.Baja;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return eliminadoOk;
        }

        public bool AsignarAptitudAUsuario(AptitudBE aptitud, UsuarioBE usuario)
        {
            Bitacora log = new Bitacora();
            int nuevoId = 0;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", usuario.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@idTecnologia", aptitud.tecnologia.id.ToString(), DbType.String),
                    SqlHelper.GetInstance().AddParameter("@experiencia", ((int)aptitud.experiencia).ToString(), DbType.String)
                };

                nuevoId = int.Parse(SqlHelper.GetInstance().ExecuteQuery("AsignarAptitud", parameters).Rows[0][0].ToString());

                log.mensaje = (nuevoId > 0) ? string.Format("Asignacion exitosa de aptitud para {0}", usuario.nombreUsuario) 
                    : string.Format("Error intentando asignar una aptitud a {0}", usuario.nombreUsuario);
                TipoEvento evento = (nuevoId > 0) ? TipoEvento.Alta : TipoEvento.Error;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return (nuevoId > 0);
        }

        public UsuarioBE ModificarAptitudPorUsuario(AptitudBE aptitud, UsuarioBE usuario)
        {
            bool modificadoOk = false;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idTecnologia", aptitud.tecnologia.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@idUsuario", usuario.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@experiencia", ((int)aptitud.experiencia).ToString(), DbType.String)
                };

                modificadoOk = SqlHelper.GetInstance().ExecuteNonQuery("ActualizarAptitudPorUsuarioTecnologia", parameters) > 0;

                log.mensaje = (!modificadoOk) ? string.Format("Error intentando actualizar la tecnologia {0} del usuario {1}", aptitud.tecnologia.codigo, usuario.nombreUsuario)
                    : string.Format("Se ha actualizado exitosamente la tecnologia {0} del usuario {1}", aptitud.tecnologia.codigo, usuario.nombreUsuario);
                TipoEvento evento = (modificadoOk) ? TipoEvento.Modificacion : TipoEvento.Error;

                usuario.aptitudes.First(x => x.tecnologia.codigo == aptitud.tecnologia.codigo).experiencia = aptitud.experiencia;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return usuario;
        }

        public UsuarioBE EliminarAptitudPorUsuario(AptitudBE aptitud, UsuarioBE usuario)
        {
            bool eliminadoOk = false;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idTecnologia", aptitud.tecnologia.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@idUsuario", usuario.id.ToString(), DbType.Int32)
                };

                eliminadoOk = SqlHelper.GetInstance().ExecuteNonQuery("EliminarAptitudPorUsuarioTecnologia", parameters) > 0;

                log.mensaje = (!eliminadoOk) ? string.Format("Error intentando eliminar la tecnologia {0} del usuario {1}", aptitud.tecnologia.codigo, usuario.nombreUsuario) 
                    : string.Format("Se ha eliminado exitosamente la tecnologia {0} del usuario {1}", aptitud.tecnologia.codigo, usuario.nombreUsuario);
                TipoEvento evento = (eliminadoOk) ? TipoEvento.Baja : TipoEvento.Error;

                AptitudBE _aptitud = usuario.aptitudes.First(x => x.tecnologia.codigo == aptitud.tecnologia.codigo);
                usuario.aptitudes.Remove(_aptitud);

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return usuario;
        }

        public UsuarioBE ObtenerAptitudPorUsuario(UsuarioBE entidad)
        {
            TecnologiaBE tecnologia = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", entidad.id.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerAptitudesPorIdUsuario", parameters);

                entidad.aptitudes = (dt.Rows.Count > 0) ? new List<AptitudBE>() : null;

                foreach (DataRow row in dt.Rows)
                {
                    tecnologia = new TecnologiaBE()
                    {
                        id = int.Parse(row["idTecnologia"].ToString()),
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    };

                    Experiencia exp = (Experiencia)(int.Parse(row["experiencia"].ToString()));

                    entidad.aptitudes.Add(new AptitudBE()
                        {
                            tecnologia = tecnologia,
                            experiencia = exp
                        }
                    );
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public override IEntidad Modificar(IEntidad entidad)
        {
            try
            {
                TecnologiaBE _entidad = (TecnologiaBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idTecnologia", _entidad.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@codigo", _entidad.codigo, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", _entidad.descripcion, DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("ActualizarTecnologia", parameters).Rows[0][0].ToString());

                log.mensaje = (entidad == null) ? string.Format("Error intentando actualizar la tecnología {0}", _entidad.codigo) 
                    : string.Format("Modificacion exitosa para la tecnología {0}", _entidad.codigo);
                TipoEvento evento = (entidad == null) ? TipoEvento.Error : TipoEvento.Modificacion;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public override IEntidad Seleccionar(int idEntidad)
        {
            TecnologiaBE tecnologia = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idTecnologia", idEntidad.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerTecnologiaPorId", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    tecnologia = new TecnologiaBE()
                    {
                        id = int.Parse(row["idTecnologia"].ToString()),
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    };
                }
            }
            catch (Exception)
            {
                throw;
            }

            return tecnologia;
        }

        public override IList<IEntidad> SeleccionarTodos()
        {
            IList<IEntidad> tecnologias = new List<IEntidad>();

            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerTecnologias");

                foreach (DataRow row in dt.Rows)
                {
                    TecnologiaBE tecnologia = new TecnologiaBE()
                    {
                        id = int.Parse(row["idTecnologia"].ToString()),
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    };

                    tecnologias.Add(tecnologia);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return tecnologias;
        }
    }
}
