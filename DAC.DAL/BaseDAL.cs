﻿using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.DAL
{
    public abstract class BaseDAL : ICRUD<IEntidad>
    {
        public abstract IEntidad Crear(IEntidad entidad);

        public abstract bool Eliminar(IEntidad entidad);

        public abstract IEntidad Modificar(IEntidad entidad);

        public abstract IEntidad Seleccionar(int idEntidad);

        public abstract IList<IEntidad> SeleccionarTodos();
    }
}
