﻿using DAC.BE;
using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.DAL
{
    public static class FactoryDAL
    {
        public static BaseDAL ObtenerDalPorNombre(string clase)
        {
            string nombreClase = string.Format("DAC.DAL.{0}DAL", clase);

            var dalAssembly = AppDomain.CurrentDomain.GetAssemblies().First(x => x.GetName().ToString().Contains("DAC.DAL"));

            return (BaseDAL)dalAssembly.CreateInstance(nombreClase);
        }
    }
}
