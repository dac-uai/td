﻿using DAC.BE;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.DAL
{
    public class LicenciaDAL : BaseDAL
    {
        private Bitacora log = null;

        public LicenciaDAL()
        {
            log = new Bitacora();
        }

        public override IEntidad Seleccionar(int idEntidad)
        {
            IEntidad Licencia = null;

            try
            {

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idLicencia", idEntidad.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerLicenciaPorId", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    Licencia = new LicenciaBE()
                    {
                        id = int.Parse(row["idLicencia"].ToString()),
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    };
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Licencia;
        }

        public override IList<IEntidad> SeleccionarTodos()
        {
            IList<IEntidad> Licencias = new List<IEntidad>();

            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerLicencias");

                foreach (DataRow row in dt.Rows)
                {
                    LicenciaBE Licencia = new LicenciaBE()
                    {
                        id = int.Parse(row["idLicencia"].ToString()),
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    };

                    Licencias.Add(Licencia);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Licencias;
        }

        public override IEntidad Crear(IEntidad entidad)
        {
            try
            {
                LicenciaBE _entidad = (LicenciaBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@codigo", _entidad.codigo, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", _entidad.descripcion, DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("CrearLicencia", parameters).Rows[0][0].ToString());

                log.mensaje = (entidad == null) ? string.Format("Error intentando dar de alta la Licencia {0}", _entidad.descripcion) : string.Format("Alta exitosa para la Licencia {0}", _entidad.descripcion);
                TipoEvento evento = (entidad == null) ? TipoEvento.Error : TipoEvento.Alta;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public override bool Eliminar(IEntidad entidad)
        {
            bool eliminadoOk = false;
            try
            {
                LicenciaBE _entidad = (LicenciaBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idLicencia", entidad.id.ToString(), DbType.Int32)
                };

                eliminadoOk = SqlHelper.GetInstance().ExecuteNonQuery("EliminarLicenciaPorId", parameters) > 0;

                log.mensaje = (!eliminadoOk) ? string.Format("Error intentando eliminar la Licencia {0}", _entidad.codigo) : string.Format("Se ha eliminado exitosamente a la Licencia {0}", _entidad.codigo);
                TipoEvento evento = (entidad == null) ? TipoEvento.LoginFallido : TipoEvento.Baja;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return eliminadoOk;
        }

        public override IEntidad Modificar(IEntidad entidad)
        {
            try
            {
                LicenciaBE _entidad = (LicenciaBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idLicencia", _entidad.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@codigo", _entidad.codigo, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", _entidad.descripcion, DbType.String)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("ActualizarLicencia", parameters).Rows[0][0].ToString());

                log.mensaje = (entidad == null) ? string.Format("Error intentando actualizar la tecnología {0}", _entidad.codigo) : string.Format("Modificacion exitosa para la tecnología {0}", _entidad.codigo);
                TipoEvento evento = (entidad == null) ? TipoEvento.Error : TipoEvento.Modificacion;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public void GuardarDVV(string tabla, string dvv)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreTabla", tabla, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvv", dvv, DbType.String)
                };

                SqlHelper.GetInstance().ExecuteQuery("GuardarDVV", parameters);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
        }

        public IList<LicenciaSolicitadaMapper> SeleccionarLicenciasPendientesMapper()
        {
            IList<LicenciaSolicitadaMapper> listado = new List<LicenciaSolicitadaMapper>();
            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerLicenciasPendientes");

                foreach (DataRow row in dt.Rows)
                {
                    LicenciaSolicitadaMapper mapper = new LicenciaSolicitadaMapper()
                    {
                        idUsuario = int.Parse(row["idUsuario"].ToString()),
                        idLicenciaSolicitada = int.Parse(row["idLicenciaSolicitada"].ToString()),
                        nombre = Encriptacion.Desencriptar(row["nombre"].ToString()),
                        apellido = Encriptacion.Desencriptar(row["apellido"].ToString()),
                        fechaDesde = DateTime.Parse(row["fechaDesde"].ToString()),
                        fechaHasta = DateTime.Parse(row["fechaHasta"].ToString()),
                        descripcionLicencia = row["descripcion"].ToString(),
                        licenciaAprobada = bool.Parse(row["licenciaAprobada"].ToString())
                    };

                    listado.Add(mapper);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return listado;
        }

        public IList<LicenciaSolicitadaBE> SeleccionarLicenciasPendientes()
        {
            IList<LicenciaSolicitadaBE> licencias = new List<LicenciaSolicitadaBE>();
            try
            {

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerLicenciasSolicitadas");

                foreach (DataRow row in dt.Rows)
                {
                    LicenciaSolicitadaBE _licenciaSolicitada = new LicenciaSolicitadaBE();

                    _licenciaSolicitada.licencia = (LicenciaBE)Seleccionar(int.Parse(row["idLicencia"].ToString()));

                    int idAprobador = string.IsNullOrEmpty(row["idAprobador"].ToString()) ? 0 : int.Parse(row["idAprobador"].ToString());

                    if (idAprobador > 0)
                    {
                        UsuarioDAL _usuarioDAL = new UsuarioDAL();
                        _licenciaSolicitada.aprobador = (UsuarioBE)_usuarioDAL.Seleccionar(idAprobador);
                    }

                    int idUsuario = string.IsNullOrEmpty(row["idUsuario"].ToString()) ? 0 : int.Parse(row["idUsuario"].ToString());

                    if (idUsuario > 0)
                    {
                        UsuarioDAL _usuarioDAL = new UsuarioDAL();
                        _licenciaSolicitada.usuario = (UsuarioBE)_usuarioDAL.Seleccionar(idUsuario);
                    }

                    _licenciaSolicitada.fechaDesde = DateTime.Parse(row["fechaDesde"].ToString());
                    _licenciaSolicitada.fechaHasta = DateTime.Parse(row["fechaHasta"].ToString());
                    _licenciaSolicitada.fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString());
                    _licenciaSolicitada.dvh = row["dvh"].ToString();
                    _licenciaSolicitada.aprobada = bool.Parse(row["licenciaAprobada"].ToString());
                    _licenciaSolicitada.id = int.Parse(row["idLicenciaSolicitada"].ToString());

                    licencias.Add(_licenciaSolicitada);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return licencias;
        }

        public LicenciaSolicitadaBE SeleccionarLicenciasPendientesPorId(int idLicenciaSolicitada)
        {
            LicenciaSolicitadaBE _licenciaSolicitada = new LicenciaSolicitadaBE();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idLicenciaSolicitada", idLicenciaSolicitada.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerLicenciaSolicitadaPorId", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    _licenciaSolicitada.licencia = (LicenciaBE)Seleccionar(int.Parse(row["idLicencia"].ToString()));

                    int idAprobador = string.IsNullOrEmpty(row["idAprobador"].ToString()) ? 0 : int.Parse(row["idAprobador"].ToString());

                    if (idAprobador > 0)
                    {
                        UsuarioDAL _usuarioDAL = new UsuarioDAL();
                        _licenciaSolicitada.aprobador = (UsuarioBE)_usuarioDAL.Seleccionar(idAprobador);
                    }

                    int idUsuario = string.IsNullOrEmpty(row["idUsuario"].ToString()) ? 0 : int.Parse(row["idUsuario"].ToString());

                    if (idUsuario > 0)
                    {
                        UsuarioDAL _usuarioDAL = new UsuarioDAL();
                        _licenciaSolicitada.usuario = (UsuarioBE)_usuarioDAL.Seleccionar(idUsuario);
                    }

                    _licenciaSolicitada.fechaDesde = DateTime.Parse(row["fechaDesde"].ToString());
                    _licenciaSolicitada.fechaHasta = DateTime.Parse(row["fechaHasta"].ToString());
                    _licenciaSolicitada.fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString());
                    _licenciaSolicitada.dvh = row["dvh"].ToString();
                    _licenciaSolicitada.aprobada = bool.Parse(row["licenciaAprobada"].ToString());
                    _licenciaSolicitada.id = int.Parse(row["idLicenciaSolicitada"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }

            return _licenciaSolicitada;
        }

        public bool AsignarSolicitudAUsuario(LicenciaSolicitadaBE licenciaSolicitada, UsuarioBE usuario)
        {
            Bitacora log = new Bitacora();
            int nuevoId = 0;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                    {
                        SqlHelper.GetInstance().AddParameter("@idUsuario", usuario.id.ToString(), DbType.String),
                        SqlHelper.GetInstance().AddParameter("@idLicencia", licenciaSolicitada.licencia.id.ToString(), DbType.String),
                        SqlHelper.GetInstance().AddParameter("@fechaDesde", licenciaSolicitada.fechaDesde.ToString(), DbType.DateTime),
                        SqlHelper.GetInstance().AddParameter("@fechaHasta", licenciaSolicitada.fechaHasta.ToString(), DbType.DateTime),
                        SqlHelper.GetInstance().AddParameter("@dvh", licenciaSolicitada.dvh, DbType.String),
                    };

                nuevoId = int.Parse(SqlHelper.GetInstance().ExecuteQuery("AsignarUsuarioLicencia", parameters).Rows[0][0].ToString());

                log.mensaje = (nuevoId > 0) ? string.Format("Asignacion exitosa de lincencia para el usuario {0}", usuario.nombreUsuario)
                    : string.Format("Error intentando asignar una licencia a {0}", usuario.nombreUsuario);
                TipoEvento evento = (nuevoId > 0) ? TipoEvento.Alta : TipoEvento.Error;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return (nuevoId > 0);
        }

        public UsuarioBE EliminarLicenciasPorUsuario(LicenciaSolicitadaBE licenciaSolicitada, UsuarioBE usuario)
        {
            throw new NotImplementedException();
        }

        public UsuarioBE ObtenerLicenciaPorUsuario(UsuarioBE entidad)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                    {
                        SqlHelper.GetInstance().AddParameter("@idUsuario", entidad.id.ToString(), DbType.Int32)
                    };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerSolicitudLicenciaPorIdUsuario", parameters);

                entidad.licenciasSolicitadas = (dt.Rows.Count > 0) ? new List<LicenciaSolicitadaBE>() : null;

                foreach (DataRow row in dt.Rows)
                {
                    LicenciaSolicitadaBE _licenciaSolicitada = new LicenciaSolicitadaBE();

                    _licenciaSolicitada.licencia = (LicenciaBE)Seleccionar(int.Parse(row["idLicencia"].ToString()));

                    int idAprobador = string.IsNullOrEmpty(row["idAprobador"].ToString()) ? 0 : int.Parse(row["idAprobador"].ToString());

                    if (idAprobador > 0)
                    {
                        UsuarioDAL _usuarioDAL = new UsuarioDAL();
                        _licenciaSolicitada.aprobador = (UsuarioBE)_usuarioDAL.Seleccionar(idAprobador);
                    }

                    _licenciaSolicitada.fechaDesde = DateTime.Parse(row["fechaDesde"].ToString());
                    _licenciaSolicitada.fechaHasta = DateTime.Parse(row["fechaHasta"].ToString());
                    _licenciaSolicitada.fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString());
                    _licenciaSolicitada.usuario = entidad;
                    _licenciaSolicitada.dvh = row["dvh"].ToString();
                    _licenciaSolicitada.aprobada = bool.Parse(row["licenciaAprobada"].ToString());

                    entidad.licenciasSolicitadas.Add(_licenciaSolicitada);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public UsuarioBE ModificarLicenciaPorUsuario(LicenciaSolicitadaBE licenciaSolicitada, UsuarioBE usuario)
        {
            bool modificadoOk = false;
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                    {
                        SqlHelper.GetInstance().AddParameter("@idUsuario", licenciaSolicitada.usuario.id.ToString(), DbType.String),
                        SqlHelper.GetInstance().AddParameter("@idLicencia", licenciaSolicitada.licencia.id.ToString(), DbType.String),
                        SqlHelper.GetInstance().AddParameter("@fechaDesde", licenciaSolicitada.fechaDesde.ToString(), DbType.DateTime),
                        SqlHelper.GetInstance().AddParameter("@fechaHasta", licenciaSolicitada.fechaHasta.ToString(), DbType.DateTime),
                        SqlHelper.GetInstance().AddParameter("@licenciaAprobada", licenciaSolicitada.aprobada.ToString(), DbType.String),
                        SqlHelper.GetInstance().AddParameter("@dvh", licenciaSolicitada.dvh, DbType.String),
                    };

                if ((usuario != null))
                {
                    parameters.Add(SqlHelper.GetInstance().AddParameter("@idAprobador", usuario.id.ToString(), DbType.String));
                }

                modificadoOk = SqlHelper.GetInstance().ExecuteNonQuery("ActualizarSolicitudPorUsuarioLicencia", parameters) > 0;

                log.mensaje = (!modificadoOk) ? string.Format("Error intentando actualizar la solicitud de Licencia del usuario {0}", licenciaSolicitada.usuario.nombreUsuario)
                    : string.Format("Se ha actualizado exitosamente la solicitud de Licencia del usuario {0}", licenciaSolicitada.usuario.nombreUsuario);
                TipoEvento evento = (modificadoOk) ? TipoEvento.Modificacion : TipoEvento.Error;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return usuario;
        }
    }
}
