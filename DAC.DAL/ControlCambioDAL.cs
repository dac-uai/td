﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAC.BE;
using DAC.UTILS.DAL;
using System.Data;
using DAC.SEGURIDAD;

namespace DAC.DAL
{
    public class ControlCambioDAL
    {
        private Bitacora log = null;

        public ControlCambioDAL()
        {
            log = new Bitacora();
        }

        public List<string> ObtenerTiposEntidadesConModificaciones()
        {
            List<string> tiposEntidades = new List<string>();

            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQueryText("SELECT distinct nombreTabla FROM ControlCambios");

                foreach (DataRow row in dt.Rows)
                {
                    tiposEntidades.Add(row[0].ToString());
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return tiposEntidades;
        }

        public List<ControlCambioMapper> ObtenerModificacionesHistoricas()
        {
            List<ControlCambioMapper> listaEntidades = new List<ControlCambioMapper>();

            try
            {
                string query = @"SELECT CONVERT(int, CC.primaryKey) as idEntidad, accion, campo, session, valorNuevo, valorOriginal, fechaModificacion, U.nombreUsuario as descripcion FROM ControlCambios CC
                    LEFT JOIN Usuario U on U.idUsuario = CC.primaryKey
					WHERE campo not in ('dvh', 'idIdioma', 'idPermiso') AND accion != 'Insert'
                    ORDER BY 1,7";

                DataTable dt = SqlHelper.GetInstance().ExecuteQueryText(query);

                foreach (DataRow row in dt.Rows)
                {
                    string _campo = row["campo"].ToString();
                    string _valorOriginal = (_campo.Equals("nombre") || _campo.Equals("apellido") || _campo.Equals("nombreUsuario")) ? Encriptacion.Desencriptar(row["valorOriginal"].ToString()) : row["valorOriginal"].ToString();
                    string _valorNuevo = (_campo.Equals("nombre") || _campo.Equals("apellido") || _campo.Equals("nombreUsuario")) ? Encriptacion.Desencriptar(row["valorNuevo"].ToString()) : row["valorNuevo"].ToString();

                    listaEntidades.Add(new ControlCambioMapper()
                    {
                        idEntidad = int.Parse(row["idEntidad"].ToString()),
                        accion = row["accion"].ToString().Trim(),
                        campo = row["campo"].ToString(),
                        session = row["session"].ToString(),
                        valorNuevo = _valorNuevo,
                        valorOriginal = _valorOriginal,
                        descripcion = Encriptacion.Desencriptar(row["descripcion"].ToString()),
                        fechaModificacion = DateTime.Parse(row["fechaModificacion"].ToString())
                    });
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return listaEntidades;
        }
    }
}
