﻿using DAC.BE;
using DAC.SEGURIDAD;
using DAC.UTILS.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DAC.INTERFACES;

namespace DAC.DAL
{
    public class UsuarioDAL : BaseDAL
    {
        private Bitacora log = null;

        public UsuarioDAL()
        {
            log = new Bitacora();
        }

        public override IEntidad Crear(IEntidad entidad)
        {
            try
            {
                UsuarioBE _entidad = (UsuarioBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreUsuario", _entidad.nombreUsuario, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@pass", _entidad.password, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvh", _entidad.dvh, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@nombre", _entidad.nombre, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@apellido", _entidad.apellido, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@mail", _entidad.mail, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@fechaNacimiento", _entidad.fechaNacimiento.ToString(), DbType.DateTime)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("CrearUsuario", parameters).Rows[0][0].ToString());

                log.mensaje = (_entidad == null) ? string.Format("Error intentando dar de alta el usuario {0}", _entidad.nombreUsuario) : string.Format("Alta exitosa para el usuario {0}", _entidad.nombreUsuario);
                TipoEvento evento = (_entidad == null) ? TipoEvento.Error : TipoEvento.Alta;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public void GuardarDVV(string tabla, string dvv)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreTabla", tabla, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvv", dvv, DbType.String)
                };

                SqlHelper.GetInstance().ExecuteQuery("GuardarDVV", parameters);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
        }

        public override bool Eliminar(IEntidad entidad)
        {
            bool eliminadoOk = false;
            try
            {
                UsuarioBE _entidad = (UsuarioBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", _entidad.id.ToString(), DbType.Int32)
                };

                eliminadoOk = SqlHelper.GetInstance().ExecuteNonQuery("EliminarUsuarioPorId", parameters) > 0;

                log.mensaje = (!eliminadoOk) ? string.Format("Error intentando eliminar el usuario {0}", _entidad.nombreUsuario)
                    : string.Format("Se ha eliminado exitosamente a {0}", _entidad.nombreUsuario);
                TipoEvento evento = (entidad == null) ? TipoEvento.LoginFallido : TipoEvento.Baja;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }
            return eliminadoOk;
        }

        public override IEntidad Modificar(IEntidad entidad)
        {
            try
            {
                UsuarioBE _entidad = (UsuarioBE)entidad;

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", _entidad.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@nombreUsuario", _entidad.nombreUsuario, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@pass", _entidad.password, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@dvh", _entidad.dvh, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@nombre", _entidad.nombre, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@apellido", _entidad.apellido, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@mail", _entidad.mail, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@fechaNacimiento", _entidad.fechaNacimiento.ToString(), DbType.DateTime)
                };

                entidad.id = int.Parse(SqlHelper.GetInstance().ExecuteQuery("ActualizarUsuario", parameters).Rows[0][0].ToString());

                log.mensaje = (_entidad == null) ? string.Format("Error intentando actualizar el usuario {0}", _entidad.nombreUsuario) : string.Format("Modificacion exitosa para el usuario {0}", _entidad.nombreUsuario);
                TipoEvento evento = (_entidad == null) ? TipoEvento.Error : TipoEvento.Modificacion;

                GestorBitacora.RegistrarEvento(log, evento);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return entidad;
        }

        public UsuarioBE ObtenerUsuarioPorNombreUsuario(string nombreUsuario)
        {
            UsuarioBE usuario = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreUsuario", nombreUsuario, DbType.String)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerUsuarioPorNombreUsuario", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    usuario = new BE.UsuarioBE()
                    {
                        id = int.Parse(row["idUsuario"].ToString()),
                        nombreUsuario = Encriptacion.Desencriptar(row["nombreUsuario"].ToString()),
                        password = row["password"].ToString(),
                        erroresLogin = int.Parse(row["erroresLogin"].ToString()),
                        esBloqueado = row["esBloqueado"].ToString().Equals("1"),
                        fechaCreacion = DateTime.Parse(row["fechaAlta"].ToString()),
                        nombre = Encriptacion.Desencriptar(row["nombre"].ToString()),
                        apellido = Encriptacion.Desencriptar(row["apellido"].ToString()),
                        mail = row["mail"].ToString(),
                        fechaNacimiento = DateTime.Parse(row["fechaNacimiento"].ToString()),
                        dvh = row["dvh"].ToString()
                    };
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return usuario;
        }

        public bool ModificarFamiliaDePermisos(ComponenteBE familia)
        {
            bool todoOk = true;

            try
            {
                int a = SqlHelper.GetInstance().ExecuteNonQueryText("Delete from PatenteFamilia where idPermisoPadre = " + familia.id.ToString());

                foreach (ComponenteBE permiso in familia.ObtenerPermisos())
                {
                    int s = SqlHelper.GetInstance().ExecuteNonQueryText(string.Format("INSERT INTO PatenteFamilia values ({0},{1},GetDate())", familia.id.ToString(), permiso.id.ToString()));
                }

            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return todoOk;
        }

        public IList<UsuarioBE> ObtenerUsuariosPorTecnologiaExperiencia(TecnologiaBE tecnologia, Experiencia experiencia)
        {
            IList<UsuarioBE> _usuarios = new List<UsuarioBE>();

            try
            {

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idTecnologia", tecnologia.id.ToString(), DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@experiencia", ((int)experiencia).ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerUsuariosPorTecnologiaExperiencia", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    UsuarioBE _usuario = new UsuarioBE()
                    {
                        id = int.Parse(row["idUsuario"].ToString()),
                        nombreUsuario = Encriptacion.Desencriptar(row["nombreUsuario"].ToString()),
                        password = row["password"].ToString(),
                        erroresLogin = int.Parse(row["erroresLogin"].ToString()),
                        esBloqueado = row["esBloqueado"].ToString().Equals("1"),
                        fechaCreacion = DateTime.Parse(row["fechaAlta"].ToString()),
                        nombre = Encriptacion.Desencriptar(row["nombre"].ToString()),
                        apellido = Encriptacion.Desencriptar(row["apellido"].ToString()),
                        mail = row["mail"].ToString(),
                        fechaNacimiento = DateTime.Parse(row["fechaNacimiento"].ToString()),
                        dvh = row["dvh"].ToString()
                    };
                    _usuarios.Add(_usuario);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return _usuarios;
        }

        private IList<ComponenteBE> PermisosMapper(DataTable dt) //TODO!
        {
            IList<ComponenteBE> _permisos = new List<ComponenteBE>();

            foreach (DataRow row in dt.Rows)
            {
                ComponenteBE permisoPadre = new PermisoCompuestoBE()
                {
                    id = int.Parse(row["idPadre"].ToString()),
                    codigo = row["codigoPadre"].ToString(),
                    descripcion = row["descripcionPadre"].ToString(),
                    esPermisoBase = row["esPermisoBasePadre"].ToString() == "True",
                    fechaCreacion = DateTime.Parse(row["fechaCreacionPadre"].ToString())
                };

                bool contienePermiso = false;
                foreach (ComponenteBE permiso in _permisos)
                {
                    if (!contienePermiso)
                    {
                        contienePermiso = permiso.id == permisoPadre.id;
                    }
                }

                if (!contienePermiso) _permisos.Add(permisoPadre);

                //int idPermisoPadre = int.Parse(row["idPadre"].ToString());
                //int idPermisoHijo = int.Parse(row["idHijo"].ToString());
                //ComponenteBE permisoHijo = new PermisoBaseBE()
                //{
                //    id = idPermisoHijo,
                //    codigo = row["codigoHijo"].ToString(),
                //    descripcion = row["descripcionHijo"].ToString(),
                //    esPermisoBase = row["esPermisoBaseHijo"].ToString() == "True",
                //    fechaCreacion = DateTime.Parse(row["fechaCreacionHijo"].ToString())
                //};

                //if (idPermisoHijo == idPermisoPadre && permisoPadre.esPermisoBase && permisoHijo.esPermisoBase) _permisos.Add(permisoHijo);
            }

            foreach (ComponenteBE _permiso in _permisos)
            {
                if (!_permiso.esPermisoBase) { 
                    IList<ComponenteBE> permisosHijos = ObtenerPermisosHijosPorPermisoPadre(_permiso);
                    foreach (ComponenteBE _p in permisosHijos)
                    {
                        _permiso.Agregar(_p);
                    }

                }
                //_permisoPadre.Agregar(permisoHijo);
            }

            return _permisos;
        }

        public bool ModificarPermisos(UsuarioBE usuarioSeleccionado)
        {
            bool todoOk = true;

            try
            {
                int a = SqlHelper.GetInstance().ExecuteNonQueryText("Delete from Usuariopatente where idusuario = " + usuarioSeleccionado.id.ToString());

                foreach (ComponenteBE permiso in usuarioSeleccionado.permisos)
                {
                    int s = SqlHelper.GetInstance().ExecuteNonQueryText(string.Format("INSERT INTO UsuarioPatente values ({0},{1})", usuarioSeleccionado.id.ToString(), permiso.id.ToString()));
                }

            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return todoOk;
        }

        public bool GuardarNuevaFamilia(ComponenteBE permisoPadre, IList<ComponenteBE> permisos)
        {
            bool todoOk = true;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@codigo", permisoPadre.codigo, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@descripcion", permisoPadre.descripcion, DbType.String)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("CrearPermiso", parameters);
                int newId = int.Parse(dt.Rows[0][0].ToString());

                foreach (ComponenteBE permiso in permisos)
                {
                    int s = SqlHelper.GetInstance().ExecuteNonQueryText(string.Format("INSERT INTO PatenteFamilia values ({0},{1},getdate())", newId.ToString(), permiso.id.ToString()));
                }

            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return todoOk;
        }

        public IList<ComponenteBE> ObtenerTodosLosPermisos()
        {
            IList<ComponenteBE> _permisos = new List<ComponenteBE>();
            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerPermisos2");

                _permisos = new List<ComponenteBE>();

                foreach (DataRow row in dt.Rows)
                {
                    bool esPermisoBase = row["esPermisoBase"].ToString() == "True";

                    ComponenteBE permiso = null;

                    if (!esPermisoBase)
                    {
                        permiso = new PermisoCompuestoBE()
                        {
                            id = int.Parse(row["idPermiso"].ToString()),
                            codigo = row["codigo"].ToString(),
                            descripcion = row["descripcion"].ToString(),
                            esPermisoBase = esPermisoBase,
                            fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                        };
                    }
                    else
                    {
                        permiso = new PermisoBaseBE()
                        {
                            id = int.Parse(row["idPermiso"].ToString()),
                            codigo = row["codigo"].ToString(),
                            descripcion = row["descripcion"].ToString(),
                            esPermisoBase = esPermisoBase,
                            fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                        };
                    }
                    _permisos.Add(permiso);
                }

                foreach (ComponenteBE permiso in _permisos)
                {
                    if (!permiso.esPermisoBase)
                    {
                        foreach (ComponenteBE permisoHijo in ObtenerPermisosHijosPorPermisoPadre(permiso))
                        {
                            permiso.Agregar(permisoHijo);
                        }
                    } 
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return _permisos;
        }

        public IList<ComponenteBE> ObtenerPermisosHijosPorPermisoPadre(ComponenteBE permisoPadre)
        {
            IList<ComponenteBE> _permisos = new List<ComponenteBE>();

            List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idPermisoPadre", permisoPadre.id.ToString(), DbType.Int32)
                };

            DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerPermisosHijosPorPermisoPadreId", parameters);

            _permisos = new List<ComponenteBE>();

            foreach (DataRow row in dt.Rows)
            {
                bool esPermisoBase = row["esPermisoBase"].ToString() == "True";

                ComponenteBE permiso = null;

                if (!esPermisoBase)
                {
                    permiso = new PermisoCompuestoBE()
                    {
                        id = int.Parse(row["idPermiso"].ToString()),
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        esPermisoBase = esPermisoBase,
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    };

                    foreach (ComponenteBE permisoHijo in ObtenerPermisosHijosPorPermisoPadre(permiso))
                    {
                        permiso.Agregar(permisoHijo);
                    }
                }
                else
                {
                    permiso = new PermisoBaseBE()
                    {
                        id = int.Parse(row["idPermiso"].ToString()),
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        esPermisoBase = esPermisoBase,
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    };
                }
                _permisos.Add(permiso);
            }

            return _permisos;
        }

        public IList<string> VerificarDvv(IDictionary<string, string> valores)
        {
            IList<string> errores = new List<string>();
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreTabla", "Usuario", DbType.String)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerDvvPorTabla");

                foreach (DataRow row in dt.Rows)
                {
                    if (!valores[row[0].ToString()].Equals(row[1].ToString().Trim()))
                    {
                        errores.Add(row[0].ToString());
                    }
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return errores;
        }

        public IList<ComponenteBE> ObtenerPermisosPorUsuario(UsuarioBE usuario)
        {
            IList<ComponenteBE> _permisos = new List<ComponenteBE>();
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", usuario.id.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerPermisosPorIdUsuario", parameters);

                _permisos = PermisosMapper(dt);
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return _permisos;
        }

        public override IEntidad Seleccionar(int idEntidad)
        {
            IEntidad usuario = null;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", idEntidad.ToString(), DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerUsuarioPorId", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    usuario = new BE.UsuarioBE()
                    {
                        id = int.Parse(row["idUsuario"].ToString()),
                        nombreUsuario = Encriptacion.Desencriptar(row["nombreUsuario"].ToString()),
                        password = row["password"].ToString(),
                        erroresLogin = int.Parse(row["erroresLogin"].ToString()),
                        esBloqueado = row["esBloqueado"].ToString().Equals("1"),
                        fechaCreacion = DateTime.Parse(row["fechaAlta"].ToString()),
                        nombre = Encriptacion.Desencriptar(row["nombre"].ToString()),
                        apellido = Encriptacion.Desencriptar(row["apellido"].ToString()),
                        mail = row["mail"].ToString(),
                        fechaNacimiento = DateTime.Parse(row["fechaNacimiento"].ToString()),
                        dvh = row["dvh"].ToString()
                    };
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return usuario;
        }

        public override IList<IEntidad> SeleccionarTodos()
        {
            IList<IEntidad> _usuarios = new List<IEntidad>();

            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerUsuarios");

                foreach (DataRow row in dt.Rows)
                {
                    UsuarioBE _usuario = new UsuarioBE()
                    {
                        id = int.Parse(row["idUsuario"].ToString()),
                        nombreUsuario = Encriptacion.Desencriptar(row["nombreUsuario"].ToString()),
                        password = row["password"].ToString(),
                        erroresLogin = int.Parse(row["erroresLogin"].ToString()),
                        esBloqueado = row["esBloqueado"].ToString().Equals("1"),
                        fechaCreacion = DateTime.Parse(row["fechaAlta"].ToString()),
                        nombre = Encriptacion.Desencriptar(row["nombre"].ToString()),
                        apellido = Encriptacion.Desencriptar(row["apellido"].ToString()),
                        mail = row["mail"].ToString(),
                        fechaNacimiento = DateTime.Parse(row["fechaNacimiento"].ToString()),
                        dvh = row["dvh"].ToString()
                    };
                    _usuarios.Add(_usuario);
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return _usuarios;
        }

        public UsuarioBE SeleccionarPorNombreAndPassword(string nombreUsuario, string password)
        {
            UsuarioBE usuario = null;
            try
            {

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@nombreUsuario", nombreUsuario, DbType.String),
                    SqlHelper.GetInstance().AddParameter("@password", password, DbType.String)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerUsuarioPorNombrePassword", parameters);

                foreach (DataRow row in dt.Rows)
                {
                    usuario = new BE.UsuarioBE()
                    {
                        id = int.Parse(row["idUsuario"].ToString()),
                        nombreUsuario = Encriptacion.Desencriptar(row["nombreUsuario"].ToString()),
                        password = row["password"].ToString(),
                        erroresLogin = int.Parse(row["erroresLogin"].ToString()),
                        esBloqueado = row["esBloqueado"].ToString().Equals("1"),
                        fechaCreacion = DateTime.Parse(row["fechaAlta"].ToString()),
                        nombre = Encriptacion.Desencriptar(row["nombre"].ToString()),
                        apellido = Encriptacion.Desencriptar(row["apellido"].ToString()),
                        mail = row["mail"].ToString(),
                        fechaNacimiento = DateTime.Parse(row["fechaNacimiento"].ToString()),
                        dvh = row["dvh"].ToString()
                    };
                }
            }
            catch (Exception e)
            {
                log.mensaje = e.Message.ToString();
                GestorBitacora.RegistrarEvento(log, TipoEvento.Error);
                throw e;
            }

            return usuario;
        }
    }
}

