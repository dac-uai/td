﻿using DAC.BLL;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAC.BE;
using Moq;

namespace DAL.BLL.TEST
{
    [TestClass()]
    public class ClienteBLL_Test
    {
        [TestMethod()]
        public void CreatUserTest()
        {
            var mock = new Mock<ClienteBLL>();
            //arrange
            ClienteBE cliente = new ClienteBE() {
                descripcion = "Un cliente"
            };
            //act
            mock.Setup(x => x.CalcularVerificacionHorizontal(cliente)).Returns("qqqq");
            mock.Setup(x => x.Guardar(cliente)).Returns(cliente);

            //assert
            Assert.AreEqual("1", "0");
        }
    }
}
