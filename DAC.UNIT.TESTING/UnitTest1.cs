﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAC.BE;
using System.Globalization;
using DAC.BLL;
using DAC.SEGURIDAD;

namespace DAC.UNIT.TESTING
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestUsuarioToString()
        {
            UsuarioBE usuario = new UsuarioBE()
            {
                nombre = "Diego",
                apellido = "Arce",
                dvh = "123456789876543",
                id = 10,
                mail = "arcedie@gmail.com",
                fechaCreacion = DateTime.ParseExact("20/10/1987", "dd/MM/yyyy", CultureInfo.InvariantCulture),
                fechaNacimiento = DateTime.ParseExact("20/10/1987", "dd/MM/yyyy", CultureInfo.InvariantCulture),
                erroresLogin = 0,
                esBloqueado = false,
                nombreUsuario = "darce",
                password = "12345678"
            };

            string actual = usuario.ToString();
            string expected = "idioma -  \nnombreUsuario - darce \nesBloqueado - False \npassword - 12345678 \nid - 10 \nnombre - Diego \napellido - Arce \nmail - arcedie@gmail.com \nfechaNacimiento - 10/20/1987 12:00:00 AM \n";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSessionUsuario()
        {
            UsuarioBLL asd = UsuarioBLL.GetInstance();

            UsuarioBE usuarioExpected = new UsuarioBE()
            {
                nombre = "Diego",
                apellido = "Arce",
                dvh = "123456789876543",
                id = 10,
                mail = "arcedie@gmail.com",
                fechaCreacion = DateTime.ParseExact("20/10/1987", "dd/MM/yyyy", CultureInfo.InvariantCulture),
                fechaNacimiento = DateTime.ParseExact("20/10/1987", "dd/MM/yyyy", CultureInfo.InvariantCulture),
                erroresLogin = 0,
                esBloqueado = false,
                nombreUsuario = "darce",
                password = "12345678"
            };

            GestorSession.GetInstance().ActualizarUsuarioEnSesion(usuarioExpected);
            UsuarioBE usuarioEnSession = GestorSession.GetInstance().sessionUsuario;

            Assert.AreEqual(usuarioExpected, usuarioEnSession);
        }
    }
}
