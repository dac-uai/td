﻿using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public abstract class ComponenteBE : IEntidad
    {
        public int id { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public bool esPermisoBase { get; set; }
        public DateTime fechaCreacion { get; set; }

        public abstract IList<ComponenteBE> ObtenerPermisos();
        public abstract void Remover(ComponenteBE componente);
        public abstract void Agregar(ComponenteBE componente);
    }
}
