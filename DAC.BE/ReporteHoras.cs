﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class ReporteHoras
    {
        public ProyectoBE proyecto { get; set; }
        public UsuarioBE usuario { get; set; }
        public int horas { get; set; }
        public DateTime fecha { get; set; }
        public string comentarios { get; set; }
    }
}
