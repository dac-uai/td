﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class PermisoCompuestoBE : ComponenteBE
    {
        private IList<ComponenteBE> permisos = new List<ComponenteBE>();


        public PermisoCompuestoBE(string Codigo, string Descripcion)
        {
            base.codigo = codigo;
            base.descripcion = descripcion;
        }

        //public PermisoCompuestoBE(string Codigo, string Descripcion, Usuario usuario)
        //{
        //    base.codigo = codigo;
        //    base.descripcion = descripcion;
        //    base.usuario = usuario;

        //}

        public PermisoCompuestoBE() { }

        public override IList<ComponenteBE> ObtenerPermisos()
        {
            return permisos;
        }

        public override void Agregar(ComponenteBE componente)
        {
            permisos.Add(componente);
        }
        public override void Remover(ComponenteBE componente)
        {
            permisos.Remove(componente);
        }
    }
}
