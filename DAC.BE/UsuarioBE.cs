﻿using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace DAC.BE
{
    public class UsuarioBE : INTERFACES.IUsuario
    {
        public IdiomaBE idioma { get; set; }
        public string nombreUsuario { get; set; }
        public bool esBloqueado { get; set; }
        public string password { get; set; }
        public int id { get; set; }
        [NoVerificable()]
        public DateTime fechaCreacion { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string mail { get; set; }
        public DateTime fechaNacimiento { get; set; }

        [NoVerificable()]
        public string dvh { get; set; }
        [NoVerificable()]
        public IList<ComponenteBE> permisos { get; set; }
        [NoVerificable()]
        public int erroresLogin { get; set; }
        [NoVerificable()]
        public IList<AptitudBE> aptitudes { get; set; }
        [NoVerificable()]
        public IList<LicenciaSolicitadaBE> licenciasSolicitadas { get; set; }

        public override string ToString()
        {
            return nombreUsuario;
        }

        public string ToStringCompleto()
        {
            string valor = string.Empty;
            IEnumerable<PropertyInfo> fields = this.GetType().GetRuntimeProperties();

            foreach (PropertyInfo field in fields)
            {
                var customAttributes = field.GetCustomAttributes();
                bool isNoVerificable = customAttributes.Any(x => x.GetType().Name == "NoVerificable");

                if (!isNoVerificable)
                {
                    valor += string.Format("{0} - {1} \n", field.Name, field.GetValue(this));
                }
            }
            return valor;
        }
    }
}