﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NoVerificable : Attribute
    {
        public NoVerificable()
        {
        }
    }
}
