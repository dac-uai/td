﻿using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class TecnologiaBE : IEntidad
    {
        public int id { get; set; }
        public DateTime fechaCreacion { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }
    }
}
