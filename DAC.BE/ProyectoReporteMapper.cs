﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class ProyectoReporteMapper
    {
        public string nombreCliente { get; set; }
        public string nombreProyecto { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }
        public int presupuesto { get; set; }
        public int gastoEstimado { get; set; }
        public int gananciaEstimada { get; set; }
        public int gastoReal { get; set; }
        public int gananciaReal { get; set; }
    }
}
