﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
   public class ControlCambioMapper
    {
        public int idEntidad { get; set; }
        public string accion { get; set; }
        public string campo { get; set; }
        public string valorOriginal { get; set; }
        public string valorNuevo { get; set; }
        public DateTime fechaModificacion { get; set; }
        public string session { get; set; }
        public string descripcion { get; set; }
    }
}
