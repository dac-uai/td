﻿using DAC.INTERFACES;
using System;

namespace DAC.BE
{
    public class ProyectoBE : IEntidad, IDvh
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }
        public int presupuestoInicial { get; set; }
        public ClienteBE cliente { get; set; }

        [NoVerificable()]
        public DateTime fechaCreacion { get; set; }
        [NoVerificable()]
        public string dvh { get; set; }
    }
}
