﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class LicenciaSolicitadaMapper
    {
        public int idLicenciaSolicitada { get; set; }
        public int idUsuario { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string descripcionLicencia { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }
        public bool licenciaAprobada { get; set; }
    }
}
