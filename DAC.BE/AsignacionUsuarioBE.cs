﻿using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class AsignacionUsuarioBE : IEntidad, IDvh
    {
        public int id { get; set; }
        public ProyectoBE proyecto { get; set; }
        public UsuarioBE usuario { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }
        public int costoHora { get; set; }

        [NoVerificable()]
        public DateTime fechaCreacion { get; set; }
        [NoVerificable()]
        public string dvh { get; set; }
    }
}
