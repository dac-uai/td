﻿using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class ClienteBE : IEntidad, IDvh
    {
        public int id { get; set; }
        public string razonSocial { get; set; }
        public string descripcion { get; set; }
        public string cuit { get; set; }
        public bool activo { get; set; }
        public string nombreContacto { get; set; }
        public string telefonoContacto { get; set; }
        public string mailContacto { get; set; }

        [NoVerificable()]
        public DateTime fechaCreacion { get; set; }
        [NoVerificable()]
        public string dvh { get; set; }
    }
}
