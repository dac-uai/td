﻿using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class LicenciaSolicitadaBE : IEntidad, IDvh
    {
        public int id { get; set; }
        public UsuarioBE usuario { get; set; }
        public LicenciaBE licencia { get; set; }
        public bool aprobada { get; set; }
        public UsuarioBE aprobador { get; set; }
        public DateTime fechaDesde { get; set; }
        public DateTime fechaHasta { get; set; }

        [NoVerificable()]
        public string dvh { get; set; }

        [NoVerificable()]
        public DateTime fechaCreacion { get; set; }
    }
}
