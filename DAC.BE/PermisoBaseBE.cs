﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BE
{
    public class PermisoBaseBE : ComponenteBE
    {
        public override void Agregar(ComponenteBE componente)
        {
        }

        public override IList<ComponenteBE> ObtenerPermisos()
        {
            return new List<ComponenteBE>();
        }

        public override void Remover(ComponenteBE componente)
        {

        }
    }
}
