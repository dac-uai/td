﻿using DAC.BE;
using DAC.DAL;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BLL
{
    public class ProyectoBLL : VerificarBLL<ProyectoBE>, ICRUD<ProyectoBE>
    {
        private static ProyectoBLL _instance;
        private ProyectoDAL _proyectoDal;
        private AsignacionUsuarioBLL _asignacionUsuarioBLL;

        private ProyectoBLL()
        {
            _proyectoDal = new ProyectoDAL();
            _asignacionUsuarioBLL = AsignacionUsuarioBLL.GetInstance();
        }

        public static ProyectoBLL GetInstance()
        {
            if (_instance == null) _instance = new ProyectoBLL();
            return _instance;
        }

        public ProyectoBE Crear(ProyectoBE entidad)
        {
            return (ProyectoBE)_proyectoDal.Crear(entidad);
        }

        public bool Eliminar(ProyectoBE entidad)
        {
            return _proyectoDal.Eliminar(entidad);
        }

        public ProyectoBE Modificar(ProyectoBE entidad)
        {
            entidad.dvh = CalcularVerificacionHorizontal(entidad);
            return (ProyectoBE)_proyectoDal.Modificar(entidad);
        }

        public void RecalcularDVV()
        {
            try
            {
                string dvv = ObtenerDVV();

                _proyectoDal.GuardarDVV("Proyecto", dvv);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ObtenerDVV()
        {
            string valor = string.Empty;

            try
            {
                IList<ProyectoBE> todos = SeleccionarTodos();

                foreach (ProyectoBE proyecto in todos)
                {
                    valor += proyecto.dvh;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Encriptacion.GenerarHash(valor);
        }

        public ProyectoBE Seleccionar(int idEntidad)
        {
            return (ProyectoBE)_proyectoDal.Seleccionar(idEntidad);
        }

        public IList<ProyectoBE> SeleccionarTodos()
        {
            IList<ProyectoBE> proyectos = new List<ProyectoBE>();

            foreach (IEntidad entidad in _proyectoDal.SeleccionarTodos())
            {
                proyectos.Add((ProyectoBE)entidad);
            }

            return proyectos;
        }

        public IList<ProyectoBE> SeleccionarPorCliente(ClienteBE cliente)
        {
            return _proyectoDal.SeleccionarPorIdCliente(cliente.id);
        }

        public ProyectoBE Guardar(ProyectoBE proyecto)
        {
            try
            {
                IList<ProyectoBE> todas = SeleccionarTodos();
                bool existeProyecto = todas.Any(x => x.id == proyecto.id);

                if (existeProyecto)
                {
                    ProyectoBE ProyectoExistente = todas.First(x => x.id == proyecto.id);
                    proyecto.fechaCreacion = ProyectoExistente.fechaCreacion;
                }


                proyecto = existeProyecto ? Modificar(proyecto) : Crear(proyecto);
            }
            catch (Exception)
            {
                throw;
            }

            return proyecto;
        }

        public bool VerificarIdentidad()
        {
            IList<ProyectoBE> proyectos = SeleccionarTodos();
            bool identidadOk = true;
            foreach (ProyectoBE proyecto in proyectos)
            {
                if (!identidadOk) return identidadOk;

                identidadOk = proyecto.dvh == CalcularVerificacionHorizontal(proyecto);
            }

            return identidadOk;
        }

        public void RecalcularDVHTodos()
        {
            IList<ProyectoBE> proyectos = SeleccionarTodos();

            foreach (ProyectoBE proyecto in proyectos)
            {
                Modificar(proyecto);
            }
        }

        public IList<ProyectoReporteMapper> ObtenerReporteMapper()
        {
            IList<ProyectoReporteMapper> mappers = new List<ProyectoReporteMapper>();
            try
            {
                IList<ProyectoBE> proyectos = SeleccionarTodos();
                foreach (ProyectoBE proyecto in proyectos)
                {
                    IList<AsignacionUsuarioBE> asignados = _asignacionUsuarioBLL.SeleccionarPorProyecto(proyecto);
                    IList<ReporteHoras> reporteHoras = _asignacionUsuarioBLL.ObtenerRegistroHorasPorProyecto(proyecto);

                    int gastoEstimado = 0;
                    int gastoReal = 0;

                    foreach (AsignacionUsuarioBE asignado in asignados)
                    {
                        TimeSpan periodo = (asignado.fechaHasta - asignado.fechaDesde);
                        gastoEstimado += int.Parse((periodo.Days * 0.7 * 8 * asignado.costoHora).ToString().Split('.')[0]);
                    }

                    foreach (ReporteHoras reporte in reporteHoras)
                    {
                        foreach (AsignacionUsuarioBE asignado in asignados)
                        {
                            TimeSpan periodo = (asignado.fechaHasta - asignado.fechaDesde);
                            if (asignado.fechaDesde <= reporte.fecha && asignado.fechaHasta >= reporte.fecha)
                            {
                                gastoReal += asignado.costoHora * reporte.horas;
                            }
                        }
                    }

                    ProyectoReporteMapper mapper = new ProyectoReporteMapper()
                    {
                        fechaDesde = proyecto.fechaDesde,
                        fechaHasta = proyecto.fechaHasta,
                        nombreCliente = proyecto.cliente.razonSocial,
                        nombreProyecto = proyecto.nombre,
                        presupuesto = proyecto.presupuestoInicial,
                        gastoEstimado = gastoEstimado,
                        gananciaEstimada = proyecto.presupuestoInicial - gastoEstimado,
                        gastoReal = gastoReal,
                        gananciaReal = proyecto.presupuestoInicial - gastoReal
                    };

                    mappers.Add(mapper);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return mappers;
        }
    }
}
