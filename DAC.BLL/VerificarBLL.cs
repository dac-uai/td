﻿using DAC.BE;
using DAC.DAL;
using DAC.INTERFACES;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BLL
{
    public abstract class VerificarBLL<T>
    {
        public string CalcularVerificacionHorizontal(T entidad)
        {
            string dvh = string.Empty;

            try
            {
                Type fieldsType = typeof(T);
                IEnumerable<PropertyInfo> fields = fieldsType.GetRuntimeProperties();

                foreach (PropertyInfo field in fields)
                {
                    dvh += CalcularPropiedad(entidad, field);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return SEGURIDAD.Encriptacion.GenerarHash(dvh);
        }

        public bool VerificarHorizontal(T entidad, string dvh)
        {
            return CalcularVerificacionHorizontal(entidad) == dvh;
        }

        private string CalcularPropiedad(T entidad, PropertyInfo field)
        {
            int total = 0;
            string valor = string.Empty;
            try
            {
                var customAttributes = field.GetCustomAttributes();
                bool isNoVerificable = customAttributes.Any(x => x.GetType().Name == "NoVerificable");
                if (!isNoVerificable)
                {
                    //La propiedad es de tipo Entidad
                    if (typeof(IEntidad).IsAssignableFrom(field.PropertyType))
                    {
                        var _entidad = (field.GetValue(entidad) as IEntidad);
                        if (_entidad != null)
                        {
                            valor = _entidad.id.ToString();
                        }
                    }
                    else
                    {
                        valor = field.GetValue(entidad).ToString();
                    }

                    if (!string.IsNullOrEmpty(valor))
                    {
                        total += valor.GetHashCode();
                        valor = SEGURIDAD.Encriptacion.GenerarHash(total.ToString());
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return valor;
        }

        public bool VerificarVertical(T entidad)
        {
            try
            {
                //Assembly.GetAssembly(entidad.GetType());
                //DAL.FactoryDAL < entidad.GetType()>.k
                var prueba = 2;
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
    }
}
