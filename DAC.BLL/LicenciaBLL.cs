﻿using DAC.BE;
using DAC.DAL;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BLL
{
    public class LicenciaBLL : VerificarBLL<LicenciaSolicitadaBE>, ICRUD<LicenciaBE>
    {
        private static LicenciaBLL _instance = null;
        private LicenciaDAL _LicenciaDal = null;

        private LicenciaBLL()
        {
            _LicenciaDal = new LicenciaDAL();
        }

        public static LicenciaBLL GetInstance()
        {
            if (_instance == null) _instance = new LicenciaBLL();
            return _instance;
        }

        public LicenciaBE Crear(LicenciaBE entidad)
        {
            return (LicenciaBE)_LicenciaDal.Crear(entidad);
        }

        public bool Eliminar(LicenciaBE entidad)
        {
            return _LicenciaDal.Eliminar(entidad);
        }

        public LicenciaBE Modificar(LicenciaBE entidad)
        {
            return (LicenciaBE)_LicenciaDal.Modificar(entidad);
        }

        public LicenciaBE Seleccionar(int idEntidad)
        {
            return (LicenciaBE)_LicenciaDal.Seleccionar(idEntidad);
        }

        public void RecalcularDVV()
        {
            try
            {
                string dvv = ObtenerDVV();

                _LicenciaDal.GuardarDVV("LicenciaSolicitada", dvv);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ObtenerDVV()
        {
            string valor = string.Empty;

            try
            {
                IList<LicenciaSolicitadaBE> todos = SeleccionarLicenciasPendientes();

                foreach (LicenciaSolicitadaBE licencia in todos)
                {
                    valor += licencia.dvh;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Encriptacion.GenerarHash(valor);
        }

        public IList<LicenciaBE> SeleccionarTodos()
        {
            IList<LicenciaBE> licencias = new List<LicenciaBE>();
            foreach (IEntidad entidad in _LicenciaDal.SeleccionarTodos())
            {
                licencias.Add((LicenciaBE)entidad);
            }
            return licencias;
        }

        public LicenciaBE Guardar(LicenciaBE entidad)
        {
            try
            {
                IList<LicenciaBE> todas = SeleccionarTodos();
                bool existeLicencia = todas.Any(x => x.id == entidad.id);

                if (existeLicencia)
                {
                    LicenciaBE LicenciaExistente = todas.First(x => x.id == entidad.id);
                    entidad.fechaCreacion = LicenciaExistente.fechaCreacion;
                }

                entidad = existeLicencia ? Modificar(entidad) : Crear(entidad);
            }
            catch (Exception)
            {
                throw;
            }

            return entidad;
        }

        public bool AsignarSolicitudAUsuario(LicenciaSolicitadaBE licenciaSolicitada, ref UsuarioBE usuario)
        {
            bool asignacionOk = false;
            try
            {
                licenciaSolicitada.dvh = CalcularVerificacionHorizontal(licenciaSolicitada);
                asignacionOk = _LicenciaDal.AsignarSolicitudAUsuario(licenciaSolicitada, usuario);

                if (asignacionOk) CargarLicenciaPorUsuario(ref usuario);
            }
            catch (Exception)
            {
                throw;
            }
            return asignacionOk;
        }

        public void CargarLicenciaPorUsuario(ref UsuarioBE usuario)
        {
            try
            {
                usuario = _LicenciaDal.ObtenerLicenciaPorUsuario(usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void EliminarLicenciasPorUsuario(LicenciaSolicitadaBE licenciaSolicitada, ref UsuarioBE usuario)
        {
            try
            {
                usuario = _LicenciaDal.EliminarLicenciasPorUsuario(licenciaSolicitada, usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ModificarLicenciaPorUsuario(LicenciaSolicitadaBE licenciaSolicitada, ref UsuarioBE usuario)
        {
            try
            {
                licenciaSolicitada.dvh = CalcularVerificacionHorizontal(licenciaSolicitada);
                usuario = _LicenciaDal.ModificarLicenciaPorUsuario(licenciaSolicitada, usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<LicenciaSolicitadaMapper> SeleccionarLicenciasPendientesMapper()
        {
            try
            {
                return _LicenciaDal.SeleccionarLicenciasPendientesMapper();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<LicenciaSolicitadaBE> SeleccionarLicenciasPendientes()
        {
            try
            {
                return _LicenciaDal.SeleccionarLicenciasPendientes();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public LicenciaSolicitadaBE SeleccionarLicenciasPendientesPorId(int idUsuario)
        {
            try
            {
                return _LicenciaDal.SeleccionarLicenciasPendientesPorId(idUsuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool VerificarIdentidad()
        {
            IList<LicenciaSolicitadaBE> licenciasSolicitadas = SeleccionarLicenciasPendientes();
            bool identidadOk = true;
            foreach (LicenciaSolicitadaBE licencia in licenciasSolicitadas)
            {
                if (!identidadOk) return identidadOk;

                identidadOk = licencia.dvh == CalcularVerificacionHorizontal(licencia);
            }

            return identidadOk;
        }

        public void RecalcularDVHTodos()
        {
            IList<LicenciaSolicitadaBE> licencias = SeleccionarLicenciasPendientes();

            foreach (LicenciaSolicitadaBE licencia in licencias)
            {
                UsuarioBE usuario = licencia.aprobador;
                ModificarLicenciaPorUsuario(licencia, ref usuario);
            }
        }
    }
}
