﻿using DAC.BE;
using DAC.DAL;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BLL
{
    public class AsignacionUsuarioBLL : VerificarBLL<AsignacionUsuarioBE>, ICRUD<AsignacionUsuarioBE>
    {
        private static AsignacionUsuarioBLL _instance;
        private AsignacionUsuarioDAL _AsignacionUsuarioDal;

        private AsignacionUsuarioBLL()
        {
            _AsignacionUsuarioDal = new AsignacionUsuarioDAL();
        }

        public static AsignacionUsuarioBLL GetInstance()
        {
            if (_instance == null) _instance = new AsignacionUsuarioBLL();
            return _instance;
        }

        public AsignacionUsuarioBE Crear(AsignacionUsuarioBE entidad)
        {
            entidad.dvh = CalcularVerificacionHorizontal(entidad);
            return (AsignacionUsuarioBE)_AsignacionUsuarioDal.Crear(entidad);
        }

        public bool Eliminar(AsignacionUsuarioBE entidad)
        {
            return _AsignacionUsuarioDal.Eliminar(entidad);
        }

        public AsignacionUsuarioBE Modificar(AsignacionUsuarioBE entidad)
        {
            entidad.dvh = CalcularVerificacionHorizontal(entidad);
            RecalcularDVV();
            return (AsignacionUsuarioBE)_AsignacionUsuarioDal.Modificar(entidad);
        }

        public void RecalcularDVV()
        {
            try
            {
                string dvv = ObtenerDVV();

                _AsignacionUsuarioDal.GuardarDVV("AsignacionUsuario", dvv);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ObtenerDVV()
        {
            string valor = string.Empty;

            try
            {
                IList<AsignacionUsuarioBE> todos = SeleccionarTodos();

                foreach (AsignacionUsuarioBE AsignacionUsuario in todos)
                {
                    valor += AsignacionUsuario.dvh;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Encriptacion.GenerarHash(valor);
        }

        public AsignacionUsuarioBE Seleccionar(int idEntidad)
        {
            return (AsignacionUsuarioBE)_AsignacionUsuarioDal.Seleccionar(idEntidad);
        }

        public IList<AsignacionUsuarioBE> SeleccionarTodos()
        {
            IList<AsignacionUsuarioBE> asignaciones = new List<AsignacionUsuarioBE>();

            foreach (IEntidad entidad in _AsignacionUsuarioDal.SeleccionarTodos())
            {
                asignaciones.Add((AsignacionUsuarioBE)entidad);
            }

            return asignaciones;
        }

        public AsignacionUsuarioBE Guardar(AsignacionUsuarioBE AsignacionUsuario)
        {
            try
            {
                IList<AsignacionUsuarioBE> todas = SeleccionarTodos();
                bool existeAsignacionUsuario = todas.Any(x => x.id == AsignacionUsuario.id);

                if (existeAsignacionUsuario)
                {
                    AsignacionUsuarioBE AsignacionUsuarioExistente = todas.First(x => x.id == AsignacionUsuario.id);
                    AsignacionUsuario.fechaCreacion = AsignacionUsuarioExistente.fechaCreacion;
                }


                AsignacionUsuario = existeAsignacionUsuario ? Modificar(AsignacionUsuario) : Crear(AsignacionUsuario);
                RecalcularDVV();
            }
            catch (Exception)
            {
                throw;
            }

            return AsignacionUsuario;
        }

        public bool VerificarIdentidad()
        {
            IList<AsignacionUsuarioBE> AsignacionUsuarios = SeleccionarTodos();
            bool identidadOk = true;
            foreach (AsignacionUsuarioBE AsignacionUsuario in AsignacionUsuarios)
            {
                if (!identidadOk) return identidadOk;

                identidadOk = AsignacionUsuario.dvh == CalcularVerificacionHorizontal(AsignacionUsuario);
            }

            return identidadOk;
        }

        public void RecalcularDVHTodos()
        {
            IList<AsignacionUsuarioBE> AsignacionUsuarios = SeleccionarTodos();

            foreach (AsignacionUsuarioBE AsignacionUsuario in AsignacionUsuarios)
            {
                Modificar(AsignacionUsuario);
            }
        }

        public IList<AsignacionUsuarioBE> SeleccionarPorCliente(ClienteBE cliente)
        {
            return _AsignacionUsuarioDal.SeleccionarPorIdCliente(cliente.id);
        }

        public IList<AsignacionUsuarioBE> SeleccionarPorProyecto(ProyectoBE proyecto)
        {
            return _AsignacionUsuarioDal.SeleccionarPorIdProyecto(proyecto.id);
        }

        public IList<AsignacionUsuarioBE> SeleccionarPorUsuario(UsuarioBE usuario)
        {
            return _AsignacionUsuarioDal.SeleccionarPorIdUsuario(usuario.id);
        }

        public IList<AsignacionUsuarioMapper> SeleccionarPorProyectoMapper(ProyectoBE proyecto)
        {
            return _AsignacionUsuarioDal.SeleccionarPorProyectoMapper(proyecto);
        }

        public AsignacionUsuarioBE SeleccionarPorUsuarioProyectoFecha(UsuarioBE usuario, DateTime fechaDesde, DateTime fechaHasta, ProyectoBE proyecto)
        {
            return _AsignacionUsuarioDal.SeleccionarPorUsuarioProyectoFecha(usuario, fechaDesde, fechaHasta, proyecto);
        }

        public bool RegistarHorasPorProyecto(ReporteHoras reporte)
        {
            return _AsignacionUsuarioDal.RegistrarHorasPorProyecto(reporte);
        }

        public IList<ReporteHoras> ObtenerRegistroHorasPorProyecto(ProyectoBE proyecto)
        {
            return _AsignacionUsuarioDal.ObtenerRegistroHorasPorProyecto(proyecto);
        }
    }
}
