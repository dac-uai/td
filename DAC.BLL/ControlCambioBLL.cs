﻿using DAC.BE;
using DAC.DAL;
using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BLL
{
    public class ControlCambioBLL
    {
        private ControlCambioDAL _controlCambioDal = null;

        public ControlCambioBLL()
        {
            _controlCambioDal = new ControlCambioDAL();
        }

        public List<string> ObtenerTiposEntidadesConModificaciones()
        {
            return _controlCambioDal.ObtenerTiposEntidadesConModificaciones();
        }

        public List<ControlCambioMapper> ObtenerModificacionesHistoricas()
        {
            return _controlCambioDal.ObtenerModificacionesHistoricas();
        }

        public UsuarioBE ObtenerEntidadPorMappers(IList<ControlCambioMapper> mappers)
        {
            UsuarioBE entidad = null;
            if (mappers.Any())
            {
                UsuarioDAL _dal = new UsuarioDAL();

                entidad = (UsuarioBE)_dal.Seleccionar(mappers.First().idEntidad);
                IEnumerable<PropertyInfo> fields = entidad.GetType().GetRuntimeProperties();

                foreach (ControlCambioMapper mapper in mappers)
                {
                    foreach (PropertyInfo field in fields)
                    {
                        if (mapper.campo == field.Name)
                        {
                            dynamic valorCampo = mapper.valorOriginal;

                            if (field.Name == "fechaNacimiento") valorCampo = DateTime.Parse(valorCampo.ToString());

                            field.SetValue(entidad, valorCampo);
                        }
                    }
                }
            }

            return entidad;
        }
    }
}