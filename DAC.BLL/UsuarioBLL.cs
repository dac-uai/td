﻿using DAC.BE;
using DAC.DAL;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BLL
{
    public class UsuarioBLL : VerificarBLL<UsuarioBE>, ICRUD<UsuarioBE>
    {
        private static UsuarioBLL _instance = null;
        private UsuarioDAL _usuarioDal = null;
        private UsuarioBLL()
        {
            _usuarioDal = new UsuarioDAL();
        }

        public static UsuarioBLL GetInstance()
        {
            if (_instance == null) _instance = new UsuarioBLL();
            return _instance;
        }

        public IUsuario ValidarUsuario(string nombreUsuario, string password)
        {
            IUsuario _usuario = null;
            try
            {
                nombreUsuario = Encriptacion.Encriptar(nombreUsuario);
                password = Encriptacion.GenerarHash(password);
                _usuario = _usuarioDal.SeleccionarPorNombreAndPassword(nombreUsuario, password);
            }
            catch (Exception)
            {
                throw;
            }

            return _usuario;
        }

        public void RecalcularDVV()
        {
            try
            {
                string dvv = ObtenerDVV();

                _usuarioDal.GuardarDVV("Usuario", dvv);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ObtenerDVV()
        {
            string valor = string.Empty;

            try
            {
                IList<UsuarioBE> todos = SeleccionarTodos();

                foreach (UsuarioBE usuario in todos)
                {
                    valor += usuario.dvh;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Encriptacion.GenerarHash(valor);
        }

        public void CargarIdioma(ref UsuarioBE usuario)
        {
            try
            {
                usuario.idioma = GestorIdioma.GetInstance().ObtenerIdiomaPorUsuario(usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public UsuarioBE Crear(UsuarioBE entidad)
        {
            entidad.password = Encriptacion.GenerarHash(entidad.password);
            entidad.nombreUsuario = Encriptacion.Encriptar(entidad.nombreUsuario);
            entidad.nombre = Encriptacion.Encriptar(entidad.nombre);
            entidad.apellido = Encriptacion.Encriptar(entidad.apellido);
            entidad.dvh = CalcularVerificacionHorizontal(entidad);

            entidad = (UsuarioBE)_usuarioDal.Crear(entidad);

            RecalcularDVV();

            return entidad;
        }

        public UsuarioBE Modificar(UsuarioBE entidad)
        {
            entidad = (UsuarioBE)_usuarioDal.Modificar(entidad);

            RecalcularDVV();

            return entidad;
        }

        public UsuarioBE Modificar(UsuarioBE entidad, bool modificarPass, string originalPass)
        {
            entidad.dvh = CalcularVerificacionHorizontal(entidad);
            entidad.password = modificarPass ? Encriptacion.GenerarHash(entidad.password) : originalPass;
            entidad.nombreUsuario = Encriptacion.Encriptar(entidad.nombreUsuario);
            entidad.nombre = Encriptacion.Encriptar(entidad.nombre);
            entidad.apellido = Encriptacion.Encriptar(entidad.apellido);

            return Modificar(entidad);
        }

        public bool Eliminar(UsuarioBE entidad)
        {
            return _usuarioDal.Eliminar(entidad);
        }

        public UsuarioBE Seleccionar(int idEntidad)
        {
            UsuarioBE usuario = (UsuarioBE)_usuarioDal.Seleccionar(idEntidad);
            CargarPermisos(ref usuario);

            return usuario;
        }

        public IList<UsuarioBE> SeleccionarTodos()
        {
            IList<UsuarioBE> _usuarios = new List<UsuarioBE>();
            try
            {
                foreach (IEntidad entidad in _usuarioDal.SeleccionarTodos())
                {
                    _usuarios.Add((UsuarioBE)entidad);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return _usuarios;
        }

        public UsuarioBE Guardar(UsuarioBE usuario)
        {
            try
            {
                UsuarioBE usuarioExistente = ObtenerUsuarioPorNombreUsuario(usuario.nombreUsuario);

                usuario = (usuarioExistente == null) ? Crear(usuarioExistente) : Modificar(usuarioExistente);
                RecalcularDVV();

            }
            catch (Exception)
            {
                throw;
            }

            throw new NotImplementedException();
        }

        public void CargarPermisos(ref UsuarioBE usuario)
        {
            try
            {
                usuario.permisos = _usuarioDal.ObtenerPermisosPorUsuario(usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<UsuarioBE> ObtenerUsuariosPorTecnologiaExperiencia(TecnologiaBE tecnologia, Experiencia experiencia)
        {
            return _usuarioDal.ObtenerUsuariosPorTecnologiaExperiencia(tecnologia, experiencia);
        }

        public UsuarioBE ObtenerUsuarioPorNombreUsuario(string nombreUsuario)
        {
            nombreUsuario = Encriptacion.Encriptar(nombreUsuario);
            return _usuarioDal.ObtenerUsuarioPorNombreUsuario(nombreUsuario);
        }

        public IList<ComponenteBE> ObtenerTodosLosPermisos()
        {
            IList<ComponenteBE> permisos = null;

            try
            {
                permisos = _usuarioDal.ObtenerTodosLosPermisos();
            }
            catch (Exception)
            {
                throw;
            }

            return permisos;
        }

        public bool UsuarioTienePermiso(IList<ComponenteBE> permisos, string permiso, ref bool tienePermiso)
        {
            try
            {
                if (!tienePermiso)
                {
                    foreach (ComponenteBE _permiso in permisos)
                    {
                        if (tienePermiso) continue;

                        tienePermiso = _permiso.codigo == permiso;
                        UsuarioTienePermiso(_permiso.ObtenerPermisos(), permiso, ref tienePermiso);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return tienePermiso;
        }

        public IList<ComponenteBE> RemoverPermisosBasePorPertenecerAFamiliaAgregada(IList<ComponenteBE> permisos, ComponenteBE permisoFamilia)
        {
            IList<ComponenteBE> newPermisos = new List<ComponenteBE>();

            try
            {
                {
                    IList<ComponenteBE> permisoFamiliaArbol = permisoFamilia.ObtenerPermisos();

                    foreach (ComponenteBE _permiso in permisos)
                    {
                        bool existePermiso = false;
                        UsuarioTienePermiso(permisoFamiliaArbol, _permiso.codigo, ref existePermiso);

                        if (existePermiso) continue;
                        newPermisos.Add(_permiso);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return newPermisos;
        }

        public bool ModificarPermisos(UsuarioBE usuarioSeleccionado)
        {
            return _usuarioDal.ModificarPermisos(usuarioSeleccionado);
        }

        public bool ModificarFamiliaDePermisos(ComponenteBE familia)
        {
            return _usuarioDal.ModificarFamiliaDePermisos(familia);
        }

        private IDictionary<string, string> ObtenerDvv()
        {
            IDictionary<string, string> valores = new Dictionary<string, string>();
            IList<UsuarioBE> usuarios = null;
            try
            {
                usuarios = SeleccionarTodos();

                int sumatoriaIdDvh = 0;
                foreach (UsuarioBE u in usuarios)
                {
                    sumatoriaIdDvh += u.id.GetHashCode();
                }
                valores.Add("ID", Encriptacion.GenerarHash(sumatoriaIdDvh.ToString()));

                int sumatoriaNombreUsuarioDvh = 0;
                foreach (UsuarioBE u in usuarios)
                {
                    sumatoriaNombreUsuarioDvh += u.nombreUsuario.GetHashCode();
                }
                valores.Add("NOMBREUSUARIO", Encriptacion.GenerarHash(sumatoriaNombreUsuarioDvh.ToString()));

                int sumatoriaPassDvh = 0;
                foreach (UsuarioBE u in usuarios)
                {
                    sumatoriaPassDvh += u.password.GetHashCode();
                }
                valores.Add("PASSWORD", Encriptacion.GenerarHash(sumatoriaPassDvh.ToString()));

                int sumatoriaBloqueadoDvh = 0;
                foreach (UsuarioBE u in usuarios)
                {
                    sumatoriaBloqueadoDvh += u.esBloqueado.GetHashCode();
                }
                valores.Add("BLOQUEADO", Encriptacion.GenerarHash(sumatoriaBloqueadoDvh.ToString()));

                int sumatoriaCreationDateDvh = 0;
                foreach (UsuarioBE u in usuarios)
                {
                    sumatoriaCreationDateDvh += u.fechaCreacion.ToString().GetHashCode();
                }
                valores.Add("FECHACREACION", Encriptacion.GenerarHash(sumatoriaCreationDateDvh.ToString()));

                int sumatoriaPermisoDvh = 0;
                foreach (UsuarioBE u in usuarios)
                {
                    sumatoriaPermisoDvh += u.fechaNacimiento.ToString().GetHashCode();
                }
                valores.Add("PERMISOID", Encriptacion.GenerarHash(sumatoriaPermisoDvh.ToString()));
            }
            catch (Exception)
            {
                throw;
            }

            return valores;
        }

        public IList<string> VerificarIntegridadVertical()
        {
            IList<string> errores = new List<string>();
            try
            {
                IDictionary<string, string> dvvCalculados = ObtenerDvv();

                errores = _usuarioDal.VerificarDvv(dvvCalculados);
            }
            catch (Exception)
            {
                throw;
            }

            return errores;
        }

        public bool VerificarIdentidad()
        {
            IList<UsuarioBE> usuarios = SeleccionarTodos();
            bool identidadOk = true;
            foreach (UsuarioBE usuario in usuarios)
            {
                if (!identidadOk) return identidadOk;

                UsuarioBE _usuario = usuario;
                CargarIdioma(ref _usuario);
                CargarPermisos(ref _usuario);
                TecnologiaBLL.GetInstance().CargarAptitudesPorUsuario(ref _usuario);

                identidadOk = usuario.dvh == CalcularVerificacionHorizontal(_usuario);
            }

            return identidadOk;
        }

        public void RecalcularDVH(UsuarioBE usuario)
        {
            Modificar(usuario, false, usuario.password);
        }

        public void RecalcularDVHTodos()
        {
            IList<UsuarioBE> usuarios = SeleccionarTodos();

            foreach (UsuarioBE usuario in usuarios)
            {
                UsuarioBE _usuario = usuario;
                CargarIdioma(ref _usuario);
                CargarPermisos(ref _usuario);
                TecnologiaBLL.GetInstance().CargarAptitudesPorUsuario(ref _usuario);

                RecalcularDVH(_usuario);
            }
        }

        public bool GuardarNuevaFamilia(ComponenteBE permisoPadre, IList<ComponenteBE> permisos)
        {
            return _usuarioDal.GuardarNuevaFamilia(permisoPadre, permisos);
        }
    }
}
