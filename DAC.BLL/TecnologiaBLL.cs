﻿using DAC.BE;
using DAC.DAL;
using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BLL
{
    public class TecnologiaBLL : ICRUD<TecnologiaBE>
    {
        private static TecnologiaBLL _instance = null;
        private TecnologiaDAL _tecnologiaDal = null;

        private TecnologiaBLL()
        {
            _tecnologiaDal = new TecnologiaDAL();
        }

        public static TecnologiaBLL GetInstance()
        {
            if (_instance == null) _instance = new TecnologiaBLL();
            return _instance;
        }

        public TecnologiaBE Crear(TecnologiaBE entidad)
        {
            return (TecnologiaBE)_tecnologiaDal.Crear(entidad);
        }

        public bool Eliminar(TecnologiaBE entidad)
        {
            return _tecnologiaDal.Eliminar(entidad);
        }

        public TecnologiaBE Modificar(TecnologiaBE entidad)
        {
            return (TecnologiaBE)_tecnologiaDal.Modificar(entidad);
        }

        public TecnologiaBE Seleccionar(int idEntidad)
        {
            return (TecnologiaBE)_tecnologiaDal.Seleccionar(idEntidad);
        }

        public IList<TecnologiaBE> SeleccionarTodos()
        {
            IList<TecnologiaBE> tecnologias = new List<TecnologiaBE>();

            foreach (IEntidad entidad in _tecnologiaDal.SeleccionarTodos())
            {
                tecnologias.Add((TecnologiaBE)entidad);
            }

            return tecnologias;
        }

        public TecnologiaBE Guardar(TecnologiaBE entidad)
        {
            try
            {
                IList<TecnologiaBE> todas = SeleccionarTodos();
                bool existeTecnologia = todas.Any(x => x.codigo == entidad.codigo);

                if (existeTecnologia)
                {
                    TecnologiaBE tecnologiaExistente = todas.First(x => x.codigo == entidad.codigo);
                    entidad.id = tecnologiaExistente.id;
                    entidad.fechaCreacion = tecnologiaExistente.fechaCreacion;
                }

                entidad = existeTecnologia ? Modificar(entidad) : Crear(entidad);
            }
            catch (Exception)
            {
                throw;
            }

            return entidad;
        }

        public bool AsignarAptitudAUsuario(AptitudBE aptitud, ref UsuarioBE usuario)
        {
            bool asignacionOk = false;
            try
            {
                asignacionOk = _tecnologiaDal.AsignarAptitudAUsuario(aptitud, usuario);

                if (asignacionOk) usuario = _tecnologiaDal.ObtenerAptitudPorUsuario(usuario);
            }
            catch (Exception)
            {
                throw;
            }
            return asignacionOk;
        }

        public void CargarAptitudesPorUsuario(ref UsuarioBE usuario)
        {
            try
            {
                usuario = _tecnologiaDal.ObtenerAptitudPorUsuario(usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void EliminarAptitudesPorUsuario(AptitudBE aptitud, ref UsuarioBE usuario)
        {
            try
            {
                usuario = _tecnologiaDal.EliminarAptitudPorUsuario(aptitud, usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ModificarAptitudesPorUsuario(AptitudBE aptitud, ref UsuarioBE usuario)
        {
            try
            {
                usuario = _tecnologiaDal.ModificarAptitudPorUsuario(aptitud, usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
