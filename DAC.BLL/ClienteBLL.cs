﻿using DAC.BE;
using DAC.DAL;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.BLL
{
    public class ClienteBLL : VerificarBLL<ClienteBE>, ICRUD<ClienteBE>
    {
        private ClienteDAL _ClienteDal;

        public ClienteBLL()
        {
            _ClienteDal = new ClienteDAL();
        }

        public ClienteBE Crear(ClienteBE entidad)
        {
            entidad.dvh = CalcularVerificacionHorizontal(entidad);
            return (ClienteBE)_ClienteDal.Crear(entidad);
        }

        public bool Eliminar(ClienteBE entidad)
        {
            return _ClienteDal.Eliminar(entidad);
        }

        public void RecalcularDVV()
        {
            try
            {
                string dvv = ObtenerDVV();

                _ClienteDal.GuardarDVV("Cliente", dvv);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ObtenerDVV()
        {
            string valor = string.Empty;

            try
            {
                IList<ClienteBE> todas = SeleccionarTodos();

                foreach (ClienteBE cliente in todas)
                {
                    valor += cliente.dvh;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Encriptacion.GenerarHash(valor);
        }

        public ClienteBE Modificar(ClienteBE entidad)
        {
            entidad.dvh = CalcularVerificacionHorizontal(entidad);
            return (ClienteBE)_ClienteDal.Modificar(entidad);
        }

        public ClienteBE Seleccionar(int idEntidad)
        {
            return (ClienteBE)_ClienteDal.Seleccionar(idEntidad);
        }

        public ClienteBE Seleccionar2(int idEntidad)
        {
            return (ClienteBE)_ClienteDal.Seleccionar(idEntidad);
        }

        public IList<ClienteBE> SeleccionarTodos()
        {
            IList<ClienteBE> clientes = new List<ClienteBE>();

            foreach (IEntidad entidad in _ClienteDal.SeleccionarTodos())
            {
                clientes.Add((ClienteBE)entidad);
            }

            return clientes;
        }

        public ClienteBE Guardar(ClienteBE Cliente)
        {
            try
            {
                IList<ClienteBE> todas = SeleccionarTodos();
                bool existeCliente = todas.Any(x => x.id == Cliente.id);

                if (existeCliente)
                {
                    ClienteBE ClienteExistente = todas.First(x => x.id == Cliente.id);
                    Cliente.fechaCreacion = ClienteExistente.fechaCreacion;
                }

                Cliente = existeCliente ? Modificar(Cliente) : Crear(Cliente);
            }
            catch (Exception)
            {
                throw;
            }

            return Cliente;
        }

        public bool VerificarIdentidad()
        {
            IList<ClienteBE> Clientes = SeleccionarTodos();
            bool identidadOk = true;
            foreach (ClienteBE Cliente in Clientes)
            {
                if (!identidadOk) return identidadOk;

                identidadOk = Cliente.dvh == CalcularVerificacionHorizontal(Cliente);
            }

            return identidadOk;
        }

        public void RecalcularDVHTodos()
        {
            IList<ClienteBE> Clientes = SeleccionarTodos();

            foreach (ClienteBE Cliente in Clientes)
            {
                Modificar(Cliente);
            }
        }
    }
}
