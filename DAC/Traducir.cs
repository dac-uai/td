﻿using DAC.BE;
using DAC.INTERFACES;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC
{
    public static class Traducir
    {
        public static void TraducirForm(ref Form form, UsuarioBE usuario)
        {
            IdiomaBE idiomaSeleccionado = usuario.idioma;
            UTILS.GestorIdioma.GetInstance().CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);
            foreach (Control control in form.Controls)
            {
                if (control.Tag != null)
                {
                    MensajeBE msj = idiomaSeleccionado.mensajes.First(x => x.etiqueta == control.Tag.ToString());
                    control.Text = (msj != null) ? msj.traduccion.texto : control.Tag.ToString();
                }
            }
        }
    }
}