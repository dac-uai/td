﻿namespace DAC.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.usuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.permisosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altaPermisosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarFamiliasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seguridadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.integridadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bitacoraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlCambioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baseDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.idiomaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tecnologiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abmTecnologiasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarTecnologiaConsultorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licenciaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarLicenciasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitarLicenciasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizarLicenciasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proyectosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionProyectosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignacionRecursosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteGananciasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarHorasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarProgramaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuarioToolStripMenuItem,
            this.seguridadToolStripMenuItem,
            this.baseDatosToolStripMenuItem,
            this.idiomaToolStripMenuItem,
            this.tecnologiaToolStripMenuItem,
            this.licenciaToolStripMenuItem,
            this.proyectosToolStripMenuItem,
            this.cerrarProgramaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1924, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // usuarioToolStripMenuItem
            // 
            this.usuarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionToolStripMenuItem,
            this.permisosToolStripMenuItem,
            this.altaPermisosToolStripMenuItem,
            this.modificarFamiliasToolStripMenuItem});
            this.usuarioToolStripMenuItem.Name = "usuarioToolStripMenuItem";
            this.usuarioToolStripMenuItem.Size = new System.Drawing.Size(196, 24);
            this.usuarioToolStripMenuItem.Tag = "usuarioToolStripMenuItem";
            this.usuarioToolStripMenuItem.Text = "usuarioToolStripMenuItem";
            // 
            // gestionToolStripMenuItem
            // 
            this.gestionToolStripMenuItem.Name = "gestionToolStripMenuItem";
            this.gestionToolStripMenuItem.Size = new System.Drawing.Size(294, 26);
            this.gestionToolStripMenuItem.Tag = "gestionToolStripMenuItem";
            this.gestionToolStripMenuItem.Text = "gestionToolStripMenuItem";
            this.gestionToolStripMenuItem.Click += new System.EventHandler(this.altaToolStripMenuItem_Click);
            // 
            // permisosToolStripMenuItem
            // 
            this.permisosToolStripMenuItem.Name = "permisosToolStripMenuItem";
            this.permisosToolStripMenuItem.Size = new System.Drawing.Size(294, 26);
            this.permisosToolStripMenuItem.Tag = "permisosToolStripMenuItem";
            this.permisosToolStripMenuItem.Text = "permisosToolStripMenuItem";
            this.permisosToolStripMenuItem.Click += new System.EventHandler(this.permisosToolStripMenuItem_Click);
            // 
            // altaPermisosToolStripMenuItem
            // 
            this.altaPermisosToolStripMenuItem.Name = "altaPermisosToolStripMenuItem";
            this.altaPermisosToolStripMenuItem.Size = new System.Drawing.Size(294, 26);
            this.altaPermisosToolStripMenuItem.Tag = "altaPermisosToolStripMenuItem";
            this.altaPermisosToolStripMenuItem.Text = "altaPermisosToolStripMenuItem";
            this.altaPermisosToolStripMenuItem.Click += new System.EventHandler(this.modificarPermisosToolStripMenuItem_Click);
            // 
            // modificarFamiliasToolStripMenuItem
            // 
            this.modificarFamiliasToolStripMenuItem.Name = "modificarFamiliasToolStripMenuItem";
            this.modificarFamiliasToolStripMenuItem.Size = new System.Drawing.Size(328, 26);
            this.modificarFamiliasToolStripMenuItem.Tag = "modificarFamiliasToolStripMenuItem";
            this.modificarFamiliasToolStripMenuItem.Text = "modificarFamiliasToolStripMenuItem";
            this.modificarFamiliasToolStripMenuItem.Click += new System.EventHandler(this.modificarFamiliasToolStripMenuItem_Click);
            // 
            // seguridadToolStripMenuItem
            // 
            this.seguridadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.integridadToolStripMenuItem,
            this.bitacoraToolStripMenuItem,
            this.controlCambioToolStripMenuItem});
            this.seguridadToolStripMenuItem.Name = "seguridadToolStripMenuItem";
            this.seguridadToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.seguridadToolStripMenuItem.Tag = "seguridadToolStripMenuItem";
            this.seguridadToolStripMenuItem.Text = "seguridadToolStripMenuItem";
            // 
            // integridadToolStripMenuItem
            // 
            this.integridadToolStripMenuItem.Name = "integridadToolStripMenuItem";
            this.integridadToolStripMenuItem.Size = new System.Drawing.Size(310, 26);
            this.integridadToolStripMenuItem.Tag = "integridadToolStripMenuItem";
            this.integridadToolStripMenuItem.Text = "integridadToolStripMenuItem";
            this.integridadToolStripMenuItem.Click += new System.EventHandler(this.integridadToolStripMenuItem_Click);
            // 
            // bitacoraToolStripMenuItem
            // 
            this.bitacoraToolStripMenuItem.Name = "bitacoraToolStripMenuItem";
            this.bitacoraToolStripMenuItem.Size = new System.Drawing.Size(310, 26);
            this.bitacoraToolStripMenuItem.Tag = "bitacoraToolStripMenuItem";
            this.bitacoraToolStripMenuItem.Text = "bitacoraToolStripMenuItem";
            this.bitacoraToolStripMenuItem.Click += new System.EventHandler(this.bitacoraToolStripMenuItem_Click);
            // 
            // controlCambioToolStripMenuItem
            // 
            this.controlCambioToolStripMenuItem.Name = "controlCambioToolStripMenuItem";
            this.controlCambioToolStripMenuItem.Size = new System.Drawing.Size(310, 26);
            this.controlCambioToolStripMenuItem.Tag = "controlCambioToolStripMenuItem";
            this.controlCambioToolStripMenuItem.Text = "controlCambioToolStripMenuItem";
            this.controlCambioToolStripMenuItem.Click += new System.EventHandler(this.controlCambioToolStripMenuItem_Click);
            // 
            // baseDatosToolStripMenuItem
            // 
            this.baseDatosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupToolStripMenuItem,
            this.restoreToolStripMenuItem});
            this.baseDatosToolStripMenuItem.Name = "baseDatosToolStripMenuItem";
            this.baseDatosToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.baseDatosToolStripMenuItem.Tag = "baseDatosToolStripMenuItem";
            this.baseDatosToolStripMenuItem.Text = "baseDatosToolStripMenuItem";
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(259, 26);
            this.backupToolStripMenuItem.Tag = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Text = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Click += new System.EventHandler(this.backupToolStripMenuItem_Click);
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(259, 26);
            this.restoreToolStripMenuItem.Tag = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Text = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Click += new System.EventHandler(this.restoreToolStripMenuItem_Click);
            // 
            // idiomaToolStripMenuItem
            // 
            this.idiomaToolStripMenuItem.Name = "idiomaToolStripMenuItem";
            this.idiomaToolStripMenuItem.Size = new System.Drawing.Size(195, 24);
            this.idiomaToolStripMenuItem.Tag = "idiomaToolStripMenuItem";
            this.idiomaToolStripMenuItem.Text = "idiomaToolStripMenuItem";
            this.idiomaToolStripMenuItem.Click += new System.EventHandler(this.idiomaToolStripMenuItem_Click);
            // 
            // tecnologiaToolStripMenuItem
            // 
            this.tecnologiaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abmTecnologiasToolStripMenuItem,
            this.gestionarTecnologiaConsultorToolStripMenuItem});
            this.tecnologiaToolStripMenuItem.Name = "tecnologiaToolStripMenuItem";
            this.tecnologiaToolStripMenuItem.Size = new System.Drawing.Size(219, 24);
            this.tecnologiaToolStripMenuItem.Tag = "tecnologiaToolStripMenuItem";
            this.tecnologiaToolStripMenuItem.Text = "tecnologiaToolStripMenuItem";
            // 
            // abmTecnologiasToolStripMenuItem
            // 
            this.abmTecnologiasToolStripMenuItem.Name = "abmTecnologiasToolStripMenuItem";
            this.abmTecnologiasToolStripMenuItem.Size = new System.Drawing.Size(409, 26);
            this.abmTecnologiasToolStripMenuItem.Tag = "abmTecnologiasToolStripMenuItem";
            this.abmTecnologiasToolStripMenuItem.Text = "abmTecnologias";
            this.abmTecnologiasToolStripMenuItem.Click += new System.EventHandler(this.abmTecnologiasToolStripMenuItem_Click);
            // 
            // gestionarTecnologiaConsultorToolStripMenuItem
            // 
            this.gestionarTecnologiaConsultorToolStripMenuItem.Name = "gestionarTecnologiaConsultorToolStripMenuItem";
            this.gestionarTecnologiaConsultorToolStripMenuItem.Size = new System.Drawing.Size(409, 26);
            this.gestionarTecnologiaConsultorToolStripMenuItem.Tag = "gestionarTecnologiaConsultorToolStripMenuItem";
            this.gestionarTecnologiaConsultorToolStripMenuItem.Text = "gestionarTecnologiaConsultorToolStripMenuItem";
            this.gestionarTecnologiaConsultorToolStripMenuItem.Click += new System.EventHandler(this.gestionarTecnologiaConsultorToolStripMenuItem_Click);
            // 
            // licenciaToolStripMenuItem
            // 
            this.licenciaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionarLicenciasToolStripMenuItem,
            this.solicitarLicenciasToolStripMenuItem,
            this.analizarLicenciasToolStripMenuItem});
            this.licenciaToolStripMenuItem.Name = "licenciaToolStripMenuItem";
            this.licenciaToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.licenciaToolStripMenuItem.Tag = "licenciaToolStripMenuItem";
            this.licenciaToolStripMenuItem.Text = "licenciaToolStripMenuItem";
            // 
            // gestionarLicenciasToolStripMenuItem
            // 
            this.gestionarLicenciasToolStripMenuItem.Name = "gestionarLicenciasToolStripMenuItem";
            this.gestionarLicenciasToolStripMenuItem.Size = new System.Drawing.Size(332, 26);
            this.gestionarLicenciasToolStripMenuItem.Tag = "gestionarLicenciasToolStripMenuItem";
            this.gestionarLicenciasToolStripMenuItem.Text = "gestionarLicenciasToolStripMenuItem";
            this.gestionarLicenciasToolStripMenuItem.Click += new System.EventHandler(this.gestionarLicenciasToolStripMenuItem_Click);
            // 
            // solicitarLicenciasToolStripMenuItem
            // 
            this.solicitarLicenciasToolStripMenuItem.Name = "solicitarLicenciasToolStripMenuItem";
            this.solicitarLicenciasToolStripMenuItem.Size = new System.Drawing.Size(332, 26);
            this.solicitarLicenciasToolStripMenuItem.Tag = "solicitarLicenciasToolStripMenuItem";
            this.solicitarLicenciasToolStripMenuItem.Text = "solicitarLicenciasToolStripMenuItem";
            this.solicitarLicenciasToolStripMenuItem.Click += new System.EventHandler(this.solicitarLicenciasToolStripMenuItem_Click);
            // 
            // analizarLicenciasToolStripMenuItem
            // 
            this.analizarLicenciasToolStripMenuItem.Name = "analizarLicenciasToolStripMenuItem";
            this.analizarLicenciasToolStripMenuItem.Size = new System.Drawing.Size(332, 26);
            this.analizarLicenciasToolStripMenuItem.Tag = "analizarLicenciasToolStripMenuItem";
            this.analizarLicenciasToolStripMenuItem.Text = "analizarLicenciasToolStripMenuItem";
            this.analizarLicenciasToolStripMenuItem.Click += new System.EventHandler(this.analizarLicenciasToolStripMenuItem_Click);
            // 
            // proyectosToolStripMenuItem
            // 
            this.proyectosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionProyectosToolStripMenuItem,
            this.gestionClientesToolStripMenuItem,
            this.asignacionRecursosToolStripMenuItem,
            this.reporteGananciasToolStripMenuItem,
            this.cargarHorasToolStripMenuItem});
            this.proyectosToolStripMenuItem.Name = "proyectosToolStripMenuItem";
            this.proyectosToolStripMenuItem.Size = new System.Drawing.Size(213, 24);
            this.proyectosToolStripMenuItem.Tag = "proyectosToolStripMenuItem";
            this.proyectosToolStripMenuItem.Text = "proyectosToolStripMenuItem";
            // 
            // gestionProyectosToolStripMenuItem
            // 
            this.gestionProyectosToolStripMenuItem.Name = "gestionProyectosToolStripMenuItem";
            this.gestionProyectosToolStripMenuItem.Size = new System.Drawing.Size(340, 26);
            this.gestionProyectosToolStripMenuItem.Tag = "gestionProyectosToolStripMenuItem";
            this.gestionProyectosToolStripMenuItem.Text = "gestionProyectosToolStripMenuItem";
            this.gestionProyectosToolStripMenuItem.Click += new System.EventHandler(this.gestionProyectosToolStripMenuItem_Click);
            // 
            // gestionClientesToolStripMenuItem
            // 
            this.gestionClientesToolStripMenuItem.Name = "gestionClientesToolStripMenuItem";
            this.gestionClientesToolStripMenuItem.Size = new System.Drawing.Size(340, 26);
            this.gestionClientesToolStripMenuItem.Tag = "gestionClientesToolStripMenuItem";
            this.gestionClientesToolStripMenuItem.Text = "gestionClientesToolStripMenuItem";
            this.gestionClientesToolStripMenuItem.Click += new System.EventHandler(this.gestionClientesToolStripMenuItem_Click);
            // 
            // asignacionRecursosToolStripMenuItem
            // 
            this.asignacionRecursosToolStripMenuItem.Name = "asignacionRecursosToolStripMenuItem";
            this.asignacionRecursosToolStripMenuItem.Size = new System.Drawing.Size(340, 26);
            this.asignacionRecursosToolStripMenuItem.Tag = "asignacionRecursosToolStripMenuItem";
            this.asignacionRecursosToolStripMenuItem.Text = "asignacionRecursosToolStripMenuItem";
            this.asignacionRecursosToolStripMenuItem.Click += new System.EventHandler(this.asignacionRecursosToolStripMenuItem_Click);
            // 
            // reporteGananciasToolStripMenuItem
            // 
            this.reporteGananciasToolStripMenuItem.Name = "reporteGananciasToolStripMenuItem";
            this.reporteGananciasToolStripMenuItem.Size = new System.Drawing.Size(340, 26);
            this.reporteGananciasToolStripMenuItem.Tag = "reporteGananciasToolStripMenuItem";
            this.reporteGananciasToolStripMenuItem.Text = "reporteGananciasToolStripMenuItem";
            this.reporteGananciasToolStripMenuItem.Click += new System.EventHandler(this.reporteGananciasToolStripMenuItem_Click);
            // 
            // cargarHorasToolStripMenuItem
            // 
            this.cargarHorasToolStripMenuItem.Name = "cargarHorasToolStripMenuItem";
            this.cargarHorasToolStripMenuItem.Size = new System.Drawing.Size(340, 26);
            this.cargarHorasToolStripMenuItem.Tag = "cargarHorasToolStripMenuItem";
            this.cargarHorasToolStripMenuItem.Text = "cargarHorasToolStripMenuItem";
            this.cargarHorasToolStripMenuItem.Click += new System.EventHandler(this.cargarHorasToolStripMenuItem_Click);
            // 
            // cerrarProgramaToolStripMenuItem
            // 
            this.cerrarProgramaToolStripMenuItem.Name = "cerrarProgramaToolStripMenuItem";
            this.cerrarProgramaToolStripMenuItem.Size = new System.Drawing.Size(251, 24);
            this.cerrarProgramaToolStripMenuItem.Tag = "cerrarProgramaToolStripMenuItem";
            this.cerrarProgramaToolStripMenuItem.Text = "cerrarProgramaToolStripMenuItem";
            this.cerrarProgramaToolStripMenuItem.Click += new System.EventHandler(this.cerrarProgramaToolStripMenuItemToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 726);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Tag = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem usuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarProgramaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seguridadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baseDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem permisosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem idiomaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tecnologiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abmTecnologiasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarTecnologiaConsultorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenciaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarLicenciasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solicitarLicenciasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizarLicenciasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proyectosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionProyectosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem integridadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignacionRecursosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteGananciasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarHorasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bitacoraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controlCambioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altaPermisosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarFamiliasToolStripMenuItem;
    }
}

