﻿using DAC.BLL;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class IntegridadForm : Form
    {
        private IdiomaBE idiomaSeleccionado = null;
        private UTILS.GestorIdioma _gestorIdioma = null;

        public IntegridadForm()
        {
            InitializeComponent();
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonRegenerar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjDeseaRegenerarIdentidad").texto, string.Empty, MessageBoxButtons.YesNo);

            if (resultado.ToString().Equals("Yes"))
            {
                new ClienteBLL().RecalcularDVHTodos();
                ProyectoBLL.GetInstance().RecalcularDVHTodos();
                UsuarioBLL.GetInstance().RecalcularDVHTodos();
                LicenciaBLL.GetInstance().RecalcularDVHTodos();

                new ClienteBLL().RecalcularDVV();
                ProyectoBLL.GetInstance().RecalcularDVV();
                UsuarioBLL.GetInstance().RecalcularDVV();
                LicenciaBLL.GetInstance().RecalcularDVV();

                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjIntegridadRegenerada").texto);
            }
        }

        private void IntegridadForm_Load(object sender, EventArgs e)
        {
            _gestorIdioma = UTILS.GestorIdioma.GetInstance();

            idiomaSeleccionado = GestorSession.GetInstance().sessionUsuario.idioma;
            _gestorIdioma.CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);
        }
    }
}
