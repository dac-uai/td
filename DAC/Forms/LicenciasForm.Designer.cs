﻿namespace DAC.Forms
{
    partial class LicenciasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.labelFechaDesde = new System.Windows.Forms.Label();
            this.labelFechaHasta = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.labelLicencia = new System.Windows.Forms.Label();
            this.ddlLicencias = new System.Windows.Forms.ComboBox();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(217, 36);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(200, 22);
            this.dtpFrom.TabIndex = 0;
            // 
            // labelFechaDesde
            // 
            this.labelFechaDesde.AutoSize = true;
            this.labelFechaDesde.Location = new System.Drawing.Point(12, 41);
            this.labelFechaDesde.Name = "labelFechaDesde";
            this.labelFechaDesde.Size = new System.Drawing.Size(118, 17);
            this.labelFechaDesde.TabIndex = 1;
            this.labelFechaDesde.Tag = "labelFechaDesde";
            this.labelFechaDesde.Text = "labelFechaDesde";
            // 
            // labelFechaHasta
            // 
            this.labelFechaHasta.AutoSize = true;
            this.labelFechaHasta.Location = new System.Drawing.Point(12, 69);
            this.labelFechaHasta.Name = "labelFechaHasta";
            this.labelFechaHasta.Size = new System.Drawing.Size(114, 17);
            this.labelFechaHasta.TabIndex = 3;
            this.labelFechaHasta.Tag = "labelFechaHasta";
            this.labelFechaHasta.Text = "labelFechaHasta";
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(217, 64);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(200, 22);
            this.dtpTo.TabIndex = 2;
            // 
            // labelLicencia
            // 
            this.labelLicencia.AutoSize = true;
            this.labelLicencia.Location = new System.Drawing.Point(12, 9);
            this.labelLicencia.Name = "labelLicencia";
            this.labelLicencia.Size = new System.Drawing.Size(90, 17);
            this.labelLicencia.TabIndex = 4;
            this.labelLicencia.Tag = "labelLicencia";
            this.labelLicencia.Text = "labelLicencia";
            // 
            // ddlLicencias
            // 
            this.ddlLicencias.FormattingEnabled = true;
            this.ddlLicencias.Location = new System.Drawing.Point(217, 6);
            this.ddlLicencias.Name = "ddlLicencias";
            this.ddlLicencias.Size = new System.Drawing.Size(200, 24);
            this.ddlLicencias.TabIndex = 5;
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(217, 93);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(200, 23);
            this.buttonGuardar.TabIndex = 6;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(12, 93);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(199, 23);
            this.buttonSalir.TabIndex = 7;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // LicenciasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 130);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.ddlLicencias);
            this.Controls.Add(this.labelLicencia);
            this.Controls.Add(this.labelFechaHasta);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.labelFechaDesde);
            this.Controls.Add(this.dtpFrom);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "SolicitarLicencias");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "LicenciasForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Licencias";
            this.Load += new System.EventHandler(this.LicenciasForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label labelFechaDesde;
        private System.Windows.Forms.Label labelFechaHasta;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label labelLicencia;
        private System.Windows.Forms.ComboBox ddlLicencias;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}