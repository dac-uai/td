﻿using DAC.BE;
using DAC.BLL;
using DAC.SEGURIDAD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class CargarHorasForm : Form
    {
        private AsignacionUsuarioBLL _asignacionUsuarioBll = null;
        private UsuarioBE _usuarioEnSession = null;
        private IList<AsignacionUsuarioBE> asignacionesPorUsuario = null;

        public CargarHorasForm()
        {
            InitializeComponent();
            _usuarioEnSession = GestorSession.GetInstance().sessionUsuario;
            _asignacionUsuarioBll = AsignacionUsuarioBLL.GetInstance();
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CargarHorasForm_Load(object sender, EventArgs e)
        {
            CargarListaProyectos();
            asignacionesPorUsuario = _asignacionUsuarioBll.SeleccionarPorUsuario(_usuarioEnSession);
            foreach (AsignacionUsuarioBE asignacion in asignacionesPorUsuario)
            {
                listBox1.Items.Add(string.Format("{0} - {1}", asignacion.fechaDesde.ToString("dd/MM/yyyy"), asignacion.fechaHasta.ToString("dd/MM/yyyy")));
            }
        }

        private void CargarListaProyectos()
        {
            IList<AsignacionUsuarioBE> asignaciones = new List<AsignacionUsuarioBE>();
            try
            {
                asignaciones = _asignacionUsuarioBll.SeleccionarPorUsuario(_usuarioEnSession);

                IList<ProyectoBE> proyectos = new List<ProyectoBE>();

                foreach (AsignacionUsuarioBE asignacion in asignaciones)
                {
                    if (proyectos.Any(x => x.id == asignacion.proyecto.id)) continue;

                    proyectos.Add(asignacion.proyecto);
                }

                proyectos.Insert(0, new ProyectoBE() { id = 0, nombre = ((MainForm)ParentForm).optionSelectMessage });

                ddlProyectos.DataSource = proyectos;
                ddlProyectos.DisplayMember = "nombre";
                ddlProyectos.ValueMember = "id";

                ddlProyectos.SelectedIndex = 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox1.Text) || ddlProyectos.SelectedIndex == 0)
                {
                    MessageBox.Show("No ha ingresado todos los valores requeridos");
                    return;
                }

                DateTime selectedDate = dateTimePicker1.Value;
                bool fechaValida = false;
                foreach (AsignacionUsuarioBE asignacion in asignacionesPorUsuario)
                {
                    if (DateTime.ParseExact(asignacion.fechaDesde.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture)
                        <= DateTime.ParseExact(selectedDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture)
                        && DateTime.ParseExact(asignacion.fechaHasta.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture)
                        >= DateTime.ParseExact(selectedDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture))
                    {
                        fechaValida = true;
                    }
                }

                if (!fechaValida)
                {
                    MessageBox.Show("La fecha ingresada es incorrecta, el usuario no posee asignacion para ese dia");
                    return;
                }

                ReporteHoras reporte = new ReporteHoras();
                reporte.usuario = _usuarioEnSession;
                reporte.proyecto = (ProyectoBE)ddlProyectos.SelectedItem;
                reporte.fecha = dateTimePicker1.Value;
                reporte.horas = int.Parse(textBox1.Text);
                reporte.comentarios = richTextBox1.Text;

                bool registroOk = _asignacionUsuarioBll.RegistarHorasPorProyecto(reporte);

                if (registroOk) MessageBox.Show("Se han cargado las horas correctamente");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}