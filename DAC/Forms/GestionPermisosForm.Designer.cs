﻿namespace DAC.Forms
{
    partial class GestionPermisosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.buttonAgregarPermiso = new System.Windows.Forms.Button();
            this.buttonRemoverPermiso = new System.Windows.Forms.Button();
            this.labelSeleccionarUsuario = new System.Windows.Forms.Label();
            this.labelPermisosDeUsuario = new System.Windows.Forms.Label();
            this.labelPermisosDelSistema = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(37, 126);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(292, 488);
            this.treeView1.TabIndex = 0;
            // 
            // treeView2
            // 
            this.helpProvider1.SetHelpKeyword(this.treeView2, "GestionPermisos");
            this.helpProvider1.SetHelpNavigator(this.treeView2, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.treeView2.Location = new System.Drawing.Point(507, 126);
            this.treeView2.Name = "treeView2";
            this.helpProvider1.SetShowHelp(this.treeView2, true);
            this.treeView2.Size = new System.Drawing.Size(292, 488);
            this.treeView2.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(37, 59);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(292, 24);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(507, 59);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(292, 23);
            this.buttonGuardar.TabIndex = 3;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(507, 657);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(292, 23);
            this.buttonSalir.TabIndex = 4;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // buttonAgregarPermiso
            // 
            this.buttonAgregarPermiso.Location = new System.Drawing.Point(335, 241);
            this.buttonAgregarPermiso.Name = "buttonAgregarPermiso";
            this.buttonAgregarPermiso.Size = new System.Drawing.Size(166, 53);
            this.buttonAgregarPermiso.TabIndex = 5;
            this.buttonAgregarPermiso.Tag = "buttonAgregarPermiso";
            this.buttonAgregarPermiso.Text = "buttonAgregarPermiso";
            this.buttonAgregarPermiso.UseVisualStyleBackColor = true;
            this.buttonAgregarPermiso.Click += new System.EventHandler(this.buttonAgregarPermiso_Click);
            // 
            // buttonRemoverPermiso
            // 
            this.buttonRemoverPermiso.Location = new System.Drawing.Point(335, 455);
            this.buttonRemoverPermiso.Name = "buttonRemoverPermiso";
            this.buttonRemoverPermiso.Size = new System.Drawing.Size(166, 53);
            this.buttonRemoverPermiso.TabIndex = 6;
            this.buttonRemoverPermiso.Tag = "buttonRemoverPermiso";
            this.buttonRemoverPermiso.Text = "buttonRemoverPermiso";
            this.buttonRemoverPermiso.UseVisualStyleBackColor = true;
            this.buttonRemoverPermiso.Click += new System.EventHandler(this.buttonRemoverPermiso_Click);
            // 
            // labelSeleccionarUsuario
            // 
            this.labelSeleccionarUsuario.AutoSize = true;
            this.labelSeleccionarUsuario.Location = new System.Drawing.Point(37, 36);
            this.labelSeleccionarUsuario.Name = "labelSeleccionarUsuario";
            this.labelSeleccionarUsuario.Size = new System.Drawing.Size(161, 17);
            this.labelSeleccionarUsuario.TabIndex = 7;
            this.labelSeleccionarUsuario.Tag = "labelSeleccionarUsuario";
            this.labelSeleccionarUsuario.Text = "labelSeleccionarUsuario";
            // 
            // labelPermisosDeUsuario
            // 
            this.labelPermisosDeUsuario.AutoSize = true;
            this.labelPermisosDeUsuario.Location = new System.Drawing.Point(34, 106);
            this.labelPermisosDeUsuario.Name = "labelPermisosDeUsuario";
            this.labelPermisosDeUsuario.Size = new System.Drawing.Size(163, 17);
            this.labelPermisosDeUsuario.TabIndex = 8;
            this.labelPermisosDeUsuario.Tag = "labelPermisosDeUsuario";
            this.labelPermisosDeUsuario.Text = "labelPermisosDeUsuario";
            // 
            // labelPermisosDelSistema
            // 
            this.labelPermisosDelSistema.AutoSize = true;
            this.labelPermisosDelSistema.Location = new System.Drawing.Point(504, 106);
            this.labelPermisosDelSistema.Name = "labelPermisosDelSistema";
            this.labelPermisosDelSistema.Size = new System.Drawing.Size(167, 17);
            this.labelPermisosDelSistema.TabIndex = 9;
            this.labelPermisosDelSistema.Tag = "labelPermisosDelSistema";
            this.labelPermisosDelSistema.Text = "labelPermisosDelSistema";
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // GestionPermisosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 709);
            this.Controls.Add(this.labelPermisosDelSistema);
            this.Controls.Add(this.labelPermisosDeUsuario);
            this.Controls.Add(this.labelSeleccionarUsuario);
            this.Controls.Add(this.buttonRemoverPermiso);
            this.Controls.Add(this.buttonAgregarPermiso);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.treeView2);
            this.Controls.Add(this.treeView1);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "GestionPErmios");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "GestionPermisosForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "GestionPermisosForm";
            this.Load += new System.EventHandler(this.GestionPermisos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.Button buttonAgregarPermiso;
        private System.Windows.Forms.Button buttonRemoverPermiso;
        private System.Windows.Forms.Label labelSeleccionarUsuario;
        private System.Windows.Forms.Label labelPermisosDeUsuario;
        private System.Windows.Forms.Label labelPermisosDelSistema;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}