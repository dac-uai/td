﻿namespace DAC.Forms
{
    partial class GestorLicenciasGrillaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonAprobar = new System.Windows.Forms.Button();
            this.buttonRechazar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(13, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1018, 612);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // buttonAprobar
            // 
            this.buttonAprobar.Location = new System.Drawing.Point(13, 631);
            this.buttonAprobar.Name = "buttonAprobar";
            this.buttonAprobar.Size = new System.Drawing.Size(161, 23);
            this.buttonAprobar.TabIndex = 1;
            this.buttonAprobar.Tag = "buttonAprobar";
            this.buttonAprobar.Text = "buttonAprobar";
            this.buttonAprobar.UseVisualStyleBackColor = true;
            this.buttonAprobar.Click += new System.EventHandler(this.buttonAprobar_Click);
            // 
            // buttonRechazar
            // 
            this.buttonRechazar.Location = new System.Drawing.Point(180, 631);
            this.buttonRechazar.Name = "buttonRechazar";
            this.buttonRechazar.Size = new System.Drawing.Size(161, 23);
            this.buttonRechazar.TabIndex = 2;
            this.buttonRechazar.Tag = "buttonRechazar";
            this.buttonRechazar.Text = "buttonRechazar";
            this.buttonRechazar.UseVisualStyleBackColor = true;
            this.buttonRechazar.Click += new System.EventHandler(this.buttonRechazar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(347, 631);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(161, 23);
            this.buttonSalir.TabIndex = 4;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // GestorLicenciasGrillaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1508, 666);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonRechazar);
            this.Controls.Add(this.buttonAprobar);
            this.Controls.Add(this.dataGridView1);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "AnalizarLicencias");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "GestorLicenciasGrillaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "GestorLicenciasGrillaForm";
            this.Load += new System.EventHandler(this.GestorLicenciasGrillaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonAprobar;
        private System.Windows.Forms.Button buttonRechazar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}