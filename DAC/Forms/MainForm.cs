﻿using DAC.BE;
using DAC.Forms;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class MainForm : Observador
    {
        public UsuarioBE usuarioEnSesion = null;
        public string optionSelectMessage = string.Empty;

        private IdiomaBE idiomaSeleccionado = null;
        public Login _loginForm = null;

        public MainForm(Forms.Login loginForm)
        {
            usuarioEnSesion = GestorSession.GetInstance().sessionUsuario;
            this.IsMdiContainer = true;
            _loginForm = loginForm;

            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            this.Size = new System.Drawing.Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.WindowState = FormWindowState.Maximized;
            ActualizarVista();
            optionSelectMessage = idiomaSeleccionado.mensajes.First(x => x.etiqueta == "optionSelect").traduccion.texto;
            this.Text = idiomaSeleccionado.mensajes.First(x => x.etiqueta == this.Name).traduccion.texto;
        }

        private void cerrarProgramaToolStripMenuItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestorSession.GetInstance().CerrarSesion();
            _loginForm.Close();
            Close();
            Dispose();
        }

        public void ActualizarVista()
        {
            usuarioEnSesion = GestorSession.GetInstance().sessionUsuario;

            if (usuarioEnSesion != null)
            {
                idiomaSeleccionado = usuarioEnSesion.idioma;
                UTILS.GestorIdioma.GetInstance().CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);

                foreach (ToolStripMenuItem toolItem in menuStrip1.Items)
                {
                    if (toolItem.Tag == null) continue;

                    toolItem.Text = idiomaSeleccionado.mensajes.First(x => x.etiqueta == toolItem.Tag.ToString()).traduccion.texto;

                    foreach (ToolStripMenuItem item in toolItem.DropDownItems)
                    {
                        item.Text = idiomaSeleccionado.mensajes.First(x => x.etiqueta == item.Tag.ToString()).traduccion.texto;
                    }
                }

                ValidarPermisos();
            }
        }

        private void ValidarPermisos()
        {
            baseDatosToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("ADM001");
            seguridadToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("ADM002");
            backupToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("ADM003");
            restoreToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("ADM004");
            altaPermisosToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("ADM006");
            modificarFamiliasToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("ADM006");
            permisosToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("USR003");
            gestionarLicenciasToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("H.R002");
            analizarLicenciasToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("H.R003");
            tecnologiaToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("CON001");
            proyectosToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("CON001");
            gestionarTecnologiaConsultorToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("CON002");
            solicitarLicenciasToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("CON003");
            cargarHorasToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("CON004");
            abmTecnologiasToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("T.L001");
            gestionClientesToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("P.M002");
            gestionProyectosToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("P.M002");
            asignacionRecursosToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("P.M002");
            reporteGananciasToolStripMenuItem.Visible = GestorSession.GetInstance().UsuarioTienePermiso("P.M002");
        }

        #region StripMenu
        private void bitacoraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new BitacoraForm());
        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new BackUpForm());
        }

        private void restoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new RestoreForm());
        }

        private void permisosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestionPermisosForm());
        }

        private void idiomaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new IdiomaForm(this));
        }

        private void abmTecnologiasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestorTecnologiaForm());
        }

        private void gestionarTecnologiaConsultorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new TecnologiaForm());
        }

        private void gestionarLicenciasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestorLicenciasForm());
        }

        private void solicitarLicenciasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new LicenciasForm());
        }

        private void analizarLicenciasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestorLicenciasGrillaForm());
        }

        private void gestionProyectosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestorProyectosForm());
        }

        private void gestionClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestorClientesForm());
        }

        private void integridadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new IntegridadForm());
        }

        private void altaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestionUsuariosForm());
        }
        private void asignacionRecursosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new AsignacionRecursosForm());
        }

        private void reporteGananciasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new ReporteEconomicoForm());
        }

        private void controlCambioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestorControlCambioForm());
        }

        private void cargarHorasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new CargarHorasForm());
        }

        private void modificarPermisosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestionAltaPermisosForm());
        }

        private void modificarFamiliasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MostrarForm(new GestionModificacionFamiliasForm());
        }
        private void MostrarForm(Form unForm)
        {
            try
            {
                bool formabierto = false;
                Form formactivo = null;
                foreach (Form item in Application.OpenForms)
                {
                    if (item.Name == unForm.Name)
                    {
                        formabierto = true;
                        formactivo = item;
                        break;
                    }
                }

                if (formabierto) formactivo.Close();

                formactivo = unForm;
                formactivo.MdiParent = this;

                ActualizarIdioma();
                formactivo.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                formactivo.WindowState = FormWindowState.Maximized;
                formactivo.Show();
                formactivo.Focus();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        public void OrdenarGrilla(ref DataGridView grid, int numeroColumnas = 0)
        {
            int _numeroColumnas = (numeroColumnas != 0) ? numeroColumnas : grid.Columns.Count;

            int gridWidth = grid.Width - 40;
            foreach (DataGridViewColumn columna in grid.Columns)
            {
                columna.Width = gridWidth / _numeroColumnas;
            }

            grid.ScrollBars = ScrollBars.Vertical;
        }
    }
}