﻿using DAC.BE;
using DAC.BLL;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class IdiomaForm : Observable
    {
        private IList<IdiomaBE> idiomas = null;
        private UsuarioBE usuarioEnSession = null;
        private List<Object> observadores = new List<Object>();

        public IdiomaForm(MainForm main)
        {
            InitializeComponent();
            usuarioEnSession = GestorSession.GetInstance().sessionUsuario;
            RegistrarObservador(main);
        }

        private void Idioma_Load(object sender, EventArgs e)
        {
            idiomas = UTILS.GestorIdioma.GetInstance().ObtenerTodosLosIdiomas();

            IList<IdiomaBE> idiomasDdl = idiomas;
            idiomasDdl.Insert(0, new IdiomaBE() { id = 0, descripcion = ((MainForm)ParentForm).optionSelectMessage });

            ddlSeleccionIdioma.DataSource = idiomasDdl;
            ddlSeleccionIdioma.DisplayMember = "descripcion";
            ddlSeleccionIdioma.ValueMember = "id";

            SeleccionarIdioma(usuarioEnSession.idioma);

            ddlSeleccionIdioma.SelectedIndex = usuarioEnSession.idioma.id;
        }

        private void ddlSeleccionIdioma_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSeleccionIdioma.SelectedIndex > 0)
            {
                IdiomaBE idiomaSeleccionado = (IdiomaBE)ddlSeleccionIdioma.SelectedItem;

                SeleccionarIdioma(idiomaSeleccionado);

                usuarioEnSession.idioma = idiomaSeleccionado;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                UTILS.GestorIdioma.GetInstance().GuardarIdioma(usuarioEnSession, usuarioEnSession.idioma);
                GestorSession.GetInstance().ActualizarUsuarioEnSesion(usuarioEnSession);
                NotificarObservadores();
            }
            catch (Exception)
            {
                throw;
            }
            this.Close();
        }

        private void SeleccionarIdioma(IdiomaBE idiomaSeleccionado)
        {
            UTILS.GestorIdioma.GetInstance().CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);

            foreach (Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    MensajeBE msj = idiomaSeleccionado.mensajes.First(x => x.etiqueta == control.Tag.ToString());
                    control.Text = msj.traduccion.texto;
                }
            }
        }
    }
}