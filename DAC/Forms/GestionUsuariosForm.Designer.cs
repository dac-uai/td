﻿namespace DAC.Forms
{
    partial class GestionUsuariosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtNombreEmpleado = new System.Windows.Forms.TextBox();
            this.labelNombreEmpleado = new System.Windows.Forms.Label();
            this.labelFechaNacimiento = new System.Windows.Forms.Label();
            this.groupBoxUsuarios = new System.Windows.Forms.GroupBox();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtNombreUsuario = new System.Windows.Forms.TextBox();
            this.labelNombreUsuario = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.labelMail = new System.Windows.Forms.Label();
            this.txtApellidoEmpleado = new System.Windows.Forms.TextBox();
            this.labelApellidoEmpleado = new System.Windows.Forms.Label();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.buttonModificar = new System.Windows.Forms.Button();
            this.groupBoxUsuarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            this.SuspendLayout();
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(379, 127);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(99, 17);
            this.labelPassword.TabIndex = 0;
            this.labelPassword.Tag = "labelPassword";
            this.labelPassword.Text = "labelPassword";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(557, 124);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(257, 22);
            this.txtPassword.TabIndex = 1;
            // 
            // txtNombreEmpleado
            // 
            this.txtNombreEmpleado.Location = new System.Drawing.Point(171, 28);
            this.txtNombreEmpleado.Name = "txtNombreEmpleado";
            this.txtNombreEmpleado.Size = new System.Drawing.Size(180, 22);
            this.txtNombreEmpleado.TabIndex = 3;
            // 
            // labelNombreEmpleado
            // 
            this.labelNombreEmpleado.AutoSize = true;
            this.labelNombreEmpleado.Location = new System.Drawing.Point(14, 31);
            this.labelNombreEmpleado.Name = "labelNombreEmpleado";
            this.labelNombreEmpleado.Size = new System.Drawing.Size(151, 17);
            this.labelNombreEmpleado.TabIndex = 2;
            this.labelNombreEmpleado.Tag = "labelNombreEmpleado";
            this.labelNombreEmpleado.Text = "labelNombreEmpleado";
            // 
            // labelFechaNacimiento
            // 
            this.labelFechaNacimiento.AutoSize = true;
            this.labelFechaNacimiento.Location = new System.Drawing.Point(379, 29);
            this.labelFechaNacimiento.Name = "labelFechaNacimiento";
            this.labelFechaNacimiento.Size = new System.Drawing.Size(147, 17);
            this.labelFechaNacimiento.TabIndex = 6;
            this.labelFechaNacimiento.Tag = "labelFechaNacimiento";
            this.labelFechaNacimiento.Text = "labelFechaNacimiento";
            // 
            // groupBoxUsuarios
            // 
            this.groupBoxUsuarios.Controls.Add(this.buttonModificar);
            this.groupBoxUsuarios.Controls.Add(this.buttonSalir);
            this.groupBoxUsuarios.Controls.Add(this.buttonGuardar);
            this.groupBoxUsuarios.Controls.Add(this.buttonEliminar);
            this.groupBoxUsuarios.Controls.Add(this.dateTimePicker1);
            this.groupBoxUsuarios.Controls.Add(this.txtNombreUsuario);
            this.groupBoxUsuarios.Controls.Add(this.labelNombreUsuario);
            this.groupBoxUsuarios.Controls.Add(this.txtMail);
            this.groupBoxUsuarios.Controls.Add(this.labelMail);
            this.groupBoxUsuarios.Controls.Add(this.labelPassword);
            this.groupBoxUsuarios.Controls.Add(this.txtPassword);
            this.groupBoxUsuarios.Controls.Add(this.labelFechaNacimiento);
            this.groupBoxUsuarios.Controls.Add(this.labelNombreEmpleado);
            this.groupBoxUsuarios.Controls.Add(this.txtApellidoEmpleado);
            this.groupBoxUsuarios.Controls.Add(this.txtNombreEmpleado);
            this.groupBoxUsuarios.Controls.Add(this.labelApellidoEmpleado);
            this.groupBoxUsuarios.Location = new System.Drawing.Point(12, 12);
            this.groupBoxUsuarios.Name = "groupBoxUsuarios";
            this.groupBoxUsuarios.Size = new System.Drawing.Size(834, 205);
            this.groupBoxUsuarios.TabIndex = 8;
            this.groupBoxUsuarios.TabStop = false;
            this.groupBoxUsuarios.Tag = "groupBoxUsuarios";
            this.groupBoxUsuarios.Text = "groupBoxUsuarios";
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(619, 176);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(195, 23);
            this.buttonSalir.TabIndex = 12;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(6, 176);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(195, 23);
            this.buttonGuardar.TabIndex = 13;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.Location = new System.Drawing.Point(418, 176);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(195, 23);
            this.buttonEliminar.TabIndex = 11;
            this.buttonEliminar.Tag = "buttonEliminar";
            this.buttonEliminar.Text = "buttonEliminar";
            this.buttonEliminar.UseVisualStyleBackColor = true;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(557, 26);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(257, 22);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // txtNombreUsuario
            // 
            this.txtNombreUsuario.Location = new System.Drawing.Point(171, 124);
            this.txtNombreUsuario.Name = "txtNombreUsuario";
            this.txtNombreUsuario.Size = new System.Drawing.Size(180, 22);
            this.txtNombreUsuario.TabIndex = 11;
            // 
            // labelNombreUsuario
            // 
            this.labelNombreUsuario.AutoSize = true;
            this.labelNombreUsuario.Location = new System.Drawing.Point(14, 127);
            this.labelNombreUsuario.Name = "labelNombreUsuario";
            this.labelNombreUsuario.Size = new System.Drawing.Size(137, 17);
            this.labelNombreUsuario.TabIndex = 10;
            this.labelNombreUsuario.Tag = "labelNombreUsuario";
            this.labelNombreUsuario.Text = "labelNombreUsuario";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(557, 72);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(257, 22);
            this.txtMail.TabIndex = 9;
            // 
            // labelMail
            // 
            this.labelMail.AutoSize = true;
            this.labelMail.Location = new System.Drawing.Point(379, 75);
            this.labelMail.Name = "labelMail";
            this.labelMail.Size = new System.Drawing.Size(63, 17);
            this.labelMail.TabIndex = 8;
            this.labelMail.Tag = "labelMail";
            this.labelMail.Text = "labelMail";
            // 
            // txtApellidoEmpleado
            // 
            this.txtApellidoEmpleado.Location = new System.Drawing.Point(171, 76);
            this.txtApellidoEmpleado.Name = "txtApellidoEmpleado";
            this.txtApellidoEmpleado.Size = new System.Drawing.Size(180, 22);
            this.txtApellidoEmpleado.TabIndex = 5;
            // 
            // labelApellidoEmpleado
            // 
            this.labelApellidoEmpleado.AutoSize = true;
            this.labelApellidoEmpleado.Location = new System.Drawing.Point(14, 79);
            this.labelApellidoEmpleado.Name = "labelApellidoEmpleado";
            this.labelApellidoEmpleado.Size = new System.Drawing.Size(151, 17);
            this.labelApellidoEmpleado.TabIndex = 4;
            this.labelApellidoEmpleado.Tag = "labelApellidoEmpleado";
            this.labelApellidoEmpleado.Text = "labelApellidoEmpleado";
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.AllowUserToAddRows = false;
            this.dgvUsuarios.AllowUserToDeleteRows = false;
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsuarios.Location = new System.Drawing.Point(12, 224);
            this.dgvUsuarios.MultiSelect = false;
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.ReadOnly = true;
            this.dgvUsuarios.RowTemplate.Height = 24;
            this.dgvUsuarios.Size = new System.Drawing.Size(834, 390);
            this.dgvUsuarios.TabIndex = 9;
            this.dgvUsuarios.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUsuarios_CellContentClick);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // buttonModificar
            // 
            this.buttonModificar.Location = new System.Drawing.Point(207, 176);
            this.buttonModificar.Name = "buttonModificar";
            this.buttonModificar.Size = new System.Drawing.Size(195, 23);
            this.buttonModificar.TabIndex = 14;
            this.buttonModificar.Tag = "buttonModificar";
            this.buttonModificar.Text = "buttonModificar";
            this.buttonModificar.UseVisualStyleBackColor = true;
            this.buttonModificar.Click += new System.EventHandler(this.buttonModificar_Click);
            // 
            // GestionUsuariosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 626);
            this.Controls.Add(this.dgvUsuarios);
            this.Controls.Add(this.groupBoxUsuarios);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "GestionUsuarios");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "GestionUsuariosForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "GestionUsuariosForm";
            this.Text = "GestionUsuariosForm";
            this.Load += new System.EventHandler(this.GestionUsuarios_Load);
            this.groupBoxUsuarios.ResumeLayout(false);
            this.groupBoxUsuarios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtNombreEmpleado;
        private System.Windows.Forms.Label labelNombreEmpleado;
        private System.Windows.Forms.Label labelFechaNacimiento;
        private System.Windows.Forms.GroupBox groupBoxUsuarios;
        private System.Windows.Forms.TextBox txtNombreUsuario;
        private System.Windows.Forms.Label labelNombreUsuario;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label labelMail;
        private System.Windows.Forms.TextBox txtApellidoEmpleado;
        private System.Windows.Forms.Label labelApellidoEmpleado;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.DataGridView dgvUsuarios;
        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.Button buttonModificar;
    }
}