﻿using DAC.BE;
using DAC.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestorClientesForm : Form
    {
        private ClienteBLL _clientesBLL;

        public GestorClientesForm()
        {
            InitializeComponent();
            _clientesBLL = new ClienteBLL();
        }

        private void GestorClientesForm_Load(object sender, EventArgs e)
        {
            CargaInicial();
            CargarClientes();
        }

        private void CargarClientes()
        {
            IList<ClienteBE> clientes = new List<ClienteBE>();
            try
            {
                clientes = _clientesBLL.SeleccionarTodos();

                clientes.Insert(0, new ClienteBE() { id = 0, razonSocial = ((MainForm)ParentForm).optionSelectMessage });

                ddlClientes.DataSource = clientes;
                ddlClientes.DisplayMember = "razonSocial";
                ddlClientes.ValueMember = "id";

                ddlClientes.SelectedIndex = 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ddlClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlClientes.SelectedIndex > 0)
            {
                ClienteBE cliente = (ClienteBE)ddlClientes.SelectedItem;

                txtDescripcion.Text = cliente.descripcion;
                txtCuit.Text = cliente.cuit;
                txtMail.Text = cliente.mailContacto;
                txtRazonSocial.Text = cliente.razonSocial;
                txtTelefono.Text = cliente.telefonoContacto;
                txtNombreContacto.Text = cliente.nombreContacto;
                checkActivo.Checked = cliente.activo;
            }
            else
            {
                CargaInicial();
            }
        }

        private void CargaInicial()
        {
            txtDescripcion.Text = string.Empty;
            txtCuit.Text = string.Empty;
            txtMail.Text = string.Empty;
            txtRazonSocial.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtNombreContacto.Text = string.Empty;
            checkActivo.Checked = false;
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            string descripcion = txtDescripcion.Text;
            string cuit = txtCuit.Text;
            string mail = txtMail.Text;
            string razonSocial = txtRazonSocial.Text;
            string telefono = txtTelefono.Text;
            string nombreContacto = txtNombreContacto.Text;

            if (!string.IsNullOrEmpty(descripcion) &&
                !string.IsNullOrEmpty(cuit) &&
                !string.IsNullOrEmpty(mail) &&
                !string.IsNullOrEmpty(razonSocial) &&
                !string.IsNullOrEmpty(telefono) &&
                !string.IsNullOrEmpty(nombreContacto))
            {
                ClienteBE cliente = new ClienteBE();

                if (ddlClientes.SelectedIndex > 0) cliente = (ClienteBE)ddlClientes.SelectedItem;

                cliente.cuit = cuit;
                cliente.descripcion = descripcion;
                cliente.activo = checkActivo.Checked;
                cliente.mailContacto = mail;
                cliente.nombreContacto = nombreContacto;
                cliente.razonSocial = razonSocial;
                cliente.telefonoContacto = telefono;

                cliente = _clientesBLL.Guardar(cliente);

                if (cliente != null)
                {
                    MessageBox.Show("Cliente guardado correctamente");
                    CargarClientes();
                    CargaInicial();
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado todos los campos requeridos");
            }
        }
    }
}