﻿using DAC.BE;
using DAC.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class AsignacionRecursosForm : Form
    {
        private ClienteBLL _clienteBLL;
        private ProyectoBLL _proyectoBLL;
        private UsuarioBLL _usuarioBLL;
        private AsignacionUsuarioBLL _asignacionUsuarioBLL;

        public AsignacionRecursosForm()
        {
            InitializeComponent();
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AsignacionRecursosForm_Load(object sender, EventArgs e)
        {
            _clienteBLL = new ClienteBLL();
            _proyectoBLL = ProyectoBLL.GetInstance();
            _usuarioBLL = UsuarioBLL.GetInstance();
            _asignacionUsuarioBLL = AsignacionUsuarioBLL.GetInstance();
            CargarClientes();
            CargarTecnologias();
            CargarExperiencia();
        }

        private void CargarClientes()
        {
            IList<ClienteBE> clientes = new List<ClienteBE>();
            try
            {
                clientes = _clienteBLL.SeleccionarTodos();

                clientes.Insert(0, new ClienteBE() { id = 0, razonSocial = ((MainForm)ParentForm).optionSelectMessage });

                ddlClientes.DataSource = clientes;
                ddlClientes.DisplayMember = "razonSocial";
                ddlClientes.ValueMember = "id";

                ddlClientes.SelectedIndex = 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CargarProyectos()
        {
            IList<ProyectoBE> proyectos = new List<ProyectoBE>();
            try
            {
                if (ddlClientes.SelectedIndex > 0)
                {
                    ClienteBE cliente = (ClienteBE)ddlClientes.SelectedItem;
                    proyectos = _proyectoBLL.SeleccionarPorCliente(cliente);
                }

                proyectos.Insert(0, new ProyectoBE() { id = 0, nombre = ((MainForm)ParentForm).optionSelectMessage });

                ddlProyectos.DataSource = proyectos;
                ddlProyectos.DisplayMember = "nombre";
                ddlProyectos.ValueMember = "id";

                ddlProyectos.SelectedIndex = 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ddlClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarProyectos();
        }

        private void ddlProyectos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProyectos.SelectedIndex > 0)
            {
                ProyectoBE proyecto = (ProyectoBE)ddlProyectos.SelectedItem;

                lblDesde.Text = proyecto.fechaDesde.ToString();
                lblHasta.Text = proyecto.fechaHasta.ToString();
                lblPresupuesto.Text = proyecto.presupuestoInicial.ToString();

                CargarDgv(true);
            }
            else
            {
                lblDesde.Text = string.Empty;
                lblHasta.Text = string.Empty;
                lblPresupuesto.Text = string.Empty;

                CargarDgv(false);
            }
        }

        private void CargarTecnologias()
        {
            try
            {
                IList<TecnologiaBE> tecnologias = BLL.TecnologiaBLL.GetInstance().SeleccionarTodos();
                tecnologias.Insert(0, new TecnologiaBE() { id = 0, descripcion = ((MainForm)ParentForm).optionSelectMessage });

                ddlTecnologia.DataSource = tecnologias;
                ddlTecnologia.DisplayMember = "descripcion";
                ddlTecnologia.ValueMember = "id";
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CargarExperiencia()
        {
            ddlExperiencia.Items.Add(((MainForm)ParentForm).optionSelectMessage);
            ddlExperiencia.Items.Add(Experiencia.Baja);
            ddlExperiencia.Items.Add(Experiencia.Media);
            ddlExperiencia.Items.Add(Experiencia.Alta);
            ddlExperiencia.SelectedIndex = 0;
        }

        private void ddlTecnologia_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarUsuariosCapacitados();
        }

        private void ddlExperiencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarUsuariosCapacitados();
        }

        private void CargarUsuariosCapacitados()
        {
            if (ddlExperiencia.SelectedIndex > 0 && ddlTecnologia.SelectedIndex > 0)
            {
                TecnologiaBE tecnologia = (TecnologiaBE)ddlTecnologia.SelectedItem;
                Experiencia experiencia = (Experiencia)ddlExperiencia.SelectedItem;

                IList<UsuarioBE> usuariosCapacitados = _usuarioBLL.ObtenerUsuariosPorTecnologiaExperiencia(tecnologia, experiencia);

                usuariosCapacitados.Insert(0, new UsuarioBE() { id = 0, nombreUsuario = ((MainForm)ParentForm).optionSelectMessage });

                ddlUsuariosCapacitados.DataSource = usuariosCapacitados;
                ddlUsuariosCapacitados.DisplayMember = "nombreUsuario";
                ddlUsuariosCapacitados.ValueMember = "id";
            }
        }

        private void ddlUsuariosCapacitados_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            if (ddlUsuariosCapacitados.SelectedIndex > 0)
            {
                UsuarioBE usuario = (UsuarioBE)ddlUsuariosCapacitados.SelectedItem;
                IList<AsignacionUsuarioBE> asignacionesPorUsuario = _asignacionUsuarioBLL.SeleccionarPorUsuario(usuario);

                if (asignacionesPorUsuario.Any())
                {
                    DateTime minFechaDesde = asignacionesPorUsuario.Select(x => x.fechaDesde).Min();
                    DateTime maxFechaHasta = asignacionesPorUsuario.Select(x => x.fechaHasta).Max();
                    string asignacionStr = string.Empty;
                    foreach (AsignacionUsuarioBE asignacion in asignacionesPorUsuario)
                    {
                        listBox1.Items.Add(string.Format("{0} - {1}", asignacion.fechaDesde.ToString("dd/MM/yyyy"), asignacion.fechaHasta.ToString("dd/MM/yyyy")));
                    }
                }

            }
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            int costoHora = 0;
            bool hayErrores = false;
            hayErrores = hayErrores || !int.TryParse(txtCostoHora.Text, out costoHora);
            hayErrores = hayErrores || (ddlProyectos.SelectedIndex == 0);
            hayErrores = hayErrores || (ddlUsuariosCapacitados.SelectedIndex == 0);

            hayErrores = hayErrores || dtpTo.Value < dtpFrom.Value;

            foreach (object item in listBox1.Items)
            {
                string asignacionStr = item.ToString();


                DateTime ocupadoDesde = DateTime.ParseExact(asignacionStr.Split('-')[0].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime ocupadoHasta = DateTime.ParseExact(asignacionStr.Split('-')[1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                hayErrores = hayErrores || (ocupadoDesde > dtpTo.Value || ocupadoHasta > dtpFrom.Value);
            }


            if (hayErrores)
            {
                MessageBox.Show("Hay errores en la carga de datos");
                return;
            }

            AsignacionUsuarioBE asignacion = new AsignacionUsuarioBE()
            {
                fechaDesde = dtpFrom.Value,
                fechaHasta = dtpTo.Value,
                costoHora = costoHora,
                usuario = (UsuarioBE)ddlUsuariosCapacitados.SelectedItem,
                proyecto = (ProyectoBE)ddlProyectos.SelectedItem
            };

            _asignacionUsuarioBLL.Guardar(asignacion);

            CargarDgv(true);
        }

        private void CargarDgv(bool cargarDatos)
        {
            if (ddlProyectos.SelectedIndex > 0)
            {
                dataGridView1.DataSource = _asignacionUsuarioBLL.SeleccionarPorProyectoMapper((ProyectoBE)ddlProyectos.SelectedItem);
            }

            if (!cargarDatos) dataGridView1.DataSource = null;

            dataGridView1.Refresh();
            dataGridView1.ClearSelection();
            dataGridView1.ReadOnly = true;
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                AsignacionUsuarioMapper usuarioMapper = null;
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    usuarioMapper = (AsignacionUsuarioMapper)dataGridView1.SelectedRows[0].DataBoundItem;
                }

                if (dataGridView1.SelectedCells.Count > 0)
                {
                    int dgvSelectedRow = dataGridView1.SelectedCells[0].RowIndex;
                    usuarioMapper = (AsignacionUsuarioMapper)dataGridView1.Rows[dgvSelectedRow].DataBoundItem;
                }

                if (usuarioMapper != null)
                {
                    ProyectoBE proyecto = (ProyectoBE)ddlProyectos.SelectedItem;
                    UsuarioBE usuario = _usuarioBLL.ObtenerUsuarioPorNombreUsuario(usuarioMapper.nombreUsuario);
                    AsignacionUsuarioBE asignacionUsuario = _asignacionUsuarioBLL.SeleccionarPorUsuarioProyectoFecha(usuario, usuarioMapper.fechaDesde, usuarioMapper.fechaHasta, proyecto);
                    _asignacionUsuarioBLL.Eliminar(asignacionUsuario);

                    MessageBox.Show("El usuario ha sido desasignado del proyecto");
                    CargarDgv(true);
                }
                else
                {
                    MessageBox.Show("No ha seleccionado ningun usuario para eliminar");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
