﻿using DAC.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestorTecnologiaForm : Form
    {
        public GestorTecnologiaForm()
        {
            InitializeComponent();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text;
            string descripcion = txtDescripcion.Text;

            if (!(string.IsNullOrEmpty(codigo) && string.IsNullOrEmpty(descripcion)))
            {
                TecnologiaBE tecnologia = new TecnologiaBE()
                {
                    codigo = codigo,
                    descripcion = descripcion
                };

                tecnologia = BLL.TecnologiaBLL.GetInstance().Guardar(tecnologia);
                CargarTecnologias();

                if (tecnologia != null)
                {
                    MessageBox.Show("Tecnologia generada correctamente");
                }
            }
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GestorTecnologiaForm_Load(object sender, EventArgs e)
        {
            CargarTecnologias();
        }

        private void ddlTecnologias_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCodigo.Text = string.Empty;
            txtDescripcion.Text = string.Empty;

            if (ddlTecnologia.SelectedIndex > 0)
            {
                TecnologiaBE tecnologia = (TecnologiaBE)ddlTecnologia.SelectedItem;

                txtCodigo.Text = tecnologia.codigo;
                txtDescripcion.Text = tecnologia.descripcion;
            }
        }

        private void CargarTecnologias()
        {
            try
            {
                IList<TecnologiaBE> tecnologias = BLL.TecnologiaBLL.GetInstance().SeleccionarTodos();
                tecnologias.Insert(0, new TecnologiaBE() { id = 0, descripcion = ((MainForm)ParentForm).optionSelectMessage });

                ddlTecnologia.DataSource = tecnologias;

                ddlTecnologia.DataSource = tecnologias;
                ddlTecnologia.DisplayMember = "descripcion";
                ddlTecnologia.ValueMember = "id";
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
