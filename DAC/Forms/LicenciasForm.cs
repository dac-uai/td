﻿using DAC.BE;
using DAC.SEGURIDAD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class LicenciasForm : Form
    {
        private BLL.LicenciaBLL _licenciaBLL;
        private UsuarioBE _usuarioEnSession;

        public LicenciasForm()
        {
            InitializeComponent();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            if (ddlLicencias.SelectedIndex > 0)
            {
                if (dtpFrom.Value > dtpTo.Value)
                {
                    MessageBox.Show("Ha ingresado fechas invalidas");
                    return;
                }

                LicenciaSolicitadaBE ls = new LicenciaSolicitadaBE();
                ls.fechaDesde = dtpFrom.Value;
                ls.fechaHasta = dtpTo.Value;
                ls.licencia = (LicenciaBE)ddlLicencias.SelectedItem;
                ls.usuario = _usuarioEnSession;

                _licenciaBLL.AsignarSolicitudAUsuario(ls, ref _usuarioEnSession);
                MessageBox.Show("Licencia generada correctamente");
            }
            else
            {
                MessageBox.Show("No ha seleccionado todos los campos requeridos");
            }
        }

        private void CargarLicencias()
        {
            try
            {
                IList<LicenciaBE> Licencias = _licenciaBLL.SeleccionarTodos();
                Licencias.Insert(0, new LicenciaBE() { id = 0, descripcion = ((MainForm)ParentForm).optionSelectMessage });

                ddlLicencias.DataSource = Licencias;

                ddlLicencias.DataSource = Licencias;
                ddlLicencias.DisplayMember = "descripcion";
                ddlLicencias.ValueMember = "id";
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LicenciasForm_Load(object sender, EventArgs e)
        {
            _licenciaBLL = BLL.LicenciaBLL.GetInstance();
            _usuarioEnSession = GestorSession.GetInstance().sessionUsuario;

            dtpFrom.Value = DateTime.Now;
            dtpTo.Value = DateTime.Now;

            CargarLicencias();

            _licenciaBLL.CargarLicenciaPorUsuario(ref _usuarioEnSession);
        }
    }
}
