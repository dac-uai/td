﻿namespace DAC.Forms
{
    partial class GestorProyectosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNombreProyecto = new System.Windows.Forms.Label();
            this.txtNombreProyecto = new System.Windows.Forms.TextBox();
            this.labelDescripcionProyecto = new System.Windows.Forms.Label();
            this.txtDescipcionProyecto = new System.Windows.Forms.RichTextBox();
            this.labelFechaDesde = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.labelFechaHasta = new System.Windows.Forms.Label();
            this.labelPresupuesto = new System.Windows.Forms.Label();
            this.txtPresupuesto = new System.Windows.Forms.TextBox();
            this.labelProyectos = new System.Windows.Forms.Label();
            this.ddlProyectos = new System.Windows.Forms.ComboBox();
            this.checkEliminar = new System.Windows.Forms.CheckBox();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.ddlClientes = new System.Windows.Forms.ComboBox();
            this.labelClientes = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // labelNombreProyecto
            // 
            this.labelNombreProyecto.AutoSize = true;
            this.labelNombreProyecto.Location = new System.Drawing.Point(17, 69);
            this.labelNombreProyecto.Name = "labelNombreProyecto";
            this.labelNombreProyecto.Size = new System.Drawing.Size(144, 17);
            this.labelNombreProyecto.TabIndex = 0;
            this.labelNombreProyecto.Tag = "labelNombreProyecto";
            this.labelNombreProyecto.Text = "labelNombreProyecto";
            // 
            // txtNombreProyecto
            // 
            this.txtNombreProyecto.Location = new System.Drawing.Point(209, 66);
            this.txtNombreProyecto.Name = "txtNombreProyecto";
            this.txtNombreProyecto.Size = new System.Drawing.Size(261, 22);
            this.txtNombreProyecto.TabIndex = 1;
            // 
            // labelDescripcionProyecto
            // 
            this.labelDescripcionProyecto.AutoSize = true;
            this.labelDescripcionProyecto.Location = new System.Drawing.Point(17, 99);
            this.labelDescripcionProyecto.Name = "labelDescripcionProyecto";
            this.labelDescripcionProyecto.Size = new System.Drawing.Size(168, 17);
            this.labelDescripcionProyecto.TabIndex = 2;
            this.labelDescripcionProyecto.Tag = "labelDescripcionProyecto";
            this.labelDescripcionProyecto.Text = "labelDescripcionProyecto";
            // 
            // txtDescipcionProyecto
            // 
            this.txtDescipcionProyecto.Location = new System.Drawing.Point(209, 99);
            this.txtDescipcionProyecto.Name = "txtDescipcionProyecto";
            this.txtDescipcionProyecto.Size = new System.Drawing.Size(261, 151);
            this.txtDescipcionProyecto.TabIndex = 4;
            this.txtDescipcionProyecto.Text = "";
            // 
            // labelFechaDesde
            // 
            this.labelFechaDesde.AutoSize = true;
            this.labelFechaDesde.Location = new System.Drawing.Point(17, 262);
            this.labelFechaDesde.Name = "labelFechaDesde";
            this.labelFechaDesde.Size = new System.Drawing.Size(118, 17);
            this.labelFechaDesde.TabIndex = 5;
            this.labelFechaDesde.Tag = "labelFechaDesde";
            this.labelFechaDesde.Text = "labelFechaDesde";
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(209, 257);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(261, 22);
            this.dtpFrom.TabIndex = 6;
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(209, 285);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(261, 22);
            this.dtpTo.TabIndex = 8;
            // 
            // labelFechaHasta
            // 
            this.labelFechaHasta.AutoSize = true;
            this.labelFechaHasta.Location = new System.Drawing.Point(17, 290);
            this.labelFechaHasta.Name = "labelFechaHasta";
            this.labelFechaHasta.Size = new System.Drawing.Size(114, 17);
            this.labelFechaHasta.TabIndex = 7;
            this.labelFechaHasta.Tag = "labelFechaHasta";
            this.labelFechaHasta.Text = "labelFechaHasta";
            // 
            // labelPresupuesto
            // 
            this.labelPresupuesto.AutoSize = true;
            this.labelPresupuesto.Location = new System.Drawing.Point(17, 316);
            this.labelPresupuesto.Name = "labelPresupuesto";
            this.labelPresupuesto.Size = new System.Drawing.Size(118, 17);
            this.labelPresupuesto.TabIndex = 9;
            this.labelPresupuesto.Tag = "labelPresupuesto";
            this.labelPresupuesto.Text = "labelPresupuesto";
            // 
            // txtPresupuesto
            // 
            this.txtPresupuesto.Location = new System.Drawing.Point(209, 313);
            this.txtPresupuesto.Name = "txtPresupuesto";
            this.txtPresupuesto.Size = new System.Drawing.Size(261, 22);
            this.txtPresupuesto.TabIndex = 10;
            // 
            // labelProyectos
            // 
            this.labelProyectos.AutoSize = true;
            this.labelProyectos.Location = new System.Drawing.Point(17, 39);
            this.labelProyectos.Name = "labelProyectos";
            this.labelProyectos.Size = new System.Drawing.Size(101, 17);
            this.labelProyectos.TabIndex = 11;
            this.labelProyectos.Tag = "labelProyectos";
            this.labelProyectos.Text = "labelProyectos";
            // 
            // ddlProyectos
            // 
            this.ddlProyectos.FormattingEnabled = true;
            this.ddlProyectos.Location = new System.Drawing.Point(209, 36);
            this.ddlProyectos.Name = "ddlProyectos";
            this.ddlProyectos.Size = new System.Drawing.Size(261, 24);
            this.ddlProyectos.TabIndex = 12;
            this.ddlProyectos.SelectedIndexChanged += new System.EventHandler(this.ddlProyectos_SelectedIndexChanged);
            // 
            // checkEliminar
            // 
            this.checkEliminar.AutoSize = true;
            this.checkEliminar.Location = new System.Drawing.Point(209, 342);
            this.checkEliminar.Name = "checkEliminar";
            this.checkEliminar.Size = new System.Drawing.Size(117, 21);
            this.checkEliminar.TabIndex = 13;
            this.checkEliminar.Tag = "checkEliminar";
            this.checkEliminar.Text = "checkEliminar";
            this.checkEliminar.UseVisualStyleBackColor = true;
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(209, 370);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(117, 23);
            this.buttonSalir.TabIndex = 14;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(353, 370);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(117, 23);
            this.buttonGuardar.TabIndex = 15;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // ddlClientes
            // 
            this.ddlClientes.FormattingEnabled = true;
            this.ddlClientes.Location = new System.Drawing.Point(209, 6);
            this.ddlClientes.Name = "ddlClientes";
            this.ddlClientes.Size = new System.Drawing.Size(261, 24);
            this.ddlClientes.TabIndex = 17;
            this.ddlClientes.SelectedIndexChanged += new System.EventHandler(this.ddlClientes_SelectedIndexChanged);
            // 
            // labelClientes
            // 
            this.labelClientes.AutoSize = true;
            this.labelClientes.Location = new System.Drawing.Point(17, 9);
            this.labelClientes.Name = "labelClientes";
            this.labelClientes.Size = new System.Drawing.Size(88, 17);
            this.labelClientes.TabIndex = 16;
            this.labelClientes.Tag = "labelClientes";
            this.labelClientes.Text = "labelClientes";
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // GestorProyectosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 403);
            this.Controls.Add(this.ddlClientes);
            this.Controls.Add(this.labelClientes);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.checkEliminar);
            this.Controls.Add(this.ddlProyectos);
            this.Controls.Add(this.labelProyectos);
            this.Controls.Add(this.txtPresupuesto);
            this.Controls.Add(this.labelPresupuesto);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.labelFechaHasta);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.labelFechaDesde);
            this.Controls.Add(this.txtDescipcionProyecto);
            this.Controls.Add(this.labelDescripcionProyecto);
            this.Controls.Add(this.txtNombreProyecto);
            this.Controls.Add(this.labelNombreProyecto);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "GestionProyectos");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.KeyPreview = true;
            this.Name = "GestorProyectosForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "GestionProyectosForm";
            this.Load += new System.EventHandler(this.GestionProyectosForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNombreProyecto;
        private System.Windows.Forms.TextBox txtNombreProyecto;
        private System.Windows.Forms.Label labelDescripcionProyecto;
        private System.Windows.Forms.RichTextBox txtDescipcionProyecto;
        private System.Windows.Forms.Label labelFechaDesde;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label labelFechaHasta;
        private System.Windows.Forms.Label labelPresupuesto;
        private System.Windows.Forms.TextBox txtPresupuesto;
        private System.Windows.Forms.Label labelProyectos;
        private System.Windows.Forms.ComboBox ddlProyectos;
        private System.Windows.Forms.CheckBox checkEliminar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.ComboBox ddlClientes;
        private System.Windows.Forms.Label labelClientes;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}