﻿namespace DAC.Forms
{
    partial class IntegridadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegenerar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // buttonRegenerar
            // 
            this.buttonRegenerar.Location = new System.Drawing.Point(13, 13);
            this.buttonRegenerar.Name = "buttonRegenerar";
            this.buttonRegenerar.Size = new System.Drawing.Size(251, 65);
            this.buttonRegenerar.TabIndex = 0;
            this.buttonRegenerar.Tag = "buttonRegenerar";
            this.buttonRegenerar.Text = "buttonRegenerar";
            this.buttonRegenerar.UseVisualStyleBackColor = true;
            this.buttonRegenerar.Click += new System.EventHandler(this.buttonRegenerar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(13, 106);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(251, 65);
            this.buttonSalir.TabIndex = 1;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // IntegridadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 255);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonRegenerar);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "DigitoVerificador");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "IntegridadForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "IntegridadForm";
            this.Text = "IntegridadForm";
            this.Load += new System.EventHandler(this.IntegridadForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegenerar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}