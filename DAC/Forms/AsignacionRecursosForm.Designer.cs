﻿namespace DAC.Forms
{
    partial class AsignacionRecursosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelProyectos = new System.Windows.Forms.Label();
            this.ddlProyectos = new System.Windows.Forms.ComboBox();
            this.ddlClientes = new System.Windows.Forms.ComboBox();
            this.labelClientes = new System.Windows.Forms.Label();
            this.labelFechaDesde = new System.Windows.Forms.Label();
            this.lblDesde = new System.Windows.Forms.Label();
            this.lblHasta = new System.Windows.Forms.Label();
            this.labelFechaHasta = new System.Windows.Forms.Label();
            this.lblPresupuesto = new System.Windows.Forms.Label();
            this.labelPresupuesto = new System.Windows.Forms.Label();
            this.ddlTecnologia = new System.Windows.Forms.ComboBox();
            this.labelTecnologia = new System.Windows.Forms.Label();
            this.ddlExperiencia = new System.Windows.Forms.ComboBox();
            this.labelExperiencia = new System.Windows.Forms.Label();
            this.labelUsuariosCapacitados = new System.Windows.Forms.Label();
            this.ddlUsuariosCapacitados = new System.Windows.Forms.ComboBox();
            this.labelOcupadoDesde = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.labelCostoHora = new System.Windows.Forms.Label();
            this.txtCostoHora = new System.Windows.Forms.TextBox();
            this.labelUsuariosAsignados = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.buttonEliminar = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelProyectos
            // 
            this.labelProyectos.AutoSize = true;
            this.labelProyectos.Location = new System.Drawing.Point(8, 45);
            this.labelProyectos.Name = "labelProyectos";
            this.labelProyectos.Size = new System.Drawing.Size(101, 17);
            this.labelProyectos.TabIndex = 0;
            this.labelProyectos.Tag = "labelProyectos";
            this.labelProyectos.Text = "labelProyectos";
            // 
            // ddlProyectos
            // 
            this.ddlProyectos.FormattingEnabled = true;
            this.ddlProyectos.Location = new System.Drawing.Point(242, 42);
            this.ddlProyectos.Name = "ddlProyectos";
            this.ddlProyectos.Size = new System.Drawing.Size(381, 24);
            this.ddlProyectos.TabIndex = 1;
            this.ddlProyectos.SelectedIndexChanged += new System.EventHandler(this.ddlProyectos_SelectedIndexChanged);
            // 
            // ddlClientes
            // 
            this.ddlClientes.FormattingEnabled = true;
            this.ddlClientes.Location = new System.Drawing.Point(242, 12);
            this.ddlClientes.Name = "ddlClientes";
            this.ddlClientes.Size = new System.Drawing.Size(381, 24);
            this.ddlClientes.TabIndex = 3;
            this.ddlClientes.SelectedIndexChanged += new System.EventHandler(this.ddlClientes_SelectedIndexChanged);
            // 
            // labelClientes
            // 
            this.labelClientes.AutoSize = true;
            this.labelClientes.Location = new System.Drawing.Point(8, 15);
            this.labelClientes.Name = "labelClientes";
            this.labelClientes.Size = new System.Drawing.Size(88, 17);
            this.labelClientes.TabIndex = 2;
            this.labelClientes.Tag = "labelClientes";
            this.labelClientes.Text = "labelClientes";
            // 
            // labelFechaDesde
            // 
            this.labelFechaDesde.AutoSize = true;
            this.labelFechaDesde.Location = new System.Drawing.Point(8, 77);
            this.labelFechaDesde.Name = "labelFechaDesde";
            this.labelFechaDesde.Size = new System.Drawing.Size(118, 17);
            this.labelFechaDesde.TabIndex = 4;
            this.labelFechaDesde.Tag = "labelFechaDesde";
            this.labelFechaDesde.Text = "labelFechaDesde";
            // 
            // lblDesde
            // 
            this.lblDesde.AutoSize = true;
            this.lblDesde.Location = new System.Drawing.Point(239, 77);
            this.lblDesde.Name = "lblDesde";
            this.lblDesde.Size = new System.Drawing.Size(46, 17);
            this.lblDesde.TabIndex = 5;
            this.lblDesde.Text = "label2";
            // 
            // lblHasta
            // 
            this.lblHasta.AutoSize = true;
            this.lblHasta.Location = new System.Drawing.Point(239, 105);
            this.lblHasta.Name = "lblHasta";
            this.lblHasta.Size = new System.Drawing.Size(46, 17);
            this.lblHasta.TabIndex = 7;
            this.lblHasta.Text = "label3";
            // 
            // labelFechaHasta
            // 
            this.labelFechaHasta.AutoSize = true;
            this.labelFechaHasta.Location = new System.Drawing.Point(8, 105);
            this.labelFechaHasta.Name = "labelFechaHasta";
            this.labelFechaHasta.Size = new System.Drawing.Size(114, 17);
            this.labelFechaHasta.TabIndex = 6;
            this.labelFechaHasta.Tag = "labelFechaHasta";
            this.labelFechaHasta.Text = "labelFechaHasta";
            // 
            // lblPresupuesto
            // 
            this.lblPresupuesto.AutoSize = true;
            this.lblPresupuesto.Location = new System.Drawing.Point(239, 133);
            this.lblPresupuesto.Name = "lblPresupuesto";
            this.lblPresupuesto.Size = new System.Drawing.Size(46, 17);
            this.lblPresupuesto.TabIndex = 9;
            this.lblPresupuesto.Text = "label5";
            // 
            // labelPresupuesto
            // 
            this.labelPresupuesto.AutoSize = true;
            this.labelPresupuesto.Location = new System.Drawing.Point(8, 133);
            this.labelPresupuesto.Name = "labelPresupuesto";
            this.labelPresupuesto.Size = new System.Drawing.Size(118, 17);
            this.labelPresupuesto.TabIndex = 8;
            this.labelPresupuesto.Tag = "labelPresupuesto";
            this.labelPresupuesto.Text = "labelPresupuesto";
            // 
            // ddlTecnologia
            // 
            this.ddlTecnologia.FormattingEnabled = true;
            this.ddlTecnologia.Location = new System.Drawing.Point(242, 162);
            this.ddlTecnologia.Name = "ddlTecnologia";
            this.ddlTecnologia.Size = new System.Drawing.Size(381, 24);
            this.ddlTecnologia.TabIndex = 13;
            this.ddlTecnologia.SelectedIndexChanged += new System.EventHandler(this.ddlTecnologia_SelectedIndexChanged);
            // 
            // labelTecnologia
            // 
            this.labelTecnologia.AutoSize = true;
            this.labelTecnologia.Location = new System.Drawing.Point(8, 165);
            this.labelTecnologia.Name = "labelTecnologia";
            this.labelTecnologia.Size = new System.Drawing.Size(108, 17);
            this.labelTecnologia.TabIndex = 12;
            this.labelTecnologia.Tag = "labelTecnologia";
            this.labelTecnologia.Text = "labelTecnologia";
            // 
            // ddlExperiencia
            // 
            this.ddlExperiencia.FormattingEnabled = true;
            this.ddlExperiencia.Location = new System.Drawing.Point(242, 192);
            this.ddlExperiencia.Name = "ddlExperiencia";
            this.ddlExperiencia.Size = new System.Drawing.Size(381, 24);
            this.ddlExperiencia.TabIndex = 11;
            this.ddlExperiencia.SelectedIndexChanged += new System.EventHandler(this.ddlExperiencia_SelectedIndexChanged);
            // 
            // labelExperiencia
            // 
            this.labelExperiencia.AutoSize = true;
            this.labelExperiencia.Location = new System.Drawing.Point(8, 195);
            this.labelExperiencia.Name = "labelExperiencia";
            this.labelExperiencia.Size = new System.Drawing.Size(111, 17);
            this.labelExperiencia.TabIndex = 10;
            this.labelExperiencia.Tag = "labelExperiencia";
            this.labelExperiencia.Text = "labelExperiencia";
            // 
            // labelUsuariosCapacitados
            // 
            this.labelUsuariosCapacitados.AutoSize = true;
            this.labelUsuariosCapacitados.Location = new System.Drawing.Point(8, 225);
            this.labelUsuariosCapacitados.Name = "labelUsuariosCapacitados";
            this.labelUsuariosCapacitados.Size = new System.Drawing.Size(172, 17);
            this.labelUsuariosCapacitados.TabIndex = 14;
            this.labelUsuariosCapacitados.Tag = "labelUsuariosCapacitados";
            this.labelUsuariosCapacitados.Text = "labelUsuariosCapacitados";
            // 
            // ddlUsuariosCapacitados
            // 
            this.ddlUsuariosCapacitados.FormattingEnabled = true;
            this.ddlUsuariosCapacitados.Location = new System.Drawing.Point(242, 222);
            this.ddlUsuariosCapacitados.Name = "ddlUsuariosCapacitados";
            this.ddlUsuariosCapacitados.Size = new System.Drawing.Size(381, 24);
            this.ddlUsuariosCapacitados.TabIndex = 15;
            this.ddlUsuariosCapacitados.SelectedIndexChanged += new System.EventHandler(this.ddlUsuariosCapacitados_SelectedIndexChanged);
            // 
            // labelOcupadoDesde
            // 
            this.labelOcupadoDesde.AutoSize = true;
            this.labelOcupadoDesde.Location = new System.Drawing.Point(8, 253);
            this.labelOcupadoDesde.Name = "labelOcupadoDesde";
            this.labelOcupadoDesde.Size = new System.Drawing.Size(137, 17);
            this.labelOcupadoDesde.TabIndex = 16;
            this.labelOcupadoDesde.Tag = "labelOcupadoDesde";
            this.labelOcupadoDesde.Text = "labelOcupadoDesde";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 421);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 17);
            this.label1.TabIndex = 22;
            this.label1.Tag = "labelFechaDesde";
            this.label1.Text = "labelFechaDesde";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 451);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 17);
            this.label2.TabIndex = 20;
            this.label2.Tag = "labelFechaHasta";
            this.label2.Text = "labelFechaHasta";
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(242, 415);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(381, 22);
            this.dtpFrom.TabIndex = 23;
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(242, 446);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(381, 22);
            this.dtpTo.TabIndex = 24;
            // 
            // labelCostoHora
            // 
            this.labelCostoHora.AutoSize = true;
            this.labelCostoHora.Location = new System.Drawing.Point(8, 479);
            this.labelCostoHora.Name = "labelCostoHora";
            this.labelCostoHora.Size = new System.Drawing.Size(105, 17);
            this.labelCostoHora.TabIndex = 25;
            this.labelCostoHora.Tag = "labelCostoHora";
            this.labelCostoHora.Text = "labelCostoHora";
            // 
            // txtCostoHora
            // 
            this.txtCostoHora.Location = new System.Drawing.Point(242, 475);
            this.txtCostoHora.Name = "txtCostoHora";
            this.txtCostoHora.Size = new System.Drawing.Size(381, 22);
            this.txtCostoHora.TabIndex = 26;
            // 
            // labelUsuariosAsignados
            // 
            this.labelUsuariosAsignados.AutoSize = true;
            this.labelUsuariosAsignados.Location = new System.Drawing.Point(8, 508);
            this.labelUsuariosAsignados.Name = "labelUsuariosAsignados";
            this.labelUsuariosAsignados.Size = new System.Drawing.Size(160, 17);
            this.labelUsuariosAsignados.TabIndex = 27;
            this.labelUsuariosAsignados.Tag = "labelUsuariosAsignados";
            this.labelUsuariosAsignados.Text = "labelUsuariosAsignados";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(11, 529);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(612, 150);
            this.dataGridView1.TabIndex = 28;
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(350, 686);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(104, 23);
            this.buttonGuardar.TabIndex = 29;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "button1";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(242, 686);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(104, 23);
            this.buttonSalir.TabIndex = 30;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // buttonEliminar
            // 
            this.buttonEliminar.Location = new System.Drawing.Point(11, 686);
            this.buttonEliminar.Name = "buttonEliminar";
            this.buttonEliminar.Size = new System.Drawing.Size(104, 23);
            this.buttonEliminar.TabIndex = 31;
            this.buttonEliminar.Tag = "buttonEliminar";
            this.buttonEliminar.Text = "button1";
            this.buttonEliminar.UseVisualStyleBackColor = true;
            this.buttonEliminar.Click += new System.EventHandler(this.buttonEliminar_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(242, 253);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(381, 148);
            this.listBox1.TabIndex = 32;
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "DAC.chm";
            // 
            // AsignacionRecursosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 721);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.buttonEliminar);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.labelUsuariosAsignados);
            this.Controls.Add(this.txtCostoHora);
            this.Controls.Add(this.labelCostoHora);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelOcupadoDesde);
            this.Controls.Add(this.ddlUsuariosCapacitados);
            this.Controls.Add(this.labelUsuariosCapacitados);
            this.Controls.Add(this.ddlTecnologia);
            this.Controls.Add(this.labelTecnologia);
            this.Controls.Add(this.ddlExperiencia);
            this.Controls.Add(this.labelExperiencia);
            this.Controls.Add(this.lblPresupuesto);
            this.Controls.Add(this.labelPresupuesto);
            this.Controls.Add(this.lblHasta);
            this.Controls.Add(this.labelFechaHasta);
            this.Controls.Add(this.lblDesde);
            this.Controls.Add(this.labelFechaDesde);
            this.Controls.Add(this.ddlClientes);
            this.Controls.Add(this.labelClientes);
            this.Controls.Add(this.ddlProyectos);
            this.Controls.Add(this.labelProyectos);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "AsignacionRecursos");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "AsignacionRecursosForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "AsignacionRecursosForm";
            this.Load += new System.EventHandler(this.AsignacionRecursosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelProyectos;
        private System.Windows.Forms.ComboBox ddlProyectos;
        private System.Windows.Forms.ComboBox ddlClientes;
        private System.Windows.Forms.Label labelClientes;
        private System.Windows.Forms.Label labelFechaDesde;
        private System.Windows.Forms.Label lblDesde;
        private System.Windows.Forms.Label lblHasta;
        private System.Windows.Forms.Label labelFechaHasta;
        private System.Windows.Forms.Label lblPresupuesto;
        private System.Windows.Forms.Label labelPresupuesto;
        private System.Windows.Forms.ComboBox ddlTecnologia;
        private System.Windows.Forms.Label labelTecnologia;
        private System.Windows.Forms.ComboBox ddlExperiencia;
        private System.Windows.Forms.Label labelExperiencia;
        private System.Windows.Forms.Label labelUsuariosCapacitados;
        private System.Windows.Forms.ComboBox ddlUsuariosCapacitados;
        private System.Windows.Forms.Label labelOcupadoDesde;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label labelCostoHora;
        private System.Windows.Forms.TextBox txtCostoHora;
        private System.Windows.Forms.Label labelUsuariosAsignados;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.Button buttonEliminar;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}