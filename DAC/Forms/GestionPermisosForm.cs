﻿using DAC.BE;
using DAC.BLL;
using DAC.SEGURIDAD;
using DAC.UTILS;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestionPermisosForm : Form
    {
        private UsuarioBLL _bllUsuario = null;
        private UsuarioBE _usuarioEnSession = null;
        private UsuarioBE _usuarioSeleccionado = null;
        private IList<ComponenteBE> permisos = null;
        private IdiomaBE idiomaSeleccionado = null;
        private GestorIdioma _gestorIdioma = null;

        public GestionPermisosForm()
        {
            InitializeComponent();
        }

        private void GestionPermisos_Load(object sender, EventArgs e)
        {
            _gestorIdioma = GestorIdioma.GetInstance();
            _bllUsuario = UsuarioBLL.GetInstance();
            _usuarioEnSession = GestorSession.GetInstance().sessionUsuario;

            IList<UsuarioBE> _usuarios = _bllUsuario.SeleccionarTodos();

            _usuarios.Insert(0, new UsuarioBE() { id = 0, nombreUsuario = ((MainForm)ParentForm).optionSelectMessage });

            comboBox1.DataSource = _usuarios;
            comboBox1.DisplayMember = "nombreUsuario";
            comboBox1.ValueMember = "id";

            idiomaSeleccionado = GestorSession.GetInstance().sessionUsuario.idioma;
            UTILS.GestorIdioma.GetInstance().CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);

            permisos = _bllUsuario.ObtenerTodosLosPermisos();
            CargarTreeViewSistema();
        }

        private void CargarTreeViewSistema()
        {
            treeView2.Nodes.Clear();

            foreach (ComponenteBE permiso in permisos)
            {
                TreeNode node = new TreeNode();
                CargarTreeView(ref treeView2, ref node, permiso);
            }

        }

        private void CargarTreeViewUsuario(UsuarioBE usuario)
        {
            treeView1.Nodes.Clear();
            foreach (ComponenteBE permiso in usuario.permisos)
            {
                TreeNode node = new TreeNode();
                CargarTreeView(ref treeView1, ref node, permiso);
            }
        }

        private void CargarTreeView(ref TreeView view, ref TreeNode node, ComponenteBE permiso)
        {
            string _texto = string.Format("{0} - {1}", permiso.codigo, permiso.descripcion);
            TreeNode _node = new TreeNode(_texto);

            if (node.Text == string.Empty)
            {
                view.Nodes.Add(_node);
            }
            else
            {
                node.Nodes.Add(_node);
            }

            IList<ComponenteBE> _permisos = permiso.ObtenerPermisos();
            foreach (ComponenteBE _p in _permisos)
            {
                CargarTreeView(ref view, ref _node, _p);
            }
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex > 0)
            {
                _usuarioSeleccionado = (UsuarioBE)comboBox1.SelectedItem;
                UsuarioBLL.GetInstance().CargarPermisos(ref _usuarioSeleccionado);

                CargarTreeViewUsuario(_usuarioSeleccionado);
            }
            else
            {
                treeView1.Nodes.Clear();
            }
        }

        private void buttonAgregarPermiso_Click(object sender, EventArgs e)
        {
            if (_usuarioSeleccionado != null)
            {
                if (treeView2.SelectedNode != null)
                {
                    AgregarPermiso(treeView2.SelectedNode);

                    CargarTreeViewUsuario(_usuarioSeleccionado);

                    treeView2.SelectedNode = null;
                }
                else
                {
                    MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
                }
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjUsuarioNoSeleccionado").texto);
            }
        }

        private void AgregarPermiso(TreeNode node)
        {
            try
            {
                string key = node.Text.Split('-')[0].Trim();

                ComponenteBE permiso = ObtenerPermisoPorCodigo(key);

                bool usuarioTienePermiso = false;
                _bllUsuario.UsuarioTienePermiso(_usuarioSeleccionado.permisos, permiso.codigo, ref usuarioTienePermiso);

                if (!permiso.esPermisoBase)
                {
                    _usuarioSeleccionado.permisos = _bllUsuario.RemoverPermisosBasePorPertenecerAFamiliaAgregada(_usuarioSeleccionado.permisos, permiso);
                }

                if (!usuarioTienePermiso)
                {
                    _usuarioSeleccionado.permisos.Add(permiso);
                }
                else
                {
                    MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjUsuarioConPermiso").texto);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private ComponenteBE ObtenerPermisoPorCodigo(string codigo)
        {
            ComponenteBE permiso = null;
            try
            {
                ObtenerPermiso(permisos, codigo, ref permiso);
            }
            catch (Exception)
            {
                throw;
            }

            return permiso;
        }

        private void ObtenerPermiso(IList<ComponenteBE> permisos, string codigo, ref ComponenteBE permiso)
        {
            try
            {
                if (permiso == null)
                {
                    foreach (ComponenteBE _permiso in permisos)
                    {
                        if (_permiso.codigo == codigo)
                            permiso = _permiso;

                        ObtenerPermiso(_permiso.ObtenerPermisos(), codigo, ref permiso);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void buttonRemoverPermiso_Click(object sender, EventArgs e)
        {
            try
            {
                if (_usuarioSeleccionado != null)
                {
                    if (treeView1.SelectedNode != null)
                    {
                        string key = treeView1.SelectedNode.Text.Split('-')[0].Trim();

                        ComponenteBE permiso = ObtenerPermisoPorCodigo(key);

                        bool usuarioTienePermiso = false;
                        _bllUsuario.UsuarioTienePermiso(_usuarioSeleccionado.permisos, permiso.codigo, ref usuarioTienePermiso);

                        IList<ComponenteBE> permisos = _usuarioSeleccionado.permisos;
                        ComponenteBE p = permisos.FirstOrDefault(x => x.codigo == permiso.codigo);

                        if (p != null)
                        {
                            permisos.Remove(p);
                        }
                        else
                        {
                            MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNoRemoverPatenteDeFamilia").texto);
                        }

                        if (usuarioTienePermiso) _usuarioSeleccionado.permisos = permisos;

                        CargarTreeViewUsuario(_usuarioSeleccionado);

                        treeView2.SelectedNode = null;
                    }
                    else
                    {
                        MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
                    }
                }
                else
                {
                    MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjUsuarioNoSeleccionado").traduccion.texto);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); ;
            }
        }

        private ComponenteBE ObtenerPermisoDentroDePermisos(ComponenteBE permiso, IList<ComponenteBE> permisos)
        {
            ComponenteBE _permiso = null;

            try
            {
                foreach (ComponenteBE _p in permisos)
                {
                    if (_permiso != null) continue;

                    if (!_p.esPermisoBase)
                    {
                        _permiso = ObtenerPermisoDentroDePermisos(permiso, _p.ObtenerPermisos());
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return _permiso;
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            if (_usuarioSeleccionado != null)
            {
                _bllUsuario.ModificarPermisos(_usuarioSeleccionado);
                if (_usuarioEnSession.nombreUsuario == _usuarioSeleccionado.nombreUsuario)
                {
                    _usuarioEnSession.permisos = _usuarioSeleccionado.permisos;
                    GestorSession.GetInstance().ActualizarUsuarioEnSesion(_usuarioEnSession);
                }
                ((MainForm)ParentForm).ActualizarVista();

                MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjCambiosGuardadosOk").traduccion.texto);
            }
            else
            {
                MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjUsuarioNoSeleccionado").traduccion.texto);
            }
        }
    }
}
