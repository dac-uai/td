﻿namespace DAC.Forms
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.labelNombreUsuario = new System.Windows.Forms.Label();
            this.txtNombreUsuario = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.buttonIngresar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.ddlSeleccionIdioma = new System.Windows.Forms.ComboBox();
            this.labelSeleccionarIdioma = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelNombreUsuario
            // 
            this.labelNombreUsuario.AutoSize = true;
            this.labelNombreUsuario.Location = new System.Drawing.Point(9, 64);
            this.labelNombreUsuario.Name = "labelNombreUsuario";
            this.labelNombreUsuario.Size = new System.Drawing.Size(137, 17);
            this.labelNombreUsuario.TabIndex = 0;
            this.labelNombreUsuario.Tag = "labelNombreUsuario";
            this.labelNombreUsuario.Text = "labelNombreUsuario";
            // 
            // txtNombreUsuario
            // 
            this.txtNombreUsuario.Location = new System.Drawing.Point(12, 85);
            this.txtNombreUsuario.Name = "txtNombreUsuario";
            this.txtNombreUsuario.Size = new System.Drawing.Size(173, 22);
            this.txtNombreUsuario.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(12, 154);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(173, 22);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(9, 133);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(99, 17);
            this.labelPassword.TabIndex = 2;
            this.labelPassword.Tag = "labelPassword";
            this.labelPassword.Text = "labelPassword";
            // 
            // buttonIngresar
            // 
            this.buttonIngresar.Location = new System.Drawing.Point(434, 233);
            this.buttonIngresar.Name = "buttonIngresar";
            this.buttonIngresar.Size = new System.Drawing.Size(128, 23);
            this.buttonIngresar.TabIndex = 4;
            this.buttonIngresar.Tag = "buttonIngresar";
            this.buttonIngresar.Text = "buttonIngresar";
            this.buttonIngresar.UseVisualStyleBackColor = true;
            this.buttonIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(304, 233);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(124, 23);
            this.buttonSalir.TabIndex = 5;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // ddlSeleccionIdioma
            // 
            this.ddlSeleccionIdioma.FormattingEnabled = true;
            this.ddlSeleccionIdioma.Location = new System.Drawing.Point(12, 223);
            this.ddlSeleccionIdioma.Name = "ddlSeleccionIdioma";
            this.ddlSeleccionIdioma.Size = new System.Drawing.Size(173, 24);
            this.ddlSeleccionIdioma.TabIndex = 6;
            this.ddlSeleccionIdioma.SelectedIndexChanged += new System.EventHandler(this.ddlSeleccionIdioma_SelectedIndexChanged_1);
            // 
            // labelSeleccionarIdioma
            // 
            this.labelSeleccionarIdioma.AutoSize = true;
            this.labelSeleccionarIdioma.Location = new System.Drawing.Point(12, 200);
            this.labelSeleccionarIdioma.Name = "labelSeleccionarIdioma";
            this.labelSeleccionarIdioma.Size = new System.Drawing.Size(153, 17);
            this.labelSeleccionarIdioma.TabIndex = 7;
            this.labelSeleccionarIdioma.Tag = "labelSeleccionarIdioma";
            this.labelSeleccionarIdioma.Text = "labelSeleccionarIdioma";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(574, 268);
            this.Controls.Add(this.labelSeleccionarIdioma);
            this.Controls.Add(this.ddlSeleccionIdioma);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonIngresar);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.txtNombreUsuario);
            this.Controls.Add(this.labelNombreUsuario);
            this.DoubleBuffered = true;
            this.Name = "Login";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNombreUsuario;
        private System.Windows.Forms.TextBox txtNombreUsuario;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Button buttonIngresar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.ComboBox ddlSeleccionIdioma;
        private System.Windows.Forms.Label labelSeleccionarIdioma;
    }
}