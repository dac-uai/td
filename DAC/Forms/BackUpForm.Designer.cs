﻿namespace DAC.Forms
{
    partial class BackUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBackUp = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.txtNombreBackup = new System.Windows.Forms.TextBox();
            this.labelNombreBackUp = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.buttonSeleccionarCarpeta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonBackUp
            // 
            this.buttonBackUp.Location = new System.Drawing.Point(12, 166);
            this.buttonBackUp.Name = "buttonBackUp";
            this.buttonBackUp.Size = new System.Drawing.Size(257, 23);
            this.buttonBackUp.TabIndex = 0;
            this.buttonBackUp.Tag = "buttonBackUp";
            this.buttonBackUp.Text = "buttonBackUp";
            this.buttonBackUp.UseVisualStyleBackColor = true;
            this.buttonBackUp.Click += new System.EventHandler(this.buttonBackUp_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(12, 218);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(257, 23);
            this.buttonSalir.TabIndex = 1;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
            // 
            // txtNombreBackup
            // 
            this.txtNombreBackup.Location = new System.Drawing.Point(12, 79);
            this.txtNombreBackup.Name = "txtNombreBackup";
            this.txtNombreBackup.Size = new System.Drawing.Size(257, 22);
            this.txtNombreBackup.TabIndex = 3;
            // 
            // labelNombreBackUp
            // 
            this.labelNombreBackUp.AutoSize = true;
            this.labelNombreBackUp.Location = new System.Drawing.Point(12, 56);
            this.labelNombreBackUp.Name = "labelNombreBackUp";
            this.labelNombreBackUp.Size = new System.Drawing.Size(137, 17);
            this.labelNombreBackUp.TabIndex = 4;
            this.labelNombreBackUp.Tag = "labelNombreBackUp";
            this.labelNombreBackUp.Text = "labelNombreBackUp";
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // buttonSeleccionarCarpeta
            // 
            this.buttonSeleccionarCarpeta.Location = new System.Drawing.Point(12, 108);
            this.buttonSeleccionarCarpeta.Name = "buttonSeleccionarCarpeta";
            this.buttonSeleccionarCarpeta.Size = new System.Drawing.Size(257, 23);
            this.buttonSeleccionarCarpeta.TabIndex = 5;
            this.buttonSeleccionarCarpeta.Tag = "buttonSeleccionarCarpeta";
            this.buttonSeleccionarCarpeta.Text = "button1";
            this.buttonSeleccionarCarpeta.UseVisualStyleBackColor = true;
            this.buttonSeleccionarCarpeta.Click += new System.EventHandler(this.button1_Click);
            // 
            // BackUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.buttonSeleccionarCarpeta);
            this.Controls.Add(this.labelNombreBackUp);
            this.Controls.Add(this.txtNombreBackup);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonBackUp);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "Backup");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "BackUpForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "BackUpForm";
            this.Load += new System.EventHandler(this.BackUpForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBackUp;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox txtNombreBackup;
        private System.Windows.Forms.Label labelNombreBackUp;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.Button buttonSeleccionarCarpeta;
    }
}