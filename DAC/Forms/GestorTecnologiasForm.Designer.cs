﻿namespace DAC.Forms
{
    partial class GestorTecnologiaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCodigo = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.labelTecnologia = new System.Windows.Forms.Label();
            this.ddlTecnologia = new System.Windows.Forms.ComboBox();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // labelCodigo
            // 
            this.labelCodigo.AutoSize = true;
            this.labelCodigo.Location = new System.Drawing.Point(11, 78);
            this.labelCodigo.Name = "labelCodigo";
            this.labelCodigo.Size = new System.Drawing.Size(82, 17);
            this.labelCodigo.TabIndex = 0;
            this.labelCodigo.Tag = "labelCodigo";
            this.labelCodigo.Text = "labelCodigo";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(10, 107);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(258, 22);
            this.txtCodigo.TabIndex = 1;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(10, 172);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(258, 22);
            this.txtDescripcion.TabIndex = 3;
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.AutoSize = true;
            this.labelDescripcion.Location = new System.Drawing.Point(11, 143);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(112, 17);
            this.labelDescripcion.TabIndex = 2;
            this.labelDescripcion.Tag = "labelDescripcion";
            this.labelDescripcion.Text = "labelDescripcion";
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(152, 218);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(116, 23);
            this.buttonGuardar.TabIndex = 4;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(12, 218);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(115, 23);
            this.buttonSalir.TabIndex = 5;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // labelTecnologia
            // 
            this.labelTecnologia.AutoSize = true;
            this.labelTecnologia.Location = new System.Drawing.Point(10, 13);
            this.labelTecnologia.Name = "labelTecnologia";
            this.labelTecnologia.Size = new System.Drawing.Size(108, 17);
            this.labelTecnologia.TabIndex = 6;
            this.labelTecnologia.Tag = "labelTecnologia";
            this.labelTecnologia.Text = "labelTecnologia";
            // 
            // ddlTecnologia
            // 
            this.ddlTecnologia.FormattingEnabled = true;
            this.ddlTecnologia.Location = new System.Drawing.Point(13, 34);
            this.ddlTecnologia.Name = "ddlTecnologia";
            this.ddlTecnologia.Size = new System.Drawing.Size(255, 24);
            this.ddlTecnologia.TabIndex = 7;
            this.ddlTecnologia.SelectedIndexChanged += new System.EventHandler(this.ddlTecnologias_SelectedIndexChanged);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // GestorTecnologiaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.ddlTecnologia);
            this.Controls.Add(this.labelTecnologia);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.labelCodigo);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "Tecnologia");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.KeyPreview = true;
            this.Name = "GestorTecnologiaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "GestorTecnologiaForm";
            this.Load += new System.EventHandler(this.GestorTecnologiaForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCodigo;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.Label labelTecnologia;
        private System.Windows.Forms.ComboBox ddlTecnologia;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}