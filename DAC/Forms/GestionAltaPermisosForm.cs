﻿using DAC.BE;
using DAC.BLL;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestionAltaPermisosForm : Form
    {
        private IList<ComponenteBE> permisos = null;
        private IList<ComponenteBE> permisosNuevos = null;
        private UsuarioBLL _bllUsuario = null;
        private IdiomaBE idiomaSeleccionado = null;
        private UTILS.GestorIdioma _gestorIdioma = null;

        public GestionAltaPermisosForm()
        {
            InitializeComponent();
            _gestorIdioma = UTILS.GestorIdioma.GetInstance();
            _bllUsuario = UsuarioBLL.GetInstance();
            permisosNuevos = new List<ComponenteBE>();

            idiomaSeleccionado = GestorSession.GetInstance().sessionUsuario.idioma;
            _gestorIdioma.CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GestionAltaPermisosForm_Load(object sender, EventArgs e)
        {
            permisos = _bllUsuario.ObtenerTodosLosPermisos();
            CargarTreeViewSistema();
        }

        private void CargarTreeViewSistema()
        {
            treeView2.Nodes.Clear();

            foreach (ComponenteBE permiso in permisos)
            {
                TreeNode node = new TreeNode();
                CargarTreeView(ref treeView2, ref node, permiso);
            }
        }

        private void CargarTreeViewNuevo()
        {
            treeView1.Nodes.Clear();

            foreach (ComponenteBE permiso in permisosNuevos)
            {
                TreeNode node = new TreeNode();
                CargarTreeView(ref treeView1, ref node, permiso);
            }
        }

        private void CargarTreeView(ref TreeView view, ref TreeNode node, ComponenteBE permiso)
        {
            string _texto = string.Format("{0} - {1}", permiso.codigo, permiso.descripcion);
            TreeNode _node = new TreeNode(_texto);

            if (node.Text == string.Empty)
            {
                view.Nodes.Add(_node);
            }
            else
            {
                node.Nodes.Add(_node);
            }

            IList<ComponenteBE> _permisos = permiso.ObtenerPermisos();
            foreach (ComponenteBE _p in _permisos)
            {
                CargarTreeView(ref view, ref _node, _p);
            }
        }

        private void buttonAgregarPermiso_Click(object sender, EventArgs e)
        {
            if (treeView2.SelectedNode != null)
            {
                AgregarPermiso(treeView2.SelectedNode);

                treeView2.SelectedNode = null;
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
            }
        }

        private void AgregarPermiso(TreeNode node)
        {
            try
            {
                string key = node.Text.Split('-')[0].Trim();

                ComponenteBE permiso = ObtenerPermisoPorCodigo(key);

                bool usuarioTienePermiso = UsuarioTienePermiso(permiso.codigo);

                if (!permiso.esPermisoBase)
                {
                    permisosNuevos = _bllUsuario.RemoverPermisosBasePorPertenecerAFamiliaAgregada(permisosNuevos, permiso);
                }
                if (!usuarioTienePermiso)
                {
                    permisosNuevos.Add(permiso);
                    CargarTreeViewNuevo();
                }
                else
                {
                    MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjUsuarioConPermiso").texto);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UsuarioTienePermiso(string codigoPermiso)
        {
            foreach (ComponenteBE permiso in permisosNuevos)
            {
                if (UsuarioTiene(permiso, codigoPermiso)) return true;
            }

            return false;
        }

        private bool UsuarioTiene(ComponenteBE permiso, string codigoPermiso)
        {

            foreach (ComponenteBE _p in permiso.ObtenerPermisos())
            {
                if (UsuarioTiene(_p, codigoPermiso)) return true;
            }

            return (permiso.codigo == codigoPermiso);
        }

        private ComponenteBE ObtenerPermisoPorCodigo(string codigo)
        {
            ComponenteBE permiso = null;
            try
            {
                ObtenerPermiso(permisos, codigo, ref permiso);
            }
            catch (Exception)
            {
                throw;
            }

            return permiso;
        }

        private void ObtenerPermiso(IList<ComponenteBE> permisos, string codigo, ref ComponenteBE permiso)
        {
            try
            {
                if (permiso == null)
                {
                    foreach (ComponenteBE _permiso in permisos)
                    {
                        if (_permiso.codigo == codigo)
                            permiso = _permiso;

                        ObtenerPermiso(_permiso.ObtenerPermisos(), codigo, ref permiso);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void buttonRemoverPermiso_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                string key = treeView1.SelectedNode.Text.Split('-')[0].Trim();

                ComponenteBE permiso = ObtenerPermisoPorCodigo(key);

                bool usuarioTienePermiso = UsuarioTienePermiso(permiso.codigo);

                if (usuarioTienePermiso)
                {
                    ComponenteBE p = permisosNuevos.FirstOrDefault(x => x.codigo == permiso.codigo);

                    if (p != null)
                    {
                        permisosNuevos.Remove(p);
                    }
                    else
                    {
                        MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNoRemoverPatenteDeFamilia").texto);
                    }

                    CargarTreeViewNuevo();
                }
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
            }
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text;
            string descripcion = txtDescripcion.Text;
            if (!string.IsNullOrEmpty(codigo) && !string.IsNullOrEmpty(descripcion))
            {
                ComponenteBE permisoPadre = new PermisoCompuestoBE()
                {
                    codigo = codigo,
                    descripcion = descripcion
                };
                _bllUsuario.GuardarNuevaFamilia(permisoPadre, permisosNuevos);

                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjCambiosGuardadosOk").texto);

                txtCodigo.Text = string.Empty;
                txtDescripcion.Text = string.Empty;
                treeView1.Nodes.Clear();
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjInformacionFaltante").texto);
            }
        }
    }
}
