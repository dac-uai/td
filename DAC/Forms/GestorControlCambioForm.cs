﻿using DAC.BE;
using DAC.BLL;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestorControlCambioForm : Form
    {
        private ControlCambioBLL _controlCambioBll = null;
        private UsuarioBE nuevoUsuario = null;
        private bool modificarPass = false;

        private MainForm mainForm = null;
        private IdiomaBE idiomaSeleccionado = null;
        private UTILS.GestorIdioma _gestorIdioma = null;

        public GestorControlCambioForm()
        {
            InitializeComponent();
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            if (nuevoUsuario != null)
            {
                DialogResult resultado = MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjValidarControlCambio").texto, string.Empty, MessageBoxButtons.YesNo);

                if (resultado.ToString() == "No") return;

                UsuarioBLL.GetInstance().Modificar(nuevoUsuario, modificarPass, nuevoUsuario.password);
                CargarGrilla();

                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjCambiosGuardadosOk").texto);
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjUsuarioNoSeleccionado").texto);
            }
        }

        private void GestorControlCambio_Load(object sender, EventArgs e)
        {
            mainForm = (MainForm)ParentForm;
            _gestorIdioma = UTILS.GestorIdioma.GetInstance();

            idiomaSeleccionado = GestorSession.GetInstance().sessionUsuario.idioma;

            _controlCambioBll = new ControlCambioBLL();
            label1.Text = string.Empty;
            labelUsuarioModificado.Visible = false;
            labelUsuarioOriginal.Visible = false;
            label2.Text = string.Empty;
            CargarGrilla();
        }

        private void CargarGrilla()
        {
            dgvModificaciones.DataSource = null;

            dgvModificaciones.ColumnCount = 8;

            dgvModificaciones.Columns[0].Name = "idEntidad";
            dgvModificaciones.Columns[0].DataPropertyName = "idEntidad";
            dgvModificaciones.Columns[0].Visible = false;

            dgvModificaciones.Columns[1].Name = "descripcion";
            dgvModificaciones.Columns[1].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerNombreUsuario").texto;
            dgvModificaciones.Columns[1].DataPropertyName = "descripcion";

            dgvModificaciones.Columns[2].Name = "accion";
            dgvModificaciones.Columns[2].DataPropertyName = "accion";
            dgvModificaciones.Columns[2].Visible = false;

            dgvModificaciones.Columns[3].Name = "campo";
            dgvModificaciones.Columns[3].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerCampo").texto;
            dgvModificaciones.Columns[3].DataPropertyName = "campo";

            dgvModificaciones.Columns[4].Name = "valorOriginal";
            dgvModificaciones.Columns[4].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerValorOriginal").texto;
            dgvModificaciones.Columns[4].DataPropertyName = "valorOriginal";

            dgvModificaciones.Columns[5].Name = "valorNuevo";
            dgvModificaciones.Columns[5].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerValorNuevo").texto;
            dgvModificaciones.Columns[5].DataPropertyName = "valorNuevo";

            dgvModificaciones.Columns[6].Name = "fechaModificacion";
            dgvModificaciones.Columns[6].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerFechaModificacion").texto;
            dgvModificaciones.Columns[6].DataPropertyName = "fechaModificacion";

            dgvModificaciones.Columns[7].Name = "session";
            dgvModificaciones.Columns[7].HeaderText = "Session";
            dgvModificaciones.Columns[7].DataPropertyName = "session";

            dgvModificaciones.DataSource = _controlCambioBll.ObtenerModificacionesHistoricas();
            dgvModificaciones.Update();
            dgvModificaciones.ReadOnly = true;
            dgvModificaciones.Width = mainForm.Width - 60;

            mainForm.OrdenarGrilla(ref dgvModificaciones, 6);

            //for (int i = 0; i < dgvModificaciones.Rows.Count; i++)
            //{
            //    if (i == 0) continue;

            //    DataGridViewRow actual = dgvModificaciones.Rows[i];
            //    DataGridViewRow previa = dgvModificaciones.Rows[i - 1];

            //    string sessionPrevia = previa.Cells["session"].Value.ToString();
            //    string sessionActual = actual.Cells["session"].Value.ToString();

            //    bool mismaSession = sessionActual == sessionPrevia;
            //    Color previaColor = previa.DefaultCellStyle.BackColor;

            //    if (!mismaSession)
            //    {
            //        actual.DefaultCellStyle.BackColor = previaColor == Color.White ? Color.Gray : Color.White;
            //    }
            //}

        }

        private void dgvModificaciones_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvModificaciones.SelectedCells.Count > 0)
            {
                IList<ControlCambioMapper> mapperList = new List<ControlCambioMapper>();
                int rowIndex = dgvModificaciones.SelectedCells[0].RowIndex;
                int idEntidad = int.Parse(dgvModificaciones.Rows[rowIndex].Cells["idEntidad"].Value.ToString());
                string sessionModificacion = dgvModificaciones.Rows[rowIndex].Cells["session"].Value.ToString();

                foreach (DataGridViewRow row in dgvModificaciones.Rows)
                {
                    if (int.Parse(row.Cells["idEntidad"].Value.ToString()) == idEntidad
                        && row.Cells["accion"].Value.ToString() != "Insert"
                        && row.Cells["session"].Value.ToString() == sessionModificacion
                        )
                    {
                        mapperList.Add(new ControlCambioMapper()
                        {
                            idEntidad = int.Parse(row.Cells["idEntidad"].Value.ToString()),
                            accion = row.Cells["accion"].Value.ToString(),
                            campo = row.Cells["campo"].Value.ToString(),
                            session = row.Cells["session"].Value.ToString(),
                            valorNuevo = row.Cells["valorNuevo"].Value.ToString(),
                            valorOriginal = row.Cells["valorOriginal"].Value.ToString(),
                            descripcion = row.Cells["descripcion"].Value.ToString(),
                            fechaModificacion = DateTime.Parse(row.Cells["fechaModificacion"].Value.ToString())
                        });
                    }
                }

                nuevoUsuario = _controlCambioBll.ObtenerEntidadPorMappers(mapperList);
                if (nuevoUsuario != null)
                {
                    label2.Text = nuevoUsuario.ToStringCompleto();
                    labelUsuarioOriginal.Visible = true;

                    UsuarioBE usuarioExistente = UsuarioBLL.GetInstance().Seleccionar(idEntidad);
                    label1.Text = usuarioExistente.ToStringCompleto();
                    labelUsuarioModificado.Visible = true;

                    modificarPass = nuevoUsuario.password == usuarioExistente.password;
                }
            }
        }
    }
}