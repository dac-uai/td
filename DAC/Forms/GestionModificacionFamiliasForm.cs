﻿using DAC.BE;
using DAC.BLL;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestionModificacionFamiliasForm : Form
    {
        private UsuarioBLL _bllUsuario = null;
        private IList<ComponenteBE> permisos = null;
        private ComponenteBE _componenteSeleccionado = null;
        private IdiomaBE idiomaSeleccionado = null;
        private UTILS.GestorIdioma _gestorIdioma = null;

        public GestionModificacionFamiliasForm()
        {
            InitializeComponent();
        }

        private void CargarTreeViewSistema()
        {
            treeView2.Nodes.Clear();

            foreach (ComponenteBE permiso in permisos)
            {
                TreeNode node = new TreeNode();
                CargarTreeView(ref treeView2, ref node, permiso);
            }

        }

        private void CargarTreeViewUsuario(ComponenteBE permisoSeleccionado)
        {
            treeView1.Nodes.Clear();
            foreach (ComponenteBE permiso in permisoSeleccionado.ObtenerPermisos())
            {
                TreeNode node = new TreeNode();
                CargarTreeView(ref treeView1, ref node, permiso);
            }
        }

        private void CargarTreeView(ref TreeView view, ref TreeNode node, ComponenteBE permiso)
        {
            string _texto = string.Format("{0} - {1}", permiso.codigo, permiso.descripcion);
            TreeNode _node = new TreeNode(_texto);

            if (node.Text == string.Empty)
            {
                view.Nodes.Add(_node);
            }
            else
            {
                node.Nodes.Add(_node);
            }

            IList<ComponenteBE> _permisos = permiso.ObtenerPermisos();
            foreach (ComponenteBE _p in _permisos)
            {
                CargarTreeView(ref view, ref _node, _p);
            }
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex > 0)
            {
                _componenteSeleccionado = (ComponenteBE)comboBox1.SelectedItem;

                CargarTreeViewUsuario(_componenteSeleccionado);
            }
            else
            {
                treeView1.Nodes.Clear();
            }
        }

        private void buttonAgregarPermiso_Click(object sender, EventArgs e)
        {
            if (_componenteSeleccionado != null)
            {
                if (treeView2.SelectedNode != null)
                {
                    AgregarPermiso(treeView2.SelectedNode);

                    CargarTreeViewUsuario(_componenteSeleccionado);

                    treeView2.SelectedNode = null;
                }
                else
                {
                    MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
                }
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
            }
        }

        private void AgregarPermiso(TreeNode node)
        {
            try
            {
                string key = node.Text.Split('-')[0].Trim();

                ComponenteBE permiso = ObtenerPermisoPorCodigo(key);

                bool usuarioTienePermiso = UsuarioTienePermiso(key);
                bool esElMismoPermiso = _componenteSeleccionado.codigo == permiso.codigo;
                bool nuevoPermisoContieneAlBase = PermisoSeleccionadoPerteneceAlPermisoNuevo(_componenteSeleccionado, permiso);
                if (!usuarioTienePermiso && !esElMismoPermiso && !nuevoPermisoContieneAlBase)
                {
                    _componenteSeleccionado.Agregar(permiso);
                }
                else
                {
                    MessageBox.Show("No se puede agregar el permiso a la familia");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool PermisoSeleccionadoPerteneceAlPermisoNuevo(ComponenteBE componente, ComponenteBE nuevoPermiso)
        {
            if (nuevoPermiso.esPermisoBase) return false;

            foreach (ComponenteBE permiso in nuevoPermiso.ObtenerPermisos())
            {
                if (UsuarioTiene(permiso, componente.codigo)) return true;
            }

            return false;
        }

        public bool UsuarioTienePermiso(string codigoPermiso)
        {
            foreach (ComponenteBE permiso in _componenteSeleccionado.ObtenerPermisos())
            {
                if (UsuarioTiene(permiso, codigoPermiso)) return true;
            }

            return false;
        }

        private bool UsuarioTiene(ComponenteBE permiso, string codigoPermiso)
        {
            foreach (ComponenteBE _p in permiso.ObtenerPermisos())
            {
                if (UsuarioTiene(_p, codigoPermiso)) return true;
            }

            return (permiso.codigo == codigoPermiso);
        }

        private ComponenteBE ObtenerPermisoPorCodigo(string codigo)
        {
            ComponenteBE permiso = null;
            try
            {
                ObtenerPermiso(permisos, codigo, ref permiso);
            }
            catch (Exception)
            {
                throw;
            }

            return permiso;
        }

        private void ObtenerPermiso(IList<ComponenteBE> permisos, string codigo, ref ComponenteBE permiso)
        {
            try
            {
                if (permiso == null)
                {
                    foreach (ComponenteBE _permiso in permisos)
                    {
                        if (_permiso.codigo == codigo)
                            permiso = _permiso;

                        ObtenerPermiso(_permiso.ObtenerPermisos(), codigo, ref permiso);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void buttonRemoverPermiso_Click(object sender, EventArgs e)
        {
            if (_componenteSeleccionado != null)
            {
                if (treeView1.SelectedNode != null)
                {
                    string key = treeView1.SelectedNode.Text.Split('-')[0].Trim();

                    ComponenteBE permiso = ObtenerPermisoPorCodigo(key);

                    bool usuarioTienePermiso = UsuarioTienePermiso(key);

                    ComponenteBE p = _componenteSeleccionado.ObtenerPermisos().First(x => x.codigo == permiso.codigo);
                    if (usuarioTienePermiso) _componenteSeleccionado.Remover(p);

                    CargarTreeViewUsuario(_componenteSeleccionado);
                }
                else
                {
                    MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
                }
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
            }
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            if (_componenteSeleccionado != null)
            {
                _bllUsuario.ModificarFamiliaDePermisos(_componenteSeleccionado);

                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjCambiosGuardadosOk").texto);
                CargarTreeViewUsuario(_componenteSeleccionado);
                CargarDropDown();
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
            }
        }

        private void GestionModificacionFamiliasForm_Load(object sender, EventArgs e)
        {
            _gestorIdioma = UTILS.GestorIdioma.GetInstance();
            _bllUsuario = UsuarioBLL.GetInstance();

            idiomaSeleccionado = GestorSession.GetInstance().sessionUsuario.idioma;
            _gestorIdioma.CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);

            CargarDropDown();

            permisos = _bllUsuario.ObtenerTodosLosPermisos();
            CargarTreeViewSistema();
        }

        private void CargarDropDown()
        {
            IList<ComponenteBE> _permisos = _bllUsuario.ObtenerTodosLosPermisos();

            _permisos.Insert(0, new PermisoCompuestoBE() { id = 0, descripcion = ((MainForm)ParentForm).optionSelectMessage });

            comboBox1.DataSource = _permisos;
            comboBox1.DisplayMember = "descripcion";
            comboBox1.ValueMember = "id";
        }
    }
}
