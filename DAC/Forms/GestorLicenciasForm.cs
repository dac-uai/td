﻿using DAC.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestorLicenciasForm : Form
    {
        public GestorLicenciasForm()
        {
            InitializeComponent();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text;
            string descripcion = txtDescripcion.Text;

            if (!(string.IsNullOrEmpty(codigo) && string.IsNullOrEmpty(descripcion)))
            {
                LicenciaBE licencia = new LicenciaBE();

                if (ddlLicencias.SelectedIndex > 0) licencia = (LicenciaBE)ddlLicencias.SelectedItem;

                licencia.codigo = codigo;
                licencia.descripcion = descripcion;

                licencia = BLL.LicenciaBLL.GetInstance().Guardar(licencia);
                CargarLicencias();

                if (licencia != null)
                {
                    MessageBox.Show("Licencia generada correctamente");
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado todos los campos requeridos");
            }
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GestorLicenciasForm_Load(object sender, EventArgs e)
        {
            CargarLicencias();
        }

        private void ddlLicencias_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCodigo.Text = string.Empty;
            txtDescripcion.Text = string.Empty;

            if (ddlLicencias.SelectedIndex > 0)
            {
                LicenciaBE Licencia = (LicenciaBE)ddlLicencias.SelectedItem;

                txtCodigo.Text = Licencia.codigo;
                txtDescripcion.Text = Licencia.descripcion;
            }
        }

        private void CargarLicencias()
        {
            try
            {
                IList<LicenciaBE> Licencias = BLL.LicenciaBLL.GetInstance().SeleccionarTodos();
                Licencias.Insert(0, new LicenciaBE() { id = 0, descripcion = ((MainForm)ParentForm).optionSelectMessage });

                ddlLicencias.DataSource = Licencias;

                ddlLicencias.DataSource = Licencias;
                ddlLicencias.DisplayMember = "descripcion";
                ddlLicencias.ValueMember = "id";
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
