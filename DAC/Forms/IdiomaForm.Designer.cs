﻿namespace DAC.Forms
{
    partial class IdiomaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddlSeleccionIdioma = new System.Windows.Forms.ComboBox();
            this.lblSeleccionarIdioma = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // ddlSeleccionIdioma
            // 
            this.ddlSeleccionIdioma.FormattingEnabled = true;
            this.ddlSeleccionIdioma.Location = new System.Drawing.Point(12, 29);
            this.ddlSeleccionIdioma.Name = "ddlSeleccionIdioma";
            this.ddlSeleccionIdioma.Size = new System.Drawing.Size(181, 24);
            this.ddlSeleccionIdioma.TabIndex = 0;
            this.ddlSeleccionIdioma.SelectedIndexChanged += new System.EventHandler(this.ddlSeleccionIdioma_SelectedIndexChanged);
            // 
            // lblSeleccionarIdioma
            // 
            this.lblSeleccionarIdioma.AutoSize = true;
            this.lblSeleccionarIdioma.Location = new System.Drawing.Point(12, 9);
            this.lblSeleccionarIdioma.Name = "lblSeleccionarIdioma";
            this.lblSeleccionarIdioma.Size = new System.Drawing.Size(46, 17);
            this.lblSeleccionarIdioma.TabIndex = 1;
            this.lblSeleccionarIdioma.Tag = "labelSeleccionarIdioma";
            this.lblSeleccionarIdioma.Text = "label1";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(118, 99);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.Tag = "buttonGuardar";
            this.btnGuardar.Text = "button1";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // IdiomaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.lblSeleccionarIdioma);
            this.Controls.Add(this.ddlSeleccionIdioma);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "Idioma");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "IdiomaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Idioma";
            this.Load += new System.EventHandler(this.Idioma_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlSeleccionIdioma;
        private System.Windows.Forms.Label lblSeleccionarIdioma;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}