﻿namespace DAC.Forms
{
    partial class CargarHorasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelProyectos = new System.Windows.Forms.Label();
            this.labelCantidadHoras = new System.Windows.Forms.Label();
            this.ddlProyectos = new System.Windows.Forms.ComboBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.labelDescripcionDelTrabajo = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.labelFechaTrabajada = new System.Windows.Forms.Label();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.labelOcupadoDesde = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.textBox1 = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // labelProyectos
            // 
            this.labelProyectos.AutoSize = true;
            this.labelProyectos.Location = new System.Drawing.Point(13, 16);
            this.labelProyectos.Name = "labelProyectos";
            this.labelProyectos.Size = new System.Drawing.Size(101, 17);
            this.labelProyectos.TabIndex = 0;
            this.labelProyectos.Tag = "labelProyectos";
            this.labelProyectos.Text = "labelProyectos";
            // 
            // labelCantidadHoras
            // 
            this.labelCantidadHoras.AutoSize = true;
            this.labelCantidadHoras.Location = new System.Drawing.Point(13, 47);
            this.labelCantidadHoras.Name = "labelCantidadHoras";
            this.labelCantidadHoras.Size = new System.Drawing.Size(132, 17);
            this.labelCantidadHoras.TabIndex = 1;
            this.labelCantidadHoras.Tag = "labelCantidadHoras";
            this.labelCantidadHoras.Text = "labelCantidadHoras";
            // 
            // ddlProyectos
            // 
            this.ddlProyectos.FormattingEnabled = true;
            this.ddlProyectos.Location = new System.Drawing.Point(174, 13);
            this.ddlProyectos.Name = "ddlProyectos";
            this.ddlProyectos.Size = new System.Drawing.Size(259, 24);
            this.ddlProyectos.TabIndex = 2;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(174, 73);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(259, 132);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // labelDescripcionDelTrabajo
            // 
            this.labelDescripcionDelTrabajo.AutoSize = true;
            this.labelDescripcionDelTrabajo.Location = new System.Drawing.Point(13, 76);
            this.labelDescripcionDelTrabajo.Name = "labelDescripcionDelTrabajo";
            this.labelDescripcionDelTrabajo.Size = new System.Drawing.Size(182, 17);
            this.labelDescripcionDelTrabajo.TabIndex = 5;
            this.labelDescripcionDelTrabajo.Tag = "labelDescripcionDelTrabajo";
            this.labelDescripcionDelTrabajo.Text = "labelDescripcionDelTrabajo";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(174, 212);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(259, 22);
            this.dateTimePicker1.TabIndex = 6;
            // 
            // labelFechaTrabajada
            // 
            this.labelFechaTrabajada.AutoSize = true;
            this.labelFechaTrabajada.Location = new System.Drawing.Point(16, 216);
            this.labelFechaTrabajada.Name = "labelFechaTrabajada";
            this.labelFechaTrabajada.Size = new System.Drawing.Size(142, 17);
            this.labelFechaTrabajada.TabIndex = 7;
            this.labelFechaTrabajada.Tag = "labelFechaTrabajada";
            this.labelFechaTrabajada.Text = "labelFechaTrabajada";
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(309, 406);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(124, 23);
            this.buttonGuardar.TabIndex = 8;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(175, 406);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(124, 23);
            this.buttonSalir.TabIndex = 9;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(174, 252);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(259, 148);
            this.listBox1.TabIndex = 34;
            // 
            // labelOcupadoDesde
            // 
            this.labelOcupadoDesde.AutoSize = true;
            this.labelOcupadoDesde.Location = new System.Drawing.Point(16, 252);
            this.labelOcupadoDesde.Name = "labelOcupadoDesde";
            this.labelOcupadoDesde.Size = new System.Drawing.Size(137, 17);
            this.labelOcupadoDesde.TabIndex = 33;
            this.labelOcupadoDesde.Tag = "labelOcupadoDesde";
            this.labelOcupadoDesde.Text = "labelOcupadoDesde";
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(175, 43);
            this.textBox1.Mask = "0";
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(258, 22);
            this.textBox1.TabIndex = 35;
            // 
            // CargarHorasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 595);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.labelOcupadoDesde);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.labelFechaTrabajada);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.labelDescripcionDelTrabajo);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.ddlProyectos);
            this.Controls.Add(this.labelCantidadHoras);
            this.Controls.Add(this.labelProyectos);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "CargaHoras");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "CargarHorasForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "CargarHorasForm";
            this.Load += new System.EventHandler(this.CargarHorasForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelProyectos;
        private System.Windows.Forms.Label labelCantidadHoras;
        private System.Windows.Forms.ComboBox ddlProyectos;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label labelDescripcionDelTrabajo;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label labelFechaTrabajada;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label labelOcupadoDesde;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.MaskedTextBox textBox1;
    }
}