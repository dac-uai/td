﻿using DAC.BE;
using DAC.BLL;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class RestoreForm : Form
    {
        string nombreArchivo = string.Empty;
        private IdiomaBE idiomaSeleccionado = null;
        private UTILS.GestorIdioma _gestorIdioma = null;
        private MainForm mainForm = null;
        private UsuarioBLL _usuarioBll = null;
        private UsuarioBE _usuarioSession = null;

        public RestoreForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            nombreArchivo = openFileDialog1.FileName;
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void restoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count > 0)
            {
                nombreArchivo = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[0].Value.ToString();

                if (string.IsNullOrEmpty(nombreArchivo))
                {
                    MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNombreArchivoVacio").texto);
                    return;
                }

                DialogResult resultado = MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjValidarRestauracion").texto, string.Empty, MessageBoxButtons.YesNo);

                if (resultado.ToString() == "No") return;

                GestorBackUp.RecuperarBackUp(nombreArchivo);

                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjRestauracionExitosa").texto);

                UsuarioBE sessionUsuarioExistente = _usuarioBll.ObtenerUsuarioPorNombreUsuario(_usuarioSession.nombreUsuario);

                if (sessionUsuarioExistente == null)
                {
                    MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjUsuarioLogueadoInexistente").texto);
                    ((MainForm)ParentForm)._loginForm.Close();
                }
                else
                {
                    GestorSession.GetInstance().ActualizarUsuarioEnSesion(sessionUsuarioExistente);
                    ((MainForm)ParentForm).ActualizarVista();
                }
            }
            else
            {
                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNingunItemSeleccionado").texto);
            }
        }

        private void RestoreForm_Load(object sender, EventArgs e)
        {
            idiomaSeleccionado = GestorSession.GetInstance().sessionUsuario.idioma;
            _gestorIdioma = UTILS.GestorIdioma.GetInstance();
            mainForm = (MainForm)ParentForm;
            _usuarioBll = UsuarioBLL.GetInstance();
            _usuarioSession = GestorSession.GetInstance().sessionUsuario;
            CargarGrilla();
        }

        private void CargarGrilla()
        {
            try
            {
                dataGridView1.DataSource = null;
                dataGridView1.ColumnCount = 3;

                dataGridView1.Columns[0].Name = "path";
                dataGridView1.Columns[0].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerPath").texto;
                dataGridView1.Columns[0].DataPropertyName = "path";

                dataGridView1.Columns[1].Name = "fecha";
                dataGridView1.Columns[1].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerFechaEvento").texto;
                dataGridView1.Columns[1].DataPropertyName = "fecha";

                dataGridView1.Columns[2].Name = "nombreUsuario";
                dataGridView1.Columns[2].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerNombreUsuario").texto;
                dataGridView1.Columns[2].DataPropertyName = "nombreUsuario";

                dataGridView1.DataSource = GestorBackUp.ObtenerListaBackups();

                dataGridView1.Update();
                dataGridView1.ReadOnly = true;
                dataGridView1.Width = mainForm.Width - 60;
                dataGridView1.ClearSelection();

                mainForm.OrdenarGrilla(ref dataGridView1);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            nombreArchivo = dataGridView1.SelectedCells[0].Value.ToString();
        }
    }
}
