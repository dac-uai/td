﻿namespace DAC.Forms
{
    partial class GestionAltaPermisosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPermisosDelSistema = new System.Windows.Forms.Label();
            this.labelPermisosDeUsuario = new System.Windows.Forms.Label();
            this.buttonRemoverPermiso = new System.Windows.Forms.Button();
            this.buttonAgregarPermiso = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.labelCodigo = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // labelPermisosDelSistema
            // 
            this.labelPermisosDelSistema.AutoSize = true;
            this.labelPermisosDelSistema.Location = new System.Drawing.Point(479, 101);
            this.labelPermisosDelSistema.Name = "labelPermisosDelSistema";
            this.labelPermisosDelSistema.Size = new System.Drawing.Size(167, 17);
            this.labelPermisosDelSistema.TabIndex = 19;
            this.labelPermisosDelSistema.Tag = "labelPermisosDelSistema";
            this.labelPermisosDelSistema.Text = "labelPermisosDelSistema";
            // 
            // labelPermisosDeUsuario
            // 
            this.labelPermisosDeUsuario.AutoSize = true;
            this.labelPermisosDeUsuario.Location = new System.Drawing.Point(9, 101);
            this.labelPermisosDeUsuario.Name = "labelPermisosDeUsuario";
            this.labelPermisosDeUsuario.Size = new System.Drawing.Size(163, 17);
            this.labelPermisosDeUsuario.TabIndex = 18;
            this.labelPermisosDeUsuario.Tag = "labelPermisosDeUsuario";
            this.labelPermisosDeUsuario.Text = "labelPermisosDeUsuario";
            // 
            // buttonRemoverPermiso
            // 
            this.buttonRemoverPermiso.Location = new System.Drawing.Point(310, 450);
            this.buttonRemoverPermiso.Name = "buttonRemoverPermiso";
            this.buttonRemoverPermiso.Size = new System.Drawing.Size(166, 53);
            this.buttonRemoverPermiso.TabIndex = 16;
            this.buttonRemoverPermiso.Tag = "buttonRemoverPermiso";
            this.buttonRemoverPermiso.Text = "buttonRemoverPermiso";
            this.buttonRemoverPermiso.UseVisualStyleBackColor = true;
            this.buttonRemoverPermiso.Click += new System.EventHandler(this.buttonRemoverPermiso_Click);
            // 
            // buttonAgregarPermiso
            // 
            this.buttonAgregarPermiso.Location = new System.Drawing.Point(310, 236);
            this.buttonAgregarPermiso.Name = "buttonAgregarPermiso";
            this.buttonAgregarPermiso.Size = new System.Drawing.Size(166, 53);
            this.buttonAgregarPermiso.TabIndex = 15;
            this.buttonAgregarPermiso.Tag = "buttonAgregarPermiso";
            this.buttonAgregarPermiso.Text = "buttonAgregarPermiso";
            this.buttonAgregarPermiso.UseVisualStyleBackColor = true;
            this.buttonAgregarPermiso.Click += new System.EventHandler(this.buttonAgregarPermiso_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(482, 652);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(292, 23);
            this.buttonSalir.TabIndex = 14;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(12, 652);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(292, 23);
            this.buttonGuardar.TabIndex = 13;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // treeView2
            // 
            this.treeView2.Location = new System.Drawing.Point(482, 121);
            this.treeView2.Name = "treeView2";
            this.treeView2.Size = new System.Drawing.Size(292, 488);
            this.treeView2.TabIndex = 11;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 121);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(292, 488);
            this.treeView1.TabIndex = 10;
            // 
            // labelCodigo
            // 
            this.labelCodigo.AutoSize = true;
            this.labelCodigo.Location = new System.Drawing.Point(12, 9);
            this.labelCodigo.Name = "labelCodigo";
            this.labelCodigo.Size = new System.Drawing.Size(82, 17);
            this.labelCodigo.TabIndex = 20;
            this.labelCodigo.Tag = "labelCodigo";
            this.labelCodigo.Text = "labelCodigo";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(12, 30);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(292, 22);
            this.txtCodigo.TabIndex = 21;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(12, 76);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(292, 22);
            this.txtDescripcion.TabIndex = 23;
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.AutoSize = true;
            this.labelDescripcion.Location = new System.Drawing.Point(12, 55);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(112, 17);
            this.labelDescripcion.TabIndex = 22;
            this.labelDescripcion.Tag = "labelDescripcion";
            this.labelDescripcion.Text = "labelDescripcion";
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // GestionAltaPermisosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 749);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.labelCodigo);
            this.Controls.Add(this.labelPermisosDelSistema);
            this.Controls.Add(this.labelPermisosDeUsuario);
            this.Controls.Add(this.buttonRemoverPermiso);
            this.Controls.Add(this.buttonAgregarPermiso);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.treeView2);
            this.Controls.Add(this.treeView1);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "AltaPermisos");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "GestionAltaPermisosForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "GestionAltaPermisosForm";
            this.Load += new System.EventHandler(this.GestionAltaPermisosForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPermisosDelSistema;
        private System.Windows.Forms.Label labelPermisosDeUsuario;
        private System.Windows.Forms.Button buttonRemoverPermiso;
        private System.Windows.Forms.Button buttonAgregarPermiso;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label labelCodigo;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}