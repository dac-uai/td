﻿using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class BitacoraForm : Form
    {
        private MainForm mainForm = null;
        private IdiomaBE idiomaSeleccionado = null;
        private UTILS.GestorIdioma _gestorIdioma = null;

        public BitacoraForm()
        {
            InitializeComponent();
        }

        private void BitacoraForm_Load(object sender, EventArgs e)
        {
            mainForm = (MainForm)ParentForm;
            _gestorIdioma = UTILS.GestorIdioma.GetInstance();

            idiomaSeleccionado = GestorSession.GetInstance().sessionUsuario.idioma;

            RecargarGrilla();
        }

        private void buttonRecargar_Click(object sender, EventArgs e)
        {
            RecargarGrilla();
        }

        private void RecargarGrilla()
        {
            try
            {
                DataTable dt = GestorBitacora.ObtenerEventos();
                dataGridView1.DataSource = null;

                dataGridView1.ColumnCount = 7;

                dataGridView1.Columns[0].Name = "mensaje";
                dataGridView1.Columns[0].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerMensaje").texto;
                dataGridView1.Columns[0].DataPropertyName = "mensaje";

                dataGridView1.Columns[1].Name = "fechaEvento";
                dataGridView1.Columns[1].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerFechaEvento").texto;
                dataGridView1.Columns[1].DataPropertyName = "fechaEvento";

                dataGridView1.Columns[2].Name = "tipoEvento";
                dataGridView1.Columns[2].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerTipoEvento").texto;
                dataGridView1.Columns[2].DataPropertyName = "tipoEvento";

                dataGridView1.Columns[3].Name = "nombreUsuario";
                dataGridView1.Columns[3].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerNombreUsuario").texto;
                dataGridView1.Columns[3].DataPropertyName = "nombreUsuario";

                dataGridView1.Columns[4].Name = "nombre";
                dataGridView1.Columns[4].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerNombre").texto;
                dataGridView1.Columns[4].DataPropertyName = "nombre";

                dataGridView1.Columns[5].Name = "apellido";
                dataGridView1.Columns[5].HeaderText = _gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "headerApellido").texto;
                dataGridView1.Columns[5].DataPropertyName = "apellido";

                dataGridView1.Columns[6].Name = "ip";
                dataGridView1.Columns[6].HeaderText = "IP";
                dataGridView1.Columns[6].DataPropertyName = "ip";


                dataGridView1.DataSource = dt;
                dataGridView1.Update();
                dataGridView1.ReadOnly = true;
                dataGridView1.Width = mainForm.Width - 60;

                mainForm.OrdenarGrilla(ref dataGridView1);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
