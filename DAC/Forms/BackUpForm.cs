﻿using DAC.BE;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class BackUpForm : Observador
    {
        private string folderPath = string.Empty;
        private IdiomaBE idiomaSeleccionado = null;
        private UTILS.GestorIdioma _gestorIdioma = null;

        public BackUpForm()
        {
            InitializeComponent();
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonBackUp_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtNombreBackup.Text) || string.IsNullOrEmpty(folderPath))
                {
                    MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjNombreArchivoVacio").texto);
                    return;
                }

                DialogResult resultado = MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjValidarBackup").texto, string.Empty, MessageBoxButtons.YesNo);

                if (resultado.ToString() == "No") return;

                UsuarioBE usuario = GestorSession.GetInstance().sessionUsuario;
                GestorBackUp.GenerarBackUp(txtNombreBackup.Text, usuario, folderPath);

                MessageBox.Show(_gestorIdioma.ObtenerTraduccion(idiomaSeleccionado, "msjBackUpGenerado").texto);
                txtNombreBackup.Text = string.Empty;
            }
            catch (Exception)
            {
                MessageBox.Show("Se ha producido un error, por favor seleccione otra ubicaciön para el backup");;
            }

        }

        private void buttonSeleccionarCarpeta_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            folderPath = folderBrowserDialog1.SelectedPath;
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void BackUpForm_Load(object sender, EventArgs e)
        {
            idiomaSeleccionado = GestorSession.GetInstance().sessionUsuario.idioma;
            _gestorIdioma = UTILS.GestorIdioma.GetInstance();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            folderPath = folderBrowserDialog1.SelectedPath;
            buttonSeleccionarCarpeta.Text = folderPath;
        }
    }
}
