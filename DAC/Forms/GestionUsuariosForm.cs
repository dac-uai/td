﻿using DAC.BE;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestionUsuariosForm : Form
    {
        private UsuarioBE usuarioEnSesion = null;
        private IdiomaBE idiomaSeleccionado = null;
        private MainForm mainForm = null;
        private BLL.UsuarioBLL _usuarioBLL = null;

        public GestionUsuariosForm()
        {
            InitializeComponent();
            usuarioEnSesion = GestorSession.GetInstance().sessionUsuario;
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GestionUsuarios_Load(object sender, EventArgs e)
        {
            _usuarioBLL = BLL.UsuarioBLL.GetInstance();
            mainForm = (MainForm)ParentForm;
            idiomaSeleccionado = usuarioEnSesion.idioma;
            UTILS.GestorIdioma.GetInstance().CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);
            foreach (Label lbl in this.Controls.OfType<Label>())
            {
                lbl.BackColor = Color.Black;
            }

            foreach (Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    MensajeBE msj = idiomaSeleccionado.mensajes.First(x => x.etiqueta == control.Tag.ToString());
                    control.Text = (msj != null) ? msj.traduccion.texto : control.Tag.ToString();

                    if (control.Controls.Count > 0)
                    {
                        foreach (Control ctrl in control.Controls)
                        {
                            if (ctrl.Tag != null)
                            {
                                MensajeBE _msj = idiomaSeleccionado.mensajes.First(x => x.etiqueta == ctrl.Tag.ToString());
                                ctrl.Text = (_msj != null) ? _msj.traduccion.texto : ctrl.Tag.ToString();
                            }
                        }
                    }
                }
            }

            CargarGrilla();

            buttonModificar.Enabled = false;
            buttonEliminar.Enabled = false;
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string _nombreUsuario = txtNombreUsuario.Text;
                string _nombre = txtNombreEmpleado.Text;
                string _apellido = txtApellidoEmpleado.Text;
                string _password = txtPassword.Text;
                string _mail = txtMail.Text;
                string _fechaNacimiento = dateTimePicker1.Text;

                if (!TodosLosDatosOk()) return;

                UsuarioBE usuario = new UsuarioBE()
                {
                    nombreUsuario = _nombreUsuario,
                    apellido = _apellido,
                    fechaNacimiento = DateTime.Parse(_fechaNacimiento),
                    mail = _mail,
                    nombre = _nombre,
                    password = _password
                };

                UsuarioBE usuarioExistente = _usuarioBLL.ObtenerUsuarioPorNombreUsuario(usuario.nombreUsuario);

                if (usuarioExistente != null)
                {
                    MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjNombreUsuarioInvalido").traduccion.texto);
                    return;
                }

                usuario = _usuarioBLL.Crear(usuario);

                if (usuario != null)
                {
                    MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjUsuarioGenerado").traduccion.texto);
                }

                ActualizarVista();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CargarGrilla()
        {
            try
            {
                IList<UsuarioBE> usuarios = BLL.UsuarioBLL.GetInstance().SeleccionarTodos();

                if (!GestorSession.GetInstance().UsuarioTienePermiso("H.R001"))
                {
                    usuarios = usuarios.Where(x => x.nombreUsuario == usuarioEnSesion.nombreUsuario).ToList();
                }

                dgvUsuarios.AutoGenerateColumns = false;
                dgvUsuarios.DataSource = null;

                dgvUsuarios.ColumnCount = 6;

                dgvUsuarios.Columns[0].Name = "nombreUsuario";
                dgvUsuarios.Columns[0].HeaderText = idiomaSeleccionado.mensajes.First(x => x.etiqueta == "headerNombreUsuario").traduccion.texto;
                dgvUsuarios.Columns[0].DataPropertyName = "nombreUsuario";

                dgvUsuarios.Columns[1].Name = "nombre";
                dgvUsuarios.Columns[1].HeaderText = idiomaSeleccionado.mensajes.First(x => x.etiqueta == "headerNombre").traduccion.texto;
                dgvUsuarios.Columns[1].DataPropertyName = "nombre";

                dgvUsuarios.Columns[2].Name = "apellido";
                dgvUsuarios.Columns[2].HeaderText = idiomaSeleccionado.mensajes.First(x => x.etiqueta == "headerApellido").traduccion.texto;
                dgvUsuarios.Columns[2].DataPropertyName = "apellido";

                dgvUsuarios.Columns[3].Name = "mail";
                dgvUsuarios.Columns[3].HeaderText = idiomaSeleccionado.mensajes.First(x => x.etiqueta == "headerMail").traduccion.texto;
                dgvUsuarios.Columns[3].DataPropertyName = "mail";

                dgvUsuarios.Columns[4].Name = "fechaNacimiento";
                dgvUsuarios.Columns[4].HeaderText = idiomaSeleccionado.mensajes.First(x => x.etiqueta == "headerFechaNacimiento").traduccion.texto;
                dgvUsuarios.Columns[4].DataPropertyName = "fechaNacimiento";

                dgvUsuarios.Columns[5].Name = "id";
                dgvUsuarios.Columns[5].DataPropertyName = "id";
                dgvUsuarios.Columns[5].Visible = false;

                dgvUsuarios.DataSource = usuarios;

                mainForm.OrdenarGrilla(ref dgvUsuarios, dgvUsuarios.ColumnCount - 1);

                dgvUsuarios.Update();
                dgvUsuarios.ClearSelection();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ActualizarVista()
        {
            CargarGrilla();
            txtNombreEmpleado.Text = string.Empty;
            txtNombreUsuario.Text = string.Empty;
            txtApellidoEmpleado.Text = string.Empty;
            txtMail.Text = string.Empty;
            txtPassword.Text = string.Empty;

            buttonModificar.Enabled = false;
            buttonEliminar.Enabled = false;

        }

        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = dgvUsuarios.SelectedCells[0].RowIndex;
                UsuarioBE _usuario = (UsuarioBE)(dgvUsuarios.Rows[rowIndex].DataBoundItem);

                txtNombreUsuario.Text = _usuario.nombreUsuario;
                txtApellidoEmpleado.Text = _usuario.apellido;
                txtMail.Text = _usuario.mail;
                txtPassword.Text = _usuario.password;
                txtNombreEmpleado.Text = _usuario.nombre;
                dateTimePicker1.Value = _usuario.fechaNacimiento;

                buttonModificar.Enabled = true;
                buttonEliminar.Enabled = true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                int rowIndex = dgvUsuarios.SelectedCells[0].RowIndex;
                UsuarioBE _usuario = (UsuarioBE)(dgvUsuarios.Rows[rowIndex].DataBoundItem);

                string mensajeAlerta = string.Format("{0} {1}?", idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjSeguroUsuarioEliminado").traduccion.texto, _usuario.nombreUsuario);
                DialogResult resultado = MessageBox.Show(mensajeAlerta, string.Empty, MessageBoxButtons.YesNo);

                if (resultado.ToString() == "No") return;

                bool borradoOk = _usuarioBLL.Eliminar(_usuario);

                if (borradoOk)
                {
                    MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjUsuarioEliminado").traduccion.texto);
                }

                ActualizarVista();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void buttonModificar_Click(object sender, EventArgs e)
        {
            string _nombreUsuario = txtNombreUsuario.Text;
            string _nombre = txtNombreEmpleado.Text;
            string _apellido = txtApellidoEmpleado.Text;
            string _password = txtPassword.Text;
            string _mail = txtMail.Text;
            string _fechaNacimiento = dateTimePicker1.Text;
            int rowIndex = dgvUsuarios.SelectedCells[0].RowIndex;

            if (!TodosLosDatosOk()) return;

            UsuarioBE usuarioOriginal = (UsuarioBE)(dgvUsuarios.Rows[rowIndex].DataBoundItem);

            UsuarioBE usuario = new UsuarioBE()
            {
                id = usuarioOriginal.id,
                nombreUsuario = _nombreUsuario,
                apellido = _apellido,
                fechaNacimiento = DateTime.Parse(_fechaNacimiento),
                mail = _mail,
                nombre = _nombre,
                password = _password
            };

            if (usuarioOriginal.nombreUsuario != usuario.nombreUsuario)
            {
                UsuarioBE usuarioExistente = _usuarioBLL.ObtenerUsuarioPorNombreUsuario(usuario.nombreUsuario);

                if (usuarioExistente != null)
                {
                    MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjNombreUsuarioInvalido").traduccion.texto);
                    return;
                }
            }

            usuario = _usuarioBLL.Modificar(usuario, usuarioOriginal.password != _password, usuarioOriginal.password);

            if (usuario != null)
            {
                MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjUsuarioGenerado").traduccion.texto);
            }

            ActualizarVista();
        }

        private bool TodosLosDatosOk()
        {
            try
            {
                string _nombreUsuario = txtNombreUsuario.Text;
                string _nombre = txtNombreEmpleado.Text;
                string _apellido = txtApellidoEmpleado.Text;
                string _password = txtPassword.Text;
                string _mail = txtMail.Text;
                string _fechaNacimiento = dateTimePicker1.Text;

                if (string.IsNullOrEmpty(_nombreUsuario) || string.IsNullOrEmpty(_nombre) || string.IsNullOrEmpty(_apellido)
                     || string.IsNullOrEmpty(_password) || string.IsNullOrEmpty(_mail) || string.IsNullOrEmpty(_fechaNacimiento))
                {
                    MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjFaltanDatos").traduccion.texto);
                    return false;
                }

                if (dateTimePicker1.Value >= DateTime.Now.AddYears(-18))
                {
                    MessageBox.Show(idiomaSeleccionado.mensajes.First(x => x.etiqueta == "msjFechaNacimientoInvalida").traduccion.texto);
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}