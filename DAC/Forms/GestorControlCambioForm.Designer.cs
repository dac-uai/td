﻿namespace DAC.Forms
{
    partial class GestorControlCambioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelEntidad = new System.Windows.Forms.Label();
            this.labelUsuarioOriginal = new System.Windows.Forms.Label();
            this.dgvModificaciones = new System.Windows.Forms.DataGridView();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelUsuarioModificado = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModificaciones)).BeginInit();
            this.SuspendLayout();
            // 
            // labelEntidad
            // 
            this.labelEntidad.AutoSize = true;
            this.labelEntidad.Location = new System.Drawing.Point(12, 9);
            this.labelEntidad.Name = "labelEntidad";
            this.labelEntidad.Size = new System.Drawing.Size(86, 17);
            this.labelEntidad.TabIndex = 2;
            this.labelEntidad.Tag = "labelEntidad";
            this.labelEntidad.Text = "labelEntidad";
            // 
            // labelUsuarioOriginal
            // 
            this.labelUsuarioOriginal.AutoSize = true;
            this.labelUsuarioOriginal.Location = new System.Drawing.Point(524, 625);
            this.labelUsuarioOriginal.Name = "labelUsuarioOriginal";
            this.labelUsuarioOriginal.Size = new System.Drawing.Size(136, 17);
            this.labelUsuarioOriginal.TabIndex = 4;
            this.labelUsuarioOriginal.Tag = "labelUsuarioOriginal";
            this.labelUsuarioOriginal.Text = "labelUsuarioOriginal";
            // 
            // dgvModificaciones
            // 
            this.dgvModificaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvModificaciones.Location = new System.Drawing.Point(15, 44);
            this.dgvModificaciones.Name = "dgvModificaciones";
            this.dgvModificaciones.RowTemplate.Height = 24;
            this.dgvModificaciones.Size = new System.Drawing.Size(1379, 569);
            this.dgvModificaciones.TabIndex = 5;
            this.dgvModificaciones.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvModificaciones_CellContentClick);
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(15, 622);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(225, 23);
            this.buttonGuardar.TabIndex = 6;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(246, 622);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(225, 23);
            this.buttonSalir.TabIndex = 7;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(689, 625);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 8;
            this.label1.Tag = "labelModificaciones";
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1432, 625);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 10;
            this.label2.Tag = "labelModificaciones";
            this.label2.Text = "label2";
            // 
            // labelUsuarioModificado
            // 
            this.labelUsuarioModificado.AutoSize = true;
            this.labelUsuarioModificado.Location = new System.Drawing.Point(1248, 625);
            this.labelUsuarioModificado.Name = "labelUsuarioModificado";
            this.labelUsuarioModificado.Size = new System.Drawing.Size(155, 17);
            this.labelUsuarioModificado.TabIndex = 9;
            this.labelUsuarioModificado.Tag = "labelUsuarioModificado";
            this.labelUsuarioModificado.Text = "labelUsuarioModificado";
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // GestorControlCambioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1770, 783);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelUsuarioModificado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.dgvModificaciones);
            this.Controls.Add(this.labelUsuarioOriginal);
            this.Controls.Add(this.labelEntidad);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "ControlCambio");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "GestorControlCambioForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "GestorControlCambioForm";
            this.Text = "GestorControlCambioForm";
            this.Load += new System.EventHandler(this.GestorControlCambio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvModificaciones)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelEntidad;
        private System.Windows.Forms.Label labelUsuarioOriginal;
        private System.Windows.Forms.DataGridView dgvModificaciones;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelUsuarioModificado;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}