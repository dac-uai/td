﻿namespace DAC.Forms
{
    partial class GestorClientesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtRazonSocial = new System.Windows.Forms.TextBox();
            this.labelRazonSocial = new System.Windows.Forms.Label();
            this.txtNombreContacto = new System.Windows.Forms.TextBox();
            this.labelNombreContacto = new System.Windows.Forms.Label();
            this.txtCuit = new System.Windows.Forms.TextBox();
            this.labelCuit = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.labelMailContacto = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.labelTelefonoContacto = new System.Windows.Forms.Label();
            this.checkActivo = new System.Windows.Forms.CheckBox();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.labelClientes = new System.Windows.Forms.Label();
            this.ddlClientes = new System.Windows.Forms.ComboBox();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.AutoSize = true;
            this.labelDescripcion.Location = new System.Drawing.Point(12, 39);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(112, 17);
            this.labelDescripcion.TabIndex = 0;
            this.labelDescripcion.Tag = "labelDescripcion";
            this.labelDescripcion.Text = "labelDescripcion";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(171, 39);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(214, 22);
            this.txtDescripcion.TabIndex = 1;
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.Location = new System.Drawing.Point(171, 67);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.Size = new System.Drawing.Size(214, 22);
            this.txtRazonSocial.TabIndex = 3;
            // 
            // labelRazonSocial
            // 
            this.labelRazonSocial.AutoSize = true;
            this.labelRazonSocial.Location = new System.Drawing.Point(12, 67);
            this.labelRazonSocial.Name = "labelRazonSocial";
            this.labelRazonSocial.Size = new System.Drawing.Size(117, 17);
            this.labelRazonSocial.TabIndex = 2;
            this.labelRazonSocial.Tag = "labelRazonSocial";
            this.labelRazonSocial.Text = "labelRazonSocial";
            // 
            // txtNombreContacto
            // 
            this.txtNombreContacto.Location = new System.Drawing.Point(171, 123);
            this.txtNombreContacto.Name = "txtNombreContacto";
            this.txtNombreContacto.Size = new System.Drawing.Size(214, 22);
            this.txtNombreContacto.TabIndex = 7;
            // 
            // labelNombreContacto
            // 
            this.labelNombreContacto.AutoSize = true;
            this.labelNombreContacto.Location = new System.Drawing.Point(12, 123);
            this.labelNombreContacto.Name = "labelNombreContacto";
            this.labelNombreContacto.Size = new System.Drawing.Size(144, 17);
            this.labelNombreContacto.TabIndex = 6;
            this.labelNombreContacto.Tag = "labelNombreContacto";
            this.labelNombreContacto.Text = "labelNombreContacto";
            // 
            // txtCuit
            // 
            this.txtCuit.Location = new System.Drawing.Point(171, 95);
            this.txtCuit.Name = "txtCuit";
            this.txtCuit.Size = new System.Drawing.Size(214, 22);
            this.txtCuit.TabIndex = 5;
            // 
            // labelCuit
            // 
            this.labelCuit.AutoSize = true;
            this.labelCuit.Location = new System.Drawing.Point(12, 95);
            this.labelCuit.Name = "labelCuit";
            this.labelCuit.Size = new System.Drawing.Size(62, 17);
            this.labelCuit.TabIndex = 4;
            this.labelCuit.Tag = "labelCuit";
            this.labelCuit.Text = "labelCuit";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(171, 179);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(214, 22);
            this.txtMail.TabIndex = 11;
            // 
            // labelMailContacto
            // 
            this.labelMailContacto.AutoSize = true;
            this.labelMailContacto.Location = new System.Drawing.Point(12, 179);
            this.labelMailContacto.Name = "labelMailContacto";
            this.labelMailContacto.Size = new System.Drawing.Size(119, 17);
            this.labelMailContacto.TabIndex = 10;
            this.labelMailContacto.Tag = "labelMailContacto";
            this.labelMailContacto.Text = "labelMailContacto";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(171, 151);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(214, 22);
            this.txtTelefono.TabIndex = 9;
            // 
            // labelTelefonoContacto
            // 
            this.labelTelefonoContacto.AutoSize = true;
            this.labelTelefonoContacto.Location = new System.Drawing.Point(12, 151);
            this.labelTelefonoContacto.Name = "labelTelefonoContacto";
            this.labelTelefonoContacto.Size = new System.Drawing.Size(150, 17);
            this.labelTelefonoContacto.TabIndex = 8;
            this.labelTelefonoContacto.Tag = "labelTelefonoContacto";
            this.labelTelefonoContacto.Text = "labelTelefonoContacto";
            // 
            // checkActivo
            // 
            this.checkActivo.AutoSize = true;
            this.checkActivo.Location = new System.Drawing.Point(171, 208);
            this.checkActivo.Name = "checkActivo";
            this.checkActivo.Size = new System.Drawing.Size(105, 21);
            this.checkActivo.TabIndex = 12;
            this.checkActivo.Tag = "checkActivo";
            this.checkActivo.Text = "checkActivo";
            this.checkActivo.UseVisualStyleBackColor = true;
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(171, 236);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(105, 23);
            this.buttonSalir.TabIndex = 13;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(282, 236);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(103, 23);
            this.buttonGuardar.TabIndex = 14;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "buttonGuardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // labelClientes
            // 
            this.labelClientes.AutoSize = true;
            this.labelClientes.Location = new System.Drawing.Point(12, 9);
            this.labelClientes.Name = "labelClientes";
            this.labelClientes.Size = new System.Drawing.Size(88, 17);
            this.labelClientes.TabIndex = 15;
            this.labelClientes.Tag = "labelClientes";
            this.labelClientes.Text = "labelClientes";
            // 
            // ddlClientes
            // 
            this.ddlClientes.FormattingEnabled = true;
            this.ddlClientes.Location = new System.Drawing.Point(171, 9);
            this.ddlClientes.Name = "ddlClientes";
            this.ddlClientes.Size = new System.Drawing.Size(214, 24);
            this.ddlClientes.TabIndex = 16;
            this.ddlClientes.SelectedIndexChanged += new System.EventHandler(this.ddlClientes_SelectedIndexChanged);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // GestorClientesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 271);
            this.Controls.Add(this.ddlClientes);
            this.Controls.Add(this.labelClientes);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.buttonSalir);
            this.Controls.Add(this.checkActivo);
            this.Controls.Add(this.txtMail);
            this.Controls.Add(this.labelMailContacto);
            this.Controls.Add(this.txtTelefono);
            this.Controls.Add(this.labelTelefonoContacto);
            this.Controls.Add(this.txtNombreContacto);
            this.Controls.Add(this.labelNombreContacto);
            this.Controls.Add(this.txtCuit);
            this.Controls.Add(this.labelCuit);
            this.Controls.Add(this.txtRazonSocial);
            this.Controls.Add(this.labelRazonSocial);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.labelDescripcion);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "GesionClientes");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "GestorClientesForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "GestorClientesForm";
            this.Load += new System.EventHandler(this.GestorClientesForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtRazonSocial;
        private System.Windows.Forms.Label labelRazonSocial;
        private System.Windows.Forms.TextBox txtNombreContacto;
        private System.Windows.Forms.Label labelNombreContacto;
        private System.Windows.Forms.TextBox txtCuit;
        private System.Windows.Forms.Label labelCuit;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label labelMailContacto;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label labelTelefonoContacto;
        private System.Windows.Forms.CheckBox checkActivo;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Label labelClientes;
        private System.Windows.Forms.ComboBox ddlClientes;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}