﻿using DAC.BE;
using DAC.BLL;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class TecnologiaForm : Form
    {
        private UsuarioBE usuarioEnSesion;
        private IdiomaBE idiomaSeleccionado;
        private IList<TecnologiaBE> tecnologiasAll;
        private IList<TecnologiaBE> tecnologiasUsuario;
        private TecnologiaBLL _tecnologiaBLL;

        public TecnologiaForm()
        {
            InitializeComponent();
            _tecnologiaBLL = TecnologiaBLL.GetInstance();
            usuarioEnSesion = GestorSession.GetInstance().sessionUsuario;
            idiomaSeleccionado = usuarioEnSesion.idioma;
        }

        private void TecnologiaForm_Load(object sender, EventArgs e)
        {
            lblUsuario.Text = usuarioEnSesion.nombreUsuario;
            tecnologiasAll = _tecnologiaBLL.SeleccionarTodos();

            ddlExperiencia.Items.Add(((MainForm)ParentForm).optionSelectMessage);
            ddlExperiencia.Items.Add(Experiencia.Baja);
            ddlExperiencia.Items.Add(Experiencia.Media);
            ddlExperiencia.Items.Add(Experiencia.Alta);
            ddlExperiencia.SelectedIndex = 0;

            ddlExperienciaUsuario.Items.Add(((MainForm)ParentForm).optionSelectMessage);
            ddlExperienciaUsuario.Items.Add(Experiencia.Baja);
            ddlExperienciaUsuario.Items.Add(Experiencia.Media);
            ddlExperienciaUsuario.Items.Add(Experiencia.Alta);
            ddlExperiencia.SelectedIndex = 0;

            CargarTecnologias();
            CargarTecnologiasUsuario();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            if (ddlTecnologias.SelectedIndex > 0 && ddlExperiencia.SelectedIndex > 0)
            {
                TecnologiaBE _tec = (TecnologiaBE)ddlTecnologias.SelectedItem;
                Experiencia _exp = (Experiencia)ddlExperiencia.SelectedItem;

                AptitudBE _aptitud = new AptitudBE()
                {
                    experiencia = _exp,
                    tecnologia = _tec
                };

                bool aptitudAsignada = _tecnologiaBLL.AsignarAptitudAUsuario(_aptitud, ref usuarioEnSesion);

                if (aptitudAsignada)
                {
                    MessageBox.Show("Aptitud asignada correctamente");
                    CargarTecnologiasUsuario();
                    CargarTecnologias();
                }
                else
                {
                    MessageBox.Show("Error asignado aptitud");
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado tecnologia o experiencia");
            }
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonGuardarModificaciones_Click(object sender, EventArgs e)
        {
            if (ddlTecnologiaUsuario.SelectedIndex > 0 && ddlExperienciaUsuario.SelectedIndex > 0)
            {
                TecnologiaBE _tec = (TecnologiaBE)ddlTecnologiaUsuario.SelectedItem;
                Experiencia _exp = (Experiencia)ddlExperienciaUsuario.SelectedItem;

                AptitudBE _aptitud = new AptitudBE()
                {
                    experiencia = _exp,
                    tecnologia = _tec
                };

                if (checkEliminar.Checked)
                {
                    _tecnologiaBLL.EliminarAptitudesPorUsuario(_aptitud, ref usuarioEnSesion);

                    MessageBox.Show("Aptitud eliminada correctamente");
                    CargarTecnologiasUsuario();
                    CargarTecnologias();
                }
                else
                {
                    _tecnologiaBLL.ModificarAptitudesPorUsuario(_aptitud, ref usuarioEnSesion);

                    MessageBox.Show("Aptitud modificada correctamente");
                    CargarTecnologiasUsuario();
                    CargarTecnologias();
                }
            }
        }

        private void CargarTecnologias()
        {
            try
            {
                IList<TecnologiaBE> tecnologias = new List<TecnologiaBE>();
                tecnologiasUsuario = (usuarioEnSesion.aptitudes != null) ? usuarioEnSesion.aptitudes.Select(x => x.tecnologia).ToList() : new List<TecnologiaBE>();

                foreach (TecnologiaBE tech in tecnologiasAll)
                {
                    if (!tecnologiasUsuario.Any(x => x.codigo == tech.codigo)) tecnologias.Add(tech);
                }

                CargarDropDown(ddlTecnologias, tecnologias);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CargarTecnologiasUsuario()
        {
            try
            {
                tecnologiasUsuario = (usuarioEnSesion.aptitudes != null) ? usuarioEnSesion.aptitudes.Select(x => x.tecnologia).ToList() : new List<TecnologiaBE>();

                CargarDropDown(ddlTecnologiaUsuario, tecnologiasUsuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CargarDropDown(ComboBox ddl, IList<TecnologiaBE> tecnologias)
        {
            tecnologias.Insert(0, new TecnologiaBE() { id = 0, descripcion = ((MainForm)ParentForm).optionSelectMessage });

            ddl.DataSource = tecnologias;

            ddl.DataSource = tecnologias;
            ddl.DisplayMember = "descripcion";
            ddl.ValueMember = "id";
        }

        private void ddlTecnologiaUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTecnologiaUsuario.SelectedIndex > 0)
                {
                    TecnologiaBE tecnologia = (TecnologiaBE)ddlTecnologiaUsuario.SelectedItem;
                    ddlExperienciaUsuario.SelectedIndex = (int)usuarioEnSesion.aptitudes.First(x => x.tecnologia.codigo == tecnologia.codigo).experiencia + 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
