﻿using DAC.BE;
using DAC.BLL;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class Login : Observable
    {
        private IList<IdiomaBE> idiomas = null;
        private IdiomaBE idiomaSeleccionado = null;
        private MainForm _main;
        private List<Object> observadores = new List<Object>();

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            ChequearBase();
            CargarIdiomas();
        }

        private string ObtenerMensajePorEtiqueta(string etiqueta)
        {
            string mensaje = string.Empty;
            try
            {
                IList<MensajeBE> mensajes = idiomaSeleccionado.mensajes;
                MensajeBE msj = mensajes.FirstOrDefault(m => m.etiqueta.Equals(etiqueta));
                mensaje = (msj != null) ? msj.traduccion.texto : mensaje;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return mensaje;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            string nombreUsuario = txtNombreUsuario.Text;
            string pass = txtPassword.Text;
            UsuarioBE _usuario = null;
            bool integridadOk = VerificarIntegridad();

            if (string.IsNullOrEmpty(nombreUsuario) || string.IsNullOrEmpty(pass))
            {
                MessageBox.Show(ObtenerMensajePorEtiqueta("msjCredencialesIncompletas"));
            }
            else
            {
                _usuario = (UsuarioBE)UsuarioBLL.GetInstance().ValidarUsuario(nombreUsuario, pass);

                if (_usuario == null)
                {
                    MessageBox.Show(ObtenerMensajePorEtiqueta("msjCredencialesIncorrectas"));
                    txtNombreUsuario.Text = string.Empty;
                    txtPassword.Text = string.Empty;
                }
                else
                {
                    UTILS.GestorIdioma.GetInstance().GuardarIdioma(_usuario, idiomaSeleccionado);
                    GestorSession.GetInstance().ActualizarUsuarioEnSesion(_usuario);
                    UsuarioBLL.GetInstance().CargarIdioma(ref _usuario);
                    UsuarioBLL.GetInstance().CargarPermisos(ref _usuario);
                    TecnologiaBLL.GetInstance().CargarAptitudesPorUsuario(ref _usuario);
                    GestorSession.GetInstance().IniciarSesion(_usuario);

                    if (!GestorSession.GetInstance().UsuarioTienePermiso("ADM001") && !integridadOk)
                    {
                        MessageBox.Show("Error de integridad, contactar al administrador!!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        GestorSession.GetInstance().CerrarSesion();
                        return;
                    }

                    _main = new MainForm(this);

                    RegistrarObservador(_main);
                    NotificarObservadores();

                    Hide();
                    _main.Show();
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CargarIdiomas()
        {
            idiomas = UTILS.GestorIdioma.GetInstance().ObtenerTodosLosIdiomas();

            IList<IdiomaBE> idiomasDdl = idiomas;

            ddlSeleccionIdioma.DataSource = idiomasDdl;
            ddlSeleccionIdioma.DisplayMember = "descripcion";
            ddlSeleccionIdioma.ValueMember = "id";
        }

        private void SeleccionarIdioma(IdiomaBE idiomaSeleccionado)
        {
            UTILS.GestorIdioma.GetInstance().CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);

            foreach (Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    MensajeBE msj = idiomaSeleccionado.mensajes.First(x => x.etiqueta == control.Tag.ToString());
                    control.Text = msj.traduccion.texto;
                }
            }
        }

        private void ddlSeleccionIdioma_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            idiomaSeleccionado = (IdiomaBE)ddlSeleccionIdioma.SelectedItem;

            SeleccionarIdioma(idiomaSeleccionado);
        }

        private bool VerificarIntegridad()
        {
            return UsuarioBLL.GetInstance().VerificarIdentidad()
                && LicenciaBLL.GetInstance().VerificarIdentidad();
        }

        private static void ChequearBase()
        {
            SqlConnection conexion;

            string strSQL;
            bool result = false;

            try
            {
                conexion = new SqlConnection(@"server=.\SQL_UAI;Trusted_Connection=yes");
                //conexion = new SqlConnection(@"Server=DESKTOP-U5M30RA;Trusted_Connection=yes");

                strSQL = string.Format("SELECT database_id FROM sys.databases WHERE Name = 'DAC'");

                using (conexion)
                {
                    using (SqlCommand sqlCmd = new SqlCommand(strSQL, conexion))
                    {
                        conexion.Open();

                        object resultadoObj = sqlCmd.ExecuteScalar();

                        int databaseID = 0;

                        if (resultadoObj != null)
                        {
                            int.TryParse(resultadoObj.ToString(), out databaseID);
                        }

                        conexion.Close();

                        result = (databaseID == 0);
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            //Chequeo si la base no existe, la instala.

            if (result)
            {
                //Instalación de la Base
                string sqlConnectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=master;Data Source=.\SQL_UAI";
                //string sqlConnectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=master;Data Source=DESKTOP-U5M30RA";

                SqlConnection conn = new SqlConnection(sqlConnectionString);

                using (SqlCommand sqlCmd = new SqlCommand(sqlConnectionString, conn))
                {
                    conn.Open();

                    DirectorySecurity ds = Directory.GetAccessControl(@"C:\Program Files\DAC");
                    ds.AddAccessRule(new FileSystemAccessRule("Todos", FileSystemRights.FullControl, AccessControlType.Allow));
                    Directory.SetAccessControl(@"C:\Program Files\DAC", ds);

                    string script = File.ReadAllText(@"C:\Program Files\DAC\DAC\DAC.sql");

                    IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$",
                                             RegexOptions.Multiline | RegexOptions.IgnoreCase);

                    foreach (string commandString in commandStrings)
                    {
                        if (commandString.Trim() != "")
                        {
                            using (var command = new SqlCommand(commandString, conn))
                            {
                                command.ExecuteNonQuery();
                            }
                        }
                    }

                    conn.Close();

                    conexion = new SqlConnection(@"server=.\SQL_UAI;Trusted_Connection=yes");
                    //conexion = new SqlConnection(@"server=DESKTOP-U5M30RA;Trusted_Connection=yes");

                    strSQL = string.Format("SELECT database_id FROM sys.databases WHERE Name = 'DAC'");

                    using (conexion)
                    {
                        using (SqlCommand sqlCmd2 = new SqlCommand(strSQL, conexion))
                        {
                            conexion.Open();

                            object resultadoObj = sqlCmd2.ExecuteScalar();

                            int databaseID = 0;

                            if (resultadoObj != null)
                            {
                                databaseID = Convert.ToInt32(resultadoObj);
                            }
                        }
                    }
                }
            }
        }
    }
}
