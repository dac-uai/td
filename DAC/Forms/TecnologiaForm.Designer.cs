﻿namespace DAC.Forms
{
    partial class TecnologiaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNombreUsuario = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.labelTecnologia = new System.Windows.Forms.Label();
            this.ddlTecnologias = new System.Windows.Forms.ComboBox();
            this.ddlExperiencia = new System.Windows.Forms.ComboBox();
            this.labelExperiencia = new System.Windows.Forms.Label();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.buttonSalir = new System.Windows.Forms.Button();
            this.checkEliminar = new System.Windows.Forms.CheckBox();
            this.labelAptitudesUsuario = new System.Windows.Forms.Label();
            this.ddlExperienciaUsuario = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlTecnologiaUsuario = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonGuardarModificaciones = new System.Windows.Forms.Button();
            this.groupBoxTecnologias = new System.Windows.Forms.GroupBox();
            this.groupBoxAptitudes = new System.Windows.Forms.GroupBox();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.groupBoxTecnologias.SuspendLayout();
            this.groupBoxAptitudes.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelNombreUsuario
            // 
            this.labelNombreUsuario.AutoSize = true;
            this.labelNombreUsuario.Location = new System.Drawing.Point(6, 32);
            this.labelNombreUsuario.Name = "labelNombreUsuario";
            this.labelNombreUsuario.Size = new System.Drawing.Size(137, 17);
            this.labelNombreUsuario.TabIndex = 0;
            this.labelNombreUsuario.Tag = "labelNombreUsuario";
            this.labelNombreUsuario.Text = "labelNombreUsuario";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(164, 31);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(46, 17);
            this.lblUsuario.TabIndex = 1;
            this.lblUsuario.Text = "label1";
            // 
            // labelTecnologia
            // 
            this.labelTecnologia.AutoSize = true;
            this.labelTecnologia.Location = new System.Drawing.Point(6, 77);
            this.labelTecnologia.Name = "labelTecnologia";
            this.labelTecnologia.Size = new System.Drawing.Size(46, 17);
            this.labelTecnologia.TabIndex = 2;
            this.labelTecnologia.Tag = "labelTecnologia";
            this.labelTecnologia.Text = "label1";
            // 
            // ddlTecnologias
            // 
            this.ddlTecnologias.FormattingEnabled = true;
            this.ddlTecnologias.Location = new System.Drawing.Point(167, 77);
            this.ddlTecnologias.Name = "ddlTecnologias";
            this.ddlTecnologias.Size = new System.Drawing.Size(236, 24);
            this.ddlTecnologias.TabIndex = 3;
            // 
            // ddlExperiencia
            // 
            this.ddlExperiencia.FormattingEnabled = true;
            this.ddlExperiencia.Location = new System.Drawing.Point(167, 124);
            this.ddlExperiencia.Name = "ddlExperiencia";
            this.ddlExperiencia.Size = new System.Drawing.Size(236, 24);
            this.ddlExperiencia.TabIndex = 5;
            // 
            // labelExperiencia
            // 
            this.labelExperiencia.AutoSize = true;
            this.labelExperiencia.Location = new System.Drawing.Point(6, 124);
            this.labelExperiencia.Name = "labelExperiencia";
            this.labelExperiencia.Size = new System.Drawing.Size(111, 17);
            this.labelExperiencia.TabIndex = 4;
            this.labelExperiencia.Tag = "labelExperiencia";
            this.labelExperiencia.Text = "labelExperiencia";
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.Location = new System.Drawing.Point(167, 166);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(236, 23);
            this.buttonGuardar.TabIndex = 6;
            this.buttonGuardar.Tag = "buttonGuardar";
            this.buttonGuardar.Text = "button1";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // buttonSalir
            // 
            this.buttonSalir.Location = new System.Drawing.Point(183, 453);
            this.buttonSalir.Name = "buttonSalir";
            this.buttonSalir.Size = new System.Drawing.Size(236, 23);
            this.buttonSalir.TabIndex = 7;
            this.buttonSalir.Tag = "buttonSalir";
            this.buttonSalir.Text = "buttonSalir";
            this.buttonSalir.UseVisualStyleBackColor = true;
            this.buttonSalir.Click += new System.EventHandler(this.buttonSalir_Click);
            // 
            // checkEliminar
            // 
            this.checkEliminar.AutoSize = true;
            this.checkEliminar.Location = new System.Drawing.Point(167, 164);
            this.checkEliminar.Name = "checkEliminar";
            this.checkEliminar.Size = new System.Drawing.Size(117, 21);
            this.checkEliminar.TabIndex = 8;
            this.checkEliminar.Tag = "checkEliminar";
            this.checkEliminar.Text = "checkEliminar";
            this.checkEliminar.UseVisualStyleBackColor = true;
            // 
            // labelAptitudesUsuario
            // 
            this.labelAptitudesUsuario.AutoSize = true;
            this.labelAptitudesUsuario.Location = new System.Drawing.Point(6, 27);
            this.labelAptitudesUsuario.Name = "labelAptitudesUsuario";
            this.labelAptitudesUsuario.Size = new System.Drawing.Size(146, 17);
            this.labelAptitudesUsuario.TabIndex = 9;
            this.labelAptitudesUsuario.Tag = "labelAptitudesUsuario";
            this.labelAptitudesUsuario.Text = "labelAptitudesUsuario";
            // 
            // ddlExperienciaUsuario
            // 
            this.ddlExperienciaUsuario.FormattingEnabled = true;
            this.ddlExperienciaUsuario.Location = new System.Drawing.Point(167, 124);
            this.ddlExperienciaUsuario.Name = "ddlExperienciaUsuario";
            this.ddlExperienciaUsuario.Size = new System.Drawing.Size(236, 24);
            this.ddlExperienciaUsuario.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 12;
            this.label1.Tag = "labelExperiencia";
            this.label1.Text = "label1";
            // 
            // ddlTecnologiaUsuario
            // 
            this.ddlTecnologiaUsuario.FormattingEnabled = true;
            this.ddlTecnologiaUsuario.Location = new System.Drawing.Point(167, 77);
            this.ddlTecnologiaUsuario.Name = "ddlTecnologiaUsuario";
            this.ddlTecnologiaUsuario.Size = new System.Drawing.Size(236, 24);
            this.ddlTecnologiaUsuario.TabIndex = 11;
            this.ddlTecnologiaUsuario.SelectedIndexChanged += new System.EventHandler(this.ddlTecnologiaUsuario_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 10;
            this.label2.Tag = "labelTecnologia";
            this.label2.Text = "label1";
            // 
            // buttonGuardarModificaciones
            // 
            this.buttonGuardarModificaciones.Location = new System.Drawing.Point(167, 191);
            this.buttonGuardarModificaciones.Name = "buttonGuardarModificaciones";
            this.buttonGuardarModificaciones.Size = new System.Drawing.Size(236, 23);
            this.buttonGuardarModificaciones.TabIndex = 14;
            this.buttonGuardarModificaciones.Tag = "buttonGuardar";
            this.buttonGuardarModificaciones.Text = "button1";
            this.buttonGuardarModificaciones.UseVisualStyleBackColor = true;
            this.buttonGuardarModificaciones.Click += new System.EventHandler(this.buttonGuardarModificaciones_Click);
            // 
            // groupBoxTecnologias
            // 
            this.groupBoxTecnologias.Controls.Add(this.labelNombreUsuario);
            this.groupBoxTecnologias.Controls.Add(this.lblUsuario);
            this.groupBoxTecnologias.Controls.Add(this.labelTecnologia);
            this.groupBoxTecnologias.Controls.Add(this.ddlTecnologias);
            this.groupBoxTecnologias.Controls.Add(this.labelExperiencia);
            this.groupBoxTecnologias.Controls.Add(this.ddlExperiencia);
            this.groupBoxTecnologias.Controls.Add(this.buttonGuardar);
            this.groupBoxTecnologias.Location = new System.Drawing.Point(16, 12);
            this.groupBoxTecnologias.Name = "groupBoxTecnologias";
            this.groupBoxTecnologias.Size = new System.Drawing.Size(414, 204);
            this.groupBoxTecnologias.TabIndex = 15;
            this.groupBoxTecnologias.TabStop = false;
            this.groupBoxTecnologias.Tag = "groupBoxTecnologias";
            this.groupBoxTecnologias.Text = "groupBoxTecnologias";
            // 
            // groupBoxAptitudes
            // 
            this.groupBoxAptitudes.Controls.Add(this.labelAptitudesUsuario);
            this.groupBoxAptitudes.Controls.Add(this.checkEliminar);
            this.groupBoxAptitudes.Controls.Add(this.buttonGuardarModificaciones);
            this.groupBoxAptitudes.Controls.Add(this.label2);
            this.groupBoxAptitudes.Controls.Add(this.ddlExperienciaUsuario);
            this.groupBoxAptitudes.Controls.Add(this.ddlTecnologiaUsuario);
            this.groupBoxAptitudes.Controls.Add(this.label1);
            this.groupBoxAptitudes.Location = new System.Drawing.Point(16, 222);
            this.groupBoxAptitudes.Name = "groupBoxAptitudes";
            this.groupBoxAptitudes.Size = new System.Drawing.Size(414, 225);
            this.groupBoxAptitudes.TabIndex = 16;
            this.groupBoxAptitudes.TabStop = false;
            this.groupBoxAptitudes.Tag = "groupBoxAptitudes";
            this.groupBoxAptitudes.Text = "groupBoxAptitudes";
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Git\\UAI\\Diploma\\td\\DAC\\DAC.chm";
            // 
            // TecnologiaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 577);
            this.Controls.Add(this.groupBoxAptitudes);
            this.Controls.Add(this.groupBoxTecnologias);
            this.Controls.Add(this.buttonSalir);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "Aptitud");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.Name = "TecnologiaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "TecnologiaForm";
            this.Load += new System.EventHandler(this.TecnologiaForm_Load);
            this.groupBoxTecnologias.ResumeLayout(false);
            this.groupBoxTecnologias.PerformLayout();
            this.groupBoxAptitudes.ResumeLayout(false);
            this.groupBoxAptitudes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelNombreUsuario;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label labelTecnologia;
        private System.Windows.Forms.ComboBox ddlTecnologias;
        private System.Windows.Forms.ComboBox ddlExperiencia;
        private System.Windows.Forms.Label labelExperiencia;
        private System.Windows.Forms.Button buttonGuardar;
        private System.Windows.Forms.Button buttonSalir;
        private System.Windows.Forms.CheckBox checkEliminar;
        private System.Windows.Forms.Label labelAptitudesUsuario;
        private System.Windows.Forms.ComboBox ddlExperienciaUsuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlTecnologiaUsuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonGuardarModificaciones;
        private System.Windows.Forms.GroupBox groupBoxTecnologias;
        private System.Windows.Forms.GroupBox groupBoxAptitudes;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}