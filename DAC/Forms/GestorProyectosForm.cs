﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAC.BLL;
using DAC.BE;

namespace DAC.Forms
{
    public partial class GestorProyectosForm : Form
    {
        private ProyectoBLL _proyectosBLL;
        private ClienteBLL _clientesBLL;
        private ClienteBE clienteSeleccionado = null;

        public GestorProyectosForm()
        {
            InitializeComponent();
            _proyectosBLL = BLL.ProyectoBLL.GetInstance();
            _clientesBLL = new ClienteBLL();
        }

        private void GestionProyectosForm_Load(object sender, EventArgs e)
        {
            CargarClientes();
            CargarProyectos();
            CargaInicial();
        }

        private void CargarClientes()
        {
            IList<ClienteBE> clientes = new List<ClienteBE>();
            try
            {
                clientes = _clientesBLL.SeleccionarTodos();

                clientes.Insert(0, new ClienteBE() { id = 0, razonSocial = ((MainForm)ParentForm).optionSelectMessage });

                ddlClientes.DataSource = clientes;
                ddlClientes.DisplayMember = "razonSocial";
                ddlClientes.ValueMember = "id";

                ddlClientes.SelectedIndex = 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CargaInicial()
        {
            txtDescipcionProyecto.Text = string.Empty;
            txtNombreProyecto.Text = string.Empty;
            txtPresupuesto.Text = string.Empty;
            dtpFrom.Value = DateTime.Now;
            dtpTo.Value = DateTime.Now;
            checkEliminar.Checked = false;
            ddlProyectos.SelectedIndex = 0;
        }

        private void CargarProyectos()
        {
            IList<ProyectoBE> proyectos = new List<ProyectoBE>();
            try
            {
                if (ddlClientes.SelectedIndex > 0)
                {
                    clienteSeleccionado = (ClienteBE)ddlClientes.SelectedItem;
                    proyectos = _proyectosBLL.SeleccionarPorCliente(clienteSeleccionado);
                    //proyectos = proyectos.Where(x => x.esbo
                }
                else
                {
                    clienteSeleccionado = null;
                }

                proyectos.Insert(0, new ProyectoBE() { id = 0, nombre = ((MainForm)ParentForm).optionSelectMessage });

                ddlProyectos.DataSource = proyectos;
                ddlProyectos.DisplayMember = "nombre";
                ddlProyectos.ValueMember = "id";

                ddlProyectos.SelectedIndex = 0;

                checkEliminar.Visible = clienteSeleccionado != null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ddlProyectos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProyectos.SelectedIndex > 0)
            {
                ProyectoBE proyectoSeleccionado = (ProyectoBE)ddlProyectos.SelectedItem;

                txtNombreProyecto.Text = proyectoSeleccionado.nombre;
                txtDescipcionProyecto.Text = proyectoSeleccionado.descripcion;
                txtPresupuesto.Text = proyectoSeleccionado.presupuestoInicial.ToString();
                dtpFrom.Value = proyectoSeleccionado.fechaDesde;
                dtpTo.Value = proyectoSeleccionado.fechaHasta;
            }
            else
            {
                CargaInicial();
            }
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            string nombre = txtNombreProyecto.Text;
            string descripcion = txtDescipcionProyecto.Text;
            string presupuesto = txtPresupuesto.Text;
            int presupuestoInt = 0;

            if (!int.TryParse(presupuesto, out presupuestoInt))
            {
                MessageBox.Show("El presupuesto no es un valor numerico entero");
                return;
            }

            if (clienteSeleccionado != null &&
                !(string.IsNullOrEmpty(nombre)
                && string.IsNullOrEmpty(descripcion)
                && string.IsNullOrEmpty(presupuesto)))
            {
                ProyectoBE Proyecto = new ProyectoBE();

                if (ddlProyectos.SelectedIndex > 0) Proyecto = (ProyectoBE)ddlProyectos.SelectedItem;

                if (checkEliminar.Checked)
                {
                    _proyectosBLL.Eliminar(Proyecto);
                    MessageBox.Show("Proyecto eliminado correctamente");
                }
                else
                {
                    Proyecto.nombre = nombre;
                    Proyecto.descripcion = descripcion;
                    Proyecto.presupuestoInicial = presupuestoInt;
                    Proyecto.fechaDesde = dtpFrom.Value;
                    Proyecto.fechaHasta = dtpTo.Value;

                    if (Proyecto.fechaHasta <= Proyecto.fechaDesde)
                    {
                        MessageBox.Show("Las fechas ingresadas son incorrectas");
                        return;
                    }

                    Proyecto.cliente = (ClienteBE)ddlClientes.SelectedItem;

                    Proyecto = _proyectosBLL.Guardar(Proyecto);

                    if (Proyecto != null)
                    {
                        MessageBox.Show("Proyecto guardado correctamente");
                    }
                }

                CargarProyectos();
                CargarClientes();
                CargaInicial();
            }
            else
            {
                MessageBox.Show("No ha seleccionado todos los campos requeridos");
            }
        }

        private void ddlClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarProyectos();
        }
    }
}
