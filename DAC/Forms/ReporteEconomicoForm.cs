﻿using DAC.BE;
using DAC.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class ReporteEconomicoForm : Form
    {
        private ProyectoBLL _proyectoBLL;
        private AsignacionUsuarioBLL _asignacionUsuarioBLL;

        public ReporteEconomicoForm()
        {
            InitializeComponent();
        }

        private void ReporteEconomicoForm_Load(object sender, EventArgs e)
        {
            buttonSalir.Left = ((MainForm)ParentForm).Width - buttonSalir.Width - 30;
            buttonSalir.Top = ((MainForm)ParentForm).Height - buttonSalir.Height - 75;
            dataGridView1.Width = ((MainForm)ParentForm).Width - 40;
            dataGridView1.Height = ((MainForm)ParentForm).Height - 115;

            _proyectoBLL = ProyectoBLL.GetInstance();
            _asignacionUsuarioBLL = AsignacionUsuarioBLL.GetInstance();

            CargarGrilla();
        }

        private void CargarGrilla()
        {
            IList<ProyectoReporteMapper> proyectos = _proyectoBLL.ObtenerReporteMapper();
            dataGridView1.DataSource = proyectos;
            dataGridView1.Refresh();

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
