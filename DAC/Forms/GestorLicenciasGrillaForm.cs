﻿using DAC.BE;
using DAC.SEGURIDAD;
using System;
using System.Windows.Forms;

namespace DAC.Forms
{
    public partial class GestorLicenciasGrillaForm : Form
    {
        private BLL.LicenciaBLL _licenciaBLL;
        private UsuarioBE _usuarioEnSession;
        private MainForm _mainForm = null;

        public GestorLicenciasGrillaForm()
        {
            InitializeComponent();
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CargarSolicitudesDeLicencia()
        {
            try
            {
                dataGridView1.DataSource = _licenciaBLL.SeleccionarLicenciasPendientesMapper();
                dataGridView1.Refresh();
                dataGridView1.Update();
                dataGridView1.ReadOnly = true;
                dataGridView1.Width = _mainForm.Width - 60;

                _mainForm.OrdenarGrilla(ref dataGridView1);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonAprobar_Click(object sender, EventArgs e)
        {
            AprobarLicencia(true);
        }

        private void buttonRechazar_Click(object sender, EventArgs e)
        {
            AprobarLicencia(false);
        }

        private void AprobarLicencia(bool aprobar)
        {
            LicenciaSolicitadaBE _solicitud;

            if (dataGridView1.SelectedCells.Count > 0)
            {
                int rowIndex = dataGridView1.SelectedCells[0].RowIndex;
                LicenciaSolicitadaMapper mapper = (LicenciaSolicitadaMapper)dataGridView1.Rows[rowIndex].DataBoundItem;
                _solicitud = _licenciaBLL.SeleccionarLicenciasPendientesPorId(mapper.idLicenciaSolicitada);
                _solicitud.aprobada = aprobar;

                _licenciaBLL.ModificarLicenciaPorUsuario(_solicitud, ref _usuarioEnSession);

                MessageBox.Show("Se ha modificado la solicitud exitosamente"); //TODO: Mensaje

                CargarSolicitudesDeLicencia();
            }
        }

        private void GestorLicenciasGrillaForm_Load(object sender, EventArgs e)
        {
            _mainForm = (MainForm)ParentForm;
            _licenciaBLL = BLL.LicenciaBLL.GetInstance();
            _usuarioEnSession = GestorSession.GetInstance().sessionUsuario;

            CargarSolicitudesDeLicencia();
        }
    }
}