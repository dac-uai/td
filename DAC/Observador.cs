﻿using DAC.BE;
using DAC.Forms;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC
{
    public class Observador : Form
    {
        public void ActualizarIdioma()
        {
            UsuarioBE usuarioEnSesion = GestorSession.GetInstance().sessionUsuario;
            IdiomaBE idiomaSeleccionado = usuarioEnSesion.idioma;

            MainForm mainForm = null;

            foreach (Form childForm in Application.OpenForms)
            {
                if (childForm.GetType().Name.Equals("MainForm"))
                {
                    mainForm = ((MainForm)childForm);
                }
            }

            if (mainForm != null)
            {
                mainForm.ActualizarVista();
                mainForm.optionSelectMessage = idiomaSeleccionado.mensajes.First(x => x.etiqueta == "optionSelect").traduccion.texto;
                mainForm.Text = idiomaSeleccionado.mensajes.First(x => x.etiqueta == mainForm.Name).traduccion.texto;

                foreach (Form item in mainForm.MdiChildren)
                {
                    SeleccionarIdioma(idiomaSeleccionado, item);
                }
            }
        }

        private void SeleccionarIdioma(IdiomaBE idiomaSeleccionado, Form form)
        {
            UTILS.GestorIdioma.GetInstance().CargarTodosLosMensajesconTraduccionesPorIdioma(ref idiomaSeleccionado);

            MensajeBE formTitle = idiomaSeleccionado.mensajes.FirstOrDefault(x => x.etiqueta == form.Name);
            form.Text = (formTitle != null) ? formTitle.traduccion.texto : form.Name;

            foreach (Control control in form.Controls)
            {
                if (control.Tag != null)
                {
                    MensajeBE msj = idiomaSeleccionado.mensajes.FirstOrDefault(x => x.etiqueta == control.Tag.ToString());
                    if (msj != null) control.Text = msj.traduccion.texto;

                    if (control.Controls.Count > 0)
                    {
                        foreach (Control ctrl in control.Controls)
                        {
                            if (ctrl.Tag != null)
                            {
                                MensajeBE _msj = idiomaSeleccionado.mensajes.FirstOrDefault(x => x.etiqueta == ctrl.Tag.ToString());
                                ctrl.Text = (_msj != null) ? _msj.traduccion.texto : ctrl.Tag.ToString();
                            }
                        }
                    }
                }
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Observador
            // 
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Name = "Observador";
            this.Load += new System.EventHandler(this.Observador_Load);
            this.ResumeLayout(false);

        }

        private void Observador_Load(object sender, EventArgs e)
        {

        }
    }
}
