﻿using DAC.BE;
using DAC.INTERFACES;
using DAC.SEGURIDAD;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC
{
    public class Observable : Form
    {
        private IList<Observador> observadores = new List<Observador>();

        public void BorrarObservador(Observador obj)
        {
            observadores.Remove(obj);
        }

        public void NotificarObservadores()
        {
            foreach (Observador observador in observadores)
            {
                observador.ActualizarIdioma();
            }
        }

        public void RegistrarObservador(Observador obj)
        {
            observadores.Add(obj);
        }
    }
}
