﻿using DAC.INTERFACES;
using DAC.UTILS.BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.UTILS.DAL
{
    public class MensajeDAL : ICRUD<MensajeBE>
    {

        public MensajeBE Crear(MensajeBE entidad)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(MensajeBE entidad)
        {
            throw new NotImplementedException();
        }

        public MensajeBE Modificar(MensajeBE entidad)
        {
            throw new NotImplementedException();
        }

        public MensajeBE Seleccionar(int idEntidad)
        {
            throw new NotImplementedException();
        }

        public IList<MensajeBE> SeleccionarTodos()
        {
            throw new NotImplementedException();
        }

        public IList<MensajeBE> SeleccionarPorIdioma(IdiomaBE idioma)
        {
            IList<MensajeBE> _mensajes = new List<MensajeBE>();

            try
            {
                SqlParameter p = new SqlParameter()
                {
                    ParameterName = "@idIdioma",
                    DbType = DbType.Int32,
                    Direction = ParameterDirection.Input,
                    Value = idioma.id
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("dbo.ObtenerMensajesPorIdioma", new List<SqlParameter>() { p });

                foreach (DataRow row in dt.Rows)
                {
                    MensajeBE _mensaje = new MensajeBE()
                    {
                        id = int.Parse(row["idMensaje"].ToString()),
                        etiqueta = row["etiqueta"].ToString(),
                        traduccion = new TraduccionBE() { texto = row["texto"].ToString() },
                        idioma = idioma
                    };

                    _mensajes.Add(_mensaje);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return _mensajes;
        }
    }
}
