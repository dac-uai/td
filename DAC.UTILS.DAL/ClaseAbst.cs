﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.UTILS.DAL
{
    public abstract class ClaseAbst
    {
        public int plata { get; set; }
        public int Debitar(int monto) {
            plata -= monto;
            return plata;
        }
    }
}
