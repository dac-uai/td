﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAC.INTERFACES;
using DAC.UTILS.BE;
using System.Data.SqlClient;
using System.Data;

namespace DAC.UTILS.DAL
{
    public class IdiomaDAL : ICRUD<BE.IdiomaBE>
    {
        public BE.IdiomaBE Crear(BE.IdiomaBE entidad)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(BE.IdiomaBE entidad)
        {
            throw new NotImplementedException();
        }

        public BE.IdiomaBE Modificar(BE.IdiomaBE entidad)
        {
            throw new NotImplementedException();
        }

        public BE.IdiomaBE Seleccionar(int idEntidad)
        {
            throw new NotImplementedException();
        }

        public IList<BE.IdiomaBE> SeleccionarTodos()
        {
            IList<IdiomaBE> _idiomas = new List<IdiomaBE>();

            try
            {
                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("dbo.ObtenerTodosLosIdiomas");

                foreach (DataRow row in dt.Rows)
                {
                    _idiomas.Add(new IdiomaBE()
                    {
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        id = int.Parse(row["idIdioma"].ToString()),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    });
                }
            }
            catch (Exception)
            {
                throw;
            }

            return _idiomas;
        }

        public bool GuardarIdiomaPorUsuario(IUsuario usuario, IdiomaBE idioma)
        {
            bool guardadoOk = false;

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", usuario.id.ToString(), System.Data.DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@idIdioma", idioma.id.ToString(), System.Data.DbType.Int32)
                };

                guardadoOk = SqlHelper.GetInstance().ExecuteNonQuery("ActualizarIdiomaPorUsuario", parameters) > 0;
            }
            catch (Exception)
            {
                throw;
            }

            return guardadoOk;
        }

        public IdiomaBE ObtenerIdiomaPorUsuario(INTERFACES.IUsuario usuario)
        {
            IdiomaBE _idioma = null;

            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("idUsuario", usuario.id.ToString(), System.Data.DbType.Int32)
                };

                DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerIdiomaPorUsuario", sp);

                foreach (DataRow row in dt.Rows)
                {
                    _idioma = new IdiomaBE()
                    {
                        id = int.Parse(row["idIdioma"].ToString()),
                        codigo = row["codigo"].ToString(),
                        descripcion = row["descripcion"].ToString(),
                        fechaCreacion = DateTime.Parse(row["fechaCreacion"].ToString())
                    };
                }
            }
            catch (Exception)
            {
                throw;
            }

            return _idioma;
        }


    }
}
