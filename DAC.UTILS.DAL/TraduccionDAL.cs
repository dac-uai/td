﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAC.UTILS.BE;

namespace DAC.UTILS.DAL
{
    public class TraduccionDAL : INTERFACES.ICRUD<TraduccionBE>
    {
        public TraduccionBE Crear(TraduccionBE entidad)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(TraduccionBE entidad)
        {
            throw new NotImplementedException();
        }

        public TraduccionBE Modificar(TraduccionBE entidad)
        {
            throw new NotImplementedException();
        }

        public TraduccionBE Seleccionar(int idEntidad)
        {
            throw new NotImplementedException();
        }

        public IList<TraduccionBE> SeleccionarTodos()
        {
            throw new NotImplementedException();
        }

        public TraduccionBE SeleccionarPorMensaje(MensajeBE mensaje)
        {
            TraduccionBE _traduccion = new TraduccionBE();

            try
            {
                if (mensaje.idioma.codigo.Equals("ES"))
                {
                    _traduccion.texto = mensaje.etiqueta.Equals("labelSeleccionarIdioma") ? "Seleccione un idioma" : "Guardar";
                }
                else
                {
                    _traduccion.texto = mensaje.etiqueta.Equals("labelSeleccionarIdioma") ? "Select a language" : "Save";
                }
            }
            catch (Exception)
            {
                throw;
            }

            return _traduccion;
        }
    }
}
