﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using DAC.UTILS.BE;
using System.IO;
using System.Xml.Serialization;

namespace DAC.UTILS.DAL
{
    public class SqlHelper
    {
        private static SqlHelper _dbHelper;
        private String _connString = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
        private SqlConnection _sqlConnection = null;
        private SqlCommand _sqlCommand = null;

        private SqlHelper() { }
        public static SqlHelper GetInstance()
        {
            if (_dbHelper == null)
            {
                _dbHelper = new SqlHelper();
            }

            return _dbHelper;
        }

        public int ExecuteNonQuery(string query, IList<SqlParameter> parametros)
        {
            _sqlCommand = new SqlCommand(query);
            int _result = 0;

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();
                _result = _sqlCommand.ExecuteNonQuery();
                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return _result;
        }

        public int ExecuteNonQueryText(string query, IList<SqlParameter> parametros)
        {
            _sqlCommand = new SqlCommand(query);
            int _result = 0;

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.Text;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();
                _result = _sqlCommand.ExecuteNonQuery();
                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return _result;
        }

        public int ExecuteNonQueryText(string query)
        {
            return ExecuteNonQueryText(query, new List<SqlParameter>());
        }

        public int ExecuteNonQuery(string query)
        {
            return ExecuteNonQuery(query, new List<SqlParameter>());
        }

        public DataTable ExecuteQuery(string query, IList<SqlParameter> parametros)
        {
            SqlCommand _sqlCommand = new SqlCommand(query);
            var dt = new DataTable();

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();

                using (SqlDataReader dr = _sqlCommand.ExecuteReader())
                {
                    dt.Load(dr);
                }

                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return dt;
        }

        public DataTable ExecuteQueryText(string query, IList<SqlParameter> parametros)
        {
            SqlCommand _sqlCommand = new SqlCommand(query);
            var dt = new DataTable();

            try
            {
                _sqlConnection = new SqlConnection(_connString);
                _sqlCommand.Connection = _sqlConnection;
                _sqlCommand.CommandType = CommandType.Text;

                foreach (SqlParameter param in parametros)
                {
                    _sqlCommand.Parameters.Add(param);
                }

                _sqlConnection.Open();

                using (SqlDataReader dr = _sqlCommand.ExecuteReader())
                {
                    dt.Load(dr);
                }

                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                GuardarMensajeXML(e);
                throw e;
            }
            finally
            {
                _sqlConnection.Dispose();
            }

            return dt;
        }

        public DataTable ExecuteQuery(string query)
        {
            return ExecuteQuery(query, new List<SqlParameter>());
        }

        public DataTable ExecuteQueryText(string query)
        {
            return ExecuteQueryText(query, new List<SqlParameter>());
        }

        public SqlParameter AddParameter(string name, string value, DbType type)
        {
            SqlParameter _parameter = new SqlParameter();
            _parameter.Value = value;
            _parameter.DbType = type;
            _parameter.ParameterName = name;

            return _parameter;
        }

        private void GuardarMensajeXML(Exception e)
        {
            MensajeErrorDB mensaje = new MensajeErrorDB()
            {
                excepcion = e.GetType().ToString(),
                mensajeError = e.Message,
                fechaError = DateTime.Now
            };

            using (StreamWriter myWriter = new StreamWriter("Prueba.xml", true))
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(MensajeErrorDB));
                mySerializer.Serialize(myWriter, mensaje);
            }
        }
    }
}
