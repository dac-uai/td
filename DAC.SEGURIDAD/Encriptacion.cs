﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DAC.SEGURIDAD
{
    public static class Encriptacion
    {
        private static byte[] _key { get { return Convert.FromBase64String("Mukd2WV3FVesAvxes0KxHJOeMdLTNYuXXRMuTaBQlKM="); } }
        private static byte[] _IV { get { return Convert.FromBase64String("JgDEqX3/cfNCA2KjilVXtA=="); } }

        
        public static string Encriptar(string valor)
        {
            string result = string.Empty;
            using (Aes aes = Aes.Create())
            {
                // Set custom symmetric algorithm
                aes.Key = _key;
                aes.IV = _IV;
                // Encrypt the string to an array of bytes.
                result = GetAesEncrypt(valor, aes.Key, aes.IV);
            }
            return result;
        }

        public static string Desencriptar(string valor)
        {
            string result = string.Empty;
            using (Aes aes = Aes.Create())
            {
                // Set custom symmetric algorithm
                aes.Key = _key;
                aes.IV = _IV;
                // Encrypt the string to an array of bytes.
                result = GetAesDecrypt(valor, aes.Key, aes.IV);
            }
            return result;
        }

        public static string GenerarHash(string valor)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(valor));

            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public static bool CompararHash(string valor, string hash)
        {
            return GenerarHash(valor).Equals(hash);
        }

        private static string GetAesDecrypt(string str, byte[] Key, byte[] IV)
        {
            string result = null;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Key;
                aes.IV = IV;

                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String(str)))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            result = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return result;
        }

        private static string GetAesEncrypt(string str, byte[] key, byte[] IV)
        {
            string result;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = IV;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(str);
                        }
                        result = Convert.ToBase64String(msEncrypt.ToArray());
                    }
                }
            }
            return result;
        }

    }
}
