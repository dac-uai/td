﻿using DAC.BE;
using DAC.UTILS.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DAC.SEGURIDAD
{
    public static class GestorBitacora
    {
        public static bool RegistrarEvento(Bitacora mensajeLog, TipoEvento criticidad)
        {
            try
            {
                UsuarioBE usuario = GestorSession.GetInstance().sessionUsuario;
                IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
                string ip = host.AddressList.FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
                int idUsuario = (usuario == null) ? -1 : usuario.id;
                IList<SqlParameter> parameters = new List<SqlParameter>()
                {
                    SqlHelper.GetInstance().AddParameter("@idUsuario", idUsuario.ToString(), System.Data.DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@criticidad", ((int)criticidad).ToString(), System.Data.DbType.Int32),
                    SqlHelper.GetInstance().AddParameter("@mensaje", mensajeLog.mensaje.ToString(), System.Data.DbType.String),
                    SqlHelper.GetInstance().AddParameter("@ip", ip, System.Data.DbType.String)
                };

                SqlHelper.GetInstance().ExecuteNonQuery("GuardarEnBitacora", parameters);
            }
            catch (Exception e)
            {
                throw e;
            }

            return false;
        }

        public static DataTable ObtenerEventos()
        {
            DataTable dt = null;

            try
            {
                dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerBitacora");

                foreach (DataRow row in dt.Rows)
                {
                    row["nombreUsuario"] = Encriptacion.Desencriptar(row["nombreUsuario"].ToString());
                    row["nombre"] = Encriptacion.Desencriptar(row["nombre"].ToString());
                    row["apellido"] = Encriptacion.Desencriptar(row["apellido"].ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return dt;
        } 
    }
}
