﻿using DAC.BE;
using DAC.UTILS.BE;
using DAC.UTILS.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.SEGURIDAD
{
    public static class GestorBackUp
    {
        public static void GenerarBackUp(string nombreBackup, UsuarioBE usuario, string backupFolder)
        {
            try
            {
                string fileName = string.Format("{0}-{1}.bak", nombreBackup, DateTime.Now.ToString("yyyy-MM-dd"));
                string finalPath = string.Format("{0}\\{1}", backupFolder, fileName);
                SqlHelper.GetInstance().ExecuteNonQueryText(String.Format("INSERT INTO BACKUPS VALUES ('{0}', GETDATE(), '{1}')", finalPath, usuario.nombreUsuario));
                SqlHelper.GetInstance().ExecuteNonQueryText(String.Format("BACKUP DATABASE [DAC] TO DISK='{0}'", finalPath));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void RecuperarBackUp(string nombreBackUp)
        {
            try
            {
                SqlHelper.GetInstance().ExecuteNonQueryText("ALTER DATABASE [DAC] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");

                SqlHelper.GetInstance().ExecuteNonQueryText(string.Format("USE MASTER RESTORE DATABASE [DAC] FROM DISK='{0}'WITH REPLACE;", nombreBackUp));

                SqlHelper.GetInstance().ExecuteNonQueryText("ALTER DATABASE [DAC] SET MULTI_USER");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BackupGrillaMapper> ObtenerListaBackups()
        {
            DataTable dt = SqlHelper.GetInstance().ExecuteQuery("ObtenerBackups");
            List<BackupGrillaMapper> lista = new List<BackupGrillaMapper>();

            foreach (DataRow row in dt.Rows)
            {
                lista.Add(new BackupGrillaMapper()
                {
                    path = row["path"].ToString(),
                    fecha = DateTime.Parse(row["fecha"].ToString()),
                    nombreUsuario = row["usuario"].ToString()
                });
            }

            return lista;
        }
    }
}