﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.SEGURIDAD
{
    public class DigitoVerificador
    {
        private static DigitoVerificador _digitoVerificador = null;

        private DigitoVerificador() { }

        public static DigitoVerificador GetInstance()
        {
            if (_digitoVerificador == null) _digitoVerificador = new DigitoVerificador();

            return _digitoVerificador;
        }

        public string ObtenerDvv(string tabla) { return null; }
        public string ObtenerDvh(string tabla, int rowId) { return null; }
        public void VerificarIntegridadTotal() { }
        public bool GenerarDvv(string tabla) { return false; }
        public bool GenerarDvh(string tabla, int rowId) { return false; }
        public bool VerificarDvv(string tabla) { return false; }
        public bool VerificarDvh(string tabla, int rowId) { return false; }
    }
}
