﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.SEGURIDAD
{
    public class Bitacora
    {
        public DateTime fecha { get; set; }
        public string mensaje { get; set; }
    }
}
