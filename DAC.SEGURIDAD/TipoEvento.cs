﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.SEGURIDAD
{
    public enum TipoEvento
    {
        Base,
        Alta,
        Modificacion,
        Baja,
        Login,
        LoginFallido,
        Error,
        Logout
    }
}