﻿using DAC.BE;
using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.SEGURIDAD
{
    public class GestorSession
    {
        public UsuarioBE sessionUsuario { get; private set; }
        private Bitacora _log = null;
        private static GestorSession _instance = null;

        private GestorSession() { _log = new Bitacora(); }

        public static GestorSession GetInstance()
        {
            if (_instance == null) _instance = new GestorSession();

            return _instance;
        }

        public UsuarioBE IniciarSesion(UsuarioBE usuario)
        {
            sessionUsuario = usuario;

            _log.mensaje = string.Format("Login exitoso para el usuario {0}", usuario.nombreUsuario);
            TipoEvento evento = TipoEvento.Login;

            GestorBitacora.RegistrarEvento(_log, evento);

            return sessionUsuario;
        }

        public void ActualizarUsuarioEnSesion(UsuarioBE usuario)
        {
            sessionUsuario = usuario;
        }

        public bool CerrarSesion()
        {

            _log.mensaje = string.Format("Logout exitoso para el usuario {0}", sessionUsuario.nombreUsuario);
            TipoEvento evento = TipoEvento.Logout;
            GestorBitacora.RegistrarEvento(_log, evento);

            sessionUsuario = null;
            return sessionUsuario == null;
        }

        public bool UsuarioTienePermiso(string codigoPermiso)
        {
            foreach (ComponenteBE permiso in sessionUsuario.permisos)
            {
                if (UsuarioTiene(permiso, codigoPermiso)) return true;
            }

            return false;
        }

        private bool UsuarioTiene(ComponenteBE permiso, string codigoPermiso)
        {
            
            foreach (ComponenteBE _p in permiso.ObtenerPermisos())
            {
                if (UsuarioTiene(_p, codigoPermiso)) return true;
            }

            return (permiso.codigo == codigoPermiso);
        }
    }
}
