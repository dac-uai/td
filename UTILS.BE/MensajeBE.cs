﻿using DAC.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.UTILS.BE
{
    public class MensajeBE : IEntidad
    {
        public string etiqueta { get; set; }
        public TraduccionBE traduccion { get; set; }
        public IdiomaBE idioma { get; set; }
        public int id { get; set; }
        public DateTime fechaCreacion { get; set; }
    }
}
