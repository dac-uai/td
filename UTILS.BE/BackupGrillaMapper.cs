﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.UTILS.BE
{
    public class BackupGrillaMapper
    {
        public string path { get; set; }
        public DateTime fecha { get; set; }
        public string nombreUsuario { get; set; }
    }
}
