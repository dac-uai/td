﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.UTILS.BE
{
    public class IdiomaBE : INTERFACES.IEntidad
    {
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public IList<MensajeBE> mensajes { get; set; }
        public int id { get; set; }
        public DateTime fechaCreacion { get; set; }
    }
}
