﻿using System;

namespace DAC.UTILS.BE
{
    public class MensajeErrorDB
    {
        public string mensajeError { get; set; }
        public string excepcion { get; set; }
        public DateTime fechaError { get; set; }
    }
}
